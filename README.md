# EvalPro2020 Testing including Exam Mode

EvalPro 20 has feature in which we conduct a exam using IP base protection. this feature has many sub features like add/reduce time for all student,add/reduce time for individual student, pause time for all student,pause time for individual student, approve a student, duration of exam, late-duration of exam, Exam group ID, IP address of student.Exam as well as non-exam assssignments has some autograding features(Part of Test BenchMark is used) like "Ignore extra whitespaces", "Output contains numbers only.", "Order of numbers is important in output", "Enter value of Error rate" and different programming languages like "c" ,"C++" and "python". because there is so many variable manual testing is nearly impossible to do for that purpose automated testing required.

## Tech Stack

**Client:** Jmeter, javac 11.0.19, html viewer like chrome or firefox

## Clone a repo
Performing a test required a Jmeter script so first clone this repo using a `git clone command` mention below.

```javascript
> git clone https://gitlab.com/Vishalcc1/evalpro-exam-test-jmeter.git
># This command clone all file in your system
> cd evalpro-exam-test-jmeter
># change a dir and every other directory that we discusses later is relative to current directory
```


## IP Setup
For IP spoofing jmeter required multiple virtual or temperary IP address. there IP address must be static IP in local network. there is a script in the repo in which we can config a range of IP.

```javascript
#Path of Script
> cd exammode/testdata
# add IP address of your network. it must be static and continuous in nature like 10.129.131.0-10.129.131.255. edit line number 6,7,8 and add your network address. make sure that address range is not more than 50 otherwise Jmeter IP Spoofing behaviour is buggy(e.g. binding error, few request stuck forever).

> sudo chmod 0777 validip.sh grouptest.sh

> ./validip.sh
#vishal@vishal-m:~/Downloads/apache-jmeter-5.5/exammode/testdata$ ./validip.sh 
#Error: Packet loss exceeded threshold (50%) for 10.129.131.50
#Error: Packet loss exceeded threshold (50%) for 10.129.131.51

#If you change the IP then make sure that change appropriate column of  `AssignmentSetup{NUMBER}.csv` so that student with new IP get enrolled and do testing properly. 
```
Now it generate list of valid IP address in Pingresult.xlsx file.
```javascript
IP Addr 
10.129.131.50
10.129.131.51
10.129.131.52
```
Now we have to copy IP as per our requirement to file IP_instructor.csv and IP_student.csv.

```javascript
# IP_student.csv after a copy IP
    IP          Name        Password        Email Address
10.129.131.50	student1	teststudent123	student1@student.com
10.129.131.51	student2	teststudent123	student2@student.com
10.129.131.52	student3	teststudent123	student3@student.com

# IP_instructor.csv after a copy IP
    IP          Name        Password        Email Address
10.129.131.70	root	    root        	root@example.com

#Few important point. 
# Make sure that instructor IP address don't assign to any student.
# Make sure that these student and instructor enroll in to the course. if there is no instructor or student then we have to setup it(see previous section for setup). 
```
**Limitation of jmeter**

Never run more than 30 student concurrently because above that behaviour is undefine.

```javascript

csrf token extract,login and open assignment
Time in second

number of student       jmeter 5.3(GUI)        jmeter 5.5(GUI)        jmeter5.5(CMD )		jmeter5.5(CMD without listner) 
10 			 17 			16		        	17          			 16
20 			 30 			32			        33                       37
30 			 50 			48			        48          			 48
40 			 1:17	 		1:03			    1:10        			 1:05
50 			 2:31 			2:03			    2:02        			 1:20
60 			 behaviour undefine(too long or too few)		    		 1:36
70			---			---			---			 1:54(Error:few packet loss2-3%)
80			---			---			---			 2:05(Error:few packet loss4-7%)
```

## Data Population
EvalPro20 Exam Mode testing required various data like student details, instructor details, student enroll in specific course, and 3 assignments. so in this section, these are some steps that help to populate a data. all of data Population is done by cypress as well.

**Instructor Data Population**

For testing exammode, we required a single instructor so do it maually or saikumar's script.but make sure that `../IP_INSTRUCTOR.csv` details is same as created Instructor details. if not then modify `../IP_INSTRUCTOR.csv` accordingly. student data Population reqired a Instructor details so first setup a instructor.

**Student Data Population**

```javascript
> cd exammode/testdata/initial\ setup/
# run using cmd...
> ../../../bin/jmeter.sh  -n -f -t createStudentAdmin.jmx
# It fetch student from ../IP_STUDENT.csv and add student using a admin page of django.
# Or use saikumar's django python script to create a student.
```

**Student Course Enrollment**

Before doing any test student must enroll into a specific course like code CS2023. course details provided it CourseSetup.csv file.
```javascript
# course file location: /exammode/testdata/assignmentsetup/CourseSetup.csv and it look like this. 

Title	          Course Code	    protocal	    url
testingcode	    CS2023	        https	        hansa.bodhi.cse.iitb.ac.in

# before runing enrollStudentInCourse.jmx must create a course and use details of CourseSetup.csv. if you change any details in course then you have to change a CourseSetup.csv file as well because these file work as a input for TEST.

# run using cmd...
> cd exammode/testdata/initial\ setup/
> ../../../bin/jmeter.sh  -n -f -t enrollStudentInCourse.jmx
# It fetch student from ../IP_STUDENT.csv and enroll student using a `CourseSetup.csv` file.
# Or use saikumar's django python script to enroll a student.

```
**Assignments Population**

Now we have to create 3 assignment name (ExamMode1, ExamMode2, ExamMode3) manually  in the course and other details of assignment don't matter because every Test script going to change assignment as per test requirements. Don't change a course name otherwise you have to change a course name for every csv file AssignmentSetup.csv. 
```javascript
>cd exammode/testdata/assignmentsetup
> cat AssignmentSetup.csv
name_tabid	name	description	publish_on	deadlineGap	deadline .......
1	ExamMode1	Add 2 number	0	2	Now + 2min ......
1	ExamMode2	Add 2 number	0	2	Now + 2min ...... 
1	ExamMode3	Add 2 number	0	2	Now + 2min ......
# you have to change {name} as per your created assignments(assignmentsetup.csv to assignmentsetup17.csv). so try to avoid any changes in csv.

```


## Environment Variables

To run this project, you will need to add the following environment setting to your jmeter.sh file. but don't do it again because it's done already.


This config need to be done in jmeter before running a test.

1.  If embedded resources download fails due to missing resources or other reasons, if this property is true
```
# Parent sample will not be marked as failed
httpsampler.ignore_failed_embedded_resources=true
```
2. Enable retry attempt
```
# true if it's OK to retry requests that have been sent
# This will retry Idempotent and non Idempotent requests
# This should usually be false, but it can be useful
# when testing against some Load Balancers like Amazon ELB
httpclient4.request_sent_retry_enabled=true
```
3. Number of retries to attempt (default 0)
`httpclient4.retrycount=5`

4. Add the next line to user.properties file (lives in "bin" folder of your JMeter installation)
```
# Save cookie 
CookieManager.save.cookies=true
```
## Running Tests

Every test is testing some combination of features. This  [Excel sheet](https://docs.google.com/spreadsheets/d/1k7KhpNPQ-mEUGKH9ZWRCazh25bYE5Z0gXP0BjFB-Cf0/edit?usp=sharing) has all test details that are reqired to analysics a test.

To run tests, we have 3 ways. 
1. Jmeter Dashboard :- Dashboard provide a GUI to run the tests and see a result in sequential ways. it is usefull in a scenario where we need to edit or debug a test. to open a dashboard move to `bin` directory. run the script `./jmeter` in bash.
![JMeter screen](https://raw.githubusercontent.com/apache/jmeter/master/xdocs/images/screenshots/jmeter_screen.png)
2. Continuous Testing:- It start test in `var{Start}` and end at `var{end}`. it Continuous in nature. and generate a report in `TEST{varnumber}` directory. and if test get fail then it generate a FailTest.csv file.this csv file has relative path of TEST.

```bash
  > cd exammode/testdata
  ># sudo ./grouptest.sh -r {start} {end}
  > sudo ./grouptest.sh -g 1 15
  # above cmd run TEST1,2,3,....15
  # report get store into a /exammode/functionalTesting/TEST1/report
```


3. Group Testing:- It start test in group of number. and generate a report in `TEST{varnumber}` directory. and if test get fail then it generate a `FailTest.csv` file.this csv file has relative path of TEST.

```bash
  > cd exammode/testdata
  ># sudo ./grouptest.sh -g {start} {end}
  > sudo ./grouptest.sh -g 1 15
  # above cmd run TEST1,15
  # report get store into a /exammode/functionalTesting/TEST1/report
```
## Troubleshoot fail test

after a running a test, there is many test get failed and result get store in `FailTest.csv`. `FailTest.csv` file contain a relative address of `TEST{varnumber}`. `FailTest.csv` has information that TEST61 get failed then understand a TEST61 scenario(assignments details, testing ) we need a Business requirement table [sheet](https://docs.google.com/spreadsheets/d/1k7KhpNPQ-mEUGKH9ZWRCazh25bYE5Z0gXP0BjFB-Cf0/edit?usp=sharing). Jmeter script contains a information of test steps, test data etc with the help of these details developer can understand a scenarios where the test fail and resolve a bug.


