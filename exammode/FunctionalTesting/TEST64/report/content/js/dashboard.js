/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.24812030075188, "KoPercent": 0.7518796992481203};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9191176470588235, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [0.25, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.9166666666666666, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [0.5, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [0.75, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.25, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 133, 1, 0.7518796992481203, 298.4511278195489, 0, 1236, 301.0, 651.2000000000003, 892.9999999999995, 1216.9599999999998, 1.4470835282725303, 37.46575884502606, 2.541533949341196], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Fetch Section Id-ASSIGNMENT2", 2, 0, 0.0, 364.5, 353, 376, 364.5, 376.0, 376.0, 376.0, 0.024889242869231918, 1.2966469134227687, 0.012007115212305243], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 2, 0, 0.0, 361.5, 355, 368, 361.5, 368.0, 368.0, 368.0, 0.02485799868252607, 1.2954682897386183, 0.012016317722510161], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 2, 0, 0.0, 366.0, 361, 371, 366.0, 371.0, 371.0, 371.0, 0.024877787867102857, 1.2960987352754594, 0.012001589068700011], "isController": false}, {"data": ["LOGIN-1-0", 4, 0, 0.0, 115.5, 108, 125, 114.5, 125.0, 125.0, 125.0, 0.04900279315921007, 0.028281885504973785, 0.03617784338707306], "isController": false}, {"data": ["LOGIN-1-1", 4, 0, 0.0, 49.75, 46, 54, 49.5, 54.0, 54.0, 54.0, 0.049047858447880524, 0.06399212781871912, 0.034295182274103955], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 165.0, 165, 165, 165.0, 165.0, 165.0, 165.0, 6.0606060606060606, 102.6278409090909, 7.877604166666666], "isController": false}, {"data": ["Modify Assignment3", 2, 0, 0.0, 1096.0, 1012, 1180, 1096.0, 1180.0, 1180.0, 1180.0, 0.024562782471998428, 1.2877350543144526, 0.3208153654020927], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 162.0, 162, 162, 162.0, 162.0, 162.0, 162.0, 6.172839506172839, 104.52835648148148, 8.02951388888889], "isController": false}, {"data": ["Test", 2, 1, 50.0, 892.5, 857, 928, 892.5, 928.0, 928.0, 928.0, 0.024609024129148158, 1.576924156525698, 0.09314900148884596], "isController": true}, {"data": ["student courses id-1", 1, 0, 0.0, 83.0, 83, 83, 83.0, 83.0, 83.0, 83.0, 12.048192771084338, 18.225244728915662, 6.012330572289156], "isController": false}, {"data": ["Delete Sections-1", 12, 0, 0.0, 336.4166666666667, 289, 441, 321.5, 436.20000000000005, 441.0, 441.0, 0.14408010854034844, 7.171925090350234, 0.06772515518628357], "isController": false}, {"data": ["Modify Assignment2-0", 2, 0, 0.0, 776.0, 710, 842, 776.0, 842.0, 842.0, 842.0, 0.024725546434576206, 0.007944047633765206, 0.30386585850187914], "isController": false}, {"data": ["Delete Sections-0", 12, 0, 0.0, 104.5, 79, 117, 104.5, 116.7, 117.0, 117.0, 0.14443735631492158, 0.0464531601689917, 0.06925658393615869], "isController": false}, {"data": ["Modify Assignment2-1", 2, 0, 0.0, 387.0, 381, 393, 387.0, 393.0, 393.0, 393.0, 0.024822827071775203, 1.2931141477702897, 0.01590212359285599], "isController": false}, {"data": ["LOGIN-1", 4, 0, 0.0, 167.0, 161, 175, 166.0, 175.0, 175.0, 175.0, 0.04897519406420648, 0.09216328023606042, 0.0704018414672968], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 172.0, 172, 172, 172.0, 172.0, 172.0, 172.0, 5.813953488372093, 98.45112645348838, 7.557003997093024], "isController": false}, {"data": ["Delete Sections", 12, 0, 0.0, 441.41666666666663, 388, 540, 429.5, 537.6, 540.0, 540.0, 0.14390214654035255, 7.20934764360235, 0.13664145880801054], "isController": false}, {"data": ["Modify Assignment2", 2, 0, 0.0, 1164.0, 1092, 1236, 1164.0, 1236.0, 1236.0, 1236.0, 0.024605693757535494, 1.2897083994611354, 0.31815594550453974], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 83.0, 83, 83, 83.0, 83.0, 83.0, 83.0, 12.048192771084338, 9.212631777108433, 7.965455572289156], "isController": false}, {"data": ["Modify Assignment1", 2, 0, 0.0, 1071.0, 1029, 1113, 1071.0, 1113.0, 1113.0, 1113.0, 0.024664561957379635, 1.2929023676437945, 0.3240836729849053], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 2, 0, 0.0, 86.5, 79, 94, 86.5, 94.0, 94.0, 94.0, 0.024920876218007825, 0.00800680495676228, 0.011705997520372815], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-1", 2, 0, 0.0, 361.0, 328, 394, 361.0, 394.0, 394.0, 394.0, 0.18667164457718874, 6.537609232079522, 0.08768462992346462], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 2, 0, 0.0, 391.5, 349, 434, 391.5, 434.0, 434.0, 434.0, 0.024815743107427353, 1.2927935857508004, 0.01165661370573492], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-0", 2, 0, 0.0, 305.0, 301, 309, 305.0, 309.0, 309.0, 309.0, 0.1869857890800299, 0.06007648887434555, 0.2793830637621541], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 110.0, 110, 110, 110.0, 110.0, 110.0, 110.0, 9.09090909090909, 151.07421875, 5.894886363636363], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 51.0, 51, 51, 51.0, 51.0, 51.0, 51.0, 19.607843137254903, 6.184895833333334, 12.791053921568627], "isController": false}, {"data": ["Debug Sampler-1", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 3816.40625, 0.0], "isController": false}, {"data": ["CSRF-1", 6, 0, 0.0, 77.33333333333334, 55, 164, 60.0, 164.0, 164.0, 164.0, 0.0658544616397761, 1.7686167476127759, 0.024373868126440566], "isController": false}, {"data": ["Test-1", 1, 0, 0.0, 505.0, 505, 505, 505.0, 505.0, 505.0, 505.0, 1.9801980198019802, 61.42868193069307, 5.876779084158416], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 2, 0, 0.0, 90.5, 86, 95, 90.5, 95.0, 95.0, 95.0, 0.02495694926252215, 0.008018394831415808, 0.011722941987571438], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 2, 0, 0.0, 357.0, 354, 360, 357.0, 360.0, 360.0, 360.0, 0.024873765639380145, 1.2958041649565952, 0.011683868430216649], "isController": false}, {"data": ["GET SOULTION CODE ID1", 2, 0, 0.0, 451.5, 446, 457, 451.5, 457.0, 457.0, 457.0, 0.024845337772367016, 1.3022450868344555, 0.02334103021193073], "isController": false}, {"data": ["GET SOULTION CODE ID2", 2, 0, 0.0, 478.0, 443, 513, 478.0, 513.0, 513.0, 513.0, 0.024786833234186, 1.299251243989193, 0.023286067940709892], "isController": false}, {"data": ["GET SOULTION CODE ID3", 2, 0, 0.0, 451.5, 447, 456, 451.5, 456.0, 456.0, 456.0, 0.024732273143224594, 1.296922725558331, 0.023283116513738777], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 14.16015625], "isController": false}, {"data": ["JSR223 Sampler", 2, 0, 0.0, 7.5, 5, 10, 7.5, 10.0, 10.0, 10.0, 0.02500593891049124, 3.907177954764257E-4, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 2, 0, 0.0, 341.0, 337, 345, 341.0, 345.0, 345.0, 345.0, 0.024765654989660338, 1.290389784322102, 0.015889682937701995], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 118.0, 118, 118, 118.0, 118.0, 118.0, 118.0, 8.474576271186441, 140.83189883474577, 5.495233050847458], "isController": false}, {"data": ["Modify Assignment1-1", 2, 0, 0.0, 368.0, 340, 396, 368.0, 396.0, 396.0, 396.0, 0.024875931292677768, 1.295989863835371, 0.015936143484371695], "isController": false}, {"data": ["Modify Assignment3-0", 2, 0, 0.0, 753.5, 666, 841, 753.5, 841.0, 841.0, 841.0, 0.024665474502065732, 0.007948834556329777, 0.30633122263673923], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 2, 1, 50.0, 666.5, 629, 704, 666.5, 704.0, 704.0, 704.0, 0.18142235123367198, 6.412057641282656, 0.3562894026669086], "isController": false}, {"data": ["Modify Assignment1-0", 2, 0, 0.0, 702.0, 688, 716, 702.0, 716.0, 716.0, 716.0, 0.024785911687796654, 0.007963442329379981, 0.3097996911055756], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 2, 0, 0.0, 448.5, 447, 450, 448.5, 450.0, 450.0, 450.0, 0.024846881095250517, 1.302386639987328, 0.023342480091436524], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 2, 0, 0.0, 375.5, 372, 379, 375.5, 379.0, 379.0, 379.0, 0.024868198547697205, 1.2954534327439569, 0.0116812534193773], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 2, 0, 0.0, 92.0, 89, 95, 92.0, 95.0, 95.0, 95.0, 0.02484225170169424, 0.008005803771053808, 0.011693325508024047], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 2, 0, 0.0, 112.0, 111, 113, 112.0, 113.0, 113.0, 113.0, 0.024834539878062407, 0.01433321588665516, 0.018674409869246147], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 2, 0, 0.0, 75.5, 74, 77, 75.5, 77.0, 77.0, 77.0, 0.024963179310516986, 0.0080203964776954, 0.011725868406600266], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 2, 0, 0.0, 163.0, 161, 165, 163.0, 165.0, 165.0, 165.0, 0.024819130585855578, 0.046705531873968455, 0.03601682426814589], "isController": false}, {"data": ["Debug Sampler", 5, 0, 0.0, 1.2, 0, 3, 1.0, 3.0, 3.0, 3.0, 0.05535198326156026, 0.30817649118242907, 0.0], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 62.0, 62, 62, 62.0, 62.0, 62.0, 62.0, 16.129032258064516, 5.08757560483871, 10.505922379032258], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 110.0, 110, 110, 110.0, 110.0, 110.0, 110.0, 9.09090909090909, 151.07421875, 5.894886363636363], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 2, 0, 0.0, 50.0, 49, 51, 50.0, 51.0, 51.0, 51.0, 0.02485429171482186, 0.03242708372168164, 0.017378586784973095], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 2, 0, 0.0, 358.5, 357, 360, 358.5, 360.0, 360.0, 360.0, 0.024761668936486316, 1.2904843614584625, 0.011655394948619538], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["ERROR: You are late for Exam.Contact to Instructor for more details.", 1, 100.0, 0.7518796992481203], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 133, 1, "ERROR: You are late for Exam.Contact to Instructor for more details.", 1, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 2, 1, "ERROR: You are late for Exam.Contact to Instructor for more details.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
