import csv
import os,sys

# Function to process a single CSV file
def process_csv_file(file_path, output_csv):
    with open(file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            failure_message = row['failureMessage']
            if failure_message:  # Check if failureMessage is not empty
                filename = os.path.basename(file_path)
                # Append the filename to the output CSV
                with open(output_csv, 'a') as output_file:
                    output_file.write(filename + '\n')
                break

# Function to process all CSV files in a directory
def process_csv_files(directory, output_csv):
    with open(output_csv, 'w') as output_file:
        for file_name in os.listdir(directory):
            if file_name.endswith('.csv'):  # Process only CSV files
                file_path = os.path.join(directory, file_name)
                process_csv_file(file_path, output_csv)

# Specify the directory containing the CSV files
csv_directory = sys.argv[1]
# Specify the output CSV file to store the filenames
output_csv_file = sys.argv[2]

if os.path.exists(output_csv_file):  # Check if the file exists
    os.remove(output_csv_file)  # Delete the file
	

# Process the CSV files and store the filenames in the output CSV
process_csv_file(csv_directory, output_csv_file)
