#!/bin/bash

# Specify the arguments to pass to the Python script
arg1="jtlfile.jtl"
arg2="ans.csv"

# Call the Python script and pass the arguments
python3 test.py "$arg1" "$arg2"

