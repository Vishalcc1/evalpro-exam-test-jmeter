/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.79518072289157, "KoPercent": 1.2048192771084338};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9302325581395349, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "BeanShell Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [0.5, 500, 1500, "OPEN SECTION PAGE-1"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [0.25, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.9166666666666666, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 83, 1, 1.2048192771084338, 255.698795180723, 0, 1184, 166.0, 481.8000000000003, 814.3999999999999, 1184.0, 6.629922517772985, 151.11000029954468, 9.99605599488777], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["BeanShell Sampler", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 0.0, 0.0], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 441.0, 441, 441, 441.0, 441.0, 441.0, 441.0, 2.2675736961451247, 118.11534509637188, 1.0939271541950113], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 438.0, 438, 438, 438.0, 438.0, 438.0, 438.0, 2.28310502283105, 118.97786458333333, 1.1036494006849316], "isController": false}, {"data": ["OPEN SECTION PAGE-1", 2, 1, 50.0, 255.5, 221, 290, 255.5, 290.0, 290.0, 290.0, 2.5806451612903225, 106.03704637096774, 1.2726814516129032], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 349.0, 349, 349, 349.0, 349.0, 349.0, 349.0, 2.865329512893983, 149.2713556590258, 1.382297636103152], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 122.66666666666667, 112, 137, 119.0, 137.0, 137.0, 137.0, 1.910828025477707, 1.1028314092356688, 1.4107285031847132], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 48.0, 45, 52, 47.0, 52.0, 52.0, 52.0, 2.0093770931011385, 2.6216091761553915, 1.4049941393168117], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 144.0, 144, 144, 144.0, 144.0, 144.0, 144.0, 6.944444444444444, 117.60118272569446, 9.026421440972223], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1047.0, 1047, 1047, 1047.0, 1047.0, 1047.0, 1047.0, 0.9551098376313276, 50.080773937440306, 12.428553307067814], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 146.0, 146, 146, 146.0, 146.0, 146.0, 146.0, 6.8493150684931505, 115.98351883561645, 8.909460616438357], "isController": false}, {"data": ["Test", 2, 1, 50.0, 721.5, 538, 905, 721.5, 905.0, 905.0, 905.0, 1.3486176668914363, 118.80544609743762, 3.4466137053270396], "isController": true}, {"data": ["student courses id-1", 1, 0, 0.0, 81.0, 81, 81, 81.0, 81.0, 81.0, 81.0, 12.345679012345679, 18.675250771604937, 6.160783179012346], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 333.0, 301, 360, 332.5, 360.0, 360.0, 360.0, 1.750291715285881, 87.12544213098016, 0.8227282672112018], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 821.0, 821, 821, 821.0, 821.0, 821.0, 821.0, 1.2180267965895248, 0.3913386875761267, 14.910122944579781], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 116.83333333333333, 95, 173, 108.5, 173.0, 173.0, 173.0, 1.838235294117647, 0.5912032781862745, 0.8814194623161765], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 362.0, 362, 362, 362.0, 362.0, 362.0, 362.0, 2.7624309392265194, 143.8865029350829, 1.769682320441989], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 172.66666666666666, 160, 183, 175.0, 183.0, 183.0, 183.0, 1.8564356435643563, 3.4935073097153464, 2.668626237623762], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 170.0, 170, 170, 170.0, 170.0, 170.0, 170.0, 5.88235294117647, 99.60937499999999, 7.645909926470588], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 451.33333333333337, 416, 534, 439.0, 534.0, 534.0, 534.0, 1.665741254858412, 83.45244395474737, 1.5816950652415325], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1184.0, 1184, 1184, 1184.0, 1184.0, 1184.0, 1184.0, 0.8445945945945946, 44.26368507179054, 10.879929001266893], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 76.0, 76, 76, 76.0, 76.0, 76.0, 76.0, 13.157894736842104, 10.074013157894736, 8.699115953947368], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1180.0, 1180, 1180, 1180.0, 1180.0, 1180.0, 1180.0, 0.847457627118644, 44.42697298728814, 11.13529528601695], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 72.0, 72, 72, 72.0, 72.0, 72.0, 72.0, 13.888888888888888, 4.462348090277778, 6.523980034722222], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 366.0, 366, 366, 366.0, 366.0, 366.0, 366.0, 2.73224043715847, 142.32998206967213, 1.2834059084699454], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 99.0, 99, 99, 99.0, 99.0, 99.0, 99.0, 10.101010101010102, 167.86024305555554, 6.549873737373737], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 14.181385869565217], "isController": false}, {"data": ["Debug Sampler-1", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 3820.3125, 0.0], "isController": false}, {"data": ["CSRF-1", 5, 0, 0.0, 99.0, 53, 166, 66.0, 166.0, 166.0, 166.0, 0.41552397573339983, 11.159009988157568, 0.1537925652372642], "isController": false}, {"data": ["Test-1", 1, 0, 0.0, 498.0, 498, 498, 498.0, 498.0, 498.0, 498.0, 2.008032128514056, 62.27644954819277, 5.959384412650603], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 103.0, 103, 103, 103.0, 103.0, 103.0, 103.0, 9.70873786407767, 3.119311286407767, 4.560452063106796], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 391.0, 391, 391, 391.0, 391.0, 391.0, 391.0, 2.557544757033248, 133.23709239130434, 1.2013467071611252], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-1", 1, 0, 0.0, 357.0, 357, 357, 357.0, 357.0, 357.0, 357.0, 2.8011204481792715, 102.32569590336135, 1.3513217787114846], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 434.0, 434, 434, 434.0, 434.0, 434.0, 434.0, 2.304147465437788, 120.77197940668204, 2.1646385368663594], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 439.0, 439, 439, 439.0, 439.0, 439.0, 439.0, 2.277904328018223, 119.39421981776765, 2.1399843394077447], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 411.0, 411, 411, 411.0, 411.0, 411.0, 411.0, 2.4330900243309004, 127.55664537712896, 2.2905261557177616], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 14.16015625], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 13.0, 13, 13, 13.0, 13.0, 13.0, 13.0, 76.92307692307693, 1.2019230769230769, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 315.0, 315, 315, 315.0, 315.0, 315.0, 315.0, 3.1746031746031744, 165.4358878968254, 2.036830357142857], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 97.0, 97, 97, 97.0, 97.0, 97.0, 97.0, 10.309278350515465, 171.33134664948454, 6.684922680412371], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 391.0, 391, 391, 391.0, 391.0, 391.0, 391.0, 2.557544757033248, 133.25457560741688, 1.6384271099744245], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 731.0, 731, 731, 731.0, 731.0, 731.0, 731.0, 1.3679890560875512, 0.44085584815321477, 16.923520861833104], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 788.0, 788, 788, 788.0, 788.0, 788.0, 788.0, 1.2690355329949237, 0.4077272366751269, 15.861704869923857], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 495.0, 495, 495, 495.0, 495.0, 495.0, 495.0, 2.0202020202020203, 105.89291351010101, 1.897885101010101], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 356.0, 356, 356, 356.0, 356.0, 356.0, 356.0, 2.8089887640449436, 146.3307584269663, 1.3194566362359552], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 74.0, 74, 74, 74.0, 74.0, 74.0, 74.0, 13.513513513513514, 4.354940878378379, 6.360853040540541], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 2, 0, 0.0, 133.0, 126, 140, 133.0, 140.0, 140.0, 140.0, 2.014098690835851, 1.162433912386707, 1.514507804632427], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 78.0, 78, 78, 78.0, 78.0, 78.0, 78.0, 12.82051282051282, 4.119090544871795, 6.022135416666667], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 2, 0, 0.0, 182.0, 176, 188, 182.0, 188.0, 188.0, 188.0, 1.9212295869356388, 3.615438880883766, 2.7880343419788667], "isController": false}, {"data": ["Debug Sampler", 4, 0, 0.0, 1.0, 0, 2, 1.0, 2.0, 2.0, 2.0, 0.36429872495446264, 1.8997609289617485, 0.0], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 62.0, 62, 62, 62.0, 62.0, 62.0, 62.0, 16.129032258064516, 5.08757560483871, 10.505922379032258], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 108.0, 108, 108, 108.0, 108.0, 108.0, 108.0, 9.25925925925926, 153.87188946759258, 6.004050925925926], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 2, 0, 0.0, 48.5, 48, 49, 48.5, 49.0, 49.0, 49.0, 2.1881838074398248, 2.8548960612691463, 1.530019146608315], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 336.0, 336, 336, 336.0, 336.0, 336.0, 336.0, 2.976190476190476, 155.06998697916666, 1.4009021577380951], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["UNAUTHENTIC STUDENT(e.g. Wrong IP) able to open section page.", 1, 100.0, 1.2048192771084338], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 83, 1, "UNAUTHENTIC STUDENT(e.g. Wrong IP) able to open section page.", 1, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["OPEN SECTION PAGE-1", 2, 1, "UNAUTHENTIC STUDENT(e.g. Wrong IP) able to open section page.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
