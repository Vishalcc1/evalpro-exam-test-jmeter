/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8386363636363636, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [0.0, 500, 1500, "Test-39"], "isController": true}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [0.0, 500, 1500, "Test-40"], "isController": true}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -10"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [0.95, 500, 1500, "freezeAssignment-ALL PASS"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [0.0, 500, 1500, "Test-35"], "isController": true}, {"data": [0.0, 500, 1500, "Test-32"], "isController": true}, {"data": [0.0, 500, 1500, "Test-30"], "isController": true}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -8-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -8-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -4-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -4-0"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-5"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-6"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-6"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-7"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-7"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-4"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-8"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-5"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-2"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-3"], "isController": false}, {"data": [0.5, 500, 1500, "OPEN ASSIGNMENT 1 by user number-8"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-4"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-9"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-2"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-3"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "CSRF-9"], "isController": false}, {"data": [0.5, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -9-1"], "isController": false}, {"data": [0.0, 500, 1500, "Test29"], "isController": true}, {"data": [0.0, 500, 1500, "Test27"], "isController": true}, {"data": [0.5, 500, 1500, "CSRF-10"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "TestcasesResults"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -5-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -9-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -5-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [0.0, 500, 1500, "RunPractice"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.9166666666666666, 500, 1500, "Delete Sections"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -6-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -6-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-10"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [0.85, 500, 1500, "GetSubmissionId"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-10"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -10-1"], "isController": false}, {"data": [1.0, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -10-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -4"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -3"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -2"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -7-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-7"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-6"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-9"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -7-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -9"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-8"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -8"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-3"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -7"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -6"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-5"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -3-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -5"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-4"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 209, 0, 0.0, 407.22009569378014, 0, 2642, 301.0, 1067.0, 1821.5, 2302.0, 2.5643542489754854, 55.017845612684354, 2.7154001972344237], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 346.0, 346, 346, 346.0, 346.0, 346.0, 346.0, 2.890173410404624, 150.562793533237, 1.3942828757225434], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 338.0, 338, 338, 338.0, 338.0, 338.0, 338.0, 2.9585798816568047, 154.1784162352071, 1.4301728920118342], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 325.0, 325, 325, 325.0, 325.0, 325.0, 325.0, 3.076923076923077, 160.3125, 1.484375], "isController": false}, {"data": ["Test-39", 2, 0, 0.0, 5923.5, 5815, 6032, 5923.5, 6032.0, 6032.0, 6032.0, 0.33156498673740054, 73.34644914622015, 2.1904659524204244], "isController": true}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1148.0, 1148, 1148, 1148.0, 1148.0, 1148.0, 1148.0, 0.8710801393728222, 45.67981680095819, 11.222812772212544], "isController": false}, {"data": ["Test-40", 1, 0, 0.0, 5527.0, 5527, 5527, 5527.0, 5527.0, 5527.0, 5527.0, 0.1809299800977022, 40.01238239551294, 1.1963641555093179], "isController": true}, {"data": ["LOGIN assignment 1 by user number -10", 1, 0, 0.0, 183.0, 183, 183, 183.0, 183.0, 183.0, 183.0, 5.46448087431694, 10.240565232240437, 7.935237363387978], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 690.0, 690, 690, 690.0, 690.0, 690.0, 690.0, 1.4492753623188406, 0.4656363224637681, 17.8810009057971], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 368.0, 368, 368, 368.0, 368.0, 368.0, 368.0, 2.717391304347826, 141.56175696331522, 1.7408288043478262], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1058.0, 1058, 1058, 1058.0, 1058.0, 1058.0, 1058.0, 0.945179584120983, 49.54254784971644, 12.26702800094518], "isController": false}, {"data": ["freezeAssignment-ALL PASS", 10, 0, 0.0, 374.5, 173, 1483, 236.5, 1374.7000000000003, 1483.0, 1483.0, 4.8285852245292125, 133.5471616972477, 2.2492530782230804], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1031.0, 1031, 1031, 1031.0, 1031.0, 1031.0, 1031.0, 0.9699321047526673, 50.840923557226, 12.682051709505336], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 77.0, 77, 77, 77.0, 77.0, 77.0, 77.0, 12.987012987012989, 4.1725852272727275, 6.100344967532467], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 343.0, 343, 343, 343.0, 343.0, 343.0, 343.0, 2.9154518950437316, 151.87397503644314, 1.3694651967930027], "isController": false}, {"data": ["Test-35", 1, 0, 0.0, 5634.0, 5634, 5634, 5634.0, 5634.0, 5634.0, 5634.0, 0.17749378771742988, 39.261487176073835, 1.0866294484380545], "isController": true}, {"data": ["Test-32", 1, 0, 0.0, 5938.0, 5938, 5938, 5938.0, 5938.0, 5938.0, 5938.0, 0.1684068710003368, 56.03508388767262, 0.9675172090771303], "isController": true}, {"data": ["Test-30", 2, 0, 0.0, 5715.5, 5702, 5729, 5715.5, 5729.0, 5729.0, 5729.0, 0.3278151122766759, 72.49900118832979, 2.4362041058842814], "isController": true}, {"data": ["LOGIN assignment 1 by user number -8-1", 1, 0, 0.0, 42.0, 42, 42, 42.0, 42.0, 42.0, 42.0, 23.809523809523807, 30.87797619047619, 16.648065476190474], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 108.0, 108, 108, 108.0, 108.0, 108.0, 108.0, 9.25925925925926, 153.87188946759258, 6.004050925925926], "isController": false}, {"data": ["LOGIN assignment 1 by user number -8-0", 1, 0, 0.0, 104.0, 104, 104, 104.0, 104.0, 104.0, 104.0, 9.615384615384617, 5.549504206730769, 7.230318509615385], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 45.0, 45, 45, 45.0, 45.0, 45.0, 45.0, 22.22222222222222, 7.009548611111112, 14.496527777777779], "isController": false}, {"data": ["LOGIN assignment 1 by user number -4-1", 1, 0, 0.0, 40.0, 40, 40, 40.0, 40.0, 40.0, 40.0, 25.0, 32.6171875, 17.48046875], "isController": false}, {"data": ["LOGIN assignment 1 by user number -4-0", 1, 0, 0.0, 158.0, 158, 158, 158.0, 158.0, 158.0, 158.0, 6.329113924050633, 3.6528382120253164, 4.759196993670886], "isController": false}, {"data": ["CSRF-5", 1, 0, 0.0, 1075.0, 1075, 1075, 1075.0, 1075.0, 1075.0, 1075.0, 0.930232558139535, 24.98637354651163, 0.34429505813953487], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-6", 1, 0, 0.0, 459.0, 459, 459, 459.0, 459.0, 459.0, 459.0, 2.1786492374727673, 78.02500340413943, 1.0510280501089324], "isController": false}, {"data": ["CSRF-6", 1, 0, 0.0, 1089.0, 1089, 1089, 1089.0, 1089.0, 1089.0, 1089.0, 0.9182736455463728, 24.665152949954088, 0.3398688590449954], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-7", 1, 0, 0.0, 422.0, 422, 422, 422.0, 422.0, 422.0, 422.0, 2.3696682464454977, 84.89845601303318, 1.143179798578199], "isController": false}, {"data": ["CSRF-7", 1, 0, 0.0, 1080.0, 1080, 1080, 1080.0, 1080.0, 1080.0, 1080.0, 0.9259259259259259, 24.864366319444443, 0.34270109953703703], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-4", 1, 0, 0.0, 399.0, 399, 399, 399.0, 399.0, 399.0, 399.0, 2.506265664160401, 89.79479949874687, 1.209077380952381], "isController": false}, {"data": ["CSRF-8", 1, 0, 0.0, 1088.0, 1088, 1088, 1088.0, 1088.0, 1088.0, 1088.0, 0.9191176470588235, 24.68154009650735, 0.3401812385110294], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-5", 1, 0, 0.0, 391.0, 391, 391, 391.0, 391.0, 391.0, 391.0, 2.557544757033248, 91.59456921355499, 1.2338155370843988], "isController": false}, {"data": ["CSRF-1", 4, 0, 0.0, 83.0, 55, 153, 62.0, 153.0, 153.0, 153.0, 0.37972280235428135, 10.196984911951775, 0.140541935636985], "isController": false}, {"data": ["CSRF-2", 1, 0, 0.0, 1067.0, 1067, 1067, 1067.0, 1067.0, 1067.0, 1067.0, 0.9372071227741331, 25.167306115276478, 0.34687646438612935], "isController": false}, {"data": ["CSRF-3", 1, 0, 0.0, 1097.0, 1097, 1097, 1097.0, 1097.0, 1097.0, 1097.0, 0.9115770282588879, 24.486169667274385, 0.3373903258887876], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-8", 1, 0, 0.0, 579.0, 579, 579, 579.0, 579.0, 579.0, 579.0, 1.7271157167530224, 61.87257124352332, 0.8331984024179621], "isController": false}, {"data": ["CSRF-4", 1, 0, 0.0, 1083.0, 1083, 1083, 1083.0, 1083.0, 1083.0, 1083.0, 0.9233610341643582, 24.795489958448755, 0.3417517890120037], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-9", 1, 0, 0.0, 472.0, 472, 472, 472.0, 472.0, 472.0, 472.0, 2.1186440677966103, 75.89049258474577, 1.022080243644068], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 86.0, 86, 86, 86.0, 86.0, 86.0, 86.0, 11.627906976744185, 3.735919331395349, 5.461936773255815], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-2", 1, 0, 0.0, 464.0, 464, 464, 464.0, 464.0, 464.0, 464.0, 2.155172413793103, 77.21368197737068, 1.0397023168103448], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 346.0, 346, 346, 346.0, 346.0, 346.0, 346.0, 2.890173410404624, 150.562793533237, 1.357591221098266], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-3", 1, 0, 0.0, 396.0, 396, 396, 396.0, 396.0, 396.0, 396.0, 2.5252525252525255, 90.45780066287878, 1.218237058080808], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-1", 1, 0, 0.0, 395.0, 395, 395, 395.0, 395.0, 395.0, 395.0, 2.5316455696202533, 92.46687104430379, 1.2213212025316456], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 14.16015625], "isController": false}, {"data": ["JSR223 Sampler", 11, 0, 0.0, 36.909090909090914, 4, 147, 30.0, 128.40000000000006, 147.0, 147.0, 0.9291325280851422, 0.0013197905228482134, 0.0], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 105.0, 105, 105, 105.0, 105.0, 105.0, 105.0, 9.523809523809526, 158.26822916666669, 6.175595238095238], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 345.0, 345, 345, 345.0, 345.0, 345.0, 345.0, 2.898550724637681, 151.00203804347828, 1.8568840579710146], "isController": false}, {"data": ["CSRF-9", 1, 0, 0.0, 1100.0, 1100, 1100, 1100.0, 1100.0, 1100.0, 1100.0, 0.9090909090909091, 24.41228693181818, 0.3364701704545454], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 10, 0, 0.0, 761.1, 676, 838, 762.5, 837.3, 838.0, 838.0, 3.656307129798903, 132.41866144881172, 8.774780050274224], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 685.0, 685, 685, 685.0, 685.0, 685.0, 685.0, 1.4598540145985401, 0.4690351277372262, 18.152657390510946], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 433.0, 433, 433, 433.0, 433.0, 433.0, 433.0, 2.3094688221709005, 121.05315386836028, 2.169637702078522], "isController": false}, {"data": ["LOGIN assignment 1 by user number -9-1", 1, 0, 0.0, 43.0, 43, 43, 43.0, 43.0, 43.0, 43.0, 23.25581395348837, 30.159883720930235, 16.2609011627907], "isController": false}, {"data": ["Test29", 1, 0, 0.0, 5719.0, 5719, 5719, 5719.0, 5719.0, 5719.0, 5719.0, 0.17485574401119075, 38.66924533353733, 1.2128909665151248], "isController": true}, {"data": ["Test27", 2, 0, 0.0, 5631.0, 5631, 5631, 5631.0, 5631.0, 5631.0, 5631.0, 0.3487966515521451, 77.13464231993373, 2.1578386597488666], "isController": true}, {"data": ["CSRF-10", 1, 0, 0.0, 1076.0, 1076, 1076, 1076.0, 1076.0, 1076.0, 1076.0, 0.929368029739777, 24.956798907992564, 0.34397508131970256], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 347.0, 347, 347, 347.0, 347.0, 347.0, 347.0, 2.881844380403458, 150.12608069164267, 1.353678854466859], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 1, 0, 0.0, 132.0, 132, 132, 132.0, 132.0, 132.0, 132.0, 7.575757575757576, 4.3723366477272725, 5.696614583333333], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 68.0, 68, 68, 68.0, 68.0, 68.0, 68.0, 14.705882352941176, 4.7248391544117645, 6.90774356617647], "isController": false}, {"data": ["TestcasesResults", 10, 0, 0.0, 271.3, 243, 319, 266.5, 317.5, 319.0, 319.0, 0.16147263038914905, 6.3788311904569674, 0.07710948853544325], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 58.0, 58, 58, 58.0, 58.0, 58.0, 58.0, 17.241379310344826, 5.438442887931034, 11.23046875], "isController": false}, {"data": ["LOGIN assignment 1 by user number -5-1", 1, 0, 0.0, 45.0, 45, 45, 45.0, 45.0, 45.0, 45.0, 22.22222222222222, 28.819444444444446, 15.538194444444445], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 98.0, 98, 98, 98.0, 98.0, 98.0, 98.0, 10.204081632653061, 169.57310267857142, 6.6167091836734695], "isController": false}, {"data": ["LOGIN assignment 1 by user number -9-0", 1, 0, 0.0, 122.0, 122, 122, 122.0, 122.0, 122.0, 122.0, 8.196721311475411, 4.730724897540984, 6.163550204918033], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 28.362771739130434, 15.200407608695652], "isController": false}, {"data": ["LOGIN assignment 1 by user number -5-0", 1, 0, 0.0, 130.0, 130, 130, 130.0, 130.0, 130.0, 130.0, 7.6923076923076925, 4.439603365384615, 5.7842548076923075], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 114.0, 103, 132, 107.0, 132.0, 132.0, 132.0, 1.9083969465648853, 1.1014283158396947, 1.4089336832061068], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 48.0, 45, 50, 49.0, 50.0, 50.0, 50.0, 2.017484868863484, 2.632187289845326, 1.4106632481506387], "isController": false}, {"data": ["RunPractice", 10, 0, 0.0, 2253.7, 2160, 2642, 2183.5, 2608.2000000000003, 2642.0, 2642.0, 3.243593902043464, 92.21689506973726, 1.523602213752838], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 151.0, 151, 151, 151.0, 151.0, 151.0, 151.0, 6.622516556291391, 112.14300496688742, 8.607978062913908], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 154.0, 154, 154, 154.0, 154.0, 154.0, 154.0, 6.493506493506494, 109.95840097402598, 8.446631493506494], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 80.0, 80, 80, 80.0, 80.0, 80.0, 80.0, 12.5, 18.90869140625, 6.23779296875], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 326.0, 283, 413, 316.5, 413.0, 413.0, 413.0, 1.889168765743073, 94.04118535500629, 0.8880077141057934], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 103.5, 98, 117, 100.5, 117.0, 117.0, 117.0, 2.0222446916076846, 0.6503833838894506, 0.9696505308392315], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 163.66666666666666, 154, 180, 157.0, 180.0, 180.0, 180.0, 1.8495684340320593, 3.4805843480271266, 2.658754623921085], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 157.0, 157, 157, 157.0, 157.0, 157.0, 157.0, 6.369426751592357, 107.85728503184713, 8.279010748407643], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 430.1666666666667, 386, 520, 416.0, 520.0, 520.0, 520.0, 1.83206106870229, 91.78763120229007, 1.7396230916030535], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 66.0, 66, 66, 66.0, 66.0, 66.0, 66.0, 15.151515151515152, 11.600378787878787, 10.017163825757576], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-1", 10, 0, 0.0, 402.6, 363, 496, 390.5, 491.6, 496.0, 496.0, 4.1963911036508605, 150.6303602339488, 1.9711563680234998], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-0", 10, 0, 0.0, 357.8, 306, 433, 357.5, 430.40000000000003, 433.0, 433.0, 4.422821760283061, 1.4210042569659442, 8.536823446483856], "isController": false}, {"data": ["LOGIN assignment 1 by user number -6-1", 1, 0, 0.0, 40.0, 40, 40, 40.0, 40.0, 40.0, 40.0, 25.0, 32.421875, 17.48046875], "isController": false}, {"data": ["LOGIN assignment 1 by user number -6-0", 1, 0, 0.0, 116.0, 116, 116, 116.0, 116.0, 116.0, 116.0, 8.620689655172413, 4.975417564655173, 6.4823545258620685], "isController": false}, {"data": ["LOGIN assignment 1 by user number -2-1", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 27.7593085106383, 14.876994680851064], "isController": false}, {"data": ["LOGIN assignment 1 by user number -2-0", 1, 0, 0.0, 101.0, 101, 101, 101.0, 101.0, 101.0, 101.0, 9.900990099009901, 5.714340965346534, 7.445080445544554], "isController": false}, {"data": ["Debug Sampler-10", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 2285.8072916666665, 0.0], "isController": false}, {"data": ["Debug Sampler-1", 2, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 0.028142457117930966, 0.1789959211026215, 0.0], "isController": false}, {"data": ["GetSubmissionId", 10, 0, 0.0, 442.7, 354, 531, 447.0, 530.6, 531.0, 531.0, 4.11522633744856, 147.70969971707817, 1.9330311213991769], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-10", 1, 0, 0.0, 422.0, 422, 422, 422.0, 422.0, 422.0, 422.0, 2.3696682464454977, 84.87300059241707, 1.143179798578199], "isController": false}, {"data": ["LOGIN assignment 1 by user number -10-1", 1, 0, 0.0, 50.0, 50, 50, 50.0, 50.0, 50.0, 50.0, 20.0, 25.9375, 13.984375], "isController": false}, {"data": ["Test-1", 1, 0, 0.0, 479.0, 479, 479, 479.0, 479.0, 479.0, 479.0, 2.08768267223382, 64.74874412839249, 6.195769180584551], "isController": true}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 416.0, 416, 416, 416.0, 416.0, 416.0, 416.0, 2.403846153846154, 125.99769005408655, 2.25830078125], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 420.0, 420, 420, 420.0, 420.0, 420.0, 420.0, 2.3809523809523814, 124.79538690476191, 2.2367931547619047], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 449.0, 449, 449, 449.0, 449.0, 449.0, 449.0, 2.2271714922048997, 116.778605233853, 2.096673162583519], "isController": false}, {"data": ["LOGIN assignment 1 by user number -10-0", 1, 0, 0.0, 132.0, 132, 132, 132.0, 132.0, 132.0, 132.0, 7.575757575757576, 4.3723366477272725, 5.704012784090909], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 340.0, 340, 340, 340.0, 340.0, 340.0, 340.0, 2.941176470588235, 153.28871783088235, 1.887063419117647], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 807.0, 807, 807, 807.0, 807.0, 807.0, 807.0, 1.2391573729863692, 0.3993378252788104, 15.169996902106567], "isController": false}, {"data": ["LOGIN assignment 1 by user number -4", 1, 0, 0.0, 198.0, 198, 198, 198.0, 198.0, 198.0, 198.0, 5.050505050505051, 9.504221906565656, 7.329150883838383], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 76.0, 76, 76, 76.0, 76.0, 76.0, 76.0, 13.157894736842104, 4.240337171052632, 6.193462171052632], "isController": false}, {"data": ["LOGIN assignment 1 by user number -3", 1, 0, 0.0, 184.0, 184, 184, 184.0, 184.0, 184.0, 184.0, 5.434782608695652, 10.227369225543478, 7.886803668478261], "isController": false}, {"data": ["LOGIN assignment 1 by user number -2", 1, 0, 0.0, 149.0, 149, 149, 149.0, 149.0, 149.0, 149.0, 6.7114093959731544, 12.62977139261745, 9.73940855704698], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 1, 0, 0.0, 178.0, 178, 178, 178.0, 178.0, 178.0, 178.0, 5.617977528089887, 10.572112008426966, 8.152650983146067], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 1.0, 0, 2, 1.0, 2.0, 2.0, 2.0, 0.22456770716370986, 2.0514830240848863, 0.0], "isController": false}, {"data": ["LOGIN assignment 1 by user number -7-0", 1, 0, 0.0, 136.0, 136, 136, 136.0, 136.0, 136.0, 136.0, 7.352941176470588, 4.243738511029411, 5.529067095588235], "isController": false}, {"data": ["Debug Sampler-7", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 3427.734375, 0.0], "isController": false}, {"data": ["Debug Sampler-6", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 6855.46875, 0.0], "isController": false}, {"data": ["Debug Sampler-9", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 3427.734375, 0.0], "isController": false}, {"data": ["LOGIN assignment 1 by user number -7-1", 1, 0, 0.0, 52.0, 52, 52, 52.0, 52.0, 52.0, 52.0, 19.230769230769234, 24.939903846153847, 13.446514423076923], "isController": false}, {"data": ["LOGIN assignment 1 by user number -9", 1, 0, 0.0, 166.0, 166, 166, 166.0, 166.0, 166.0, 166.0, 6.024096385542169, 11.28929781626506, 8.741999246987952], "isController": false}, {"data": ["Debug Sampler-8", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 3427.734375, 0.0], "isController": false}, {"data": ["LOGIN assignment 1 by user number -3-0", 1, 0, 0.0, 144.0, 144, 144, 144.0, 144.0, 144.0, 144.0, 6.944444444444444, 4.007975260416667, 5.221896701388889], "isController": false}, {"data": ["LOGIN assignment 1 by user number -8", 1, 0, 0.0, 146.0, 146, 146, 146.0, 146.0, 146.0, 146.0, 6.8493150684931505, 12.835776969178083, 9.93953339041096], "isController": false}, {"data": ["Debug Sampler-3", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 3427.734375, 0.0], "isController": false}, {"data": ["LOGIN assignment 1 by user number -7", 1, 0, 0.0, 188.0, 188, 188, 188.0, 188.0, 188.0, 188.0, 5.319148936170213, 9.96820977393617, 7.718999335106383], "isController": false}, {"data": ["Debug Sampler-2", 1, 0, 0.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, Infinity, Infinity, NaN], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 372.0, 372, 372, 372.0, 372.0, 372.0, 372.0, 2.688172043010753, 140.08421538978496, 1.2653309811827957], "isController": false}, {"data": ["LOGIN assignment 1 by user number -6", 1, 0, 0.0, 156.0, 156, 156, 156.0, 156.0, 156.0, 156.0, 6.41025641025641, 12.012970753205128, 9.302383814102564], "isController": false}, {"data": ["Debug Sampler-5", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 6855.46875, 0.0], "isController": false}, {"data": ["LOGIN assignment 1 by user number -3-1", 1, 0, 0.0, 39.0, 39, 39, 39.0, 39.0, 39.0, 39.0, 25.64102564102564, 33.45352564102564, 17.928685897435898], "isController": false}, {"data": ["LOGIN assignment 1 by user number -5", 1, 0, 0.0, 175.0, 175, 175, 175.0, 175.0, 175.0, 175.0, 5.714285714285714, 10.708705357142858, 8.292410714285715], "isController": false}, {"data": ["Debug Sampler-4", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 3427.734375, 0.0], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 209, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
