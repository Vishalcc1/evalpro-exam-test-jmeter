/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9176470588235294, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [0.0, 500, 1500, "RunPractice"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [0.0, 500, 1500, "freezeAssignment-ALL PASS"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "GetSubmissionId"], "isController": false}, {"data": [0.5, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-1"], "isController": false}, {"data": [0.5, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.5, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "TestcasesResults"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 83, 0, 0.0, 313.3253012048192, 0, 2166, 236.0, 705.6, 1083.3999999999999, 2166.0, 3.535073895821798, 88.39460761478342, 5.372279151049875], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 352.0, 352, 352, 352.0, 352.0, 352.0, 352.0, 2.840909090909091, 148.01025390625, 1.3705166903409092], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 364.0, 364, 364, 364.0, 364.0, 364.0, 364.0, 2.7472527472527473, 143.1603064903846, 1.3280176854395604], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 374.0, 374, 374, 374.0, 374.0, 374.0, 374.0, 2.6737967914438503, 139.29332386363637, 1.289898061497326], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 118.33333333333333, 103, 138, 114.0, 138.0, 138.0, 138.0, 1.8808777429467085, 1.0855456504702194, 1.3886167711598747], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 47.666666666666664, 47, 48, 48.0, 48.0, 48.0, 48.0, 1.9543973941368078, 2.5498778501628667, 1.366551302931596], "isController": false}, {"data": ["RunPractice", 1, 0, 0.0, 2166.0, 2166, 2166, 2166.0, 2166.0, 2166.0, 2166.0, 0.4616805170821791, 13.128137984764543, 0.21686360226223453], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 169.0, 169, 169, 169.0, 169.0, 169.0, 169.0, 5.9171597633136095, 100.1987795857988, 7.69115199704142], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1089.0, 1089, 1089, 1089.0, 1089.0, 1089.0, 1089.0, 0.9182736455463728, 48.14749053030303, 11.860436753902663], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 144.0, 144, 144, 144.0, 144.0, 144.0, 144.0, 6.944444444444444, 117.59440104166667, 9.033203125], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 84.0, 84, 84, 84.0, 84.0, 84.0, 84.0, 11.904761904761903, 18.008277529761905, 5.940755208333333], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 322.6666666666667, 289, 357, 316.5, 357.0, 357.0, 357.0, 1.8744142455482662, 93.30397239144017, 0.8810723211496407], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 705.0, 705, 705, 705.0, 705.0, 705.0, 705.0, 1.4184397163120568, 0.4557291666666667, 17.40913120567376], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 103.0, 85, 123, 105.0, 123.0, 123.0, 123.0, 2.0134228187919465, 0.6475461409395973, 0.9654205117449665], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 355.0, 355, 355, 355.0, 355.0, 355.0, 355.0, 2.8169014084507045, 146.72095070422537, 1.8045774647887325], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 168.33333333333334, 156, 187, 162.0, 187.0, 187.0, 187.0, 1.8259281801582472, 3.4360972687157636, 2.6247717589774804], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 160.0, 160, 160, 160.0, 160.0, 160.0, 160.0, 6.25, 105.841064453125, 8.123779296875], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 426.5, 389, 469, 431.0, 469.0, 469.0, 469.0, 1.825372680255552, 91.44986404776392, 1.7332721706723457], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1061.0, 1061, 1061, 1061.0, 1061.0, 1061.0, 1061.0, 0.942507068803016, 49.394181491517436, 12.17159519321395], "isController": false}, {"data": ["freezeAssignment-ALL PASS", 1, 0, 0.0, 1539.0, 1539, 1539, 1539.0, 1539.0, 1539.0, 1539.0, 0.649772579597141, 77.98857314002599, 0.30267726608187134], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 75.0, 75, 75, 75.0, 75.0, 75.0, 75.0, 13.333333333333334, 10.208333333333334, 8.815104166666668], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1204.0, 1204, 1204, 1204.0, 1204.0, 1204.0, 1204.0, 0.8305647840531561, 43.5413854858804, 10.779498027408637], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 102.0, 102, 102, 102.0, 102.0, 102.0, 102.0, 9.803921568627452, 3.149892769607843, 4.605162377450981], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-1", 1, 0, 0.0, 351.0, 351, 351, 351.0, 351.0, 351.0, 351.0, 2.849002849002849, 104.09711093304844, 1.3382523148148149], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 380.0, 380, 380, 380.0, 380.0, 380.0, 380.0, 2.631578947368421, 137.08881578947367, 1.2361225328947367], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-0", 1, 0, 0.0, 354.0, 354, 354, 354.0, 354.0, 354.0, 354.0, 2.824858757062147, 0.9075962217514125, 4.071769067796611], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 92.0, 92, 92, 92.0, 92.0, 92.0, 92.0, 10.869565217391305, 180.6322180706522, 7.048233695652174], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 51.0, 51, 51, 51.0, 51.0, 51.0, 51.0, 19.607843137254903, 6.184895833333334, 12.791053921568627], "isController": false}, {"data": ["Debug Sampler-1", 2, 0, 0.0, 1.0, 0, 2, 1.0, 2.0, 2.0, 2.0, 0.08826125330979699, 0.38222121855692853, 0.0], "isController": false}, {"data": ["CSRF-1", 4, 0, 0.0, 80.75, 58, 141, 62.0, 141.0, 141.0, 141.0, 0.3673094582185491, 9.864357351928374, 0.13594754361799816], "isController": false}, {"data": ["GetSubmissionId", 1, 0, 0.0, 373.0, 373, 373, 373.0, 373.0, 373.0, 373.0, 2.680965147453083, 97.9573349530831, 1.2593205428954424], "isController": false}, {"data": ["Test-1", 2, 0, 0.0, 3172.5, 456, 5889, 3172.5, 5889.0, 5889.0, 5889.0, 0.11940298507462686, 21.35261194029851, 0.5412196828358209], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 88.0, 88, 88, 88.0, 88.0, 88.0, 88.0, 11.363636363636363, 3.6510120738636367, 5.337801846590909], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 343.0, 343, 343, 343.0, 343.0, 343.0, 343.0, 2.9154518950437316, 151.8825163994169, 1.3694651967930027], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-1", 1, 0, 0.0, 364.0, 364, 364, 364.0, 364.0, 364.0, 364.0, 2.7472527472527473, 100.36057692307692, 1.3253348214285714], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 560.0, 560, 560, 560.0, 560.0, 560.0, 560.0, 1.7857142857142856, 93.61746651785714, 1.6775948660714284], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 483.0, 483, 483, 483.0, 483.0, 483.0, 483.0, 2.070393374741201, 108.51974961180125, 1.9450375258799173], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 442.0, 442, 442, 442.0, 442.0, 442.0, 442.0, 2.2624434389140275, 118.62804015837104, 2.1298783936651584], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 6.71126994680851, 13.858876329787234], "isController": false}, {"data": ["JSR223 Sampler", 2, 0, 0.0, 72.5, 15, 130, 72.5, 130.0, 130.0, 130.0, 0.17299541562148602, 0.0013515266845428597, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 347.0, 347, 347, 347.0, 347.0, 347.0, 347.0, 2.881844380403458, 150.1739238112392, 1.8489958573487033], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 121.0, 121, 121, 121.0, 121.0, 121.0, 121.0, 8.264462809917356, 137.34019886363637, 5.358987603305786], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 365.0, 365, 365, 365.0, 365.0, 365.0, 365.0, 2.73972602739726, 142.7466823630137, 1.75513698630137], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 741.0, 741, 741, 741.0, 741.0, 741.0, 741.0, 1.3495276653171389, 0.4349063765182186, 16.564661352901485], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 1, 0, 0.0, 706.0, 706, 706, 706.0, 706.0, 706.0, 706.0, 1.41643059490085, 52.208746458923514, 2.7069869865439093], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 838.0, 838, 838, 838.0, 838.0, 838.0, 838.0, 1.1933174224343677, 0.3833998359188544, 14.723019838902148], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 431.0, 431, 431, 431.0, 431.0, 431.0, 431.0, 2.320185614849188, 121.61715124709977, 2.179705626450116], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 470.0, 470, 470, 470.0, 470.0, 470.0, 470.0, 2.127659574468085, 110.86062167553192, 0.9994182180851064], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 91.0, 91, 91, 91.0, 91.0, 91.0, 91.0, 10.989010989010989, 3.5413804945054945, 5.172561813186813], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 1, 0, 0.0, 236.0, 236, 236, 236.0, 236.0, 236.0, 236.0, 4.237288135593221, 2.4455442266949152, 3.186242055084746], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 90.0, 90, 90, 90.0, 90.0, 90.0, 90.0, 11.11111111111111, 3.5698784722222223, 5.219184027777778], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 1, 0, 0.0, 284.0, 284, 284, 284.0, 284.0, 284.0, 284.0, 3.5211267605633805, 6.626182878521127, 5.109760123239437], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 1.5, 1, 2, 1.5, 2.0, 2.0, 2.0, 0.21561017680034497, 1.5325377654700303, 0.0], "isController": false}, {"data": ["TestcasesResults", 1, 0, 0.0, 269.0, 269, 269, 269.0, 269.0, 269.0, 269.0, 3.717472118959108, 146.87645213754647, 1.7752381505576207], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 59.0, 59, 59, 59.0, 59.0, 59.0, 59.0, 16.949152542372882, 5.346265889830509, 11.0401218220339], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 100.0, 100, 100, 100.0, 100.0, 100.0, 100.0, 10.0, 166.19140625, 6.484375], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 27.7593085106383, 14.876994680851064], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 351.0, 351, 351, 351.0, 351.0, 351.0, 351.0, 2.849002849002849, 148.46532229344731, 1.3410345441595442], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 83, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
