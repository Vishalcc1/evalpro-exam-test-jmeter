/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8861386138613861, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5, 500, 1500, "Upload ALL FAIL PROGRAM"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Upload PARTIAL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload PARTIAL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [0.0, 500, 1500, "RunPractice"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-PARTIAL PASS"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL FAIL PROGRAM-0"], "isController": false}, {"data": [0.9166666666666666, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-ALL PASS"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL FAIL PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "GetSubmissionId"], "isController": false}, {"data": [0.25, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 1 by user number-1"], "isController": false}, {"data": [0.5, 500, 1500, "Upload PARTIAL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-ALL FAIL"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.5, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "TestcasesResults"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 99, 0, 0.0, 386.04040404040416, 0, 2180, 334.0, 943.0, 1292.0, 2180.0, 2.560057924542939, 67.15957512574022, 3.5784395525716945], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Upload ALL FAIL PROGRAM", 1, 0, 0.0, 733.0, 733, 733, 733.0, 733.0, 733.0, 733.0, 1.364256480218281, 51.707185794679404, 2.2555529502046383], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 347.0, 347, 347, 347.0, 347.0, 347.0, 347.0, 2.881844380403458, 150.1288949927954, 1.3902647694524497], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 379.0, 379, 379, 379.0, 379.0, 379.0, 379.0, 2.638522427440633, 137.48144788918205, 1.2754576187335092], "isController": false}, {"data": ["Upload PARTIAL PASS PROGRAM-1", 1, 0, 0.0, 371.0, 371, 371, 371.0, 371.0, 371.0, 371.0, 2.6954177897574128, 101.27558541105121, 1.266109332884097], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 364.0, 364, 364, 364.0, 364.0, 364.0, 364.0, 2.7472527472527473, 143.1200635302198, 1.3253348214285714], "isController": false}, {"data": ["Upload PARTIAL PASS PROGRAM-0", 1, 0, 0.0, 355.0, 355, 355, 355.0, 355.0, 355.0, 355.0, 2.8169014084507045, 0.9050396126760564, 3.4000880281690145], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 139.66666666666666, 127, 152, 140.0, 152.0, 152.0, 152.0, 1.8192844147968465, 1.049997157368102, 1.3431435718617344], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 47.0, 45, 49, 47.0, 49.0, 49.0, 49.0, 1.9329896907216495, 2.521947487113402, 1.3515826353092784], "isController": false}, {"data": ["RunPractice", 3, 0, 0.0, 2169.3333333333335, 2152, 2180, 2176.0, 2180.0, 2180.0, 2180.0, 0.19051247856734618, 5.416954419889503, 0.08948877167079444], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 162.0, 162, 162, 162.0, 162.0, 162.0, 162.0, 6.172839506172839, 104.52835648148148, 8.023485725308642], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1257.0, 1257, 1257, 1257.0, 1257.0, 1257.0, 1257.0, 0.7955449482895784, 41.8647760043755, 10.268278888225936], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 155.0, 155, 155, 155.0, 155.0, 155.0, 155.0, 6.451612903225806, 109.24899193548387, 8.392137096774194], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 90.0, 90, 90, 90.0, 90.0, 90.0, 90.0, 11.11111111111111, 16.807725694444446, 5.544704861111112], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 345.3333333333333, 290, 415, 348.0, 415.0, 415.0, 415.0, 1.79265013444876, 89.24474109276964, 0.8426389303854198], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 943.0, 943, 943, 943.0, 943.0, 943.0, 943.0, 1.0604453870625663, 0.5219379639448569, 12.911751060445388], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 107.66666666666667, 99, 112, 110.0, 112.0, 112.0, 112.0, 1.8981335020563113, 0.6104674153748814, 0.9101401850680164], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 352.0, 352, 352, 352.0, 352.0, 352.0, 352.0, 2.840909090909091, 148.03244850852275, 2.230557528409091], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 188.66666666666666, 174, 199, 193.0, 199.0, 199.0, 199.0, 1.7688679245283019, 3.3287192290683962, 2.542747641509434], "isController": false}, {"data": ["freezeAssignment-PARTIAL PASS", 1, 0, 0.0, 1093.0, 1093, 1093, 1093.0, 1093.0, 1093.0, 1093.0, 0.9149130832570906, 83.07750314501372, 0.4261850983531565], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 164.0, 164, 164, 164.0, 164.0, 164.0, 164.0, 6.097560975609756, 103.25362042682926, 7.925638338414633], "isController": false}, {"data": ["Upload ALL FAIL PROGRAM-0", 1, 0, 0.0, 347.0, 347, 347, 347.0, 347.0, 347.0, 347.0, 2.881844380403458, 0.9259050792507205, 3.410932997118156], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 453.5, 389, 528, 458.5, 528.0, 528.0, 528.0, 1.7356089094590685, 86.96321548669367, 1.6480374963841482], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1296.0, 1296, 1296, 1296.0, 1296.0, 1296.0, 1296.0, 0.7716049382716049, 40.586118344907405, 10.00072337962963], "isController": false}, {"data": ["freezeAssignment-ALL PASS", 1, 0, 0.0, 1084.0, 1084, 1084, 1084.0, 1084.0, 1084.0, 1084.0, 0.9225092250922509, 83.74473881457564, 0.42972353551660514], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 69.0, 69, 69, 69.0, 69.0, 69.0, 69.0, 14.492753623188406, 11.096014492753623, 9.581634963768115], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1292.0, 1292, 1292, 1292.0, 1292.0, 1292.0, 1292.0, 0.7739938080495357, 40.71101610874613, 9.994648558436532], "isController": false}, {"data": ["Upload ALL FAIL PROGRAM-1", 1, 0, 0.0, 386.0, 386, 386, 386.0, 386.0, 386.0, 386.0, 2.5906735751295336, 97.35771534974093, 1.2169081930051813], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 75.0, 75, 75, 75.0, 75.0, 75.0, 75.0, 13.333333333333334, 4.283854166666667, 8.190104166666668], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-1", 1, 0, 0.0, 419.0, 419, 419, 419.0, 419.0, 419.0, 419.0, 2.3866348448687353, 89.67360903937949, 1.1210657816229117], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 343.0, 343, 343, 343.0, 343.0, 343.0, 343.0, 2.9154518950437316, 151.90814048833818, 1.790839103498542], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-0", 1, 0, 0.0, 442.0, 442, 442, 442.0, 442.0, 442.0, 442.0, 2.2624434389140275, 0.7268983314479638, 2.7264210972850678], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 101.0, 101, 101, 101.0, 101.0, 101.0, 101.0, 9.900990099009901, 164.53627784653463, 6.420173267326732], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 53.0, 53, 53, 53.0, 53.0, 53.0, 53.0, 18.867924528301884, 5.951503537735849, 12.308372641509434], "isController": false}, {"data": ["Debug Sampler-1", 2, 0, 0.0, 1.0, 0, 2, 1.0, 2.0, 2.0, 2.0, 0.052887666596149775, 0.2512680644436217, 0.0], "isController": false}, {"data": ["CSRF-1", 4, 0, 0.0, 87.5, 57, 162, 65.5, 162.0, 162.0, 162.0, 0.340280731603573, 9.138647782858358, 0.12594374734155678], "isController": false}, {"data": ["GetSubmissionId", 3, 0, 0.0, 391.3333333333333, 345, 469, 360.0, 469.0, 469.0, 469.0, 0.8237232289950577, 30.953706325507962, 0.3869246808072488], "isController": false}, {"data": ["Test-1", 2, 0, 0.0, 7718.0, 514, 14922, 7718.0, 14922.0, 14922.0, 14922.0, 0.0750778933143136, 30.029471005856077, 0.5978003936897031], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 99.0, 99, 99, 99.0, 99.0, 99.0, 99.0, 10.101010101010102, 3.2453440656565653, 6.2046243686868685], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 362.0, 362, 362, 362.0, 362.0, 362.0, 362.0, 2.7624309392265194, 143.95934046961327, 1.6968447859116023], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-1", 1, 0, 0.0, 410.0, 410, 410, 410.0, 410.0, 410.0, 410.0, 2.4390243902439024, 91.64681783536587, 1.1766387195121952], "isController": false}, {"data": ["Upload PARTIAL PASS PROGRAM", 1, 0, 0.0, 726.0, 726, 726, 726.0, 726.0, 726.0, 726.0, 1.3774104683195594, 52.19632403581267, 2.309583763774105], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 719.0, 719, 719, 719.0, 719.0, 719.0, 719.0, 1.3908205841446453, 72.91621392559111, 1.7086448191933241], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 418.0, 418, 418, 418.0, 418.0, 418.0, 418.0, 2.3923444976076556, 125.4205293062201, 2.93903259569378], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 428.0, 428, 428, 428.0, 428.0, 428.0, 428.0, 2.336448598130841, 122.53577686915888, 2.9159973714953273], "isController": false}, {"data": ["freezeAssignment-ALL FAIL", 1, 0, 0.0, 1095.0, 1095, 1095, 1095.0, 1095.0, 1095.0, 1095.0, 0.91324200913242, 82.93111444063928, 0.4254066780821918], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 48.0, 48, 48, 48.0, 48.0, 48.0, 48.0, 20.833333333333332, 6.571451822916667, 13.570149739583334], "isController": false}, {"data": ["JSR223 Sampler", 4, 0, 0.0, 59.75, 18, 143, 39.0, 143.0, 143.0, 143.0, 0.13998740113389796, 5.468257856792888E-4, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 353.0, 353, 353, 353.0, 353.0, 353.0, 353.0, 2.8328611898017, 147.65459012039662, 2.251903328611898], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 113.0, 113, 113, 113.0, 113.0, 113.0, 113.0, 8.849557522123893, 147.06339878318585, 5.738384955752212], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 361.0, 361, 361, 361.0, 361.0, 361.0, 361.0, 2.770083102493075, 144.33918369113573, 2.174948060941828], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 903.0, 903, 903, 903.0, 903.0, 903.0, 903.0, 1.1074197120708749, 0.5558727851605758, 13.413404969545958], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 1, 0, 0.0, 861.0, 861, 861, 861.0, 861.0, 861.0, 861.0, 1.1614401858304297, 44.01223141695703, 1.945185467479675], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 930.0, 930, 930, 930.0, 930.0, 930.0, 930.0, 1.075268817204301, 0.5292338709677419, 13.040784610215052], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 461.0, 461, 461, 461.0, 461.0, 461.0, 461.0, 2.1691973969631237, 113.74093343275487, 2.6648928958785247], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 351.0, 351, 351, 351.0, 351.0, 351.0, 351.0, 2.849002849002849, 148.44862891737893, 1.750022257834758], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 70.0, 70, 70, 70.0, 70.0, 70.0, 70.0, 14.285714285714285, 4.603794642857142, 8.914620535714285], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 1, 0, 0.0, 110.0, 110, 110, 110.0, 110.0, 110.0, 110.0, 9.09090909090909, 5.2468039772727275, 6.8359375], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 367.0, 367, 367, 367.0, 367.0, 367.0, 367.0, 2.7247956403269753, 0.8754470367847411, 1.6737270095367849], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 1, 0, 0.0, 157.0, 157, 157, 157.0, 157.0, 157.0, 157.0, 6.369426751592357, 11.986216162420382, 9.24313296178344], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 3.5, 3, 4, 3.5, 4.0, 4.0, 4.0, 0.19815713861091844, 1.455893781581294, 0.0], "isController": false}, {"data": ["TestcasesResults", 3, 0, 0.0, 264.0, 258, 272, 262.0, 272.0, 272.0, 272.0, 0.20388745412532283, 8.053886286020116, 0.09736422369851841], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 54.0, 54, 54, 54.0, 54.0, 54.0, 54.0, 18.51851851851852, 5.8412905092592595, 12.062355324074074], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 109.0, 109, 109, 109.0, 109.0, 109.0, 109.0, 9.174311926605505, 152.46022075688074, 5.948967889908257], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 27.7593085106383, 14.876994680851064], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 357.0, 357, 357, 357.0, 357.0, 357.0, 357.0, 2.8011204481792715, 146.0029324229692, 1.7479648109243697], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 99, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
