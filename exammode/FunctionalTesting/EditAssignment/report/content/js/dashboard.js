/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 91.93548387096774, "KoPercent": 8.064516129032258};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9144385026737968, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "CSRF-1-5"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-6"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-7"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-8"], "isController": false}, {"data": [0.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-8"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-9"], "isController": false}, {"data": [0.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-9"], "isController": false}, {"data": [0.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-2"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-3"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-6"], "isController": false}, {"data": [0.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-7"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-4"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-10"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-5"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-6"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-5"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-8"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-7"], "isController": false}, {"data": [0.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-2"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-4"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-3"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-0"], "isController": false}, {"data": [0.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-2"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-9"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-3"], "isController": false}, {"data": [0.6666666666666666, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-4"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-20"], "isController": false}, {"data": [0.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [0.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-19"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-18"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-13"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-12"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-11"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-10"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-17"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-16"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-15"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-14"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-22"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-21"], "isController": false}, {"data": [0.0, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "CSRF-1-20"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-22"], "isController": false}, {"data": [0.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [0.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [0.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-23"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-22"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1-21"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-19"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-18"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-15"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-14"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-17"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-16"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-11"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-10"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-13"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-12"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-21"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-20"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-12"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-11"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-14"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-13"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-16"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-15"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-18"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-17"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-19"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 186, 15, 8.064516129032258, 17.32258064516129, 1, 259, 7.0, 33.30000000000001, 94.65, 185.0499999999996, 91.5805022156573, 6118.752115645002, 116.62348827548006], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["CSRF-1-5", 3, 0, 0.0, 15.0, 9, 21, 15.0, 21.0, 21.0, 21.0, 2.3023791250959325, 187.29764245970838, 1.0949791346891788], "isController": false}, {"data": ["CSRF-1-6", 3, 0, 0.0, 13.0, 6, 19, 14.0, 19.0, 19.0, 19.0, 2.304147465437788, 8.667554723502304, 1.0508172523041475], "isController": false}, {"data": ["CSRF-1-7", 3, 0, 0.0, 8.333333333333334, 4, 12, 9.0, 12.0, 12.0, 12.0, 2.331002331002331, 44.73293633449884, 1.069893648018648], "isController": false}, {"data": ["LOGIN-1-8", 3, 0, 0.0, 8.666666666666666, 6, 11, 9.0, 11.0, 11.0, 11.0, 2.638522427440633, 0.9585257255936676, 2.0020819591029024], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 1, 100.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 29.0, 34.48275862068965, 108.70150862068965, 15.389278017241379], "isController": false}, {"data": ["CSRF-1-8", 3, 0, 0.0, 10.666666666666666, 5, 14, 13.0, 14.0, 14.0, 14.0, 2.31839258114374, 190.30969257148377, 1.0641059698608963], "isController": false}, {"data": ["LOGIN-1-9", 3, 0, 0.0, 8.333333333333334, 6, 12, 7.0, 12.0, 12.0, 12.0, 2.65017667844523, 0.9627594964664312, 2.01092507729682], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 1, 100.0, 30.0, 30, 30, 30.0, 30.0, 30.0, 30.0, 33.333333333333336, 105.078125, 14.876302083333334], "isController": false}, {"data": ["CSRF-1-9", 3, 0, 0.0, 10.333333333333334, 6, 13, 12.0, 13.0, 13.0, 13.0, 2.3255813953488373, 252.42550872093022, 1.067405523255814], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 1, 100.0, 27.0, 27, 27, 27.0, 27.0, 27.0, 27.0, 37.03703703703704, 117.0066550925926, 16.529224537037038], "isController": false}, {"data": ["LOGIN-1-2", 3, 0, 0.0, 10.666666666666666, 5, 19, 8.0, 19.0, 19.0, 19.0, 2.6223776223776225, 0.9526606206293707, 2.002636035839161], "isController": false}, {"data": ["LOGIN-1-3", 3, 0, 0.0, 13.666666666666666, 10, 17, 14.0, 17.0, 17.0, 17.0, 2.617801047120419, 0.9458851439790577, 1.9710201243455499], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 96.33333333333333, 94, 98, 97.0, 98.0, 98.0, 98.0, 2.4271844660194173, 67.90743830906149, 1.791944781553398], "isController": false}, {"data": ["student courses id-1-0", 1, 0, 0.0, 12.0, 12, 12, 12.0, 12.0, 12.0, 12.0, 83.33333333333333, 26.611328125, 38.004557291666664], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 17.333333333333332, 4, 34, 14.0, 34.0, 34.0, 34.0, 2.633889376646181, 0.9568426251097454, 1.995994293239684], "isController": false}, {"data": ["LOGIN-1-6", 3, 0, 0.0, 13.333333333333334, 8, 19, 13.0, 19.0, 19.0, 19.0, 2.6223776223776225, 0.9475387893356644, 1.9770268793706296], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 1, 100.0, 30.0, 30, 30, 30.0, 30.0, 30.0, 30.0, 33.333333333333336, 105.078125, 20.5078125], "isController": false}, {"data": ["LOGIN-1-7", 3, 0, 0.0, 9.0, 8, 10, 9.0, 10.0, 10.0, 10.0, 2.626970227670753, 0.9517636274080561, 1.9907508756567427], "isController": false}, {"data": ["LOGIN-1-4", 3, 0, 0.0, 10.0, 6, 13, 11.0, 13.0, 13.0, 13.0, 2.615518744551003, 0.9450604838709677, 1.966747493461203], "isController": false}, {"data": ["LOGIN-1-10", 3, 0, 0.0, 8.0, 4, 11, 9.0, 11.0, 11.0, 11.0, 2.6455026455026456, 0.961061507936508, 2.0512979497354498], "isController": false}, {"data": ["LOGIN-1-5", 3, 0, 0.0, 17.666666666666668, 9, 33, 11.0, 33.0, 33.0, 33.0, 2.6200873362445414, 0.9518286026200873, 2.03159115720524], "isController": false}, {"data": ["student courses id-1-6", 1, 0, 0.0, 4.0, 4, 4, 4.0, 4.0, 4.0, 4.0, 250.0, 90.8203125, 142.333984375], "isController": false}, {"data": ["student courses id-1-5", 1, 0, 0.0, 4.0, 4, 4, 4.0, 4.0, 4.0, 4.0, 250.0, 90.33203125, 136.474609375], "isController": false}, {"data": ["Modify Assignment3", 1, 1, 100.0, 32.0, 32, 32, 32.0, 32.0, 32.0, 32.0, 31.25, 98.5107421875, 377.38037109375], "isController": false}, {"data": ["student courses id-1-8", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 120.76822916666666, 183.91927083333334], "isController": false}, {"data": ["student courses id-1-7", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 120.44270833333333, 182.6171875], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 1, 100.0, 33.0, 33, 33, 33.0, 33.0, 33.0, 33.0, 30.303030303030305, 95.52556818181817, 18.643465909090907], "isController": false}, {"data": ["student courses id-1-2", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 121.09375, 183.91927083333334], "isController": false}, {"data": ["student courses id-1-1", 1, 0, 0.0, 59.0, 59, 59, 59.0, 59.0, 59.0, 59.0, 16.949152542372882, 471.3652012711865, 8.12698622881356], "isController": false}, {"data": ["student courses id-1-4", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 120.44270833333333, 182.29166666666666], "isController": false}, {"data": ["student courses id-1-3", 1, 0, 0.0, 4.0, 4, 4, 4.0, 4.0, 4.0, 4.0, 250.0, 90.8203125, 139.404296875], "isController": false}, {"data": ["CSRF-1-0", 3, 0, 0.0, 92.33333333333333, 49, 174, 54.0, 174.0, 174.0, 174.0, 2.0338983050847457, 56.46517478813559, 0.752780720338983], "isController": false}, {"data": ["student courses id-1", 1, 1, 100.0, 86.0, 86, 86, 86.0, 86.0, 86.0, 86.0, 11.627906976744185, 419.8310319767442, 153.6155523255814], "isController": false}, {"data": ["CSRF-1-1", 3, 0, 0.0, 16.333333333333332, 9, 25, 15.0, 25.0, 25.0, 25.0, 2.3023791250959325, 237.39012735034538, 1.054507626630852], "isController": false}, {"data": ["CSRF-1-2", 3, 0, 0.0, 11.0, 5, 16, 12.0, 16.0, 16.0, 16.0, 2.3006134969325154, 233.7616528470092, 1.0671791123466257], "isController": false}, {"data": ["student courses id-1-9", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 121.09375, 184.24479166666666], "isController": false}, {"data": ["CSRF-1-3", 3, 0, 0.0, 10.0, 4, 19, 7.0, 19.0, 19.0, 19.0, 2.3219814241486065, 6.820820433436532, 1.0566829527863777], "isController": false}, {"data": ["LOGIN-1", 3, 1, 33.333333333333336, 139.66666666666666, 134, 149, 136.0, 149.0, 149.0, 149.0, 2.356637863315004, 84.72926281421839, 41.35163000785546], "isController": false}, {"data": ["CSRF-1-4", 3, 0, 0.0, 13.333333333333334, 8, 19, 13.0, 19.0, 19.0, 19.0, 2.3059185242121445, 6.956037423136049, 1.047121204842429], "isController": false}, {"data": ["student courses id-1-20", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 1000.0, 362.3046875, 545.8984375], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 1, 100.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 29.0, 34.48275862068965, 108.70150862068965, 21.21497844827586], "isController": false}, {"data": ["Modify Assignment2", 1, 1, 100.0, 37.0, 37, 37, 37.0, 37.0, 37.0, 37.0, 27.027027027027028, 85.19847972972974, 329.86697635135135], "isController": false}, {"data": ["assignment id-1", 1, 1, 100.0, 48.0, 48, 48, 48.0, 48.0, 48.0, 48.0, 20.833333333333332, 6.754557291666667, 12.959798177083334], "isController": false}, {"data": ["Modify Assignment1", 1, 1, 100.0, 43.0, 43, 43, 43.0, 43.0, 43.0, 43.0, 23.25581395348837, 73.31031976744187, 283.0895712209303], "isController": false}, {"data": ["student courses id-1-19", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.15234375, 276.85546875], "isController": false}, {"data": ["student courses id-1-18", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.640625, 284.66796875], "isController": false}, {"data": ["student courses id-1-13", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 121.09375, 188.15104166666666], "isController": false}, {"data": ["student courses id-1-12", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 120.76822916666666, 184.89583333333334], "isController": false}, {"data": ["student courses id-1-11", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.640625, 284.66796875], "isController": false}, {"data": ["student courses id-1-10", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.640625, 276.3671875], "isController": false}, {"data": ["student courses id-1-17", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.640625, 280.2734375], "isController": false}, {"data": ["student courses id-1-16", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 120.76822916666666, 190.4296875], "isController": false}, {"data": ["student courses id-1-15", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.15234375, 284.66796875], "isController": false}, {"data": ["student courses id-1-14", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.15234375, 283.69140625], "isController": false}, {"data": ["Debug Sampler-1", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 1514.16015625, 0.0], "isController": false}, {"data": ["CSRF-1", 3, 0, 0.0, 152.33333333333334, 95, 259, 103.0, 259.0, 259.0, 259.0, 1.962066710268149, 3952.396250613146, 20.797140696533685], "isController": false}, {"data": ["CSRF-1-22", 3, 0, 0.0, 4.666666666666667, 3, 6, 5.0, 6.0, 6.0, 6.0, 2.3640661938534278, 140.86510047281325, 1.1335512706855793], "isController": false}, {"data": ["CSRF-1-21", 3, 0, 0.0, 3.3333333333333335, 3, 4, 3.0, 4.0, 4.0, 4.0, 2.3696682464454977, 53.01207049763033, 1.089954828199052], "isController": false}, {"data": ["Test-1", 1, 1, 100.0, 542.0, 542, 542, 542.0, 542.0, 542.0, 542.0, 1.8450184501845017, 3850.148105973247, 77.45293761531364], "isController": true}, {"data": ["CSRF-1-20", 3, 0, 0.0, 3.0, 2, 4, 3.0, 4.0, 4.0, 4.0, 2.3677979479084454, 6.606248766771903, 1.0798453531965273], "isController": false}, {"data": ["LOGIN-1-22", 3, 0, 0.0, 3.3333333333333335, 2, 4, 4.0, 4.0, 4.0, 4.0, 2.6595744680851063, 0.9635762965425533, 2.0700008311170213], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 1, 100.0, 30.0, 30, 30, 30.0, 30.0, 30.0, 30.0, 33.333333333333336, 105.078125, 14.453125], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 1, 100.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 29.0, 34.48275862068965, 108.70150862068965, 14.951508620689655], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 1, 100.0, 27.0, 27, 27, 27.0, 27.0, 27.0, 27.0, 37.03703703703704, 116.75347222222223, 16.05902777777778], "isController": false}, {"data": ["student courses id-1-23", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.15234375, 286.1328125], "isController": false}, {"data": ["student courses id-1-22", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 181.15234375, 276.3671875], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 17.0, 17, 17, 17.0, 17.0, 17.0, 17.0, 58.8235294117647, 0.9191176470588235, 0.0], "isController": false}, {"data": ["student courses id-1-21", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 180.6640625, 273.92578125], "isController": false}, {"data": ["CSRF-1-19", 3, 0, 0.0, 3.3333333333333335, 2, 5, 3.0, 5.0, 5.0, 5.0, 2.3677979479084454, 14.53051006314128, 1.0729084451460142], "isController": false}, {"data": ["CSRF-1-18", 3, 0, 0.0, 3.6666666666666665, 3, 5, 3.0, 5.0, 5.0, 5.0, 2.3677979479084454, 21.946064769139703, 1.091406866614049], "isController": false}, {"data": ["CSRF-1-15", 3, 0, 0.0, 3.6666666666666665, 3, 4, 4.0, 4.0, 4.0, 4.0, 2.34375, 42.153167724609375, 1.12152099609375], "isController": false}, {"data": ["CSRF-1-14", 3, 0, 0.0, 4.666666666666667, 3, 7, 4.0, 7.0, 7.0, 7.0, 2.34375, 58.550262451171875, 1.116943359375], "isController": false}, {"data": ["CSRF-1-17", 3, 0, 0.0, 13.666666666666666, 13, 15, 13.0, 15.0, 15.0, 15.0, 2.3255813953488373, 897.840207122093, 1.1060138081395348], "isController": false}, {"data": ["CSRF-1-16", 3, 0, 0.0, 13.666666666666666, 13, 15, 13.0, 15.0, 15.0, 15.0, 2.3255813953488373, 1250.0726744186047, 1.0855741279069766], "isController": false}, {"data": ["CSRF-1-11", 3, 0, 0.0, 8.0, 3, 13, 8.0, 13.0, 13.0, 13.0, 2.331002331002331, 64.1253277972028, 1.0767227564102564], "isController": false}, {"data": ["CSRF-1-10", 3, 0, 0.0, 12.666666666666666, 7, 16, 15.0, 16.0, 16.0, 16.0, 2.3219814241486065, 505.5774417085913, 1.1043017124613004], "isController": false}, {"data": ["CSRF-1-13", 3, 0, 0.0, 5.666666666666666, 3, 10, 4.0, 10.0, 10.0, 10.0, 2.3382696804364773, 13.445050662509743, 1.1097647116134062], "isController": false}, {"data": ["CSRF-1-12", 3, 0, 0.0, 8.333333333333334, 5, 13, 7.0, 13.0, 13.0, 13.0, 2.3455824863174355, 383.09271769937453, 1.1040730062548867], "isController": false}, {"data": ["LOGIN-1-21", 3, 0, 0.0, 4.0, 3, 5, 4.0, 5.0, 5.0, 5.0, 2.6548672566371683, 0.9618708517699116, 2.014484236725664], "isController": false}, {"data": ["LOGIN-1-20", 3, 0, 0.0, 3.6666666666666665, 2, 7, 2.0, 7.0, 7.0, 7.0, 2.6548672566371683, 0.9592782079646018, 2.0015210176991154], "isController": false}, {"data": ["LOGIN-1-12", 3, 0, 0.0, 10.0, 4, 15, 11.0, 15.0, 15.0, 15.0, 2.6362038664323375, 0.9576834358523727, 2.0312156744288226], "isController": false}, {"data": ["LOGIN-1-11", 3, 0, 0.0, 7.666666666666666, 4, 13, 6.0, 13.0, 13.0, 13.0, 2.631578947368421, 0.9534333881578948, 2.001953125], "isController": false}, {"data": ["LOGIN-1-14", 3, 0, 0.0, 5.0, 3, 7, 5.0, 7.0, 7.0, 7.0, 2.64783759929391, 0.9593239739629302, 2.0531084510150044], "isController": false}, {"data": ["LOGIN-1-13", 3, 0, 0.0, 5.0, 3, 9, 3.0, 9.0, 9.0, 9.0, 2.6595744680851063, 0.9635762965425533, 2.0570146276595747], "isController": false}, {"data": ["LOGIN-1-16", 3, 0, 0.0, 4.666666666666667, 3, 7, 4.0, 7.0, 7.0, 7.0, 2.64783759929391, 0.9619097528684907, 2.02983644086496], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 1.5, 1, 2, 1.5, 2.0, 2.0, 2.0, 3.1695721077654517, 18.4200425911252, 0.0], "isController": false}, {"data": ["LOGIN-1-15", 3, 0, 0.0, 4.666666666666667, 4, 6, 4.0, 6.0, 6.0, 6.0, 2.64783759929391, 0.9593239739629302, 2.0582800088261255], "isController": false}, {"data": ["LOGIN-1-18", 3, 0, 0.0, 4.333333333333333, 2, 8, 3.0, 8.0, 8.0, 8.0, 2.6525198938992043, 0.9610203912466844, 2.0152934350132625], "isController": false}, {"data": ["LOGIN-1-17", 3, 0, 0.0, 4.666666666666667, 3, 7, 4.0, 7.0, 7.0, 7.0, 2.64783759929391, 0.9619097528684907, 2.0531084510150044], "isController": false}, {"data": ["LOGIN-1-19", 3, 0, 0.0, 3.3333333333333335, 2, 5, 3.0, 5.0, 5.0, 5.0, 2.6548672566371683, 0.9618708517699116, 1.9963357300884959], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["403/Forbidden", 1, 6.666666666666667, 0.5376344086021505], "isController": false}, {"data": ["404/Not Found", 12, 80.0, 6.451612903225806], "isController": false}, {"data": ["Invalid Instructor username / password please enter valid details.", 1, 6.666666666666667, 0.5376344086021505], "isController": false}, {"data": ["javax.script.ScriptException: groovy.lang.MissingPropertyException: No such property: NOTFOUND for class: Script2", 1, 6.666666666666667, 0.5376344086021505], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 186, 15, "404/Not Found", 12, "403/Forbidden", 1, "Invalid Instructor username / password please enter valid details.", 1, "javax.script.ScriptException: groovy.lang.MissingPropertyException: No such property: NOTFOUND for class: Script2", 1, "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Modify Assignment3", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["student courses id-1", 1, 1, "javax.script.ScriptException: groovy.lang.MissingPropertyException: No such property: NOTFOUND for class: Script2", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["LOGIN-1", 3, 1, "Invalid Instructor username / password please enter valid details.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Modify Assignment2", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["assignment id-1", 1, 1, "403/Forbidden", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Modify Assignment1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 1, "404/Not Found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
