/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9526315789473684, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "PauseTime-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "PauseTime-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "ContinueTime-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "PauseTime-1"], "isController": false}, {"data": [0.6666666666666666, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [0.75, 500, 1500, "OPEN ASSIGNMENT 1 by user number-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "ContinueTime-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "ContinueTime-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 92, 0, 0.0, 246.52173913043478, 0, 1174, 149.5, 484.4000000000001, 764.3499999999998, 1174.0, 0.6902761104441777, 14.643901994391506, 0.8048580760429171], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 356.0, 356, 356, 356.0, 356.0, 356.0, 356.0, 2.8089887640449436, 146.40756671348316, 0.7104766502808989], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 342.0, 342, 342, 342.0, 342.0, 342.0, 342.0, 2.923976608187134, 152.48652229532163, 0.7424159356725145], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 493.0, 2.028397565922921, 105.76587854969574, 0.5130419624746451], "isController": false}, {"data": ["LOGIN-1-0", 4, 0, 0.0, 163.75, 102, 260, 146.5, 260.0, 260.0, 260.0, 0.3339455668725998, 0.1927361621305727, 0.1699078518951411], "isController": false}, {"data": ["LOGIN-1-1", 4, 0, 0.0, 46.0, 43, 49, 46.0, 49.0, 49.0, 49.0, 0.3360215053763441, 0.43840305779569894, 0.15783822664650538], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 155.0, 155, 155, 155.0, 155.0, 155.0, 155.0, 6.451612903225806, 109.24899193548387, 5.424647177419355], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1084.0, 1084, 1084, 1084.0, 1084.0, 1084.0, 1084.0, 0.9225092250922509, 48.40560856780442, 11.402538341789667], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 151.0, 151, 151, 151.0, 151.0, 151.0, 151.0, 6.622516556291391, 112.14300496688742, 5.574813741721854], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 87.0, 87, 87, 87.0, 87.0, 87.0, 87.0, 11.494252873563218, 17.387302442528735, 3.0980603448275863], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 336.33333333333337, 309, 358, 339.0, 358.0, 358.0, 358.0, 1.8404907975460123, 91.69143021472394, 0.4427482745398773], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 750.0, 750, 750, 750.0, 750.0, 750.0, 750.0, 1.3333333333333333, 0.4283854166666667, 16.015625], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 107.66666666666667, 97, 116, 108.5, 116.0, 116.0, 116.0, 1.9808517662594918, 0.6370708154506438, 0.4952129415648729], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 373.0, 373, 373, 373.0, 373.0, 373.0, 373.0, 2.680965147453083, 139.80028904155495, 1.1022327412868633], "isController": false}, {"data": ["LOGIN-1", 4, 0, 0.0, 211.75, 147, 305, 197.5, 305.0, 305.0, 305.0, 0.332695666638942, 0.6260786617316809, 0.325547908175996], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 145.0, 145, 145, 145.0, 145.0, 145.0, 145.0, 6.896551724137931, 116.7901400862069, 5.798760775862069], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 444.6666666666667, 414, 467, 447.5, 467.0, 467.0, 467.0, 1.7788319003854136, 89.19174325526237, 0.8726235917580788], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1124.0, 1124, 1124, 1124.0, 1124.0, 1124.0, 1124.0, 0.889679715302491, 46.67864490658363, 11.052359041370106], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 73.0, 73, 73, 73.0, 73.0, 73.0, 73.0, 13.698630136986301, 10.488013698630137, 5.912885273972603], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1174.0, 1174, 1174, 1174.0, 1174.0, 1174.0, 1174.0, 0.8517887563884157, 44.69395496166951, 10.636545730408859], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 94.0, 94, 94, 94.0, 94.0, 94.0, 94.0, 10.638297872340425, 3.41796875, 2.555684840425532], "isController": false}, {"data": ["PauseTime-1-0", 1, 0, 0.0, 103.0, 103, 103, 103.0, 103.0, 103.0, 103.0, 9.70873786407767, 3.166717233009709, 4.057949029126214], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 375.0, 375, 375, 375.0, 375.0, 375.0, 375.0, 2.6666666666666665, 139.04166666666666, 0.640625], "isController": false}, {"data": ["PauseTime-1-1", 1, 0, 0.0, 128.0, 128, 128, 128.0, 128.0, 128.0, 128.0, 7.8125, 147.72796630859375, 3.2501220703125], "isController": false}, {"data": ["ContinueTime-1", 1, 0, 0.0, 250.0, 250, 250, 250.0, 250.0, 250.0, 250.0, 4.0, 76.7265625, 3.34765625], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 100.0, 100, 100, 100.0, 100.0, 100.0, 100.0, 10.0, 166.181640625, 4.189453125], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 50.0, 50, 50, 50.0, 50.0, 50.0, 50.0, 20.0, 6.30859375, 8.45703125], "isController": false}, {"data": ["Debug Sampler-1", 3, 0, 0.0, 0.6666666666666667, 0, 2, 0.0, 2.0, 2.0, 2.0, 0.022654161569480313, 0.045404190359143975, 0.0], "isController": false}, {"data": ["CSRF-1", 6, 0, 0.0, 77.5, 54, 157, 62.0, 157.0, 157.0, 157.0, 0.045199442540208676, 1.2139404968172058, 0.006356171607216844], "isController": false}, {"data": ["PauseTime-1", 1, 0, 0.0, 232.0, 232, 232, 232.0, 232.0, 232.0, 232.0, 4.310344827586206, 82.91099811422413, 3.5947602370689653], "isController": false}, {"data": ["Test-1", 3, 0, 0.0, 605.3333333333334, 483, 743, 590.0, 743.0, 743.0, 743.0, 0.022504951089239632, 1.2013116518446558, 0.034446054694532796], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 94.0, 94, 94, 94.0, 94.0, 94.0, 94.0, 10.638297872340425, 3.41796875, 2.555684840425532], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 313.0, 313, 313, 313.0, 313.0, 313.0, 313.0, 3.1948881789137378, 166.58658646166134, 0.76752196485623], "isController": false}, {"data": ["OPEN ASSIGNMENT 1 by user number-1", 2, 0, 0.0, 434.5, 339, 530, 434.5, 530.0, 530.0, 530.0, 0.016390486961367624, 0.5871603839881333, 0.0041456407451115375], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 452.0, 452, 452, 452.0, 452.0, 452.0, 452.0, 2.2123893805309733, 116.06834209070796, 1.062983960176991], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 469.0, 469, 469, 469.0, 469.0, 469.0, 469.0, 2.1321961620469083, 111.85909181769723, 1.0244536247334755], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 491.0, 491, 491, 491.0, 491.0, 491.0, 491.0, 2.0366598778004072, 106.87492044297352, 0.9825292769857434], "isController": false}, {"data": ["ContinueTime-1-0", 1, 0, 0.0, 102.0, 102, 102, 102.0, 102.0, 102.0, 102.0, 9.803921568627452, 3.197763480392157, 4.126455269607844], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 42.0, 42, 42, 42.0, 42.0, 42.0, 42.0, 23.809523809523807, 7.510230654761904, 10.044642857142856], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 14.0, 14, 14, 14.0, 14.0, 14.0, 14.0, 71.42857142857143, 1.1160714285714286, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 373.0, 373, 373, 373.0, 373.0, 373.0, 373.0, 2.680965147453083, 139.8107615616622, 1.1048508713136729], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 113.0, 113, 113, 113.0, 113.0, 113.0, 113.0, 8.849557522123893, 147.06339878318585, 3.7074806415929205], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 383.0, 383, 383, 383.0, 383.0, 383.0, 383.0, 2.6109660574412534, 136.16035003263707, 1.073453818537859], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 710.0, 710, 710, 710.0, 710.0, 710.0, 710.0, 1.4084507042253522, 0.45389524647887325, 16.828510123239436], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 791.0, 791, 791, 791.0, 791.0, 791.0, 791.0, 1.2642225031605563, 0.4061808628318584, 15.266968236409607], "isController": false}, {"data": ["ContinueTime-1-1", 1, 0, 0.0, 148.0, 148, 148, 148.0, 148.0, 148.0, 148.0, 6.756756756756757, 127.40181587837839, 2.810916385135135], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 407.0, 407, 407, 407.0, 407.0, 407.0, 407.0, 2.457002457002457, 128.90145116707617, 1.1805128992628993], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 373.0, 373, 373, 373.0, 373.0, 373.0, 373.0, 2.680965147453083, 139.78981652144773, 0.6440599865951743], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 97.0, 97, 97, 97.0, 97.0, 97.0, 97.0, 10.309278350515465, 3.322326030927835, 2.4867106958762886], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 2, 0, 0.0, 125.5, 111, 140, 125.5, 140.0, 140.0, 140.0, 0.01641982200912942, 0.009476674616597156, 0.00857871560047289], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 79.0, 79, 79, 79.0, 79.0, 79.0, 79.0, 12.658227848101266, 4.066950158227848, 3.0409414556962027], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 2, 0, 0.0, 174.0, 159, 189, 174.0, 189.0, 189.0, 189.0, 0.016413219206749118, 0.03088698575332573, 0.01628499093169639], "isController": false}, {"data": ["Debug Sampler", 3, 0, 0.0, 1.3333333333333333, 1, 2, 1.0, 2.0, 2.0, 2.0, 0.022874745518456106, 0.12943740278233154, 0.0], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 9.171195652173914], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 99.0, 99, 99, 99.0, 99.0, 99.0, 99.0, 10.101010101010102, 167.8701073232323, 4.231770833333333], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 2, 0, 0.0, 47.5, 47, 48, 47.5, 48.0, 48.0, 48.0, 0.016428319136527546, 0.021433822623438282, 0.007716817875654053], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 393.0, 393, 393, 393.0, 393.0, 393.0, 393.0, 2.544529262086514, 132.7056496501272, 0.6137682888040712], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 92, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
