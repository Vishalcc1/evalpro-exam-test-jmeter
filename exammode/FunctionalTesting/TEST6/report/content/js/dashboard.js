/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 95.65217391304348, "KoPercent": 4.3478260869565215};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9285714285714286, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [0.5, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 69, 3, 4.3478260869565215, 280.9130434782608, 1, 1134, 189.0, 473.0, 915.0, 1134.0, 6.207827260458839, 153.7083017037787, 10.789895411605938], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Debug Sampler-1", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 1826.171875, 0.0], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 338.0, 338, 338, 338.0, 338.0, 338.0, 338.0, 2.9585798816568047, 154.6782544378698, 1.4272836538461537], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 377.0, 377, 377, 377.0, 377.0, 377.0, 377.0, 2.6525198938992043, 138.6900074602122, 1.2822239721485411], "isController": false}, {"data": ["CSRF-1", 3, 0, 0.0, 140.0, 60, 191, 169.0, 191.0, 191.0, 191.0, 1.563314226159458, 41.99015193460136, 0.5786094645648775], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 403.0, 403, 403, 403.0, 403.0, 403.0, 403.0, 2.4813895781637716, 129.751957971464, 1.1970766129032258], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 124.0, 116, 137, 119.0, 137.0, 137.0, 137.0, 1.6216216216216215, 0.9359163851351351, 1.1972128378378377], "isController": false}, {"data": ["Test-1", 1, 0, 0.0, 526.0, 526, 526, 526.0, 526.0, 526.0, 526.0, 1.9011406844106464, 58.97806261882129, 5.6421548241444865], "isController": true}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 51.333333333333336, 50, 52, 52.0, 52.0, 52.0, 52.0, 1.6835016835016834, 2.1964436026936025, 1.1771359427609427], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 157.0, 157, 157, 157.0, 157.0, 157.0, 157.0, 6.369426751592357, 107.89460589171975, 8.279010748407643], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 101.0, 101, 101, 101.0, 101.0, 101.0, 101.0, 9.900990099009901, 3.181079826732673, 6.08176051980198], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 368.0, 368, 368, 368.0, 368.0, 368.0, 368.0, 2.717391304347826, 142.12168817934784, 1.6691788383152175], "isController": false}, {"data": ["Modify Assignment3", 1, 1, 100.0, 1043.0, 1043, 1043, 1043.0, 1043.0, 1043.0, 1043.0, 0.9587727708533077, 50.48256981064238, 12.559548777564718], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 430.0, 430, 430, 430.0, 430.0, 430.0, 430.0, 2.3255813953488373, 122.38826308139535, 2.857013081395349], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 189.0, 189, 189, 189.0, 189.0, 189.0, 189.0, 5.291005291005291, 89.59573412698413, 6.882440476190476], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 473.0, 473, 473, 473.0, 473.0, 473.0, 473.0, 2.1141649048625792, 110.95030060782241, 2.5972846194503174], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 450.0, 450, 450, 450.0, 450.0, 450.0, 450.0, 2.2222222222222223, 116.61024305555556, 2.7734375], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 51.0, 51, 51, 51.0, 51.0, 51.0, 51.0, 19.607843137254903, 6.184895833333334, 12.771905637254903], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 10.0, 10, 10, 10.0, 10.0, 10.0, 10.0, 100.0, 1.5625, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 356.0, 356, 356, 356.0, 356.0, 356.0, 356.0, 2.8089887640449436, 146.49260445926967, 2.2329266151685396], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 93.0, 93, 93, 93.0, 93.0, 93.0, 93.0, 10.752688172043012, 16.265540994623656, 5.365843413978495], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 105.0, 105, 105, 105.0, 105.0, 105.0, 105.0, 9.523809523809526, 158.32403273809524, 6.175595238095238], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 326.8333333333333, 290, 350, 329.5, 350.0, 350.0, 350.0, 1.837109614206981, 91.78282158986528, 0.8635372014696877], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 334.0, 334, 334, 334.0, 334.0, 334.0, 334.0, 2.9940119760479043, 156.592089258982, 2.350767215568862], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 748.0, 748, 748, 748.0, 748.0, 748.0, 748.0, 1.3368983957219251, 0.658004679144385, 16.491895053475936], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 116.0, 102, 122, 119.5, 122.0, 122.0, 122.0, 1.953125, 0.6281534830729166, 0.9365081787109375], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 385.0, 385, 385, 385.0, 385.0, 385.0, 385.0, 2.5974025974025974, 135.46824269480518, 2.039366883116883], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 686.0, 686, 686, 686.0, 686.0, 686.0, 686.0, 1.4577259475218658, 0.7317100947521865, 17.93686224489796], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 177.0, 170, 190, 171.0, 190.0, 190.0, 190.0, 1.5781167806417675, 2.9697568713834825, 2.268542872172541], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 787.0, 787, 787, 787.0, 787.0, 787.0, 787.0, 1.2706480304955527, 0.6253970775095299, 15.572883576874204], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 470.0, 470, 470, 470.0, 470.0, 470.0, 470.0, 2.127659574468085, 111.96185172872342, 2.6138630319148937], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 183.0, 183, 183, 183.0, 183.0, 183.0, 183.0, 5.46448087431694, 92.53329918032787, 7.102757855191257], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 443.1666666666667, 409, 473, 442.0, 473.0, 473.0, 473.0, 1.770956316410862, 89.04733849247344, 1.6816014241440378], "isController": false}, {"data": ["Modify Assignment2", 1, 1, 100.0, 1134.0, 1134, 1134, 1134.0, 1134.0, 1134.0, 1134.0, 0.8818342151675485, 46.42633239638448, 11.570629409171078], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 71.0, 71, 71, 71.0, 71.0, 71.0, 71.0, 14.084507042253522, 10.783450704225354, 9.311729753521128], "isController": false}, {"data": ["Modify Assignment1", 1, 1, 100.0, 1122.0, 1122, 1122, 1122.0, 1122.0, 1122.0, 1122.0, 0.8912655971479501, 47.05342719474153, 11.62300857843137], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 353.0, 353, 353, 353.0, 353.0, 353.0, 353.0, 2.8328611898017, 148.17468572946177, 1.7401071175637395], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 90.0, 90, 90, 90.0, 90.0, 90.0, 90.0, 11.11111111111111, 3.5698784722222223, 6.825086805555556], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 382.0, 382, 382, 382.0, 382.0, 382.0, 382.0, 2.617801047120419, 136.5397987565445, 1.608004744764398], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 91.0, 91, 91, 91.0, 91.0, 91.0, 91.0, 10.989010989010989, 3.5413804945054945, 6.857400412087912], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 76.0, 76, 76, 76.0, 76.0, 76.0, 76.0, 13.157894736842104, 4.227487664473684, 8.082339638157896], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1.0, 0.21317416329140906, 1.3003832138669793, 0.0], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 64.0, 64, 64, 64.0, 64.0, 64.0, 64.0, 15.625, 4.9285888671875, 10.1776123046875], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 119.0, 119, 119, 119.0, 119.0, 119.0, 119.0, 8.403361344537815, 139.6484375, 5.44905462184874], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 121.0, 121, 121, 121.0, 121.0, 121.0, 121.0, 8.264462809917356, 137.34019886363637, 5.358987603305786], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 67.0, 67, 67, 67.0, 67.0, 67.0, 67.0, 14.925373134328359, 4.707905783582089, 9.736473880597014], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 359.0, 359, 359, 359.0, 359.0, 359.0, 359.0, 2.785515320334262, 145.2711525069638, 1.7382268454038998], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["In assignment1, Deadline can not be before Publish date sp please edit it.", 3, 100.0, 4.3478260869565215], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 69, 3, "In assignment1, Deadline can not be before Publish date sp please edit it.", 3, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Modify Assignment3", 1, 1, "In assignment1, Deadline can not be before Publish date sp please edit it.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Modify Assignment2", 1, 1, "In assignment1, Deadline can not be before Publish date sp please edit it.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["Modify Assignment1", 1, 1, "In assignment1, Deadline can not be before Publish date sp please edit it.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
