/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8333333333333334, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Upload ALL FAIL PROGRAM"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-5"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-6"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-7"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-8"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [1.0, 500, 1500, "GetSubmissionId"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-9"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-22"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-21"], "isController": false}, {"data": [0.0, 500, 1500, "Test-1"], "isController": true}, {"data": [0.0, 500, 1500, "RunPractice"], "isController": false}, {"data": [0.5, 500, 1500, "OPEN ASSIGNMENT FILE UPLOAD-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-20"], "isController": false}, {"data": [0.5, 500, 1500, "Upload PARTIAL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-ALL FAIL"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-2"], "isController": false}, {"data": [0.5, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-3"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-PARTIAL PASS"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-4"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-19"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-18"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-15"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-14"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-17"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-16"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-11"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-10"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-13"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1-12"], "isController": false}, {"data": [0.5, 500, 1500, "Fetch Section Id"], "isController": false}, {"data": [0.5, 500, 1500, "freezeAssignment-ALL PASS"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment publish,deadline"], "isController": false}, {"data": [1.0, 500, 1500, "TestcasesResults"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 67, 0, 0.0, 378.4477611940301, 0, 2297, 115.0, 1334.0, 1839.3999999999953, 2297.0, 1.6628198446380265, 1936.0836171915719, 16.009657966334103], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["clear exam histroy-1", 1, 0, 0.0, 292.0, 292, 292, 292.0, 292.0, 292.0, 292.0, 3.4246575342465753, 4871.796072345891, 68.12861194349315], "isController": false}, {"data": ["Upload ALL FAIL PROGRAM", 1, 0, 0.0, 542.0, 542, 542, 542.0, 542.0, 542.0, 542.0, 1.8450184501845017, 90.44013779981549, 38.867908210332104], "isController": false}, {"data": ["CSRF-1-5", 1, 0, 0.0, 22.0, 22, 22, 22.0, 22.0, 22.0, 22.0, 45.45454545454545, 3697.709517045455, 21.617542613636363], "isController": false}, {"data": ["CSRF-1-6", 1, 0, 0.0, 14.0, 14, 14, 14.0, 14.0, 14.0, 14.0, 71.42857142857143, 268.69419642857144, 32.57533482142857], "isController": false}, {"data": ["Debug Sampler-1", 3, 0, 0.0, 0.33333333333333337, 0, 1, 0.0, 1.0, 1.0, 1.0, 0.07863695937090433, 0.22451978211009174, 0.0], "isController": false}, {"data": ["CSRF-1-7", 1, 0, 0.0, 19.0, 19, 19, 19.0, 19.0, 19.0, 19.0, 52.63157894736842, 1010.022615131579, 24.157072368421055], "isController": false}, {"data": ["CSRF-1-8", 1, 0, 0.0, 57.0, 57, 57, 57.0, 57.0, 57.0, 57.0, 17.543859649122805, 1440.1212993421052, 8.052357456140351], "isController": false}, {"data": ["CSRF-1", 6, 0, 0.0, 259.5, 229, 351, 245.0, 351.0, 351.0, 351.0, 0.14891293557033655, 299.97117235586467, 1.57841894793011], "isController": false}, {"data": ["GetSubmissionId", 3, 0, 0.0, 305.3333333333333, 276, 333, 307.0, 333.0, 333.0, 333.0, 0.992063492063492, 48.23036799355159, 19.704667348710316], "isController": false}, {"data": ["CSRF-1-9", 1, 0, 0.0, 57.0, 57, 57, 57.0, 57.0, 57.0, 57.0, 17.543859649122805, 1904.2626096491229, 8.052357456140351], "isController": false}, {"data": ["CSRF-1-22", 1, 0, 0.0, 28.0, 28, 28, 28.0, 28.0, 28.0, 28.0, 35.714285714285715, 2128.0691964285716, 17.124720982142858], "isController": false}, {"data": ["CSRF-1-21", 1, 0, 0.0, 12.0, 12, 12, 12.0, 12.0, 12.0, 12.0, 83.33333333333333, 1864.2578125, 38.330078125], "isController": false}, {"data": ["Test-1", 2, 0, 0.0, 8906.5, 1866, 15947, 8906.5, 15947.0, 15947.0, 15947.0, 0.083108248493663, 1500.1808659489923, 18.989747818408475], "isController": true}, {"data": ["RunPractice", 3, 0, 0.0, 2210.0, 2165, 2297, 2168.0, 2297.0, 2297.0, 2297.0, 0.18683440244130287, 65.28427048950614, 7.617637556828798], "isController": false}, {"data": ["OPEN ASSIGNMENT FILE UPLOAD-1", 1, 0, 0.0, 559.0, 559, 559, 559.0, 559.0, 559.0, 559.0, 1.7889087656529516, 4753.254626006261, 31.602890205724506], "isController": false}, {"data": ["CSRF-1-20", 1, 0, 0.0, 10.0, 10, 10, 10.0, 10.0, 10.0, 10.0, 100.0, 279.00390625, 45.60546875], "isController": false}, {"data": ["Upload PARTIAL PASS PROGRAM", 1, 0, 0.0, 563.0, 563, 563, 563.0, 563.0, 563.0, 563.0, 1.7761989342806395, 87.05456261101244, 37.43894316163411], "isController": false}, {"data": ["freezeAssignment-ALL FAIL", 1, 0, 0.0, 1227.0, 1227, 1227, 1227.0, 1227.0, 1227.0, 1227.0, 0.8149959250203749, 161.98282778117357, 14.82194249185004], "isController": false}, {"data": ["JSR223 Sampler", 4, 0, 0.0, 120.25, 31, 285, 82.5, 285.0, 285.0, 285.0, 0.16101763143064166, 0.0, 0.0], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 98.0, 98, 98, 98.0, 98.0, 98.0, 98.0, 10.204081632653061, 37.05955038265306, 10.054607780612244], "isController": false}, {"data": ["CSRF-1-0", 1, 0, 0.0, 54.0, 54, 54, 54.0, 54.0, 54.0, 54.0, 18.51851851851852, 514.1782407407408, 6.8540219907407405], "isController": false}, {"data": ["CSRF-1-1", 1, 0, 0.0, 39.0, 39, 39, 39.0, 39.0, 39.0, 39.0, 25.64102564102564, 2643.7550080128203, 11.743790064102564], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 1, 0, 0.0, 505.0, 505, 505, 505.0, 505.0, 505.0, 505.0, 1.9801980198019802, 97.06837871287128, 41.68858292079208], "isController": false}, {"data": ["CSRF-1-2", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 2161.880817819149, 9.869514627659575], "isController": false}, {"data": ["LOGIN-1", 4, 0, 0.0, 1342.25, 1334, 1351, 1342.0, 1351.0, 1351.0, 1351.0, 0.43006128373293195, 5799.553643425438, 3.6884894769379635], "isController": false}, {"data": ["CSRF-1-3", 1, 0, 0.0, 15.0, 15, 15, 15.0, 15.0, 15.0, 15.0, 66.66666666666667, 195.83333333333334, 30.338541666666668], "isController": false}, {"data": ["freezeAssignment-PARTIAL PASS", 1, 0, 0.0, 1257.0, 1257, 1257, 1257.0, 1257.0, 1257.0, 1257.0, 0.7955449482895784, 158.07959800119332, 14.468196847653143], "isController": false}, {"data": ["CSRF-1-4", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 3.0, 333.3333333333333, 1005.5338541666666, 151.3671875], "isController": false}, {"data": ["CSRF-1-19", 1, 0, 0.0, 10.0, 10, 10, 10.0, 10.0, 10.0, 10.0, 100.0, 613.671875, 45.3125], "isController": false}, {"data": ["CSRF-1-18", 1, 0, 0.0, 11.0, 11, 11, 11.0, 11.0, 11.0, 11.0, 90.9090909090909, 842.5958806818182, 41.90340909090909], "isController": false}, {"data": ["CSRF-1-15", 1, 0, 0.0, 19.0, 19, 19, 19.0, 19.0, 19.0, 19.0, 52.63157894736842, 946.5974506578948, 25.185032894736842], "isController": false}, {"data": ["CSRF-1-14", 1, 0, 0.0, 68.0, 68, 68, 68.0, 68.0, 68.0, 68.0, 14.705882352941176, 367.3741957720588, 7.008272058823529], "isController": false}, {"data": ["CSRF-1-17", 1, 0, 0.0, 115.0, 115, 115, 115.0, 115.0, 115.0, 115.0, 8.695652173913043, 3357.141644021739, 4.135529891304348], "isController": false}, {"data": ["CSRF-1-16", 1, 0, 0.0, 113.0, 113, 113, 113.0, 113.0, 113.0, 113.0, 8.849557522123893, 4756.913716814159, 4.130945796460177], "isController": false}, {"data": ["CSRF-1-11", 1, 0, 0.0, 15.0, 15, 15, 15.0, 15.0, 15.0, 15.0, 66.66666666666667, 1833.984375, 30.794270833333336], "isController": false}, {"data": ["CSRF-1-10", 1, 0, 0.0, 92.0, 92, 92, 92.0, 92.0, 92.0, 92.0, 10.869565217391305, 2366.6886039402175, 5.169412364130435], "isController": false}, {"data": ["Delete Sections", 2, 0, 0.0, 400.5, 339, 462, 400.5, 462.0, 462.0, 462.0, 2.493765586034913, 154.1300557200748, 52.203475685785534], "isController": false}, {"data": ["CSRF-1-13", 1, 0, 0.0, 31.0, 31, 31, 31.0, 31.0, 31.0, 31.0, 32.25806451612903, 185.48387096774195, 15.309979838709678], "isController": false}, {"data": ["CSRF-1-12", 1, 0, 0.0, 60.0, 60, 60, 60.0, 60.0, 60.0, 60.0, 16.666666666666668, 2722.086588541667, 7.845052083333334], "isController": false}, {"data": ["Fetch Section Id", 1, 0, 0.0, 602.0, 602, 602, 602.0, 602.0, 602.0, 602.0, 1.6611295681063123, 5374.085080980067, 30.215038413621265], "isController": false}, {"data": ["freezeAssignment-ALL PASS", 1, 0, 0.0, 1220.0, 1220, 1220, 1220.0, 1220.0, 1220.0, 1220.0, 0.819672131147541, 425.9397412909836, 14.708472079918034], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 66.0, 66, 66, 66.0, 66.0, 66.0, 66.0, 15.151515151515152, 8.744673295454545, 10.964133522727272], "isController": false}, {"data": ["Debug Sampler", 4, 0, 0.0, 0.25, 0, 1, 0.0, 1.0, 1.0, 1.0, 0.10959804915472504, 0.38485076684385017, 0.0], "isController": false}, {"data": ["Modify Assignment publish,deadline", 1, 0, 0.0, 989.0, 989, 989, 989.0, 989.0, 989.0, 989.0, 1.0111223458038423, 64.84611981799797, 38.40290065722952], "isController": false}, {"data": ["TestcasesResults", 3, 0, 0.0, 256.3333333333333, 241, 265, 263.0, 265.0, 265.0, 265.0, 0.1588057805304113, 9.552178037822245, 5.28556505081785], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 67, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
