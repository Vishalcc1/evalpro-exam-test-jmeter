/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.5, "KoPercent": 2.5};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9146341463414634, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.0, 500, 1500, "OPEN ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "OPEN ASSIGNMENT1 FILE UPLOAD-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [0.5, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [0.5, 500, 1500, "OPEN ASSIGNMENT2 FILE UPLOAD-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [0.5, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN assignment 1 by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 80, 2, 2.5, 274.0874999999999, 0, 1204, 173.0, 525.1000000000004, 862.75, 1204.0, 0.31566652987783705, 7.69471846737981, 0.4794493690615234], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 357.0, 357, 357, 357.0, 357.0, 357.0, 357.0, 2.8011204481792715, 145.9482230392157, 1.3513217787114846], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 350.0, 350, 350, 350.0, 350.0, 350.0, 350.0, 2.857142857142857, 148.9341517857143, 1.381138392857143], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 448.0, 448, 448, 448.0, 448.0, 448.0, 448.0, 2.232142857142857, 116.58586774553571, 1.0768345424107142], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 113.66666666666667, 106, 127, 108.0, 127.0, 127.0, 127.0, 1.9342359767891684, 1.1163412717601549, 1.4280101547388782], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 47.0, 43, 50, 48.0, 50.0, 50.0, 50.0, 2.0215633423180592, 2.637508423180593, 1.4135149932614555], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 174.0, 174, 174, 174.0, 174.0, 174.0, 174.0, 5.747126436781609, 97.31950431034484, 7.470141882183909], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1175.0, 1175, 1175, 1175.0, 1175.0, 1175.0, 1175.0, 0.851063829787234, 44.612699468085104, 10.921708776595745], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 143.0, 143, 143, 143.0, 143.0, 143.0, 143.0, 6.993006993006993, 118.41673951048952, 9.096372377622378], "isController": false}, {"data": ["student courses id-1", 1, 0, 0.0, 77.0, 77, 77, 77.0, 77.0, 77.0, 77.0, 12.987012987012989, 19.64539366883117, 6.480823863636363], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 313.8333333333333, 282, 332, 317.5, 332.0, 332.0, 332.0, 1.913265306122449, 95.34968162069515, 0.8993343431122449], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 775.0, 775, 775, 775.0, 775.0, 775.0, 775.0, 1.2903225806451613, 0.4145665322580645, 15.563256048387096], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 104.5, 98, 112, 105.0, 112.0, 112.0, 112.0, 2.0456870098874873, 0.6579227753153768, 0.9808909393112855], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 362.0, 362, 362, 362.0, 362.0, 362.0, 362.0, 2.7624309392265194, 143.86492144337018, 1.769682320441989], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 162.33333333333334, 150, 176, 161.0, 176.0, 176.0, 176.0, 1.8808777429467085, 3.5395033307210033, 2.7037617554858935], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 155.0, 155, 155, 155.0, 155.0, 155.0, 155.0, 6.451612903225806, 109.24899193548387, 8.385836693548388], "isController": false}, {"data": ["OPEN ASSIGNMENT3", 1, 1, 100.0, 322.0, 322, 322, 322.0, 322.0, 322.0, 322.0, 3.105590062111801, 101.01356269409938, 1.5012373835403727], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 419.0, 387, 444, 418.0, 444.0, 444.0, 444.0, 1.854140914709518, 92.99947128012978, 1.7605888828800988], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1138.0, 1138, 1138, 1138.0, 1138.0, 1138.0, 1138.0, 0.8787346221441125, 46.046037456063274, 11.161817607644991], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 66.0, 66, 66, 66.0, 66.0, 66.0, 66.0, 15.151515151515152, 11.600378787878787, 10.017163825757576], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1204.0, 1204, 1204, 1204.0, 1204.0, 1204.0, 1204.0, 0.8305647840531561, 43.53084120639535, 10.469658430232558], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 95.0, 95, 95, 95.0, 95.0, 95.0, 95.0, 10.526315789473683, 3.3819901315789473, 4.944490131578947], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 314.0, 314, 314, 314.0, 314.0, 314.0, 314.0, 3.1847133757961785, 165.90988256369425, 1.4959444665605095], "isController": false}, {"data": ["OPEN ASSIGNMENT1 FILE UPLOAD-1", 3, 0, 0.0, 408.6666666666667, 344, 532, 350.0, 532.0, 532.0, 532.0, 0.012421691585960177, 0.4308086870582536, 0.005992495745570632], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 97.0, 97, 97, 97.0, 97.0, 97.0, 97.0, 10.309278350515465, 171.32127899484536, 6.684922680412371], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 45.0, 45, 45, 45.0, 45.0, 45.0, 45.0, 22.22222222222222, 7.009548611111112, 14.496527777777779], "isController": false}, {"data": ["Debug Sampler-1", 2, 0, 0.0, 0.5, 0, 1, 0.5, 1.0, 1.0, 1.0, 0.007916998190965915, 0.04481917823538027, 0.0], "isController": false}, {"data": ["CSRF-1", 4, 0, 0.0, 88.25, 56, 148, 74.5, 148.0, 148.0, 148.0, 0.3618599601954044, 9.71933236837344, 0.13393059073638502], "isController": false}, {"data": ["Test-1", 2, 1, 50.0, 1444.0, 452, 2436, 1444.0, 2436.0, 2436.0, 2436.0, 0.14893141708243354, 19.74766596544791, 0.572236973155112], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 102.0, 102, 102, 102.0, 102.0, 102.0, 102.0, 9.803921568627452, 3.149892769607843, 4.605162377450981], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 463.0, 463, 463, 463.0, 463.0, 463.0, 463.0, 2.1598272138228944, 112.4902976511879, 1.0145282127429804], "isController": false}, {"data": ["OPEN ASSIGNMENT2 FILE UPLOAD-1", 2, 1, 50.0, 312.0, 278, 346, 312.0, 346.0, 346.0, 346.0, 0.016531794774299672, 0.5692087443895222, 0.00797529943213285], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 462.0, 2.1645021645021645, 113.43978287337661, 2.0334483225108224], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 410.0, 410, 410, 410.0, 410.0, 410.0, 410.0, 2.4390243902439024, 127.8463224085366, 2.291349085365854], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 453.0, 453, 453, 453.0, 453.0, 453.0, 453.0, 2.207505518763797, 115.72157836644591, 2.0781594922737305], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 78.0, 78, 78, 78.0, 78.0, 78.0, 78.0, 12.82051282051282, 4.043970352564102, 8.350861378205128], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 13.0, 13, 13, 13.0, 13.0, 13.0, 13.0, 76.92307692307693, 1.2019230769230769, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 312.0, 312, 312, 312.0, 312.0, 312.0, 312.0, 3.205128205128205, 166.97966746794873, 2.056415264423077], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 96.0, 96, 96, 96.0, 96.0, 96.0, 96.0, 10.416666666666666, 173.10587565104166, 6.754557291666667], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 345.0, 345, 345, 345.0, 345.0, 345.0, 345.0, 2.898550724637681, 150.9850543478261, 1.8568840579710146], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 863.0, 863, 863, 863.0, 863.0, 863.0, 863.0, 1.1587485515643106, 0.3734248261877173, 14.126774333719583], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 858.0, 858, 858, 858.0, 858.0, 858.0, 858.0, 1.1655011655011656, 0.3744627768065268, 13.945039335664337], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 565.0, 565, 565, 565.0, 565.0, 565.0, 565.0, 1.7699115044247788, 92.75096792035399, 1.6627488938053099], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 384.0, 384, 384, 384.0, 384.0, 384.0, 384.0, 2.6041666666666665, 135.64554850260416, 1.2232462565104167], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 91.0, 91, 91, 91.0, 91.0, 91.0, 91.0, 10.989010989010989, 3.5413804945054945, 5.172561813186813], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-0", 1, 0, 0.0, 122.0, 122, 122, 122.0, 122.0, 122.0, 122.0, 8.196721311475411, 4.730724897540984, 6.163550204918033], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 78.0, 78, 78, 78.0, 78.0, 78.0, 78.0, 12.82051282051282, 4.119090544871795, 6.022135416666667], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1", 1, 0, 0.0, 172.0, 172, 172, 172.0, 172.0, 172.0, 172.0, 5.813953488372093, 10.940906613372094, 8.437045784883722], "isController": false}, {"data": ["Debug Sampler", 2, 0, 0.0, 0.5, 0, 1, 0.5, 1.0, 1.0, 1.0, 0.21222410865874364, 1.5728210552843804, 0.0], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 6.71126994680851, 13.858876329787234], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 107.0, 107, 107, 107.0, 107.0, 107.0, 107.0, 9.345794392523365, 155.3099445093458, 6.060163551401869], "isController": false}, {"data": ["LOGIN assignment 1 by user number -1-1", 1, 0, 0.0, 49.0, 49, 49, 49.0, 49.0, 49.0, 49.0, 20.408163265306122, 26.62627551020408, 14.269770408163264], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 362.0, 362, 362, 362.0, 362.0, 362.0, 362.0, 2.7624309392265194, 143.92157285911603, 1.3002848756906078], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Upload Assignment is not found", 2, 100.0, 2.5], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 80, 2, "Upload Assignment is not found", 2, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["OPEN ASSIGNMENT3", 1, 1, "Upload Assignment is not found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["OPEN ASSIGNMENT2 FILE UPLOAD-1", 2, 1, "Upload Assignment is not found", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
