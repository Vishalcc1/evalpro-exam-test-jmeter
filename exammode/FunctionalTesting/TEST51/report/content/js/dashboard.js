/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.77777777777777, "KoPercent": 2.2222222222222223};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8978494623655914, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "BeanShell Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT2"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT3"], "isController": false}, {"data": [1.0, 500, 1500, "Fetch Section Id-ASSIGNMENT1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1"], "isController": false}, {"data": [1.0, 500, 1500, "Parse submission ID-1"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "student courses id-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Sections-0"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1"], "isController": false}, {"data": [0.9166666666666666, 500, 1500, "Delete Sections"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment2"], "isController": false}, {"data": [1.0, 500, 1500, "assignment id-1"], "isController": false}, {"data": [0.0, 500, 1500, "Modify Assignment1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-0"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID2-1"], "isController": false}, {"data": [1.0, 500, 1500, "Upload ALL PASS PROGRAM-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 3 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler-1"], "isController": false}, {"data": [1.0, 500, 1500, "CSRF-1"], "isController": false}, {"data": [0.5, 500, 1500, "GetSubmissionId"], "isController": false}, {"data": [1.0, 500, 1500, "Test-1"], "isController": true}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -2"], "isController": false}, {"data": [0.5, 500, 1500, "GET SOULTION CODE ID1"], "isController": false}, {"data": [0.5, 500, 1500, "GET SOULTION CODE ID2"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "JSR223 Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment3-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 2 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "Modify Assignment1-1"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment3-0"], "isController": false}, {"data": [0.25, 500, 1500, "Upload ALL PASS PROGRAM"], "isController": false}, {"data": [0.5, 500, 1500, "Modify Assignment1-0"], "isController": false}, {"data": [1.0, 500, 1500, "PUBLIC A SOULTION CODE1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -2-1"], "isController": false}, {"data": [1.0, 500, 1500, "LOGIN by user number -2-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-0"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID1-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "OPEN ASSIGNMENT 2 by user number-1"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-0"], "isController": false}, {"data": [1.0, 500, 1500, "assignment 1 clear exam histroy-1-1"], "isController": false}, {"data": [1.0, 500, 1500, "GET SOULTION CODE ID3-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 90, 2, 2.2222222222222223, 280.5555555555556, 1, 1532, 171.0, 567.1000000000003, 919.6500000000008, 1532.0, 5.746759466189899, 132.14466369484708, 8.541819647532087], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["BeanShell Sampler", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 0.0, 0.0], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT2", 1, 0, 0.0, 344.0, 344, 344, 344.0, 344.0, 344.0, 344.0, 2.9069767441860463, 151.43248092296514, 1.4023891715116281], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT3", 1, 0, 0.0, 388.0, 388, 388, 388.0, 388.0, 388.0, 388.0, 2.577319587628866, 134.30502980025773, 1.245872261597938], "isController": false}, {"data": ["Fetch Section Id-ASSIGNMENT1", 1, 0, 0.0, 420.0, 420, 420, 420.0, 420.0, 420.0, 420.0, 2.3809523809523814, 124.03506324404762, 1.1486235119047619], "isController": false}, {"data": ["LOGIN-1-0", 3, 0, 0.0, 118.33333333333333, 104, 135, 116.0, 135.0, 135.0, 135.0, 1.9723865877712032, 1.1383598372781065, 1.4561760355029587], "isController": false}, {"data": ["LOGIN-1-1", 3, 0, 0.0, 47.666666666666664, 44, 54, 45.0, 54.0, 54.0, 54.0, 2.0435967302452314, 2.666255108991826, 1.4289211512261581], "isController": false}, {"data": ["assignment 2 clear exam histroy-1", 1, 0, 0.0, 146.0, 146, 146, 146.0, 146.0, 146.0, 146.0, 6.8493150684931505, 115.98351883561645, 8.902771832191782], "isController": false}, {"data": ["Modify Assignment3", 1, 0, 0.0, 1112.0, 1112, 1112, 1112.0, 1112.0, 1112.0, 1112.0, 0.8992805755395684, 47.135826495053955, 11.81710881294964], "isController": false}, {"data": ["assignment 3 clear exam histroy-1", 1, 0, 0.0, 155.0, 155, 155, 155.0, 155.0, 155.0, 155.0, 6.451612903225806, 109.24899193548387, 8.392137096774194], "isController": false}, {"data": ["Parse submission ID-1", 1, 0, 0.0, 317.0, 317, 317, 317.0, 317.0, 317.0, 317.0, 3.1545741324921135, 116.32800177444796, 1.5218355678233437], "isController": false}, {"data": ["Test", 2, 1, 50.0, 1547.0, 1194, 1900, 1547.0, 1900.0, 1900.0, 1900.0, 0.5509641873278236, 76.96011966253444, 2.6133135330578514], "isController": true}, {"data": ["student courses id-1", 1, 0, 0.0, 76.0, 76, 76, 76.0, 76.0, 76.0, 76.0, 13.157894736842104, 19.903885690789473, 6.566097861842105], "isController": false}, {"data": ["Delete Sections-1", 6, 0, 0.0, 332.6666666666667, 295, 411, 322.0, 411.0, 411.0, 411.0, 1.8315018315018314, 91.16699814942002, 0.8609012515262516], "isController": false}, {"data": ["Modify Assignment2-0", 1, 0, 0.0, 795.0, 795, 795, 795.0, 795.0, 795.0, 795.0, 1.2578616352201257, 0.40413718553459116, 15.437057783018867], "isController": false}, {"data": ["Delete Sections-0", 6, 0, 0.0, 105.0, 102, 111, 104.0, 111.0, 111.0, 111.0, 1.9505851755526658, 0.6273366384915474, 0.9352903527308192], "isController": false}, {"data": ["Modify Assignment2-1", 1, 0, 0.0, 375.0, 375, 375, 375.0, 375.0, 375.0, 375.0, 2.6666666666666665, 138.84895833333334, 1.7083333333333333], "isController": false}, {"data": ["LOGIN-1", 3, 0, 0.0, 168.0, 153, 180, 171.0, 180.0, 180.0, 180.0, 1.9035532994923858, 3.582175007931472, 2.7363578680203045], "isController": false}, {"data": ["LOGIN by user number -1-1", 1, 0, 0.0, 49.0, 49, 49, 49.0, 49.0, 49.0, 49.0, 20.408163265306122, 26.62627551020408, 14.269770408163264], "isController": false}, {"data": ["LOGIN by user number -1-0", 1, 0, 0.0, 107.0, 107, 107, 107.0, 107.0, 107.0, 107.0, 9.345794392523365, 5.3939106308411215, 7.027599299065421], "isController": false}, {"data": ["assignment 1 clear exam histroy-1", 1, 0, 0.0, 171.0, 171, 171, 171.0, 171.0, 171.0, 171.0, 5.847953216374268, 99.07255116959064, 7.601197002923976], "isController": false}, {"data": ["Delete Sections", 6, 0, 0.0, 438.6666666666667, 400, 523, 427.5, 523.0, 523.0, 523.0, 1.7756732761171945, 88.95909246448653, 1.6860803862089375], "isController": false}, {"data": ["Modify Assignment2", 1, 0, 0.0, 1170.0, 1170, 1170, 1170.0, 1170.0, 1170.0, 1170.0, 0.8547008547008547, 44.77747729700855, 11.036825587606838], "isController": false}, {"data": ["assignment id-1", 1, 0, 0.0, 72.0, 72, 72, 72.0, 72.0, 72.0, 72.0, 13.888888888888888, 10.633680555555557, 9.182400173611112], "isController": false}, {"data": ["Modify Assignment1", 1, 0, 0.0, 1532.0, 1532, 1532, 1532.0, 1532.0, 1532.0, 1532.0, 0.6527415143603134, 34.21793407310705, 8.576794019255875], "isController": false}, {"data": ["GET SOULTION CODE ID2-0", 1, 0, 0.0, 125.0, 125, 125, 125.0, 125.0, 125.0, 125.0, 8.0, 2.5703125, 3.7578125], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-1", 2, 0, 0.0, 353.0, 345, 361, 353.0, 361.0, 361.0, 361.0, 0.8350730688935283, 30.79128066283925, 0.3922560020876827], "isController": false}, {"data": ["GET SOULTION CODE ID2-1", 1, 0, 0.0, 382.0, 382, 382, 382.0, 382.0, 382.0, 382.0, 2.617801047120419, 136.30971858638742, 1.2296506871727748], "isController": false}, {"data": ["Upload ALL PASS PROGRAM-0", 2, 0, 0.0, 295.5, 285, 306, 295.5, 306.0, 306.0, 306.0, 0.848536274925753, 0.27262542426813746, 1.2728044123886295], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-1", 1, 0, 0.0, 109.0, 109, 109, 109.0, 109.0, 109.0, 109.0, 9.174311926605505, 152.46022075688074, 5.948967889908257], "isController": false}, {"data": ["assignment 3 clear exam histroy-1-0", 1, 0, 0.0, 46.0, 46, 46, 46.0, 46.0, 46.0, 46.0, 21.73913043478261, 6.857167119565218, 14.181385869565217], "isController": false}, {"data": ["Debug Sampler-1", 1, 0, 0.0, 2.0, 2, 2, 2.0, 2.0, 2.0, 2.0, 500.0, 1907.71484375, 0.0], "isController": false}, {"data": ["CSRF-1", 5, 0, 0.0, 72.2, 53, 139, 55.0, 139.0, 139.0, 139.0, 0.35688793718772305, 9.584323139721628, 0.13209035956459672], "isController": false}, {"data": ["GetSubmissionId", 2, 1, 50.0, 336.5, 336, 337, 336.5, 337.0, 337.0, 337.0, 0.8435259384226065, 31.105842735132857, 0.3962265394348376], "isController": false}, {"data": ["Test-1", 1, 0, 0.0, 440.0, 440, 440, 440.0, 440.0, 440.0, 440.0, 2.2727272727272725, 70.48561789772727, 6.744939630681818], "isController": true}, {"data": ["PUBLIC A SOULTION CODE1-0", 1, 0, 0.0, 98.0, 98, 98, 98.0, 98.0, 98.0, 98.0, 10.204081632653061, 3.278459821428571, 4.79312818877551], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1-1", 1, 0, 0.0, 400.0, 400, 400, 400.0, 400.0, 400.0, 400.0, 2.5, 130.23681640625, 1.17431640625], "isController": false}, {"data": ["LOGIN by user number -1", 1, 0, 0.0, 156.0, 156, 156, 156.0, 156.0, 156.0, 156.0, 6.41025641025641, 12.063050881410257, 9.302383814102564], "isController": false}, {"data": ["LOGIN by user number -2", 1, 0, 0.0, 173.0, 173, 173, 173.0, 173.0, 173.0, 173.0, 5.780346820809248, 10.877664378612717, 8.388276734104046], "isController": false}, {"data": ["GET SOULTION CODE ID1", 1, 0, 0.0, 572.0, 572, 572, 572.0, 572.0, 572.0, 572.0, 1.7482517482517483, 91.64492733828672, 1.6424005681818183], "isController": false}, {"data": ["GET SOULTION CODE ID2", 1, 0, 0.0, 508.0, 508, 508, 508.0, 508.0, 508.0, 508.0, 1.968503937007874, 103.13307394192913, 1.8493171751968505], "isController": false}, {"data": ["GET SOULTION CODE ID3", 1, 0, 0.0, 490.0, 490, 490, 490.0, 490.0, 490.0, 490.0, 2.0408163265306123, 106.94555165816327, 1.9212372448979593], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-0", 1, 0, 0.0, 47.0, 47, 47, 47.0, 47.0, 47.0, 47.0, 21.27659574468085, 6.71126994680851, 13.858876329787234], "isController": false}, {"data": ["JSR223 Sampler", 1, 0, 0.0, 13.0, 13, 13, 13.0, 13.0, 13.0, 13.0, 76.92307692307693, 1.2019230769230769, 0.0], "isController": false}, {"data": ["Modify Assignment3-1", 1, 0, 0.0, 365.0, 365, 365, 365.0, 365.0, 365.0, 365.0, 2.73972602739726, 142.7199272260274, 1.7578125], "isController": false}, {"data": ["assignment 2 clear exam histroy-1-1", 1, 0, 0.0, 98.0, 98, 98, 98.0, 98.0, 98.0, 98.0, 10.204081632653061, 169.57310267857142, 6.6167091836734695], "isController": false}, {"data": ["Modify Assignment1-1", 1, 0, 0.0, 459.0, 459, 459, 459.0, 459.0, 459.0, 459.0, 2.1786492374727673, 113.50890182461873, 1.3956971677559913], "isController": false}, {"data": ["Modify Assignment3-0", 1, 0, 0.0, 740.0, 740, 740, 740.0, 740.0, 740.0, 740.0, 1.3513513513513513, 0.43549408783783783, 16.89057221283784], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 2, 1, 50.0, 649.0, 630, 668, 649.0, 668.0, 668.0, 668.0, 0.7401924500370096, 27.530605222982977, 1.457976730199852], "isController": false}, {"data": ["Modify Assignment1-0", 1, 0, 0.0, 1072.0, 1072, 1072, 1072.0, 1072.0, 1072.0, 1072.0, 0.9328358208955224, 0.29970994636194026, 11.659536788712686], "isController": false}, {"data": ["PUBLIC A SOULTION CODE1", 1, 0, 0.0, 499.0, 499, 499, 499.0, 499.0, 499.0, 499.0, 2.004008016032064, 105.04211548096193, 1.8826715931863727], "isController": false}, {"data": ["LOGIN by user number -2-1", 1, 0, 0.0, 49.0, 49, 49, 49.0, 49.0, 49.0, 49.0, 20.408163265306122, 26.62627551020408, 14.269770408163264], "isController": false}, {"data": ["LOGIN by user number -2-0", 1, 0, 0.0, 123.0, 123, 123, 123.0, 123.0, 123.0, 123.0, 8.130081300813009, 4.692263719512195, 6.113440040650406], "isController": false}, {"data": ["GET SOULTION CODE ID1-1", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 493.0, 2.028397565922921, 105.678720841785, 0.9527922160243408], "isController": false}, {"data": ["GET SOULTION CODE ID3-0", 1, 0, 0.0, 129.0, 129, 129, 129.0, 129.0, 129.0, 129.0, 7.751937984496124, 2.4981831395348837, 3.648861434108527], "isController": false}, {"data": ["GET SOULTION CODE ID1-0", 1, 0, 0.0, 79.0, 79, 79, 79.0, 79.0, 79.0, 79.0, 12.658227848101266, 4.066950158227848, 5.9459058544303796], "isController": false}, {"data": ["Debug Sampler", 4, 0, 0.0, 1.5, 1, 2, 1.5, 2.0, 2.0, 2.0, 0.2820476660555634, 1.4963124471160625, 0.0], "isController": false}, {"data": ["OPEN ASSIGNMENT 2 by user number-1", 1, 0, 0.0, 366.0, 366, 366, 366.0, 366.0, 366.0, 366.0, 2.73224043715847, 100.74336150956285, 1.3180925546448088], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-0", 1, 0, 0.0, 66.0, 66, 66, 66.0, 66.0, 66.0, 66.0, 15.151515151515152, 4.7792376893939394, 9.86919981060606], "isController": false}, {"data": ["assignment 1 clear exam histroy-1-1", 1, 0, 0.0, 105.0, 105, 105, 105.0, 105.0, 105.0, 105.0, 9.523809523809526, 158.34263392857144, 6.175595238095238], "isController": false}, {"data": ["GET SOULTION CODE ID3-1", 1, 0, 0.0, 360.0, 360, 360, 360.0, 360.0, 360.0, 360.0, 2.7777777777777777, 144.66959635416669, 1.3075086805555556], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["unauthentic student (e.g. wrong IP) is able to upload file.", 2, 100.0, 2.2222222222222223], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 90, 2, "unauthentic student (e.g. wrong IP) is able to upload file.", 2, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["GetSubmissionId", 2, 1, "unauthentic student (e.g. wrong IP) is able to upload file.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Upload ALL PASS PROGRAM", 2, 1, "unauthentic student (e.g. wrong IP) is able to upload file.", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
