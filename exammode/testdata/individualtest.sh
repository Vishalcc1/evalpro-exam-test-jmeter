#!/bin/bash

sudo rm  ../FunctionalTesting/TEST$1/logfile.log
sudo rm  ../FunctionalTesting/TEST$1/jtlfile.jtl
sudo rm -rf ../FunctionalTesting/TEST$1/report/*
sudo mkdir -p  ../FunctionalTesting/TEST$1/report


	../../bin/jmeter.sh  -n -f -t ../FunctionalTesting/TEST$1/TestPlan.jmx -l ../FunctionalTesting/TEST$1/jtlfile.jtl -j ../FunctionalTesting/TEST$1/logfile.log -e -o ../FunctionalTesting/TEST$1/report
