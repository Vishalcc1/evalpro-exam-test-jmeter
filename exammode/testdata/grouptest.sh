#!/bin/bash

outputcsv=FailTest.csv
sudo rm $outputcsv


perform_cleanup_and_test() {
    local index=$1
    
    sudo rm ../FunctionalTesting/TEST$index/logfile.log
    sudo rm ../FunctionalTesting/TEST$index/jtlfile.jtl
    sudo rm -rf ../FunctionalTesting/TEST$index/report
    sudo mkdir -p ../FunctionalTesting/TEST$index/report

    ../../bin/jmeter.sh -n -f -t ../FunctionalTesting/TEST$index/TestPlan.jmx -l ../FunctionalTesting/TEST$index/jtlfile.jtl -j ../FunctionalTesting/TEST$index/logfile.log -e -o ../FunctionalTesting/TEST$index/report
    python3 failTest.py "../FunctionalTesting/TEST$index/jtlfile.jtl" $outputcsv
}


while [[ $# -gt 0 ]]; do
    key="$1"
    
    case $key in
        -r|--range)
            start=$2
            end=$3
            shift 3
            for ((i=$start; i<=$end; i++)); do
                perform_cleanup_and_test $i
            done
            ;;
        -g|--group)
            shift
            numbers=($@)
            for i in "${numbers[@]}"; do
                perform_cleanup_and_test $i
            done
            break
            ;;
        *)
            echo "Invalid option: $key"
            exit 1
            ;;
    esac
done

