#!/bin/bash

count=2 # Number of packets to send
packet_loss_threshold=50 # Percentage of packet loss threshold
output_file=ping_results.xlsx
ipsubset=10.129.131.
start=50
end=100
# Check if the output file exists and create it if it doesn't
if [[ ! -f $output_file ]]; then
    touch $output_file
fi

# Define an array to store the IP addresses with packet loss
failed_ips=()

# Define a function to ping an IP address and add it to the array if there is packet loss
ping_ip() {
    local ip=$1
    sudo ip addr del $ip/32 dev enp2s0
    local ping_result=$(ping -c $count $ip)
    local packet_loss=$(echo "$ping_result" | grep -oP '\d+(?=% packet loss)')

    if ((packet_loss >= packet_loss_threshold)); then
    	sudo ip addr add $ip dev enp2s0
        echo "Error: Packet loss exceeded threshold ($packet_loss_threshold%) for $ip"
        # Add the IP address to the array
        echo -e $ip >> $output_file
    fi
}
echo -e "IP Addr "> $output_file

for  ((i=$start; i<=$end; i++));
do
    
    ip=$ipsubset$i
    ping_ip $ip 
done
