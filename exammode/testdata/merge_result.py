import pandas as pd

# Path to the first result file
file1 = '../FunctionalTesting/TEST61/first.csv'

# Path to the second result file
file2 = '../FunctionalTesting/TEST62/second.csv'

# Path to the output merged result file
output_file = 'results.csv'

# Read the first result file
df1 = pd.read_csv(file1)

# Read the second result file
df2 = pd.read_csv(file2)

# Concatenate the dataframes
merged_df = pd.concat([df1, df2])

# Write the merged dataframe to the output file
merged_df.to_csv(output_file, index=False)
