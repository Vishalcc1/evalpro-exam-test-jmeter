#include <iostream>
#include <sys/types.h>
#include <unistd.h>
using namespace std;
int main() {
    // Perform operations in the sandbox
    // ...
    // Verify if the program is running inside the sandbox
    if (chdir("/") != 0) {
        cout << "Not running inside the sandbox." << std::endl;
        return 1;
    }
	int a,b;
	cin>>a>>b;
	cout<<a+b;


    // Output success message
    cout << " Running inside the sandbox." << std::endl;

    return 0;
}

