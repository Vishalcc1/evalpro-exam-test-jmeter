#include <iostream>

using namespace std;

int main() {
    // Allocate a large amount of memory (1 GB)
    const unsigned long long size = 1024 * 1024 *1024;  // 1 GB
    int* hugeArray = new int[size];

    if (hugeArray == nullptr) {
        cerr << "Failed to allocate memory." << endl;
        return 1;
    }

    // Use the allocated memory
    for (unsigned long long i = 0; i < size; ++i) {
        hugeArray[i] = i;
    }
	int a,b;
	cin>>a>>b;
	cout<<a+b;
    // Free the allocated memory
    delete[] hugeArray;

    return 0;
}

