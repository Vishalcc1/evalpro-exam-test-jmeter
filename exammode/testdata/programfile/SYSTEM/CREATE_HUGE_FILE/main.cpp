#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

int main() {
    const std::string filename = "large_file.txt";
    const int fileSizeInKB = 1025;  // Size of the file in kilobytes (greater than 1024KB)

    // Open the file in binary mode
    std::ofstream file(filename, std::ios::binary);
    if (!file) {
        std::cout << "Failed to open the file for writing." << std::endl;
        return 1;
    }

    // Calculate the number of bytes to write
    const std::streamsize fileSizeInBytes = fileSizeInKB * 2024;

    // Prepare a buffer with random data
    const int bufferSize = 1024;  // Buffer size in bytes
    char buffer[bufferSize];
    std::srand(std::time(nullptr));  // Seed the random number generator
    for (int i = 0; i < bufferSize; ++i) {
        buffer[i] = std::rand() % 256;  // Fill buffer with random data
    }

    // Write data to the file until the desired file size is reached
    std::streamsize bytesWritten = 0;
    while (bytesWritten < fileSizeInBytes) {
        const std::streamsize remainingBytes = fileSizeInBytes - bytesWritten;
        const std::streamsize writeSize = std::min(static_cast<std::streamsize>(bufferSize), remainingBytes);

        file.write(buffer, writeSize);
        bytesWritten += writeSize;
    }

    // Close the file
    file.close();

    // Check the file size
std::ifstream fileRead(filename, std::ios::binary | std::ios::ate);
if (fileRead) {
    const std::streamsize fileSize = fileRead.tellg();
    if(fileSize >1024*2024)
    {
    std::cout << "File size: " << fileSize << " bytes" << std::endl;
    }
    else{
    
	int a,b;
	std::cin>>a>>b;
	std::cout<<a+b;
    }
    fileRead.close();
} else {
    std::cout << "Failed to open the file for reading." << std::endl;
}


    return 0;
}
