#include<iostream>
#include<unistd.h>
#include<sys/types.h>
using namespace std;
int main()
{
  //create a variable to hold the process ID
  pid_t pid;

  //create a child process
  //thus making 2 processes run at the same time
  //Store the Process ID in 'pid'
  pid = fork();

  //Check if 'pid' is 0
  //If yes, then it's parent process
  //Else, it is the child process
  if(pid==0)
    cout<<"Output from the child process."<<endl;
  else{
    cout<<"Output from the parent process. "<<endl;
	
	int a,b;
	cin>>a>>b;
	cout<<a+b;
}
  
  return 0;
}
