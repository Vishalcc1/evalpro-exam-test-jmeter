#include <iostream>
#include <stdexcept>
using namespace std;
int main() {
    // Allocate memory
    int* ptr = new (std::nothrow) int[100];
    if (ptr == nullptr) {
        // Memory allocation failed
        throw std::runtime_error("Failed to allocate memory.");
    }
	int a,b;
	cin>>a>>b;
	cout<<a+b;
    // Use the allocated memory
    // ...

    // Deallocate memory
    delete[] ptr;

    return 0;
}
