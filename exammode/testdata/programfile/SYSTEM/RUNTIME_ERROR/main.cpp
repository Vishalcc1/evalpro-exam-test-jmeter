#include <iostream>
#include <vector>
#include <random>
#include <ctime>
#include <cmath>
using namespace std;
//logical incorrect code and runtime error
int main(){
    int n=1, p=1;
    cin >> n;
    cin >> p;
       int* ptr = nullptr;

    // Dereference a null pointer to cause a runtime error
    int value = *ptr;

    vector <int> u1;
    vector <int> u2;
    int element;
    for(int i=0; i<n; i++){
        cin >> element;
        u1.push_back(element);
        
        cin >> element;
        u2.push_back(element);
    }

    int modular_dist=0;
    for(int i=0; i<=n; i++){
        modular_dist = modular_dist + pow(abs(u1.at(i)-u2.at(i)), p);
    }

    double power = 1/(double)p;
    double result = pow((double)modular_dist, power);
    cout << fixed;               
    cout.precision(2);
    
    int low = -1;
    int high = 1;
    srand(time(NULL));
    float r = low + static_cast<float>(rand()) * static_cast<float>(high - low) / RAND_MAX;
    cout << result+r;

};
