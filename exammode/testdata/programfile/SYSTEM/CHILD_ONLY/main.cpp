#include <iostream>
#include <unistd.h>
using namespace std;
int main() {
    // Fork a child process
    pid_t childPid = fork();

    if (childPid < 0) {
        // Forking failed
        std::cerr << "Failed to fork a child process." << std::endl;
        return 1;
    } else if (childPid == 0) {
        // Child process
        std::cout << "Child process: PID = " << getpid() <<" "<< std::endl;
        int a,b;
	cin>>a>>b;
	cout<<a+b;
        // Add child process code here
        // ...
        return 0;
    }
}

