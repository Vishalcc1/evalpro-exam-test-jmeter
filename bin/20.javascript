/**
 * React Persistent State
 * Base code taken from: https://github.com/mgechev/react-pstate
 */

(function() {
    'use strict';

    /**
     * API for dictionary based persistent storage
     *
     * @see ReactPersistentState
     */
    var DictPersistentStorage = {

        /**
         * Dictionary to store all persistent states.
         *
         * Mapping is key: dict (key is PID from global PID map)
         * @type {{}}
         * @private
         */
        _globalPersistentDict: {},

        /**
         * Set key value to dict storage
         *
         * @param {string} id
         * @param {object} data
         * @returns {object} data
         */
        set: function(id, data) {
            this._globalPersistentDict[id] = data;
            return data;
        },

        /**
         * Get value by key from dict storage
         *
         * @param {string} id
         * @returns {*|{}} Saved object or empty object is nothing is saved
         */
        get: function(id) {
            return this._globalPersistentDict[id] || {};
        },

        /**
         * Remove key from persistent storage
         *
         * @param {string} id
         * @returns {string} Return deleted key
         */
        remove: function(id) {
            delete this._globalPersistentDict[id];
            return id;
        }

    };

    /**
     * Mixin for persistent storage.
     * @public
     */
    var ReactPersistentStateMixin = {

        /**
         * Persistent Storage used with this object.
         *
         * A new object can inherit this object and override persistentStorage to plug a custom storage with
         * suitable APIs. Default is DictPersistentStorage
         *
         * @see DictPersistentStorage
         */
        persistentStorage: DictPersistentStorage,

        /**
         * Initialize component with Persistent storage as DictPersistentStorage.
         * Restore state from persistent storage
         */
        componentWillMount: function() {
            if (typeof this.getPId == 'function') {
                this.setPId(this.getPId());
            }

            this.setPStorage(this.persistentStorage);
            this.restorePState();
        },

        /**
         * Store current state to persistent storage during un-mounting of component
         */
        componentWillUnmount: function() {
            this._pstorage.set(this._pid, this.state);
        },

        /**
         * Sets unique identifier for the current component.
         *
         * @param {string} id
         */
        setPId: function(id) {
            this._pid = id;
        },

        /**
         * Sets storage.
         *
         * @param {object} storage This object must implement the
         *  storage API (set, get and remove method).
         */
        setPStorage: function(storage) {
            this._pstorage = storage;
        },

        /**
         * Sets the component's state and save it using the provided storage.
         *
         * setPState can be used instead of setState if state needs to be persisted on every update
         *
         * @param {object} state
         * @param {function=} cb
         */
        setPState: function(state, cb) {
            var self = this;
            if (this._pid === undefined) {
                throw new Error('You must explicitly set the ' +
                    'component\'s persistent id');
            }
            if (!this._pstorage || typeof this._pstorage.set !== 'function') {
                throw new Error('Set storage befor using setPState');
            }
            // Save the current state in case the persistence storage fails
            var old = this.state;
            this.setState(state, function() {
                // No object assign :(
                var prop;
                var stateToStore = {};
                for (prop in old) {
                    stateToStore[prop] = old[prop];
                }
                for (prop in state) {
                    stateToStore[prop] = state[prop];
                }
                // Try to save the storage
                self._pstorage.set(self._pid, stateToStore);
                if (typeof cb == 'function') {
                    cb.apply(arguments, this);
                }
            });
        },

        /**
         * Removes the state from the persistent storage.
         *
         * @return {*} Removed data
         */
        removePState: function() {
            if (this._pid === undefined) {
                throw new Error('You must explicitly set the ' +
                    'component\'s persistent id');
            }
            return this._pstorage.remove(this._pid);
        },

        /**
         * Restores the component state based on its content in
         * the provided persistent storage.
         *
         * @param {function=} cb The callback will be invoked once the storage is set
         */
        restorePState: function(cb) {

            if (this._pid === undefined) {
                throw new Error('You must explicitly set the ' +
                    'component\'s persistent id');
            }

            var data = this._pstorage.get(this._pid);
            this.setState(data, cb);
            return data;
        }
    };

    /**
     * Dictionary for storing global Persistent Ids. Keeping all persistent Ids here will avoid name conflicts in
     * code if introduced by mistake
     *
     * @type {{}}
     */
    var GlobalPersistentIdMap = {

        /**
         * Key for course information
         *
         * 0 -> course id
         * @const
         */
        COURSE_INFORMATION: 'course_information_{0}',

        /**
         * Key for course groups
         *
         * 0 -> course id
         * @const
         */
        COURSE_GROUPS: 'course_groups_{0}',

        /**
         * Key for storing concepts of a group
         *
         * 0 -> group id
         * @const
         */
        GROUP_CONCEPTS: 'group_concepts_{0}',

        /**
         * Key for storing forum detail and settings of a forum
         *
         * 0 -> forum id
         * @const
         */
        COURSE_FORUM: 'course_forum_{0}',

        /**
         * Key to store threads of a forum
         *
         * 0 -> thread id
         * @const
         */
        FORUM_THREAD: 'forum_thread_{0}',

        /**
         * Key to store comments on a thread
         *
         * 0 -> thread_id
         * @const
         */
        THREAD_COMMENTS: 'thread_comments_{0}',

        /**
         * Key to store replies on a comment
         *
         * 0 -> comment id
         * @const
         */
        COMMENT_REPLIES: 'comment_replies_{0}',

        /**
         * Key to store student progress of a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_STUDENT_PROGRESS: 'course_student_progress_{0}',

        /**
         * Key to store scorecard of a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_SCORECARD: 'course_scorecard_{0}',

        /**
         * Key to store public marks setting of a student on scorecard
         *
         * 0 -> course id
         * @const
         */
        COURSE_SCORECARD_STUDENT_MARKS_SETTINGS: 'course_scorecard_student_marks_settings_{0}',

        /**
         * Key to store schedule of a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_SCHEDULE: 'course_schedule_{0}',

        /**
         * Key to store concept titles of a group for schedule page
         *
         * 0 -> concept id list
         * @const
         */
        SCHEDULE_CONCEPT_TITLES: 'schedule_concept_titles_{0}',

        /**
         * Key to store detail of a course page by page id
         *
         * 0 -> page id
         * @const
         */
        COURSE_PAGE: 'course_page_{0}',

        /**
         * Key to store details of course grading app for a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_REPORT_GRADING_APP: 'course_rga_{0}',

        /**
         * Key to store list of chatrooms of a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_CHATROOMS: 'course_chatrooms_{0}',

        /**
         * Key to store marks of a student for a course
         *
         * 0 -> course id
         * @const
         */
        COURSE_STUDENT_MARKS: 'course_student_marks_{0}'
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports.ReactPersistentStateMixin = ReactPersistentStateMixin;
        module.exports.GlobalPersistentIdMap = GlobalPersistentIdMap;
        module.exports.DictPersistentStorage = DictPersistentStorage;
    }

    if (typeof window !== 'undefined') {
        window.ReactPersistentStateMixin = ReactPersistentStateMixin;
        window.GlobalPersistentIdMap = GlobalPersistentIdMap;
        window.DictPersistentStorage = DictPersistentStorage;
    }

}());
