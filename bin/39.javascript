/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/static/frontend/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.tsx","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Reducers/Common/publicCoursesReducer.ts":
/*!*****************************************************!*\
  !*** ./src/Reducers/Common/publicCoursesReducer.ts ***!
  \*****************************************************/
/*! exports provided: publicCoursesReducer, paginationInfoReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publicCoursesReducer", function() { return publicCoursesReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "paginationInfoReducer", function() { return paginationInfoReducer; });
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");


function publicCoursesReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].publicCoursesMap; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["publicCoursesListActionTypes"].LOAD_PUBLIC_COURSES_LIST_SUCCESS:
            return action.publicCoursesMap;
        default:
            return state;
    }
}
;
function paginationInfoReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].paginationInfo; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["publicCoursesListActionTypes"].LOAD_PAGINATION_INFO_SUCCESS:
            return action.paginationInfo;
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Reducers/Instructor/instructorConceptDataReducer.ts":
/*!*****************************************************************!*\
  !*** ./src/Reducers/Instructor/instructorConceptDataReducer.ts ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return instructorConceptDataReducer; });
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};


function instructorConceptDataReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].instructorConceptDataMap; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorConceptDataActionTypes"].LOAD_INSTRUCTOR_CONCEPT_DATA_SUCCESS:
            {
                var conceptId = action.conceptId, data = action.data;
                var instructorConceptDataMap = new Map(state);
                instructorConceptDataMap.set(conceptId, data);
                return instructorConceptDataMap;
            }
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorConceptDataActionTypes"].DEL_INSTRUCTOR_CONCEPT_ELEM_SUCCESS:
            {
                var conceptId = action.conceptId, elemType_1 = action.elemType, id_1 = action.id;
                var instructorConceptDataMap = new Map(state);
                var data = __assign({}, instructorConceptDataMap.get(conceptId));
                var newPlaylist = data.playlist.filter(function (elem) {
                    return (elem.type !== elemType_1 || elem.content.id !== id_1);
                });
                var newData = __assign(__assign({}, data), { playlist: newPlaylist });
                instructorConceptDataMap.set(conceptId, newData);
                return instructorConceptDataMap;
            }
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorConceptDataActionTypes"].ADD_INSTRUCTOR_CONCEPT_ELEM_SUCCESS:
            {
                var conceptId = action.conceptId, elemType = action.elemType, content = action.content;
                var instructorConceptDataMap = new Map(state);
                var data = __assign({}, instructorConceptDataMap.get(conceptId));
                var newPlaylist = data.playlist.map(function (e) { return (__assign({}, e)); });
                newPlaylist.push({ type: elemType, content: content });
                var newData = __assign(__assign({}, data), { playlist: newPlaylist });
                instructorConceptDataMap.set(conceptId, newData);
                return instructorConceptDataMap;
            }
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorConceptDataActionTypes"].REO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS:
            {
                var conceptId = action.conceptId, playlist = action.playlist;
                var instructorConceptDataMap = new Map(state);
                var data = __assign({}, instructorConceptDataMap.get(conceptId));
                var newData = __assign(__assign({}, data), { playlist: playlist });
                instructorConceptDataMap.set(conceptId, newData);
                return instructorConceptDataMap;
            }
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorConceptDataActionTypes"].INFO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS:
            {
                console.log('info');
                var conceptId = action.conceptId, title = action.title, description = action.description;
                var instructorConceptDataMap = new Map(state);
                var data = __assign({}, instructorConceptDataMap.get(conceptId));
                var newData = __assign(__assign({}, data), { title: title,
                    description: description });
                instructorConceptDataMap.set(conceptId, newData);
                return instructorConceptDataMap;
            }
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Reducers/Instructor/instructorCourseGCReducer.ts":
/*!**************************************************************!*\
  !*** ./src/Reducers/Instructor/instructorCourseGCReducer.ts ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return instructorCourseGCReducer; });
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};


function instructorCourseGCReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_0__["default"].instructorCourseGC; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].LOAD_INSTRUCTOR_COURSE_GROUPS_SUCCESS:
            {
                var courseid = action.courseid, groups = action.groups;
                var instructorCourseGroupsMap = new Map(state.instructorCourseGroupsMap);
                instructorCourseGroupsMap.set(courseid, groups);
                return __assign(__assign({}, state), { instructorCourseGroupsMap: instructorCourseGroupsMap });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].LOAD_INSTRUCTOR_COURSE_GROUPS_FAILURE:
            return state;
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].ADD_INSTRUCTOR_COURSE_GROUPS_SUCCESS:
            {
                var courseid = action.courseid, group = action.group;
                var instructorCourseGroupsMap = new Map(state.instructorCourseGroupsMap);
                instructorCourseGroupsMap.get(courseid).push(group);
                return __assign(__assign({}, state), { instructorCourseGroupsMap: instructorCourseGroupsMap });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].DELETE_INSTRUCTOR_COURSE_GROUPS_SUCCESS:
            {
                var courseid = action.courseid, groupid_1 = action.groupid;
                var instructorCourseGroupsMap = new Map(state.instructorCourseGroupsMap);
                var instructorGroupConceptsMap_1 = new Map(state.instructorGroupConceptsMap);
                var arr = instructorCourseGroupsMap.get(courseid).filter(function (group) {
                    return group.id !== groupid_1;
                });
                instructorGroupConceptsMap_1.delete(groupid_1);
                instructorCourseGroupsMap.set(courseid, arr);
                return {
                    instructorGroupConceptsMap: instructorGroupConceptsMap_1,
                    instructorCourseGroupsMap: instructorCourseGroupsMap
                };
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].EDIT_INSTRUCTOR_COURSE_GROUPS_SUCCESS:
            {
                var courseid = action.courseid, groupid_2 = action.groupid, data = action.data;
                var instructorCourseGroupsMap = new Map(state.instructorCourseGroupsMap);
                var i = instructorCourseGroupsMap.get(courseid).findIndex(function (group) {
                    return group.id === groupid_2;
                });
                instructorCourseGroupsMap.get(courseid)[i].title = data.title;
                instructorCourseGroupsMap.get(courseid)[i].description = data.description;
                return __assign(__assign({}, state), { instructorCourseGroupsMap: instructorCourseGroupsMap });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].REORDER_INSTRUCTOR_COURSE_GROUPS_SUCCESS:
            {
                var courseid = action.courseid, groups = action.groups;
                var instructorCourseGroupsMap = new Map(state.instructorCourseGroupsMap);
                instructorCourseGroupsMap.set(courseid, __spreadArrays(groups));
                return __assign(__assign({}, state), { instructorCourseGroupsMap: instructorCourseGroupsMap });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].LOAD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS:
            var groupid = action.groupid, concepts = action.concepts;
            var instructorGroupConceptsMap = new Map(state.instructorGroupConceptsMap);
            instructorGroupConceptsMap.set(groupid, concepts);
            return __assign(__assign({}, state), { instructorGroupConceptsMap: instructorGroupConceptsMap });
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].LOAD_INSTRUCTOR_GROUP_CONCEPTS_FAILURE:
            return state;
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].DELETE_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS:
            {
                var conceptid_1 = action.conceptid, groupid_3 = action.groupid;
                var instructorGroupConceptsMap_2 = new Map(state.instructorGroupConceptsMap);
                var arr = instructorGroupConceptsMap_2.get(groupid_3).filter(function (concept) {
                    return concept.id !== conceptid_1;
                });
                instructorGroupConceptsMap_2.set(groupid_3, arr);
                return __assign(__assign({}, state), { instructorGroupConceptsMap: instructorGroupConceptsMap_2 });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].ADD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS:
            {
                var data = action.data, groupid_4 = action.groupid;
                var instructorGroupConceptsMap_3 = new Map(state.instructorGroupConceptsMap);
                var arr = __spreadArrays(instructorGroupConceptsMap_3.get(groupid_4));
                arr.push(data);
                instructorGroupConceptsMap_3.set(groupid_4, arr);
                return __assign(__assign({}, state), { instructorGroupConceptsMap: instructorGroupConceptsMap_3 });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].PUBLISH_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS:
            {
                var conceptid_2 = action.conceptid, groupid_5 = action.groupid;
                var instructorGroupConceptsMap_4 = new Map(state.instructorGroupConceptsMap);
                var newConcepts = instructorGroupConceptsMap_4.get(groupid_5).map(function (concept) {
                    if (concept.id == conceptid_2) {
                        return __assign(__assign({}, concept), { is_published: !concept.is_published });
                    }
                    else {
                        return concept;
                    }
                });
                instructorGroupConceptsMap_4.set(groupid_5, newConcepts);
                return __assign(__assign({}, state), { instructorGroupConceptsMap: instructorGroupConceptsMap_4 });
            }
        case Screens__WEBPACK_IMPORTED_MODULE_1__["instructorCourseGCActionTypes"].REORDER_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS:
            {
                var groupid_6 = action.groupid, concepts_1 = action.concepts;
                var instructorGroupConceptsMap_5 = new Map(state.instructorGroupConceptsMap);
                instructorGroupConceptsMap_5.set(groupid_6, __spreadArrays(concepts_1));
                return __assign(__assign({}, state), { instructorGroupConceptsMap: instructorGroupConceptsMap_5 });
            }
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Reducers/Instructor/instructorCoursesListReducer.ts":
/*!*****************************************************************!*\
  !*** ./src/Reducers/Instructor/instructorCoursesListReducer.ts ***!
  \*****************************************************************/
/*! exports provided: instructorCoursesMapReducer, instructorInstituteListReducer, instructorDepartmentListReducer, instructorImportCoursesListReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instructorCoursesMapReducer", function() { return instructorCoursesMapReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instructorInstituteListReducer", function() { return instructorInstituteListReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instructorDepartmentListReducer", function() { return instructorDepartmentListReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "instructorImportCoursesListReducer", function() { return instructorImportCoursesListReducer; });
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};


function instructorCoursesMapReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].instructorCoursesMap; }
    var newState;
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].LOAD_INSTRUCTOR_COURSES_LIST_SUCCESS:
            return action.instructorCoursesMap;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].PUBLISH_COURSE_SUCCESS:
            newState = new Map(state);
            var course = newState.get(action.courseId);
            course.is_published = !course.is_published;
            newState.set(course.id, course);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].DELETE_COURSE_SUCCESS:
            newState = new Map(state);
            newState.delete(action.courseId);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].ADD_COURSE_SUCCESS:
            newState = new Map(state);
            newState.set(action.course.id, action.course);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].IMPORT_COURSE_SUCCESS:
            newState = new Map(state);
            newState.set(action.course.id, action.course);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].DOWNLOAD_COURSE_SUCCESS:
            newState = new Map(state);
            var c = newState.get(action.courseId);
            c.archive.progress = 'Submitted';
            newState.set(c.id, c);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].DELETE_ARCHIVE_SUCCESS:
            newState = new Map(state);
            var arch = newState.get(action.courseId);
            arch.archive.progress = 'not_present';
            arch.archive.active = null;
            arch.archive.link = null;
            newState.set(arch.id, arch);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].LOAD_ARCHIVE_STATUS_SUCCESS:
            newState = new Map(state);
            var crse = newState.get(action.resp.crseid);
            crse.archive = action.resp.arch;
            newState.set(crse.id, crse);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].DISABLE_ARCHIVE_SUCCESS:
            newState = new Map(state);
            var disable_crse = newState.get(action.courseId);
            if (disable_crse.archive.active != null) {
                disable_crse.archive.active = !disable_crse.archive.active;
                newState.set(disable_crse.id, disable_crse);
            }
            return newState;
        default:
            return state;
    }
}
;
function instructorInstituteListReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].instituteList; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].LOAD_INSTITUTE_LIST_SUCCESS:
            return action.instituteList;
        default:
            return state;
    }
}
function instructorDepartmentListReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].instituteList; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].LOAD_DEPARTMENT_LIST_SUCCESS:
            return action.departmentList;
        default:
            return state;
    }
}
function instructorImportCoursesListReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].instructorImportCoursesList; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].LOAD_IMPORT_COURSES_LIST_SUCCESS:
            return action.data;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].ADD_COURSE_SUCCESS:
            if (state.length !== 0) {
                return __spreadArrays(state, [
                    action.course
                ]);
            }
            return state;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["instructorCoursesListActionTypes"].IMPORT_COURSE_SUCCESS:
            return __spreadArrays(state, [
                action.course
            ]);
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/Reducers/Student/studentConceptDataReducer.ts":
/*!***********************************************************!*\
  !*** ./src/Reducers/Student/studentConceptDataReducer.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return studentConceptDataReducer; });
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");


function studentConceptDataReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].studentConceptDataMap; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["studentConceptDataActionTypes"].LOAD_STUDENT_CONCEPT_DATA_SUCCESS:
            {
                var conceptId = action.conceptId, data = action.data;
                var studentConceptDataMap = new Map(state);
                studentConceptDataMap.set(conceptId, data);
                return studentConceptDataMap;
            }
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Reducers/Student/studentCourseGroupsReducer.ts":
/*!************************************************************!*\
  !*** ./src/Reducers/Student/studentCourseGroupsReducer.ts ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return studentCourseGroupsReducer; });
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");


function studentCourseGroupsReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_0__["default"].studentCourseGroupsMap; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_1__["studentCourseGroupsActionTypes"].LOAD_STUDENT_COURSE_GROUPS_SUCCESS:
            var courseid = action.courseid, groups = action.groups;
            var newState = new Map(state);
            newState.set(courseid, groups);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_1__["studentCourseGroupsActionTypes"].LOAD_STUDENT_COURSE_GROUPS_FAILURE:
            return state;
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Reducers/Student/studentCoursesListReducer.ts":
/*!***********************************************************!*\
  !*** ./src/Reducers/Student/studentCoursesListReducer.ts ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return studentCoursesListReducer; });
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");


function studentCoursesListReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_1__["default"].studentCoursesMap; }
    var newState;
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_0__["studentCoursesListActionTypes"].LOAD_STUDENT_COURSES_LIST_SUCCESS:
            return action.studentCoursesMap;
        case Screens__WEBPACK_IMPORTED_MODULE_0__["studentCoursesListActionTypes"].UNREGISTER_COURSE_SUCCESS:
            newState = new Map(state);
            newState.delete(action.courseId);
            return newState;
        default:
            return state;
    }
}
;


/***/ }),

/***/ "./src/Reducers/Student/studentGroupConceptsReducer.ts":
/*!*************************************************************!*\
  !*** ./src/Reducers/Student/studentGroupConceptsReducer.ts ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return studentGroupConceptsReducer; });
/* harmony import */ var store_InitialState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! store/InitialState */ "./src/store/InitialState.ts");
/* harmony import */ var Screens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens */ "./src/Screens/index.ts");


function studentGroupConceptsReducer(state, action) {
    if (state === void 0) { state = store_InitialState__WEBPACK_IMPORTED_MODULE_0__["default"].studentGroupConceptsMap; }
    switch (action.type) {
        case Screens__WEBPACK_IMPORTED_MODULE_1__["studentGroupConceptsActionTypes"].LOAD_STUDENT_GROUP_CONCEPTS_SUCCESS:
            var groupid = action.groupid, concepts = action.concepts;
            var newState = new Map(state);
            newState.set(groupid, concepts);
            return newState;
        case Screens__WEBPACK_IMPORTED_MODULE_1__["studentGroupConceptsActionTypes"].LOAD_STUDENT_GROUP_CONCEPTS_FAILURE:
            return state;
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/Routes.tsx":
/*!************************!*\
  !*** ./src/Routes.tsx ***!
  \************************/
/*! exports provided: routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toastify */ "./node_modules/react-toastify/esm/react-toastify.js");
/* harmony import */ var Screens_ContactUs_Common_Components_ContactUsPage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/ContactUs/Common/Components/ContactUsPage */ "./src/Screens/ContactUs/Common/Components/ContactUsPage.tsx");
/* harmony import */ var Screens_CourseBody_Common_Containers_CourseBodyPage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Screens/CourseBody/Common/Containers/CourseBodyPage */ "./src/Screens/CourseBody/Common/Containers/CourseBodyPage.tsx");
/* harmony import */ var Screens_CoursesList_Common_Containers_CoursesListPage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Screens/CoursesList/Common/Containers/CoursesListPage */ "./src/Screens/CoursesList/Common/Containers/CoursesListPage.tsx");
/* harmony import */ var Screens_Concept_Common_Containers_ConceptPage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Screens/Concept/Common/Containers/ConceptPage */ "./src/Screens/Concept/Common/Containers/ConceptPage.tsx");
/* harmony import */ var Screens_LandingPage_Common_Components_LandingPage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Screens/LandingPage/Common/Components/LandingPage */ "./src/Screens/LandingPage/Common/Components/LandingPage.tsx");
/* harmony import */ var Screens_Login_Common_Components_LoginPage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Screens/Login/Common/Components/LoginPage */ "./src/Screens/Login/Common/Components/LoginPage.tsx");
/* harmony import */ var Screens_Mission_Common_Components_MissionPage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Screens/Mission/Common/Components/MissionPage */ "./src/Screens/Mission/Common/Components/MissionPage.tsx");
/* harmony import */ var Screens_NotFound_Common_Components_NotFound__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Screens/NotFound/Common/Components/NotFound */ "./src/Screens/NotFound/Common/Components/NotFound.tsx");
/* harmony import */ var Screens_PublicCourses_Common_Containers_PublicCoursesPage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Screens/PublicCourses/Common/Containers/PublicCoursesPage */ "./src/Screens/PublicCourses/Common/Containers/PublicCoursesPage.tsx");
/* harmony import */ var Screens_Layout__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Screens/Layout */ "./src/Screens/Layout.tsx");














// const Layout = React.lazy(() => import('Screens/Layout'));
// const ContactUsPage = React.lazy(() => import('Screens/ContactUs/Common/Components/ContactUsPage'));
// const CourseBodyPage = React.lazy(() => import('Screens/CourseBody/Common/Containers/CourseBodyPage'));
// const CoursesListPage = React.lazy(() => import('Screens/CoursesList/Common/Containers/CoursesListPage'));
// const ConceptPage = React.lazy(() => import('Screens/Concept/Common/Containers/ConceptPage'));
// const LandingPage = React.lazy(() => import('Screens/LandingPage/Common/Components/LandingPage'));
// const LoginPage = React.lazy(() => import('Screens/Login/Common/Components/LoginPage'));
// const MissionPage = React.lazy(() => import('Screens/Mission/Common/Components/MissionPage'));
// const NotFound = React.lazy(() => import('Screens/NotFound/Common/Components/NotFound'));
// const PublicCoursesPage = React.lazy(() => import('Screens/PublicCourses/Common/Containers/PublicCoursesPage'));
var isLoggedIn = Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_2__["default"])().mode;
var routes = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout__WEBPACK_IMPORTED_MODULE_13__["default"], null,
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_toastify__WEBPACK_IMPORTED_MODULE_3__["ToastContainer"], null),
    !isLoggedIn ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null,
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend', exact: true, component: Screens_LandingPage_Common_Components_LandingPage__WEBPACK_IMPORTED_MODULE_8__["default"] }),
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/login', exact: true, component: Screens_Login_Common_Components_LoginPage__WEBPACK_IMPORTED_MODULE_9__["default"] }),
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/courseware/', exact: true, component: Screens_PublicCourses_Common_Containers_PublicCoursesPage__WEBPACK_IMPORTED_MODULE_12__["default"] }),
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], { to: '/frontend/login' })))
        : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/mission', exact: true, component: Screens_Mission_Common_Components_MissionPage__WEBPACK_IMPORTED_MODULE_10__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/contact', exact: true, component: Screens_ContactUs_Common_Components_ContactUsPage__WEBPACK_IMPORTED_MODULE_4__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/courseware/', exact: true, component: Screens_PublicCourses_Common_Containers_PublicCoursesPage__WEBPACK_IMPORTED_MODULE_12__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/courseware/courseslist', exact: true, component: Screens_CoursesList_Common_Containers_CoursesListPage__WEBPACK_IMPORTED_MODULE_6__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/courseware/course/:courseId/:pageRef?', component: Screens_CourseBody_Common_Containers_CourseBodyPage__WEBPACK_IMPORTED_MODULE_5__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { path: '/frontend/concept/:conceptId', exact: true, component: Screens_Concept_Common_Containers_ConceptPage__WEBPACK_IMPORTED_MODULE_7__["default"] }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], { to: '/frontend/courseware/courseslist' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], { component: Screens_NotFound_Common_Components_NotFound__WEBPACK_IMPORTED_MODULE_11__["default"] })))));


/***/ }),

/***/ "./src/Screens/AssignmentsGroup/Student/AssignmentsGroup.tsx":
/*!*******************************************************************!*\
  !*** ./src/Screens/AssignmentsGroup/Student/AssignmentsGroup.tsx ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var AssignmentsGroup = /** @class */ (function (_super) {
    __extends(AssignmentsGroup, _super);
    function AssignmentsGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AssignmentsGroup.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "AssignmentsGroup PlaceHolder");
    };
    return AssignmentsGroup;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (AssignmentsGroup);
;


/***/ }),

/***/ "./src/Screens/ChatroomForm/Instructor/ChatroomForm.tsx":
/*!**************************************************************!*\
  !*** ./src/Screens/ChatroomForm/Instructor/ChatroomForm.tsx ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var ChatroomForm = /** @class */ (function (_super) {
    __extends(ChatroomForm, _super);
    function ChatroomForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChatroomForm.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "ChatroomForm PlaceHolder");
    };
    return ChatroomForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ChatroomForm);
;


/***/ }),

/***/ "./src/Screens/ChatroomForm/Student/ChatroomForm.tsx":
/*!***********************************************************!*\
  !*** ./src/Screens/ChatroomForm/Student/ChatroomForm.tsx ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var ChatroomForm = /** @class */ (function (_super) {
    __extends(ChatroomForm, _super);
    function ChatroomForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChatroomForm.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "ChatroomForm PlaceHolder");
    };
    return ChatroomForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ChatroomForm);
;


/***/ }),

/***/ "./src/Screens/Concept/Common/Containers/ConceptPage.tsx":
/*!***************************************************************!*\
  !*** ./src/Screens/Concept/Common/Containers/ConceptPage.tsx ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var _Instructor_Containers_ConceptInstructorPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Instructor/Containers/ConceptInstructorPage */ "./src/Screens/Concept/Instructor/Containers/ConceptInstructorPage.tsx");
/* harmony import */ var Screens_Concept_Student_Containers_ConceptStudentPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/Concept/Student/Containers/ConceptStudentPage */ "./src/Screens/Concept/Student/Containers/ConceptStudentPage.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


// Components


var ConceptPage = /** @class */ (function (_super) {
    __extends(ConceptPage, _super);
    function ConceptPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ConceptPage.prototype.render = function () {
        switch (Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode) {
            case 'I':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Instructor_Containers_ConceptInstructorPage__WEBPACK_IMPORTED_MODULE_2__["default"], { match: this.props.match });
            default:
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Concept_Student_Containers_ConceptStudentPage__WEBPACK_IMPORTED_MODULE_3__["default"], { match: this.props.match });
        }
    };
    return ConceptPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ConceptPage);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Actions/ActionTypes.ts":
/*!***************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Actions/ActionTypes.ts ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_INSTRUCTOR_CONCEPT_DATA_SUCCESS: 'LOAD_INSTRUCTOR_CONCEPT_DATA_SUCCESS',
    LOAD_INSTRUCTOR_CONCEPT_DATA_FAILURE: 'LOAD_INSTRUCTOR_CONCEPT_DATA_FAILURE',
    ADD_INSTRUCTOR_CONCEPT_ELEM_SUCCESS: 'ADD_INSTRUCTOR_CONCEPT_ELEM_SUCCESS',
    ADD_INSTRUCTOR_CONCEPT_ELEM_FAILURE: 'ADD_INSTRUCTOR_CONCEPT_ELEM_FAILURE',
    DEL_INSTRUCTOR_CONCEPT_ELEM_SUCCESS: 'DEL_INSTRUCTOR_CONCEPT_ELEM_SUCCESS',
    DEL_INSTRUCTOR_CONCEPT_ELEM_FAILURE: 'DEL_INSTRUCTOR_CONCEPT_ELEM_FAILURE',
    REO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS: 'REO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS',
    REO_INSTRUCTOR_CONCEPT_ELEM_FAILURE: 'REO_INSTRUCTOR_CONCEPT_ELEM_FAILURE',
    INFO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS: 'INFO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS',
    INFO_INSTRUCTOR_CONCEPT_ELEM_FAILURE: 'INFO_INSTRUCTOR_CONCEPT_ELEM_FAILURE',
});


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Actions/Actions.ts":
/*!***********************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Actions/Actions.ts ***!
  \***********************************************************/
/*! exports provided: loadConceptDataRequest, loadConceptDataSuccess, deleteConceptElementRequest, deleteConceptElementSuccess, addConceptElementRequest, addConceptElementSuccess, reorderConceptElementRequest, reorderConceptElementSuccess, infoEditConceptElementRequest, infoEditConceptElementSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadConceptDataRequest", function() { return loadConceptDataRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadConceptDataSuccess", function() { return loadConceptDataSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteConceptElementRequest", function() { return deleteConceptElementRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteConceptElementSuccess", function() { return deleteConceptElementSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addConceptElementRequest", function() { return addConceptElementRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addConceptElementSuccess", function() { return addConceptElementSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderConceptElementRequest", function() { return reorderConceptElementRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderConceptElementSuccess", function() { return reorderConceptElementSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "infoEditConceptElementRequest", function() { return infoEditConceptElementRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "infoEditConceptElementSuccess", function() { return infoEditConceptElementSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/Concept/Instructor/Actions/ActionTypes.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
/* harmony import */ var Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/PatchData */ "./src/Utils/PatchData.js");




// load req
function loadConceptDataRequest(conceptId) {
    return function (dispatch) {
        var url = "/concept/api/concept/" + conceptId + "/get_concept_page_data/";
        return fetch(url)
            .then(function (res) { return res.json(); })
            .then(function (res) {
            dispatch(loadConceptDataSuccess(conceptId, res));
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Concept Data Loading failed', 'error');
            throw (err);
        });
    };
}
function loadConceptDataSuccess(conceptId, data) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTRUCTOR_CONCEPT_DATA_SUCCESS,
        conceptId: conceptId,
        data: data
    };
}
// delete req
function deleteConceptElementRequest(conceptId, typeStr, typeId, id) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptId + "/delete_element/?format=json";
        var data = { 'type': typeId, 'id': id };
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(url, data)
            .then(function (res) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element Deleted Successfully', 'success');
            dispatch(deleteConceptElementSuccess(conceptId, typeStr, id));
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Delete Element Failed!', 'error');
            throw (err);
        });
    };
}
function deleteConceptElementSuccess(conceptId, type, id) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DEL_INSTRUCTOR_CONCEPT_ELEM_SUCCESS,
        conceptId: conceptId,
        elemType: type,
        id: id
    };
}
// add req
function addConceptElementRequest(conceptId, type, formData) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptId + "/add_" + type + "/?format=json";
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(url, formData, "FormData")
            .then(function (response) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element Added Succesfully', 'success');
            dispatch(addConceptElementSuccess(conceptId, type, response));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element Adding Failed!', 'error');
            throw (error);
        });
    };
}
function addConceptElementSuccess(conceptId, type, content) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_INSTRUCTOR_CONCEPT_ELEM_SUCCESS,
        elemType: type,
        conceptId: conceptId,
        content: content
    };
}
// reorder req
function reorderConceptElementRequest(conceptId, formData) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptId + "/reorder/?format=json";
        return Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, formData, "FormData")
            .then(function (response) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element Reordered Succesfully', 'success');
            dispatch(reorderConceptElementSuccess(conceptId, response));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element Reordering Failed!', 'error');
            throw (error);
        });
    };
}
function reorderConceptElementSuccess(conceptId, playlist) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].REO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS,
        conceptId: conceptId,
        playlist: playlist
    };
}
function infoEditConceptElementRequest(conceptId, data) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptId + "/?format=json";
        return Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, data)
            .then(function (response) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element info Edited Succesfully', 'success');
            dispatch(infoEditConceptElementSuccess(conceptId, response));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Element info Editing Failed!', 'error');
            throw (error);
        });
    };
}
function infoEditConceptElementSuccess(conceptId, _a) {
    var title = _a.title, description = _a.description;
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].INFO_INSTRUCTOR_CONCEPT_ELEM_SUCCESS,
        conceptId: conceptId,
        title: title,
        description: description
    };
}


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Assets/content_developer.sass":
/*!**********************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Assets/content_developer.sass ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Concept/Instructor/Assets/jasny-bootstrap.min.sass":
/*!************************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Assets/jasny-bootstrap.min.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Concept/Instructor/Components/AddElementForm.tsx":
/*!**********************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Components/AddElementForm.tsx ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//Components

var AddElementForm = /** @class */ (function (_super) {
    __extends(AddElementForm, _super);
    function AddElementForm(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            is_quiz_video: false,
            loading: false
        };
        return _this;
    }
    AddElementForm.prototype.onSave = function (event) {
        var _this = this;
        event.preventDefault();
        var form = event.currentTarget;
        var formData = new FormData();
        console.log(form);
        Array.from(form.elements).forEach(function (e, i) {
            if (e['type'] === 'file') {
                if (!e['files'][0])
                    formData.append(e['id'], new File([""], ""));
                else
                    formData.append(e['id'], e['files'][0]);
            }
            else if (e['type'] !== "submit") {
                if (_this.props.type === "video" && e['id'] === "description")
                    formData.append('content', e['value']);
                else
                    formData.append(e['id'], e['value']);
            }
        });
        formData.append('is_quiz_video', String(this.state.is_quiz_video));
        this.props.saveCallback(this.props.type, formData);
    };
    AddElementForm.prototype.render = function () {
        var form = null;
        if (this.props.type === 'video') {
            form = [['Video File', 'video_file'], ['Camtasia Config File', 'video_config_file'], ['Slides etc.', 'other_file']].map(function (e) {
                return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { key: e[1], as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: e[1] },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, e[0]),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { type: "file" }))));
            });
        }
        else if (this.props.type === 'document') {
            form = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: "document_file" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, "Document"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { type: "file" }))));
        }
        else {
            form = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Render quiz here ");
        }
        if (this.state.loading) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "Loading..."));
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], { onSubmit: this.onSave },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: "title" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, "Title"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { required: true, type: "text", placeholder: "Title..." }))),
                form,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: "description" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, "Description"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { as: "textarea", rows: 4, placeholder: "Description..." }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"] },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: { span: 4, offset: 4 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], { type: "submit" }, "Save"))))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], AddElementForm.prototype, "onSave", null);
    return AddElementForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (AddElementForm);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Components/ConceptDocumentEdit.tsx":
/*!***************************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Components/ConceptDocumentEdit.tsx ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/PatchData */ "./src/Utils/PatchData.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ConceptDocumentEdit = /** @class */ (function (_super) {
    __extends(ConceptDocumentEdit, _super);
    function ConceptDocumentEdit(props) {
        return _super.call(this, props) || this;
    }
    ConceptDocumentEdit.prototype.onSave = function (event) {
        var _this = this;
        event.preventDefault();
        event.stopPropagation();
        var url = "/document/api/page/" + this.props.data.content.id + "/?format=json";
        var form = event.currentTarget;
        var data = {
            title: form["title"].value,
            description: form["description"].value
        };
        Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, data)
            .then(function (response) {
            console.log('response concept doc=>', response);
            _this.props.saveCallback();
        })
            .catch(function (err) { return console.log(err); });
    };
    ConceptDocumentEdit.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], { onSubmit: this.onSave },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: "title" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, "Title"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { required: true, type: "text", defaultValue: this.props.data.content.title }))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], controlId: "description" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, { column: true, sm: 2 }, "Description"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 10 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { as: "textarea", rows: 4, defaultValue: this.props.data.content.description }))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: 4 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: this.props.data.content.document_file },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "btn btn-link" }, "View Document")))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"] },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: { span: 4, offset: 2 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], { variant: "success", type: "submit" }, "Save")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: { span: 4, offset: 2 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], { variant: "warning", type: "reset" }, "Revert")))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptDocumentEdit.prototype, "onSave", null);
    return ConceptDocumentEdit;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ConceptDocumentEdit);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Components/ConceptInfo.tsx":
/*!*******************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Components/ConceptInfo.tsx ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConceptInfo = /** @class */ (function (_super) {
    __extends(ConceptInfo, _super);
    function ConceptInfo(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    ConceptInfo.prototype.onSave = function (event) {
        event.preventDefault();
        event.stopPropagation();
        var form = event.currentTarget;
        var data = {
            title: form["title"].value,
            description: form["description"].value
        };
        this.props.onSaveInfoCallback(data);
    };
    ConceptInfo.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"], { defaultActiveKey: "-1" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], { style: { border: 'none' } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Header, eventKey: "0", style: { border: 'none', backgroundColor: 'white' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", null, this.props.title),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", { style: { color: 'grey' } }, this.props.description)),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"].Collapse, { eventKey: "0" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"], { onSubmit: this.onSave },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], controlId: "title" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, { column: true, sm: 2 }, "Title"),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: 10 },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, { required: true, type: "text", defaultValue: this.props.title }))),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], controlId: "description" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, { column: true, sm: 2 }, "Description"),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: 10 },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, { as: "textarea", rows: 4, defaultValue: this.props.description }))),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"] },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "success", type: "submit" }, "Save")),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "warning", type: "reset" }, "Revert")))))))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], ConceptInfo.prototype, "onSave", null);
    return ConceptInfo;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ConceptInfo);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Components/LearningElementEdit.tsx":
/*!***************************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Components/LearningElementEdit.tsx ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Components_ConceptDocumentEdit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Components/ConceptDocumentEdit */ "./src/Screens/Concept/Instructor/Components/ConceptDocumentEdit.tsx");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var Screens_Video_Instructor_Components_ConceptVideoBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/Video/Instructor/Components/ConceptVideoBox */ "./src/Screens/Video/Instructor/Components/ConceptVideoBox.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// ConceptVideoBox
// QuizEditAdmin
//Components




// This variable is used in multiple modules
var QuestionModuleEditId = "concept-learning-element-quiz";
var LearningElementEdit = /** @class */ (function (_super) {
    __extends(LearningElementEdit, _super);
    function LearningElementEdit() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LearningElementEdit.prototype.editDocument = function () {
        this.props.refresh();
    };
    LearningElementEdit.prototype.render = function () {
        if (!this.props.data) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null);
        }
        var elementNode = null;
        if (this.props.data.type === 'video') {
            elementNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Video_Instructor_Components_ConceptVideoBox__WEBPACK_IMPORTED_MODULE_4__["default"], { refresh: this.props.refresh, concept: this.props.concept, data: this.props.data });
        }
        else if (this.props.data.type === 'quiz') {
            var questionModuleEditId = QuestionModuleEditId + this.props.data.content.id;
            elementNode = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'quiz-edit-admin' }, "QuizEditAdmin"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: questionModuleEditId })));
        }
        else if (this.props.data.type === 'document') {
            elementNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_ConceptDocumentEdit__WEBPACK_IMPORTED_MODULE_1__["default"], { data: this.props.data, saveCallback: this.editDocument });
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, null, elementNode)));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], LearningElementEdit.prototype, "editDocument", null);
    return LearningElementEdit;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (LearningElementEdit);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Components/LearningElementsList.tsx":
/*!****************************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Components/LearningElementsList.tsx ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var _Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assets/content_developer.sass */ "./src/Screens/Concept/Instructor/Assets/content_developer.sass");
/* harmony import */ var _Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Switch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Switch */ "./node_modules/@material-ui/core/esm/Switch/index.js");
/* harmony import */ var _material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/Delete */ "./node_modules/@material-ui/icons/Delete.js");
/* harmony import */ var _material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/Description */ "./node_modules/@material-ui/icons/Description.js");
/* harmony import */ var _material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_PlayCircleFilled__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/PlayCircleFilled */ "./node_modules/@material-ui/icons/PlayCircleFilled.js");
/* harmony import */ var _material_ui_icons_PlayCircleFilled__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PlayCircleFilled__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/ContactSupport */ "./node_modules/@material-ui/icons/ContactSupport.js");
/* harmony import */ var _material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-bootstrap/Modal */ "./node_modules/react-bootstrap/esm/Modal.js");
/* harmony import */ var _material_ui_icons_AddCircleOutlineOutlined__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons/AddCircleOutlineOutlined */ "./node_modules/@material-ui/icons/AddCircleOutlineOutlined.js");
/* harmony import */ var _material_ui_icons_AddCircleOutlineOutlined__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AddCircleOutlineOutlined__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_icons_ImportExport__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/icons/ImportExport */ "./node_modules/@material-ui/icons/ImportExport.js");
/* harmony import */ var _material_ui_icons_ImportExport__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ImportExport__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _AddElementForm__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./AddElementForm */ "./src/Screens/Concept/Instructor/Components/AddElementForm.tsx");
/* harmony import */ var dragula__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! dragula */ "./node_modules/dragula/dragula.js");
/* harmony import */ var dragula__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(dragula__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! dragula/dist/dragula.css */ "./node_modules/dragula/dist/dragula.css");
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//assets













// dragula for reordering


var LearningElementsList = /** @class */ (function (_super) {
    __extends(LearningElementsList, _super);
    function LearningElementsList(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            add: false,
            order: false,
            type: undefined,
        };
        return _this;
    }
    LearningElementsList.prototype.toggleAdd = function () {
        this.setState(function (state) {
            return __assign(__assign({}, state), { add: !state.add });
        });
    };
    LearningElementsList.prototype.toggleOrder = function () {
        var _this = this;
        if (this.state.order) {
            // save order here 
            this.dragulaService.destroy();
            var type2int_1 = { 'video': 0, 'quiz': 1, 'document': 2 };
            console.log('playlistcopy', this.playlistCopy);
            var playlistPayload = this.playlistCopy.map(function (p) {
                return [p.content.id, type2int_1[p.type]];
            });
            console.log('payload ', playlistPayload);
            this.props.onReorderCallback(playlistPayload);
            this.setState({ order: false });
        }
        else {
            // set up dragula service
            this.playlistCopy = this.props.playlist.map(function (e) { return (__assign({}, e)); });
            this.id2playlist = {};
            this.playlistCopy.forEach(function (p) {
                _this.id2playlist[p.type + "-" + p.content.id] = p;
            });
            this.dragulaService = dragula__WEBPACK_IMPORTED_MODULE_14__([document.getElementById("playlistContainer")], {
                revertOnSpill: true,
                moves: function (el, container, handle) {
                    console.log(handle);
                    return handle.classList.contains("playlistHandle");
                }
            }).on('drop', function (el, container, handle) {
                console.log('element', el);
                console.log('container', container);
                console.log('handle', handle);
                this.playlistCopy = Array.from(container.children).map(function (p) {
                    return this.id2playlist[p["id"]];
                }.bind(this));
            }.bind(this));
            this.setState({ order: true });
        }
    };
    LearningElementsList.prototype.handleTypeSelect = function (eventKey, event) {
        this.setState({
            type: eventKey,
            add: true
        });
    };
    LearningElementsList.prototype.capitaliseFirstLetter = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    LearningElementsList.prototype.onSave = function (type, formData) {
        this.props.onAddCallback(type, formData);
        this.toggleAdd();
    };
    LearningElementsList.prototype.render = function () {
        var _this = this;
        var id = { 'video': 0, 'quiz': 1, 'document': 2 };
        var elementList = this.props.playlist.map(function (item, i) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Row"], { key: item.type + "-" + item.content.id, id: item.type + "-" + item.content.id, className: "border no-gutters" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Col"], { style: { marginLeft: '4%' }, xs: { span: 1 } }, !_this.state.order ?
                    (item.type == "video" ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_PlayCircleFilled__WEBPACK_IMPORTED_MODULE_6___default.a, { fontSize: "large", color: "primary" }) :
                        item.type == "quiz" ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_7___default.a, { fontSize: "large", color: "primary" }) :
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_5___default.a, { fontSize: "large", color: "primary" }))
                    :
                        (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "playlistHandle" }, "#"))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Col"], { xs: { span: 7 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Button"], { variant: "link", onClick: function () { return _this.props.onSelectCallback(i); } }, item.content.title + " (" + _this.capitaliseFirstLetter(item.type) + ")")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Col"], { xs: { span: 2 } },
                    "Published:",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_core_Switch__WEBPACK_IMPORTED_MODULE_3__["default"], { edge: "end" })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Col"], { style: { marginLeft: '4%' }, xs: { span: 1 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_core__WEBPACK_IMPORTED_MODULE_8__["IconButton"], { edge: "end", onClick: function () {
                            _this.props.onDeleteCallback(item.type, id[item.type], item.content.id);
                        }, "aria-label": "delete" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_Delete__WEBPACK_IMPORTED_MODULE_4___default.a, null)))));
        });
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, this.state.add ?
            (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_10__["default"], { show: this.state.add, onHide: this.toggleAdd },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_10__["default"].Header, { closeButton: true },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_10__["default"].Title, null, "Add " + this.state.type)),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Modal__WEBPACK_IMPORTED_MODULE_10__["default"].Body, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_AddElementForm__WEBPACK_IMPORTED_MODULE_13__["default"], { cancelCallback: this.toggleAdd, saveCallback: this.onSave, type: this.state.type }))))
            :
                (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "control-buttons" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["DropdownButton"], { drop: "right", variant: "light", title: react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_AddCircleOutlineOutlined__WEBPACK_IMPORTED_MODULE_11___default.a, { fontSize: "small" }),
                                " Add Content"), id: "dropdown-button-drop-right", onSelect: this.handleTypeSelect },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Dropdown"].Item, { eventKey: "document" }, "Document"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Dropdown"].Item, { eventKey: "video" }, "Video"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Dropdown"].Item, { eventKey: "quiz" }, "Quiz")),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_9__["Button"], { onClick: this.toggleOrder, variant: "light" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_ImportExport__WEBPACK_IMPORTED_MODULE_12___default.a, { fontSize: "small" }),
                                this.state.order ? 'Save Order' : 'Reorder'))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'playlistContainer' }, elementList)))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], LearningElementsList.prototype, "toggleAdd", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], LearningElementsList.prototype, "toggleOrder", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], LearningElementsList.prototype, "handleTypeSelect", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], LearningElementsList.prototype, "onSave", null);
    return LearningElementsList;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (LearningElementsList);


/***/ }),

/***/ "./src/Screens/Concept/Instructor/Containers/ConceptInstructorPage.tsx":
/*!*****************************************************************************!*\
  !*** ./src/Screens/Concept/Instructor/Containers/ConceptInstructorPage.tsx ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Components_LearningElementsList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Components/LearningElementsList */ "./src/Screens/Concept/Instructor/Components/LearningElementsList.tsx");
/* harmony import */ var _Components_LearningElementEdit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Components/LearningElementEdit */ "./src/Screens/Concept/Instructor/Components/LearningElementEdit.tsx");
/* harmony import */ var _Components_ConceptInfo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Components/ConceptInfo */ "./src/Screens/Concept/Instructor/Components/ConceptInfo.tsx");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/Concept/Instructor/Actions/Actions.ts");
/* harmony import */ var Screens_Forum_Instructor_Forum__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Screens/Forum/Instructor/Forum */ "./src/Screens/Forum/Instructor/Forum.tsx");
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
/* harmony import */ var _Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../Assets/content_developer.sass */ "./src/Screens/Concept/Instructor/Assets/content_developer.sass");
/* harmony import */ var _Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_Assets_content_developer_sass__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _Assets_jasny_bootstrap_min_sass__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../Assets/jasny-bootstrap.min.sass */ "./src/Screens/Concept/Instructor/Assets/jasny-bootstrap.min.sass");
/* harmony import */ var _Assets_jasny_bootstrap_min_sass__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_Assets_jasny_bootstrap_min_sass__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/icons/NavigateNext */ "./node_modules/@material-ui/icons/NavigateNext.js");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_14__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// components



// actions



//assets





var ConceptInstructorPage = /** @class */ (function (_super) {
    __extends(ConceptInstructorPage, _super);
    function ConceptInstructorPage(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            currentItemIndex: undefined
        };
        return _this;
    }
    ConceptInstructorPage.prototype.componentDidMount = function () {
        // if (!this.props.data) {
        this.props.actions.loadConceptDataRequest(this.props.conceptId);
        // }
    };
    ConceptInstructorPage.prototype.componentDidUpdate = function (prevProps) {
        if (prevProps.match.params.conceptId !== this.props.match.params.conceptId) {
            this.props.actions.loadConceptDataRequest(this.props.conceptId);
        }
    };
    ConceptInstructorPage.prototype.onSelectElement = function (index) {
        if (this.state.currentItemIndex === index) {
            this.setState({
                currentItemIndex: undefined
            });
        }
        else {
            this.setState({
                currentItemIndex: index
            });
        }
    };
    ConceptInstructorPage.prototype.onAddElement = function (type, formData) {
        return this.props.actions.addConceptElementRequest(this.props.conceptId, type, formData);
    };
    ConceptInstructorPage.prototype.onDeleteElement = function (typeStr, typeId, id) {
        var flag = confirm('Are you Sure?');
        if (flag) {
            this.props.actions.deleteConceptElementRequest(this.props.conceptId, typeStr, typeId, id);
        }
    };
    ConceptInstructorPage.prototype.onReorderElement = function (payload) {
        var _this = this;
        var formData = new FormData();
        formData.append('playlist', JSON.stringify(payload));
        return this.props.actions.reorderConceptElementRequest(this.props.conceptId, formData)
            .catch(function (err) {
            _this.props.actions.loadConceptDataRequest(_this.props.conceptId);
        });
    };
    ConceptInstructorPage.prototype.onPublishElement = function () {
        // to be done
    };
    ConceptInstructorPage.prototype.onSaveInfo = function (data) {
        this.props.actions.infoEditConceptElementRequest(this.props.conceptId, data);
    };
    ConceptInstructorPage.prototype.render = function () {
        var _this = this;
        if (!this.props.data) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Loading..."));
        }
        // create breadcrumb nodes
        var conceptNodes = [];
        this.props.data.group_playlist.forEach(function (item, i) {
            conceptNodes.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: item.id, className: "dropdown-item" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { to: "/frontend/concept/" + item.id + "/" }, item.title)));
        });
        var groupNodes = [];
        this.props.data.course_playlist.forEach(function (item, i) {
            if (item.concept.length === 0) {
                groupNodes.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: item.id, className: "dropdown-item" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { to: "/frontend/courseware/course/" + item.id }, item.title)));
            }
            else {
                var cNodes_1 = [];
                item.concept.forEach(function (result) {
                    cNodes_1.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: result.id, className: "dropdown-item" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { to: "/frontend/concept/" + result.id }, result.title)));
                });
                groupNodes.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: item.id, className: "dropdown-submenu" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { className: "dropdown-item", to: "/frontend/courseware/course/" + item.id }, item.title),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { className: "dropdown-menu" }, cNodes_1)));
            }
        });
        // get forum
        var forum = null;
        if (this.props.data.tag) {
            forum = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Forum_Instructor_Forum__WEBPACK_IMPORTED_MODULE_9__["default"], { id: this.props.data.forum });
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_10__["default"], { navbarClass: 'navbar-light' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Container"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: { span: 12 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h3", { className: "course-title" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { to: "frontend/courseware/course/" + this.props.data.course + " /" }, this.props.data.course_title)))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: 12 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Card"], null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Container"], { style: { maxHeight: '50px', maxWidth: '90%' } },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { marginTop: '1%' } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dropdown" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "btn btn-light dropdown-toggle", style: { fontSize: '21px' }, type: "button", id: "dropdownMenu1", "data-toggle": "dropdown", "aria-haspopup": "true", "aria-expanded": "false" }, this.props.data.group_title),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { className: "dropdown-menu multi-level", role: "menu", "aria-labelledby": "dropdownMenu" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "dropdown-item" }, "Other Topics"),
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "dropdown-divider" }),
                                                groupNodes))),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { marginTop: '2%' } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_14___default.a, null)),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { marginTop: '1%' } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dropdown" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "btn btn-light dropdown-toggle", style: { fontSize: '21px' }, type: "button", id: "dropdownMenu1", "data-toggle": "dropdown", "aria-haspopup": "true", "aria-expanded": "false" }, this.props.data.title),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { className: "dropdown-menu multi-level", role: "menu", "aria-labelledby": "dropdownMenu" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "dropdown-item" }, "Other Sections"),
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "dropdown-divider" }),
                                                conceptNodes))))),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Card"].Body, null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Container"], { style: { maxWidth: '90%' } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: 12 },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_ConceptInfo__WEBPACK_IMPORTED_MODULE_7__["default"], { title: this.props.data.title, description: this.props.data.description, onSaveInfoCallback: this.onSaveInfo }))),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: 12 },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_LearningElementsList__WEBPACK_IMPORTED_MODULE_5__["default"], { playlist: this.props.data.playlist, onSelectCallback: this.onSelectElement, onAddCallback: this.onAddElement, onDeleteCallback: this.onDeleteElement, onPublishCallback: this.onPublishElement, onReorderCallback: this.onReorderElement })))))))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                this.state.currentItemIndex !== undefined ?
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: 12 },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_LearningElementEdit__WEBPACK_IMPORTED_MODULE_6__["default"], { data: this.props.data.playlist[this.state.currentItemIndex], concept: this.props.conceptId, refresh: function () { _this.props.actions.loadConceptDataRequest(_this.props.conceptId); } })))
                    : null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_13__["Col"], { md: 12 }, forum)))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onSelectElement", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onAddElement", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onDeleteElement", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onReorderElement", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onPublishElement", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptInstructorPage.prototype, "onSaveInfo", null);
    return ConceptInstructorPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
var mapStateToProps = function (state, _a) {
    var match = _a.match;
    var conceptId = match.params.conceptId;
    var data = undefined;
    if (state.instructorConceptDataMap.has(conceptId)) {
        data = state.instructorConceptDataMap.get(conceptId);
    }
    return {
        match: match,
        conceptId: conceptId,
        data: data
    };
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_3__["bindActionCreators"])({
        loadConceptDataRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__["loadConceptDataRequest"],
        deleteConceptElementRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__["deleteConceptElementRequest"],
        addConceptElementRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__["addConceptElementRequest"],
        reorderConceptElementRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__["reorderConceptElementRequest"],
        infoEditConceptElementRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_8__["infoEditConceptElementRequest"],
    }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_4__["connect"])(mapStateToProps, mapDispatchToProps)(ConceptInstructorPage));


/***/ }),

/***/ "./src/Screens/Concept/Student/Actions/ActionTypes.ts":
/*!************************************************************!*\
  !*** ./src/Screens/Concept/Student/Actions/ActionTypes.ts ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_STUDENT_CONCEPT_DATA_SUCCESS: 'LOAD_STUDENT_CONCEPT_DATA_SUCCESS',
    LOAD_STUDENT_CONCEPT_DATA_FAILURE: 'LOAD_STUDENT_CONCEPT_DATA_FAILURE',
});


/***/ }),

/***/ "./src/Screens/Concept/Student/Actions/Actions.ts":
/*!********************************************************!*\
  !*** ./src/Screens/Concept/Student/Actions/Actions.ts ***!
  \********************************************************/
/*! exports provided: loadConceptDataRequest, loadConceptDataSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadConceptDataRequest", function() { return loadConceptDataRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadConceptDataSuccess", function() { return loadConceptDataSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/Concept/Student/Actions/ActionTypes.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");


// load req
function loadConceptDataRequest(conceptId) {
    return function (dispatch) {
        var url = "/concept/api/concept/" + conceptId + "/get_concept_page_data/";
        return fetch(url)
            .then(function (res) { return res.json(); })
            .then(function (res) {
            dispatch(loadConceptDataSuccess(conceptId, res));
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Concept Data Loading failed', 'error');
            throw (err);
        });
    };
}
function loadConceptDataSuccess(conceptId, data) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_STUDENT_CONCEPT_DATA_SUCCESS,
        conceptId: conceptId,
        data: data
    };
}


/***/ }),

/***/ "./src/Screens/Concept/Student/Assets/concept_student.sass":
/*!*****************************************************************!*\
  !*** ./src/Screens/Concept/Student/Assets/concept_student.sass ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Concept/Student/Components/Concept.tsx":
/*!************************************************************!*\
  !*** ./src/Screens/Concept/Student/Components/Concept.tsx ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/NavigateNext */ "./node_modules/@material-ui/icons/NavigateNext.js");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_NavigateBefore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/NavigateBefore */ "./node_modules/@material-ui/icons/NavigateBefore.js");
/* harmony import */ var _material_ui_icons_NavigateBefore__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_NavigateBefore__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var _Assets_concept_student_sass__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Assets/concept_student.sass */ "./src/Screens/Concept/Student/Assets/concept_student.sass");
/* harmony import */ var _Assets_concept_student_sass__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_Assets_concept_student_sass__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_ListAlt__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/ListAlt */ "./node_modules/@material-ui/icons/ListAlt.js");
/* harmony import */ var _material_ui_icons_ListAlt__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ListAlt__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/icons/Description */ "./node_modules/@material-ui/icons/Description.js");
/* harmony import */ var _material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_icons_OndemandVideo__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/OndemandVideo */ "./node_modules/@material-ui/icons/OndemandVideo.js");
/* harmony import */ var _material_ui_icons_OndemandVideo__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_OndemandVideo__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var Screens_Video_Student_Components_VideoTOC__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Screens/Video/Student/Components/VideoTOC */ "./src/Screens/Video/Student/Components/VideoTOC.tsx");
/* harmony import */ var Screens_Video_Student_Components_Video__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Screens/Video/Student/Components/Video */ "./src/Screens/Video/Student/Components/Video.tsx");
/* harmony import */ var Screens_Video_Student_Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Screens/Video/Student/Assets/videoStud.sass */ "./src/Screens/Video/Student/Assets/videoStud.sass");
/* harmony import */ var Screens_Video_Student_Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(Screens_Video_Student_Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_12__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var Concept = /** @class */ (function (_super) {
    __extends(Concept, _super);
    function Concept(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            currentItem: null,
            startTime: 0
        };
        return _this;
    }
    Concept.prototype.handleNav = function (flag) {
        var _a = this.props.data, id = _a.id, group_playlist = _a.group_playlist;
        var index = group_playlist.findIndex(function (e) { return e.id === id; });
        var prev = group_playlist[index - 1].id;
        var next = group_playlist[index + 1].id;
        if (flag) {
            this.props.history.push("/frontend/concept/" + next);
        }
        else {
            this.props.history.push("/frontend/concept/" + prev);
        }
    };
    Concept.prototype.setCurrentItem = function (index) {
        this.setState({
            currentItem: this.props.data.playlist[index]
        });
    };
    Concept.prototype.handleTimeUpdate = function (index) {
        if (this.state.currentItem.type === 'video') {
            this.setState({
                startTime: this.state.currentItem.content.markers[index]['time']
            });
        }
    };
    Concept.prototype.render = function () {
        var _this = this;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { "md-span": 12 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h3", { className: "course-title" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { to: "/courseware/course/" + this.props.data.course + " /" }, this.props.data.course_title)))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { "md-span": 12 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"].Item, { href: "#" }, "Home"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"].Item, { href: "https://getbootstrap.com/docs/4.0/components/breadcrumb/" }, "Library"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"].Item, { active: true }, "Data")))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { "md-span": 12 },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", { className: 'concept-header' }, this.props.data.title),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", { className: 'concept-header' }, this.props.data.description))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], { className: "concept-nav-buttons" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { className: "navBtn", variant: 'light', onClick: function () { _this.handleNav(false); } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_NavigateBefore__WEBPACK_IMPORTED_MODULE_4___default.a, { fontSize: "small" }),
                        "Previous")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { className: "navBtn", variant: 'light', onClick: function () { _this.handleNav(true); } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                        "Next",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_3___default.a, { fontSize: "small" })))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { md: { span: 4 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Header, null, " Modules "),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], { variant: "flush" }, this.props.data.playlist.map(function (mod, i) {
                            var type = mod.type;
                            var _a = mod.content, id = _a.id, title = _a.title, markers = _a.markers;
                            if (type === 'video') {
                                return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"], { key: id, defaultActiveKey: "0" },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, action: true, onClick: function () { _this.setCurrentItem(i); }, eventKey: "0" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_OndemandVideo__WEBPACK_IMPORTED_MODULE_9___default.a, null),
                                                title)),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Accordion"].Collapse, { eventKey: "0" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, null,
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Table of Content"),
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Video_Student_Components_VideoTOC__WEBPACK_IMPORTED_MODULE_10__["default"], { markers: markers, clickCallback: _this.handleTimeUpdate }))))));
                            }
                            else if (type === 'quiz') {
                                return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, { key: id, action: true, onClick: function () { return _this.setCurrentItem(i); } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_Description__WEBPACK_IMPORTED_MODULE_8___default.a, null),
                                        title)));
                            }
                            else {
                                return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, { key: id, action: true, onClick: function () { return _this.setCurrentItem(i); } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_ListAlt__WEBPACK_IMPORTED_MODULE_7___default.a, null),
                                        title)));
                            }
                        })))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null, !this.state.currentItem ?
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Select a Module")
                    :
                        this.state.currentItem.type === 'video' ?
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "video-panel" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { className: "video-panel-heading", "md-span": 12 },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", null, this.state.currentItem.content.title))),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { "md-span": 12 },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Video_Student_Components_Video__WEBPACK_IMPORTED_MODULE_11__["default"], { videoFile: this.state.currentItem.content.video_file, videoStartTime: this.state.startTime }))))
                            :
                                this.state.currentItem.type === 'document' ?
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: this.state.currentItem.content.document_file, target: "_blank" }, "View")
                                    :
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Quiz")))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_5__["default"]
    ], Concept.prototype, "handleNav", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_5__["default"]
    ], Concept.prototype, "setCurrentItem", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_5__["default"]
    ], Concept.prototype, "handleTimeUpdate", null);
    return Concept;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Concept));


/***/ }),

/***/ "./src/Screens/Concept/Student/Containers/ConceptStudentPage.tsx":
/*!***********************************************************************!*\
  !*** ./src/Screens/Concept/Student/Containers/ConceptStudentPage.tsx ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var Screens_Forum_Student_Forum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/Forum/Student/Forum */ "./src/Screens/Forum/Student/Forum.tsx");
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
/* harmony import */ var _Components_Concept__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Components/Concept */ "./src/Screens/Concept/Student/Components/Concept.tsx");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/Concept/Student/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



// components



// actions

var ConceptStudentPage = /** @class */ (function (_super) {
    __extends(ConceptStudentPage, _super);
    function ConceptStudentPage(props) {
        return _super.call(this, props) || this;
    }
    ConceptStudentPage.prototype.componentDidMount = function () {
        this.props.actions.loadConceptDataRequest(this.props.match.params.conceptId);
    };
    ConceptStudentPage.prototype.componentDidUpdate = function (prevProps) {
        if (prevProps.match.params.conceptId != this.props.match.params.conceptId)
            this.props.actions.loadConceptDataRequest(this.props.match.params.conceptId);
    };
    ConceptStudentPage.prototype.render = function () {
        var data = this.props.data;
        if (!data) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Loading...");
        }
        var forum = data.tag ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Forum_Student_Forum__WEBPACK_IMPORTED_MODULE_3__["default"], { id: data.tag }) : null;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], { navbarClass: 'navbar-light' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Concept__WEBPACK_IMPORTED_MODULE_5__["default"], { forum: forum, data: data })));
    };
    return ConceptStudentPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
var mapStateToProps = function (state, _a) {
    var match = _a.match;
    var conceptId = match.params.conceptId;
    var data = undefined;
    if (state.studentConceptDataMap.has(conceptId)) {
        data = state.studentConceptDataMap.get(conceptId);
    }
    return {
        match: match,
        data: data
    };
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_1__["bindActionCreators"])({
        loadConceptDataRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_6__["loadConceptDataRequest"],
    }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(ConceptStudentPage));


/***/ }),

/***/ "./src/Screens/ContactUs/Common/Assets/ContactUsPage.sass":
/*!****************************************************************!*\
  !*** ./src/Screens/ContactUs/Common/Assets/ContactUsPage.sass ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/ContactUs/Common/Components/ContactUsPage.tsx":
/*!*******************************************************************!*\
  !*** ./src/Screens/ContactUs/Common/Components/ContactUsPage.tsx ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Assets_ContactUsPage_sass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Assets/ContactUsPage.sass */ "./src/Screens/ContactUs/Common/Assets/ContactUsPage.sass");
/* harmony import */ var _Assets_ContactUsPage_sass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Assets_ContactUsPage_sass__WEBPACK_IMPORTED_MODULE_1__);

// Assets

var ContactUs = function () { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'row' },
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", { className: 'justify' },
        "This website is currently in the development phase. There are some know bugs, yet to be rectified. Some of the major ones are listed under the FAQ session. If you wish to report a bug or suggest an enhancement or in general give any feedback, please send an email to ",
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: 'mailto:bodhitree.iitb@gmail.com' }, "bodhitree.iitb@gmail.com"),
        ". Your feedback will help us improve the overall quality and is much appreciated."),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'row' },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("address", { className: 'col-md-3' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'lead' }, "Kameswari Chebrolu"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'muted' }, "Department of CSE, IIT Bombay "),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'muted' }, "Powai - 400076"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'muted' }, "Mumbai."),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null))))); };
/* harmony default export */ __webpack_exports__["default"] = (ContactUs);


/***/ }),

/***/ "./src/Screens/CourseBody/Common/Containers/CourseBodyPage.tsx":
/*!*********************************************************************!*\
  !*** ./src/Screens/CourseBody/Common/Containers/CourseBodyPage.tsx ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var _Instructor_Containers_CourseBodyInstructorPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Instructor/Containers/CourseBodyInstructorPage */ "./src/Screens/CourseBody/Instructor/Containers/CourseBodyInstructorPage.tsx");
/* harmony import */ var _Student_Containers_CourseBodyStudentPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Student/Containers/CourseBodyStudentPage */ "./src/Screens/CourseBody/Student/Containers/CourseBodyStudentPage.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();




var CourseBodyPage = /** @class */ (function (_super) {
    __extends(CourseBodyPage, _super);
    function CourseBodyPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseBodyPage.prototype.render = function () {
        switch (Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode) {
            case 'I':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Instructor_Containers_CourseBodyInstructorPage__WEBPACK_IMPORTED_MODULE_2__["default"], { match: this.props.match });
            default:
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Student_Containers_CourseBodyStudentPage__WEBPACK_IMPORTED_MODULE_3__["default"], { match: this.props.match });
        }
    };
    return CourseBodyPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseBodyPage);


/***/ }),

/***/ "./src/Screens/CourseBody/Instructor/Assets/CourseBodyInstructorPage.sass":
/*!********************************************************************************!*\
  !*** ./src/Screens/CourseBody/Instructor/Assets/CourseBodyInstructorPage.sass ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CourseBody/Instructor/Components/CourseContent.tsx":
/*!************************************************************************!*\
  !*** ./src/Screens/CourseBody/Instructor/Components/CourseContent.tsx ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Screens_CoursePlaylist_Instructor_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens/CoursePlaylist/Instructor/Containers/CoursePlaylist */ "./src/Screens/CoursePlaylist/Instructor/Containers/CoursePlaylist.tsx");
/* harmony import */ var Screens_CourseInfo_Instructor_CourseInfo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Screens/CourseInfo/Instructor/CourseInfo */ "./src/Screens/CourseInfo/Instructor/CourseInfo.tsx");
/* harmony import */ var Screens_Forum_Instructor_Forum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/Forum/Instructor/Forum */ "./src/Screens/Forum/Instructor/Forum.tsx");
/* harmony import */ var Screens_Progress_Instructor_Progress__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/Progress/Instructor/Progress */ "./src/Screens/Progress/Instructor/Progress.tsx");
/* harmony import */ var Screens_CourseStudent_Instructor_CourseStudent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Screens/CourseStudent/Instructor/CourseStudent */ "./src/Screens/CourseStudent/Instructor/CourseStudent.tsx");
/* harmony import */ var Screens_PageAdditionForm_Instructor_PageAdditionForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Screens/PageAdditionForm/Instructor/PageAdditionForm */ "./src/Screens/PageAdditionForm/Instructor/PageAdditionForm.tsx");
/* harmony import */ var Screens_CourseSettings_Instructor_CourseSettings__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Screens/CourseSettings/Instructor/CourseSettings */ "./src/Screens/CourseSettings/Instructor/CourseSettings.tsx");
/* harmony import */ var Screens_ChatroomForm_Instructor_ChatroomForm__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Screens/ChatroomForm/Instructor/ChatroomForm */ "./src/Screens/ChatroomForm/Instructor/ChatroomForm.tsx");
/* harmony import */ var Screens_ExamsTab_Instructor_ExamsTab__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Screens/ExamsTab/Instructor/ExamsTab */ "./src/Screens/ExamsTab/Instructor/ExamsTab.tsx");
/* harmony import */ var Screens_Rga_Instructor_Rga__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Screens/Rga/Instructor/Rga */ "./src/Screens/Rga/Instructor/Rga.tsx");
/* harmony import */ var Screens_StudentActivityOptionsList_Instructor_StudentActivityOptionsList__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Screens/StudentActivityOptionsList/Instructor/StudentActivityOptionsList */ "./src/Screens/StudentActivityOptionsList/Instructor/StudentActivityOptionsList.tsx");
/* harmony import */ var Screens_TimedQuizAdmin_Instructor_TimedQuizAdmin__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Screens/TimedQuizAdmin/Instructor/TimedQuizAdmin */ "./src/Screens/TimedQuizAdmin/Instructor/TimedQuizAdmin.tsx");
/* harmony import */ var Screens_Marks_Instructor_Marks__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Screens/Marks/Instructor/Marks */ "./src/Screens/Marks/Instructor/Marks.tsx");
/* harmony import */ var Screens_Schedule_Instructor_Schedule__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! Screens/Schedule/Instructor/Schedule */ "./src/Screens/Schedule/Instructor/Schedule.tsx");
/* harmony import */ var Screens_EmailArchive_Instructor_EmailArchive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! Screens/EmailArchive/Instructor/EmailArchive */ "./src/Screens/EmailArchive/Instructor/EmailArchive.tsx");
/* harmony import */ var Screens_Page_Instructor_Page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! Screens/Page/Instructor/Page */ "./src/Screens/Page/Instructor/Page.tsx");
/* harmony import */ var _Assets_CourseBodyInstructorPage_sass__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../Assets/CourseBodyInstructorPage.sass */ "./src/Screens/CourseBody/Instructor/Assets/CourseBodyInstructorPage.sass");
/* harmony import */ var _Assets_CourseBodyInstructorPage_sass__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_Assets_CourseBodyInstructorPage_sass__WEBPACK_IMPORTED_MODULE_17__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// Components
















// Assets

;
var CourseContent = /** @class */ (function (_super) {
    __extends(CourseContent, _super);
    function CourseContent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseContent.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, this.getIntendedComponent(this.props.id));
    };
    CourseContent.prototype.getIntendedComponent = function (component_id) {
        switch (component_id) {
            case 'content':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursePlaylist_Instructor_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__["default"], { courseid: this.props.courseid, groupid: this.props.groupid });
            case 'information':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CourseInfo_Instructor_CourseInfo__WEBPACK_IMPORTED_MODULE_2__["default"], { course: this.props.courseid });
            case 'discussion-forum':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Forum_Instructor_Forum__WEBPACK_IMPORTED_MODULE_3__["default"], { id: this.props.forumid });
            case 'progress':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Progress_Instructor_Progress__WEBPACK_IMPORTED_MODULE_4__["default"], { course: this.props.courseid });
            case 'students':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CourseStudent_Instructor_CourseStudent__WEBPACK_IMPORTED_MODULE_5__["default"], { courseid: this.props.courseid });
            case 'add-page':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_PageAdditionForm_Instructor_PageAdditionForm__WEBPACK_IMPORTED_MODULE_6__["default"], { heading: 'Add Page', saveCallBack: this.props.callback });
            case 'settings':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CourseSettings_Instructor_CourseSettings__WEBPACK_IMPORTED_MODULE_7__["default"], { courseid: this.props.courseid });
            case 'chat':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_ChatroomForm_Instructor_ChatroomForm__WEBPACK_IMPORTED_MODULE_8__["default"], { teacher: true, course: this.props.courseid });
            case 'exams':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: "exams-tab" + this.props.courseid },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_ExamsTab_Instructor_ExamsTab__WEBPACK_IMPORTED_MODULE_9__["default"], { courseid: this.props.courseid }));
            case 'report-grading':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Rga_Instructor_Rga__WEBPACK_IMPORTED_MODULE_10__["default"], { courseid: this.props.courseid });
            case 'student-activity':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_StudentActivityOptionsList_Instructor_StudentActivityOptionsList__WEBPACK_IMPORTED_MODULE_11__["default"], { courseid: this.props.courseid });
            case 'timed-quiz':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_TimedQuizAdmin_Instructor_TimedQuizAdmin__WEBPACK_IMPORTED_MODULE_12__["default"], { courseid: this.props.courseid });
            case 'marks':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Marks_Instructor_Marks__WEBPACK_IMPORTED_MODULE_13__["default"], { courseid: this.props.courseid });
            case 'schedule':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Schedule_Instructor_Schedule__WEBPACK_IMPORTED_MODULE_14__["default"], { course: this.props.courseid });
            case 'email':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_EmailArchive_Instructor_EmailArchive__WEBPACK_IMPORTED_MODULE_15__["default"], { courseid: this.props.courseid });
            default:
                if (this.props.id > 0)
                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Page_Instructor_Page__WEBPACK_IMPORTED_MODULE_16__["default"], { id: this.props.id, deleteCallBack: this.props.deleteCallBack });
                window.history.replaceState('', '', "/courseware/course/" + this.props.courseid + "/content/");
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursePlaylist_Instructor_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__["default"], { groupid: this.props.groupid, courseid: this.props.courseid });
        }
    };
    return CourseContent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseContent);
;


/***/ }),

/***/ "./src/Screens/CourseBody/Instructor/Containers/CourseBodyInstructorPage.tsx":
/*!***********************************************************************************!*\
  !*** ./src/Screens/CourseBody/Instructor/Containers/CourseBodyInstructorPage.tsx ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var _Components_CourseContent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Components/CourseContent */ "./src/Screens/CourseBody/Instructor/Components/CourseContent.tsx");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
/* harmony import */ var Utils_GetCookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Utils/GetCookie */ "./src/Utils/GetCookie.js");
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_icons_md__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/md */ "./node_modules/react-icons/md/index.esm.js");
/* harmony import */ var react_icons_ri__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-icons/ri */ "./node_modules/react-icons/ri/index.esm.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// Components

// Utils






var CourseBodyInstructorPage = /** @class */ (function (_super) {
    __extends(CourseBodyInstructorPage, _super);
    function CourseBodyInstructorPage(props) {
        var _this = _super.call(this, props) || this;
        var pageRef = _this.props.match.params.pageRef;
        if (!pageRef) {
            pageRef = 'content';
        }
        _this.state = {
            pages: [],
            course: null,
            pageRef: pageRef,
            order: false
        };
        return _this;
    }
    CourseBodyInstructorPage.prototype.componentDidMount = function () {
        this.loadData();
    };
    CourseBodyInstructorPage.prototype.componentDidUpdate = function () {
        var sideListNode = document.getElementById('sideList');
        var selectedLinkNodes = sideListNode.querySelectorAll('li.selectedLink');
        if (selectedLinkNodes) {
            Array.prototype.forEach.call(selectedLinkNodes, function (node) {
                node.classList.remove('selectedLink');
            });
        }
        sideListNode = document.getElementById("sideList-" + this.state.pageRef);
        if (sideListNode) {
            sideListNode.classList.add('selectedLink');
        }
        var pageNode = document.getElementsByClassName('sortable-items')[0];
        if (pageNode) {
            if (this.state.order) {
                pageNode.classList.add('sortable-items');
            }
            else {
                pageNode.classList.remove('sortable-items');
            }
        }
    };
    CourseBodyInstructorPage.prototype.loadData = function () {
        var _this = this;
        var url = "/courseware/api/course/" + this.props.match.params.courseId + "/data/?format=json";
        fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (response) {
            _this.setState({
                course: response['course'],
                pages: response['pages']
            });
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    CourseBodyInstructorPage.prototype.addPage = function (data) {
        var _this = this;
        var url = "/courseware/api/course/" + this.props.match.params.courseId + "/add_page/?format=json";
        fetch(url, {
            method: 'POST',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_4__["default"])('csrftoken') },
            body: JSON.stringify(data)
        })
            .then(function (response) { return response.json(); })
            .then(function (response) {
            var pages = _this.state.pages;
            pages.push(response);
            _this.setState({
                pages: pages,
                pageRef: response.id
            });
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    CourseBodyInstructorPage.prototype.saveOrder = function (data) {
        var url = "/courseware/api/course/" + this.props.match.params.courseId + "/reorder_pages/?format=json";
        fetch(url, {
            method: 'PATCH',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_4__["default"])('csrftoken') },
            body: JSON.stringify(data)
        })
            .then(function () {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_3__["default"])('Successfully reordered the pages', 'success');
            // this.saveOrderSuccess('#sideList');
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    CourseBodyInstructorPage.prototype.handleDeletePage = function (pageid) {
        var pageNode = document.getElementById("sideList-" + pageid);
        pageNode.parentNode.removeChild(pageNode);
        window.history.pushState('', '', "/frontend/courseware/course/" + this.props.match.params.courseId + "/content/");
        this.setState({ pageRef: 'content' });
    };
    CourseBodyInstructorPage.prototype.handleClick = function (pageRef) {
        window.history.pushState('', '', "/frontend/courseware/course/" + this.props.match.params.courseId + "/" + pageRef + "/");
        var sideListNode = document.getElementById('sideList');
        var selectedLinkNodes = sideListNode.querySelectorAll('li.selectedLink');
        if (selectedLinkNodes) {
            Array.prototype.forEach.call(selectedLinkNodes, function (node) {
                node.classList.remove('selectedLink');
            });
        }
        sideListNode = document.getElementById("sideList-" + pageRef);
        if (sideListNode) {
            sideListNode.classList.add('selectedLink');
        }
        this.setState({ pageRef: pageRef });
    };
    CourseBodyInstructorPage.prototype.myhandleSortClick = function () {
        // this.handleSortClick('#sideList', 'li.page');
    };
    CourseBodyInstructorPage.prototype.myhandleSaveOrder = function () {
        // this.handleSaveOrder('#sideList', 9);
    };
    CourseBodyInstructorPage.prototype.goToAssignmentsUrl = function () {
        window.location.href = "/assignments/" + this.props.match.params.courseId;
    };
    CourseBodyInstructorPage.prototype.render = function () {
        var _this = this;
        console.log(this.state.course);
        if (!this.state.course) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Loading...");
        }
        var pages = this.state.pages.map(function (page) { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: "p_" + page.id, id: "sideList-" + page.id, onClick: function () { return _this.handleClick(page.id); }, className: 'page' }, page.title)); });
        var page_order = null;
        if (pages.length > 0) {
            if (!this.state.order) {
                page_order = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { id: 'pageReorder', className: 'pagesort-btn', onClick: this.myhandleSortClick },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'glyphicon glyphicon-sort' }),
                    "\u00A0\u00A0Reorder Pages"));
            }
            else {
                page_order = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { id: 'pageOrderSave', className: 'pagesort-btn', onClick: this.myhandleSaveOrder },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'glyphicon glyphicon-save' }),
                    "\u00A0\u00A0Save Current Order"));
            }
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_5__["default"], { navbarClass: "navbar-light" }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "courseHeader" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "container" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-8 courseHeaderTitle" }, this.state.course.title)))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'courseBody', className: 'd-flex flex-row container' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-xl-3 sideListContainer' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { id: 'sideList', role: 'menu', "aria-labelledby": 'dLabel' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Course Content"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Multimedia Book"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Assignments"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-3", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Programming"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-3", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Other Labs"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "About Course"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Interactions"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('schedule'); }, id: 'sideList-schedule' }, "Schedule"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('discussion-forum'); }, id: 'sideList-discussion-forum' }, "Discussion Forum"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('email'); }, id: 'sideList-email' }, "Email + Archive"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Performance"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('exams'); }, id: 'sideList-progress' }, "In/out video quizzes"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('marks'); }, id: 'sideList-progress' }, "Grades"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('progress'); }, id: 'sideList-progress' }, "Leaderboard"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Course Management"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('settings'); }, id: 'sideList-settings' }, "Settings"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('information'); }, id: 'sideList-information' }, "People"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-white d-flex justify-content-between" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { variant: "light", id: 'sideList-add-page', className: "btn btn-sm border" },
                                " ",
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_ri__WEBPACK_IMPORTED_MODULE_8__["RiFileAddLine"], { size: 18, color: "#3f3f3f", className: "mr-2" }),
                                "Add Page"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { variant: "light", className: "btn btn-sm border" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_md__WEBPACK_IMPORTED_MODULE_7__["MdImportExport"], { color: "#3f3f3f", size: 20 }))),
                        page_order,
                        pages)),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'course-content-div', className: 'col-xl-9' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_CourseContent__WEBPACK_IMPORTED_MODULE_2__["default"], { id: this.state.pageRef, courseid: this.props.match.params.courseId, forumid: null, callback: this.addPage, groupid: null, deleteCallBack: this.handleDeletePage })))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyInstructorPage.prototype, "loadData", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyInstructorPage.prototype, "addPage", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyInstructorPage.prototype, "saveOrder", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyInstructorPage.prototype, "handleDeletePage", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyInstructorPage.prototype, "handleClick", null);
    return CourseBodyInstructorPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseBodyInstructorPage);
;


/***/ }),

/***/ "./src/Screens/CourseBody/Student/Assets/CourseBodyStudentPage.sass":
/*!**************************************************************************!*\
  !*** ./src/Screens/CourseBody/Student/Assets/CourseBodyStudentPage.sass ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CourseBody/Student/Components/CourseContent.tsx":
/*!*********************************************************************!*\
  !*** ./src/Screens/CourseBody/Student/Components/CourseContent.tsx ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Screens_CoursePlaylist_Student_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens/CoursePlaylist/Student/Containers/CoursePlaylist */ "./src/Screens/CoursePlaylist/Student/Containers/CoursePlaylist.tsx");
/* harmony import */ var Screens_CourseInfo_Student_CourseInfo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Screens/CourseInfo/Student/CourseInfo */ "./src/Screens/CourseInfo/Student/CourseInfo.tsx");
/* harmony import */ var Screens_Forum_Student_Forum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/Forum/Student/Forum */ "./src/Screens/Forum/Student/Forum.tsx");
/* harmony import */ var Screens_Progress_Student_Progress__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/Progress/Student/Progress */ "./src/Screens/Progress/Student/Progress.tsx");
/* harmony import */ var Screens_PublicProgress_Student_PublicProgress__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Screens/PublicProgress/Student/PublicProgress */ "./src/Screens/PublicProgress/Student/PublicProgress.tsx");
/* harmony import */ var Screens_ChatroomForm_Student_ChatroomForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Screens/ChatroomForm/Student/ChatroomForm */ "./src/Screens/ChatroomForm/Student/ChatroomForm.tsx");
/* harmony import */ var Screens_Rga_Student_Rga__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Screens/Rga/Student/Rga */ "./src/Screens/Rga/Student/Rga.tsx");
/* harmony import */ var Screens_AssignmentsGroup_Student_AssignmentsGroup__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Screens/AssignmentsGroup/Student/AssignmentsGroup */ "./src/Screens/AssignmentsGroup/Student/AssignmentsGroup.tsx");
/* harmony import */ var Screens_TimedQuiz_Student_TimedQuiz__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! Screens/TimedQuiz/Student/TimedQuiz */ "./src/Screens/TimedQuiz/Student/TimedQuiz.tsx");
/* harmony import */ var Screens_Marks_Student_Marks__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! Screens/Marks/Student/Marks */ "./src/Screens/Marks/Student/Marks.tsx");
/* harmony import */ var Screens_Schedule_Student_Schedule__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! Screens/Schedule/Student/Schedule */ "./src/Screens/Schedule/Student/Schedule.tsx");
/* harmony import */ var Screens_EmailStudentArchive_Student_EmailStudentArchive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! Screens/EmailStudentArchive/Student/EmailStudentArchive */ "./src/Screens/EmailStudentArchive/Student/EmailStudentArchive.tsx");
/* harmony import */ var Screens_Page_Student_Page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! Screens/Page/Student/Page */ "./src/Screens/Page/Student/Page.tsx");
/* harmony import */ var _Assets_CourseBodyStudentPage_sass__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../Assets/CourseBodyStudentPage.sass */ "./src/Screens/CourseBody/Student/Assets/CourseBodyStudentPage.sass");
/* harmony import */ var _Assets_CourseBodyStudentPage_sass__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_Assets_CourseBodyStudentPage_sass__WEBPACK_IMPORTED_MODULE_14__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// Components













// Assets

var CourseContent = /** @class */ (function (_super) {
    __extends(CourseContent, _super);
    function CourseContent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseContent.prototype.getIntendedComponent = function (component_id) {
        switch (component_id) {
            case 'content':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursePlaylist_Student_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__["default"], { courseid: this.props.courseid, groupid: this.props.groupid });
            case 'information':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CourseInfo_Student_CourseInfo__WEBPACK_IMPORTED_MODULE_2__["default"], { course: this.props.courseid });
            case 'discussion-forum':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Forum_Student_Forum__WEBPACK_IMPORTED_MODULE_3__["default"], { id: this.props.forumid });
            case 'progress':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Progress_Student_Progress__WEBPACK_IMPORTED_MODULE_4__["default"], { course: this.props.courseid });
            case 'score-card':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_PublicProgress_Student_PublicProgress__WEBPACK_IMPORTED_MODULE_5__["default"], { course: this.props.courseid });
            case 'chat':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_ChatroomForm_Student_ChatroomForm__WEBPACK_IMPORTED_MODULE_6__["default"], { teacher: false, course: this.props.courseid });
            case 'report-grading':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Rga_Student_Rga__WEBPACK_IMPORTED_MODULE_7__["default"], { courseid: this.props.courseid });
            case 'assignments':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_AssignmentsGroup_Student_AssignmentsGroup__WEBPACK_IMPORTED_MODULE_8__["default"], { key: '0', group: { 'id': '0', 'title': 'Programming Assignments', }, courseid: this.props.courseid, index: 0 });
            case 'timed-quiz':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_TimedQuiz_Student_TimedQuiz__WEBPACK_IMPORTED_MODULE_9__["default"], { courseid: this.props.courseid });
            case 'marks':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Marks_Student_Marks__WEBPACK_IMPORTED_MODULE_10__["default"], { teacher: false, courseid: this.props.courseid });
            case 'schedule':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Schedule_Student_Schedule__WEBPACK_IMPORTED_MODULE_11__["default"], { course: this.props.courseid });
            case 'email':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_EmailStudentArchive_Student_EmailStudentArchive__WEBPACK_IMPORTED_MODULE_12__["default"], { courseid: this.props.courseid });
            default:
                if (this.props.id > 0)
                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Page_Student_Page__WEBPACK_IMPORTED_MODULE_13__["default"], { id: this.props.id });
                window.history.replaceState('', '', "/courseware/course/" + this.props.courseid + "/content/");
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursePlaylist_Student_Containers_CoursePlaylist__WEBPACK_IMPORTED_MODULE_1__["default"], { groupid: this.props.groupid, courseid: this.props.courseid });
        }
    };
    CourseContent.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, this.getIntendedComponent(this.props.id));
    };
    return CourseContent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseContent);
;


/***/ }),

/***/ "./src/Screens/CourseBody/Student/Containers/CourseBodyStudentPage.tsx":
/*!*****************************************************************************!*\
  !*** ./src/Screens/CourseBody/Student/Containers/CourseBodyStudentPage.tsx ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var _Components_CourseContent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Components/CourseContent */ "./src/Screens/CourseBody/Student/Components/CourseContent.tsx");
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// Components


var CourseBodyStudentPage = /** @class */ (function (_super) {
    __extends(CourseBodyStudentPage, _super);
    function CourseBodyStudentPage(props) {
        var _this = _super.call(this, props) || this;
        var pageRef = _this.props.match.params.pageRef;
        if (!pageRef) {
            pageRef = 'content';
        }
        _this.state = {
            course: null,
            pages: [],
            pageRef: pageRef
        };
        return _this;
    }
    CourseBodyStudentPage.prototype.componentDidMount = function () {
        this.loadData();
    };
    CourseBodyStudentPage.prototype.componentDidUpdate = function () {
        var sideListNode = document.getElementById('sideList');
        var selectedLinkNodes = sideListNode.querySelectorAll('li.selectedLink');
        if (selectedLinkNodes) {
            Array.prototype.forEach.call(selectedLinkNodes, function (node) {
                node.classList.remove('selectedLink');
            });
        }
        sideListNode = document.getElementById("sideList-" + this.state.pageRef);
        if (sideListNode) {
            sideListNode.classList.add('selectedLink');
        }
    };
    CourseBodyStudentPage.prototype.loadData = function () {
        var _this = this;
        var url = "/courseware/api/course/" + this.props.match.params.courseId + "/data/?format=json";
        fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (response) {
            _this.setState({
                course: response['course'],
                pages: response['pages']
            });
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    CourseBodyStudentPage.prototype.handleClick = function (pageRef) {
        window.history.pushState('', '', "/frontend/courseware/course/" + this.props.match.params.courseId + "/" + pageRef + "/");
        var sideListNode = document.getElementById('sideList');
        var selectedLinkNodes = sideListNode.querySelectorAll('li.selectedLink');
        if (selectedLinkNodes) {
            Array.prototype.forEach.call(selectedLinkNodes, function (node) {
                node.classList.remove('selectedLink');
            });
        }
        sideListNode = document.getElementById("sideList-" + pageRef);
        if (sideListNode) {
            sideListNode.classList.add('selectedLink');
        }
        this.setState({ pageRef: pageRef });
    };
    CourseBodyStudentPage.prototype.render = function () {
        var _this = this;
        if (!this.state.course) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Loading...");
        }
        var pages = this.state.pages.map(function (page) { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { id: "sideList-" + page.id, key: "p_" + page.id, onClick: function () { return _this.handleClick(page.id); }, className: 'page' }, page.title)); });
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_3__["default"], { navbarClass: 'navbar-light' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "courseHeader" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "container" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-8 courseHeaderTitle" }, this.state.course.title)))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'courseBody', className: 'd-flex flex-row container' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-xl-3 sideListContainer' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { id: 'sideList', role: 'menu', "aria-labelledby": 'dLabel' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Course Content"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Multimedia Book"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Assignments"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-3", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Programming"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-3", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "Other Labs"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('content'); }, id: 'sideList-content' }, "About Course"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Interactions"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('schedule'); }, id: 'sideList-schedule' }, "Schedule"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('discussion-forum'); }, id: 'sideList-discussion-forum' }, "Discussion Forum"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('email'); }, id: 'sideList-email' }, "Email + Archive"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Performance"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('exams'); }, id: 'sideList-progress' }, "In/out video quizzes"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('marks'); }, id: 'sideList-progress' }, "Grades"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-2", onClick: function () { return _this.handleClick('progress'); }, id: 'sideList-progress' }, "Leaderboard"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: "level-1" }, "Additional Pages"),
                        pages)),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: 'course-content-div', className: 'col-xl-9' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_CourseContent__WEBPACK_IMPORTED_MODULE_2__["default"], { id: this.state.pageRef, courseid: this.props.match.params.courseId, forumid: null, groupid: null })))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CourseBodyStudentPage.prototype, "loadData", null);
    return CourseBodyStudentPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseBodyStudentPage);
;


/***/ }),

/***/ "./src/Screens/CourseInfo/Instructor/CourseInfo.tsx":
/*!**********************************************************!*\
  !*** ./src/Screens/CourseInfo/Instructor/CourseInfo.tsx ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var CourseInfo = /** @class */ (function (_super) {
    __extends(CourseInfo, _super);
    function CourseInfo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseInfo.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "CourseInfo PlaceHolder");
    };
    return CourseInfo;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseInfo);
;


/***/ }),

/***/ "./src/Screens/CourseInfo/Student/CourseInfo.tsx":
/*!*******************************************************!*\
  !*** ./src/Screens/CourseInfo/Student/CourseInfo.tsx ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var CourseInfo = /** @class */ (function (_super) {
    __extends(CourseInfo, _super);
    function CourseInfo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseInfo.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "CourseInfo PlaceHolder");
    };
    return CourseInfo;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseInfo);
;


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Actions/ActionTypes.ts":
/*!**********************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Actions/ActionTypes.ts ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'LOAD_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    LOAD_INSTRUCTOR_COURSE_GROUPS_FAILURE: 'LOAD_INSTRUCTOR_COURSE_GROUPS_FAILURE',
    ADD_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'ADD_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    EDIT_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'EDIT_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    DELETE_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'DELETE_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    REORDER_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'REORDER_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    REORDER_GLOBAL_INSTRUCTOR_COURSE_GROUPS_SUCCESS: 'REORDER_GLOBAL_INSTRUCTOR_COURSE_GROUPS_SUCCESS',
    LOAD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS: 'LOAD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS',
    LOAD_INSTRUCTOR_GROUP_CONCEPTS_FAILURE: 'LOAD_INSTRUCTOR_GROUP_CONCEPTS_FAILURE',
    DELETE_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS: 'DELETE_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS',
    ADD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS: 'ADD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS',
    PUBLISH_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS: 'PUBLISH_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS',
    REORDER_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS: 'REORDER_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS',
});


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Actions/Actions.ts":
/*!******************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Actions/Actions.ts ***!
  \******************************************************************/
/*! exports provided: loadCourseGroupsRequest, loadCourseGroupsSuccess, addCourseGroupRequest, addCourseGroupSuccess, editCourseGroupRequest, editCourseGroupSuccess, deleteCourseGroupRequest, deleteCourseGroupSuccess, reorderCourseGroupsRequest, reorderCourseGroupsSuccess, reorderGlobalRequest, reorderGlobalSuccess, loadGroupConceptsRequest, loadGroupConceptsSuccess, deleteGroupConceptsRequest, deleteGroupConceptsSuccess, publishGroupConceptsRequest, publishGroupConceptsSuccess, reorderGroupConceptsRequest, reorderGroupConceptsSuccess, addGroupConceptsRequest, addGroupConceptsSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCourseGroupsRequest", function() { return loadCourseGroupsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCourseGroupsSuccess", function() { return loadCourseGroupsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCourseGroupRequest", function() { return addCourseGroupRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCourseGroupSuccess", function() { return addCourseGroupSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editCourseGroupRequest", function() { return editCourseGroupRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editCourseGroupSuccess", function() { return editCourseGroupSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCourseGroupRequest", function() { return deleteCourseGroupRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCourseGroupSuccess", function() { return deleteCourseGroupSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderCourseGroupsRequest", function() { return reorderCourseGroupsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderCourseGroupsSuccess", function() { return reorderCourseGroupsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderGlobalRequest", function() { return reorderGlobalRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderGlobalSuccess", function() { return reorderGlobalSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadGroupConceptsRequest", function() { return loadGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadGroupConceptsSuccess", function() { return loadGroupConceptsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteGroupConceptsRequest", function() { return deleteGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteGroupConceptsSuccess", function() { return deleteGroupConceptsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publishGroupConceptsRequest", function() { return publishGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publishGroupConceptsSuccess", function() { return publishGroupConceptsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderGroupConceptsRequest", function() { return reorderGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reorderGroupConceptsSuccess", function() { return reorderGroupConceptsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addGroupConceptsRequest", function() { return addGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addGroupConceptsSuccess", function() { return addGroupConceptsSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/CoursePlaylist/Instructor/Actions/ActionTypes.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
/* harmony import */ var Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/PatchData */ "./src/Utils/PatchData.js");
/* harmony import */ var Utils_DeleteData__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Utils/DeleteData */ "./src/Utils/DeleteData.js");





//Groups Actions
function loadCourseGroupsRequest(courseid) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseid + "/groups/?format=json";
        return fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (response) {
            var groups = response === "" ? [] : response;
            dispatch(loadCourseGroupsSuccess(courseid, groups));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Group Loading Failed", 'error');
            throw (error);
        });
    };
}
function loadCourseGroupsSuccess(courseid, groups) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        groups: groups
    };
}
function addCourseGroupRequest(courseid, data) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseid + "/add_group/?format=json";
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(url, data, "FormData")
            .then(function (response) {
            dispatch(addCourseGroupSuccess(courseid, response));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Chapter Added Successfully at the bottom', 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter Adding Failed", 'error');
            throw (error);
        });
    };
}
function addCourseGroupSuccess(courseid, group) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        group: group
    };
}
function editCourseGroupRequest(courseid, groupid, data) {
    return function (dispatch) {
        var url = "/courseware/api/group/" + groupid + "/?format=json";
        return Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, data, "FormData")
            .then(function (response) {
            dispatch(editCourseGroupSuccess(courseid, groupid, response));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Chapter info Updated Successfully', 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter editing Failed", 'error');
            throw (error);
        });
    };
}
function editCourseGroupSuccess(courseid, groupid, data) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].EDIT_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        groupid: groupid,
        data: data
    };
}
function deleteCourseGroupRequest(courseid, groupid) {
    return function (dispatch) {
        var url = "/courseware/api/group/" + groupid + "/?format=json";
        return Object(Utils_DeleteData__WEBPACK_IMPORTED_MODULE_4__["default"])(url, {})
            .then(function (response) {
            dispatch(deleteCourseGroupSuccess(courseid, groupid));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Chapter Deleted Successfully', 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter delete Failed", 'error');
            throw (error);
        });
    };
}
function deleteCourseGroupSuccess(courseid, groupid) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DELETE_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        groupid: groupid
    };
}
function reorderCourseGroupsRequest(courseid, formData, groups) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseid + "/reorder_groups/?format=json";
        return Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, formData, "FormData")
            .then(function (response) {
            dispatch(reorderCourseGroupsSuccess(courseid, groups));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter reordered Successfully", 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter reorder Failed", 'error');
            throw (error);
        });
    };
}
function reorderCourseGroupsSuccess(courseid, groups) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].REORDER_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        groups: groups
    };
}
// prashanth
function reorderGlobalRequest(courseid, payload) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseid + "/reorder_global/?format=json";
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(url, payload)
            .then(function (response) {
            if (response['success']) {
                dispatch(reorderGlobalSuccess(courseid));
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter reordered Successfully", 'success');
            }
            else {
                throw (response);
            }
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Chapter reorder Failed", 'error');
            throw (error);
        });
    };
}
function reorderGlobalSuccess(courseid) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].REORDER_GLOBAL_INSTRUCTOR_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
    };
}
// Concept Actions
// load
function loadGroupConceptsRequest(groupid) {
    return function (dispatch) {
        var url = "/courseware/api/group/" + groupid + "/concepts/?format=json";
        return fetch(url)
            .then(function (res) { return res.json(); })
            .then(function (res) {
            var concepts = res === "" ? [] : res;
            dispatch(loadGroupConceptsSuccess(groupid, concepts));
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Load Concept Failed", "error");
            throw (err);
        });
    };
}
function loadGroupConceptsSuccess(groupid, concepts) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS,
        groupid: groupid,
        concepts: concepts
    };
}
// delete
function deleteGroupConceptsRequest(groupid, conceptid) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptid + "/?format=json";
        return Object(Utils_DeleteData__WEBPACK_IMPORTED_MODULE_4__["default"])(url, {})
            .then(function (response) {
            dispatch(deleteGroupConceptsSuccess(groupid, conceptid));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Concept Deleted Successfully', 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Concept delete Failed", 'error');
            throw (error);
        });
    };
}
function deleteGroupConceptsSuccess(groupid, conceptid) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DELETE_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS,
        conceptid: conceptid,
        groupid: groupid
    };
}
// publish
function publishGroupConceptsRequest(groupid, conceptid) {
    return function (dispatch) {
        var url = "/courseware/api/concept/" + conceptid + "/publish/?format=json";
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(url)
            .then(function (response) {
            dispatch(publishGroupConceptsSuccess(groupid, conceptid));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Concept publish Failed", 'error');
            throw (error);
        });
    };
}
function publishGroupConceptsSuccess(groupid, conceptid) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].PUBLISH_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS,
        conceptid: conceptid,
        groupid: groupid
    };
}
// reorder
function reorderGroupConceptsRequest(groupid, formData, concepts) {
    return function (dispatch) {
        var url = "/courseware/api/group/" + groupid + "/reorder_concepts/?format=json";
        return Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, formData, "FormData")
            .then(function (response) {
            dispatch(reorderGroupConceptsSuccess(groupid, concepts));
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Concept reordered Success", 'success');
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Concept reorder Failed", 'error');
            throw (error);
        });
    };
}
function reorderGroupConceptsSuccess(groupid, concepts) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].REORDER_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS,
        concepts: concepts,
        groupid: groupid
    };
}
// add
function addGroupConceptsRequest(groupid, type, sectionData, elemData) {
    return function (dispatch) {
        var secUrl = "/courseware/api/group/" + groupid + "/add_concept/?format=json";
        // make hit for adding section
        return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(secUrl, sectionData, 'FormData')
            .then(function (res) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Section created successfully', 'success');
            if (type !== 'section') {
                // make a hit for learning element
                var elemUrl = "/courseware/api/concept/" + res.id + "/add_" + type + "/?format=json";
                return Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])(elemUrl, elemData, 'FormData')
                    .then(function (elemRes) {
                    Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Learning element added successfully', 'success');
                    dispatch(addGroupConceptsSuccess(groupid, res));
                })
                    .catch(function (err) {
                    Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Learning element adding failed', 'error');
                });
            }
            else {
                dispatch(addGroupConceptsSuccess(groupid, res));
            }
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Adding Concept Failed', 'error');
            throw (err);
        });
    };
}
function addGroupConceptsSuccess(groupid, data) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_INSTRUCTOR_GROUP_CONCEPTS_SUCCESS,
        data: data,
        groupid: groupid
    };
}


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Assets/CoursePlayListInstructorPage.sass":
/*!****************************************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Assets/CoursePlayListInstructorPage.sass ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Assets/toggleSwitch.sass":
/*!************************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Assets/toggleSwitch.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Components/GenericForm.tsx":
/*!**************************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Components/GenericForm.tsx ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GenericForm = /** @class */ (function (_super) {
    __extends(GenericForm, _super);
    function GenericForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenericForm.prototype.handleSave = function (event) {
        event.preventDefault();
        var form = event.currentTarget;
        var formData = new FormData();
        formData.append('title', form.title.value.trim());
        formData.append('description', form.description.value.trim());
        this.props.saveCallBack(formData);
    };
    GenericForm.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], { onSubmit: this.handleSave },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { controlId: "title" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, null, "Title"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { size: "sm", required: true, type: "text", defaultValue: this.props.title, placeholder: "Enter title" })),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { controlId: "description" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, null, "Description"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, { size: "sm", as: "textarea", rows: 4, defaultValue: this.props.description, placeholder: "Enter Description" })),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"] },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], { sm: { span: 12 } },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], { className: "btn btn-sm", variant: "success", type: "submit" }, "Save"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], { className: "btn btn-sm mx-2", variant: "danger", onClick: this.props.closeCallBack }, "Cancel")))))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], GenericForm.prototype, "handleSave", null);
    return GenericForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (GenericForm);


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Components/Group.tsx":
/*!********************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Components/Group.tsx ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _GenericForm__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./GenericForm */ "./src/Screens/CoursePlaylist/Instructor/Components/GenericForm.tsx");
/* harmony import */ var Screens_Concept_Instructor_Components_AddElementForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Screens/Concept/Instructor/Components/AddElementForm */ "./src/Screens/Concept/Instructor/Components/AddElementForm.tsx");
/* harmony import */ var _Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Assets/toggleSwitch.sass */ "./src/Screens/CoursePlaylist/Instructor/Assets/toggleSwitch.sass");
/* harmony import */ var _Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
/* harmony import */ var react_icons_md__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-icons/md */ "./node_modules/react-icons/md/index.esm.js");
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-icons/fi */ "./node_modules/react-icons/fi/index.esm.js");
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-icons/fc */ "./node_modules/react-icons/fc/index.esm.js");
/* harmony import */ var react_icons_gr__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-icons/gr */ "./node_modules/react-icons/gr/index.esm.js");
/* harmony import */ var react_icons_bs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-icons/bs */ "./node_modules/react-icons/bs/index.esm.js");
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! dragula/dist/dragula.css */ "./node_modules/dragula/dist/dragula.css");
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursePlaylist/Instructor/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//Components


//Assets










// actions

var Group = /** @class */ (function (_super) {
    __extends(Group, _super);
    function Group(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            order: false,
            add: false,
            edit: false,
            type: undefined,
            open: false,
            prevopen: false
        };
        return _this;
    }
    Group.prototype.handleEdit = function (event) {
        event.stopPropagation();
        this.setState({
            edit: true
        });
    };
    Group.prototype.saveEdit = function (data) {
        var _this = this;
        this.props.editCallBack(this.props.group.id, data)
            .then(function (res) {
            _this.setState({ edit: false });
        });
    };
    Group.prototype.cancelEdit = function (event) {
        this.setState({
            edit: false
        });
    };
    Group.prototype.deleteGroup = function (event) {
        event.stopPropagation();
        this.props.deleteCallBack(this.props.group.id);
    };
    Group.prototype.componentDidMount = function () {
        if (!this.props.loaded) {
            this.props.actions.loadGroupConceptsRequest(this.props.group.id);
        }
        this.setState({
            open: this.props.index == 0,
            prevopen: this.props.index == 0
        });
    };
    Group.prototype.handleAddConcept = function (type, data) {
        var sectionData = undefined;
        var elemData = undefined;
        if (type === 'section') {
            sectionData = data;
        }
        else {
            elemData = data;
            sectionData = new FormData();
            sectionData.append('title', data.get('title'));
            sectionData.append('description', data.get('description'));
        }
        this.props.actions.addGroupConceptsRequest(this.props.group.id, type, sectionData, elemData);
        this.setState({
            type: undefined,
            add: false
        });
    };
    Group.prototype.openModal = function (eventKey, event) {
        console.log(eventKey);
        this.setState({
            add: true,
            type: eventKey
        });
    };
    Group.prototype.closeModal = function () {
        this.setState({
            add: false
        });
    };
    Group.prototype.publishAll = function (e) {
        var _this = this;
        e.stopPropagation();
        // publish all unpublished concept
        var conceptToPublish = this.props.concepts.filter(function (concept) { return !concept.is_published; });
        conceptToPublish.forEach(function (concept) {
            _this.handlePublishConcept(concept);
        });
    };
    Group.prototype.handlePublishConcept = function (concept) {
        this.props.actions.publishGroupConceptsRequest(this.props.group.id, concept.id);
    };
    Group.prototype.handleDeleteConcept = function (conceptid) {
        if (confirm('Are you sure ?'))
            this.props.actions.deleteGroupConceptsRequest(this.props.group.id, conceptid);
    };
    Group.prototype.saveOrder = function () {
        var _this = this;
        var playlist = this.conceptsCopy.map(function (c) { return c.id; });
        var formData = new FormData();
        formData.append('playlist', JSON.stringify(playlist));
        this.props.actions.reorderGroupConceptsRequest(this.props.group.id, formData, this.conceptsCopy)
            .catch(function (err) {
            _this.props.actions.loadGroupConceptsRequest(_this.props.group.id);
        })
            .then(function (res) {
            _this.setState({ order: false });
        });
    };
    Group.prototype.handleToggle = function (event) {
        var _this = this;
        this.setState(function (state) { return ({
            prevopen: _this.state.open,
            open: !_this.state.open
        }); });
        console.log(this.state.open);
    };
    // @autobind
    // public handleReorder() {
    //     if (!this.state.order) {
    //         this.conceptsCopy = [...this.props.concepts];
    //         this.idToConcept = {};
    //         this.props.concepts.map((c) => {
    //             this.idToConcept[c.id] = c;
    //         });
    //         this.dragulaService = dragula([document.getElementById(`conceptListContainer-${this.props.group.id}`)], {
    //             revertOnSpill: true,
    //             moves: function (el, container, handle) {
    //                 console.log(handle);
    //                 return handle.classList.contains(`conceptListHandle`);
    //             }
    //         }).on('drop', function (el, container, handle) {
    //             console.log('element', el);
    //             console.log('container', container);
    //             console.log('handle', handle);
    //             this.conceptsCopy = Array.from(container.children).map(function (c) {
    //                 return this.idToConcept[c["id"]]
    //             }.bind(this))
    //         }.bind(this));
    //         this.setState({
    //             order: true
    //         });
    //     }
    //     else {
    //         this.dragulaService.destroy();
    //         this.saveOrder();
    //     }
    // }
    Group.prototype.render = function () {
        var _this = this;
        if (this.state.edit) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_GenericForm__WEBPACK_IMPORTED_MODULE_5__["default"], { heading: "Edit Chapter", title: this.props.group.title, description: this.props.group.description, saveCallBack: this.saveEdit, closeCallBack: this.cancelEdit });
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Card"], { id: this.props.group.id },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Card"].Header, eventKey: this.props.orderParent ? '-1' : "" + this.props.index, onClick: this.handleToggle },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { md: { span: 8 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", { className: "groupListHandle" },
                            this.props.orderParent ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_gr__WEBPACK_IMPORTED_MODULE_13__["GrDrag"], { size: 20, className: "groupListHandle", color: "#ccc" }) : '',
                            this.props.group.title)),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { md: { span: 4 }, className: "text-right" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Button"], { className: "btn btn-sm mx-2 shadow", onClick: this.publishAll, style: { backgroundColor: "darkcyan" } }, "Publish All"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Button"], { className: "btn btn-sm", variant: "light", onClick: this.handleEdit },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fa__WEBPACK_IMPORTED_MODULE_9__["FaEdit"], { size: 18 })),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Button"], { className: "btn btn-sm", variant: "light", onClick: this.deleteGroup },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_md__WEBPACK_IMPORTED_MODULE_10__["MdDelete"], { size: 18 })),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Button"], { className: "btn btn-sm", variant: "light" }, this.state.open ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_bs__WEBPACK_IMPORTED_MODULE_14__["BsFillCaretUpFill"], { size: 18 }) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_bs__WEBPACK_IMPORTED_MODULE_14__["BsFillCaretDownFill"], { size: 18 })))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { md: { span: 12 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", { style: { color: 'grey' } }, this.props.group.description)))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Accordion"].Collapse, { eventKey: this.props.orderChild ? "-1" : "" + this.props.index },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Card"].Body, { style: { marginLeft: '5%', width: '90%' } }, this.state.add ?
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Modal"], { show: this.state.add, onHide: this.closeModal },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Modal"].Header, { closeButton: true },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Modal"].Title, null, "Add " + this.state.type)),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Modal"].Body, null, this.state.type === 'section' ?
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_GenericForm__WEBPACK_IMPORTED_MODULE_5__["default"], { heading: "Add Section", title: "", description: "", saveCallBack: function (data) { return _this.handleAddConcept('section', data); }, closeCallBack: this.closeModal })
                            :
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Concept_Instructor_Components_AddElementForm__WEBPACK_IMPORTED_MODULE_6__["default"], { cancelCallback: this.closeModal, saveCallback: this.handleAddConcept, type: this.state.type })))
                    :
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-flex flex-row justify-content-between" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["DropdownButton"], { className: "shadow", drop: "right", size: "sm", variant: "primary", title: react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fi__WEBPACK_IMPORTED_MODULE_11__["FiPlusCircle"], { size: 15 }),
                                        " Add Content"), id: "dropdown-button-drop-right", onSelect: this.openModal },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Dropdown"].Item, { eventKey: "section" }, "Section"),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Dropdown"].Item, { eventKey: "document" }, "Document"),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Dropdown"].Item, { eventKey: "video" }, "Video"),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Dropdown"].Item, { eventKey: "quiz" }, "Quiz"))),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Container"], { style: { minHeight: '20px', padding: '0px' }, id: "conceptListContainer-" + this.props.group.id }, !this.props.loaded ? null :
                                this.props.concepts.map(function (concept, i) {
                                    var type_of_concept = 'section';
                                    var bit = 0;
                                    bit += (concept.quizzes.length != 0) ? 4 : 0;
                                    bit += (concept.videos.length != 0) ? 2 : 0;
                                    bit += (concept.pages.length != 0) ? 1 : 0;
                                    console.log(bit);
                                    type_of_concept = bit == 4 ? 'quiz' : type_of_concept;
                                    type_of_concept = bit == 2 ? 'video' : type_of_concept;
                                    type_of_concept = bit == 1 ? 'page' : type_of_concept;
                                    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Row"], { key: concept.id, id: "" + concept.id, className: "border no-gutters", style: { padding: '1%' } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { xs: { span: 1 } }, _this.state.order || _this.props.orderChild ?
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_gr__WEBPACK_IMPORTED_MODULE_13__["GrDrag"], { size: 20, className: "conceptListHandle", color: "#ccc" }) : ''),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { xs: { span: 1 } },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_12__["FcFolder"], { className: type_of_concept == 'section' ? '' : "d-none", size: 20 }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_12__["FcDocument"], { className: type_of_concept == 'page' ? '' : "d-none", size: 20 }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_12__["FcCamcorderPro"], { className: type_of_concept == 'video' ? '' : "d-none", size: 20 }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_12__["FcQuestions"], { className: type_of_concept == 'quiz' ? '' : "d-none", size: 20 })),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { xs: { span: 5 }, className: "align-items-center" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], { className: "text-dark align-middle", to: "/frontend/concept/" + concept.id }, concept.title)),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Col"], { xs: { span: 5 }, className: "justify-content-center" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-flex flex-row align-items-center justify-content-end" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "align-middle", style: { fontSize: '15px' } }, "Published :"),
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: "switch" },
                                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { className: "switch-input", type: "checkbox", onClick: function (e) { _this.handlePublishConcept(concept); }, checked: concept.is_published, id: "publishToggle" }),
                                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "switch-label", "data-on": "Yes", "data-off": "No" }),
                                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "switch-handle" })),
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_8__["Button"], { className: "btn btn-sm mx-3", variant: "light", onClick: function (e) { _this.handleDeleteConcept(concept.id); } },
                                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_md__WEBPACK_IMPORTED_MODULE_10__["MdDelete"], { size: 18 }))))));
                                })))))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "handleEdit", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "saveEdit", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "cancelEdit", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "deleteGroup", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "handleAddConcept", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "openModal", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "closeModal", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "publishAll", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "handlePublishConcept", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "handleDeleteConcept", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "saveOrder", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], Group.prototype, "handleToggle", null);
    return Group;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
var mapStateToProps = function (state, _a) {
    var group = _a.group, index = _a.index, orderParent = _a.orderParent, orderChild = _a.orderChild, deleteCallBack = _a.deleteCallBack, editCallBack = _a.editCallBack;
    var common = {
        group: group,
        index: index,
        orderChild: orderChild,
        orderParent: orderParent,
        deleteCallBack: deleteCallBack,
        editCallBack: editCallBack
    };
    var sGC = state.instructorCourseGC;
    if (sGC.instructorGroupConceptsMap.has(group.id)) {
        return __assign({ loaded: true, concepts: sGC.instructorGroupConceptsMap.get(group.id) }, common);
    }
    else {
        return __assign({ loaded: false, concepts: [] }, common);
    }
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_3__["bindActionCreators"])({
        loadGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__["loadGroupConceptsRequest"],
        deleteGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__["deleteGroupConceptsRequest"],
        publishGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__["publishGroupConceptsRequest"],
        reorderGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__["reorderGroupConceptsRequest"],
        addGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_16__["addGroupConceptsRequest"],
    }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_4__["connect"])(mapStateToProps, mapDispatchToProps)(Group));


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Instructor/Containers/CoursePlaylist.tsx":
/*!*****************************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Instructor/Containers/CoursePlaylist.tsx ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Components_GenericForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Components/GenericForm */ "./src/Screens/CoursePlaylist/Instructor/Components/GenericForm.tsx");
/* harmony import */ var _Components_Group__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Components/Group */ "./src/Screens/CoursePlaylist/Instructor/Components/Group.tsx");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/fi */ "./node_modules/react-icons/fi/index.esm.js");
/* harmony import */ var react_icons_io__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-icons/io */ "./node_modules/react-icons/io/index.esm.js");
/* harmony import */ var react_icons_md__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-icons/md */ "./node_modules/react-icons/md/index.esm.js");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursePlaylist/Instructor/Actions/Actions.ts");
/* harmony import */ var dragula__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! dragula */ "./node_modules/dragula/dragula.js");
/* harmony import */ var dragula__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(dragula__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! dragula/dist/dragula.css */ "./node_modules/dragula/dist/dragula.css");
/* harmony import */ var dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(dragula_dist_dragula_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _Assets_CoursePlayListInstructorPage_sass__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Assets/CoursePlayListInstructorPage.sass */ "./src/Screens/CoursePlaylist/Instructor/Assets/CoursePlayListInstructorPage.sass");
/* harmony import */ var _Assets_CoursePlayListInstructorPage_sass__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_Assets_CoursePlayListInstructorPage_sass__WEBPACK_IMPORTED_MODULE_13__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};




//Components


//Assets 




// actions

// dragula for reordering


// Assets

;
var CoursePlaylist = /** @class */ (function (_super) {
    __extends(CoursePlaylist, _super);
    function CoursePlaylist(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            showform: false,
            showMessage: false,
            order: false,
            orderGlobal: false
        };
        return _this;
    }
    CoursePlaylist.prototype.addgroupform = function () {
        this.setState({
            showform: true,
            showMessage: false
        });
    };
    CoursePlaylist.prototype.addGroup = function (data) {
        var _this = this;
        this.props.actions.addCourseGroupRequest(this.props.courseid, data)
            .then(function (res) {
            _this.setState({
                showform: false,
                showMessage: true
            });
        })
            .catch(function (err) { return console.log(err); });
    };
    CoursePlaylist.prototype.handleEditGroup = function (groupid, data) {
        return this.props.actions.editCourseGroupRequest(this.props.courseid, groupid, data);
    };
    CoursePlaylist.prototype.closePanel = function (event) {
        this.setState({
            showform: false
        });
    };
    CoursePlaylist.prototype.handleDeleteGroup = function (groupid) {
        if (confirm('Are you sure?'))
            this.props.actions.deleteCourseGroupRequest(this.props.courseid, groupid);
    };
    CoursePlaylist.prototype.componentDidMount = function () {
        if (!this.props.loaded) {
            this.props.actions.loadCourseGroupsRequest(this.props.courseid);
        }
    };
    CoursePlaylist.prototype.saveOrder = function () {
        var _this = this;
        var playlist = this.groupsCopy.map(function (g) { return g.id; });
        var formData = new FormData();
        formData.append('playlist', JSON.stringify(playlist));
        this.props.actions.reorderCourseGroupsRequest(this.props.courseid, formData, this.groupsCopy)
            .catch(function (err) {
            _this.props.actions.loadCourseGroupsRequest(_this.props.courseid);
        })
            .then(function (res) {
            _this.setState({ order: false });
        });
    };
    CoursePlaylist.prototype.handleOrder = function () {
        var _this = this;
        if (!this.state.order) {
            //store a copy 
            this.groupsCopy = __spreadArrays(this.props.groups);
            this.idToGroup = {};
            this.props.groups.forEach(function (g) {
                _this.idToGroup[g.id] = g;
            });
            this.dragulaService = dragula__WEBPACK_IMPORTED_MODULE_11__([document.getElementById("groupListContainer")], {
                revertOnSpill: true,
                moves: function (el, container, handle) {
                    console.log(handle);
                    return handle.classList.contains("groupListHandle");
                }
            }).on('drop', function (el, container, handle) {
                console.log('element', el);
                console.log('container', container);
                console.log('handle', handle);
                this.groupsCopy = Array.from(container.children).map(function (g) {
                    return this.idToGroup[g["id"]];
                }.bind(this));
            }.bind(this));
            this.setState({
                order: true
            });
        }
        else {
            this.dragulaService.destroy();
            this.saveOrder();
        }
    };
    CoursePlaylist.prototype.handleOrderGlobal = function () {
        if (!this.state.orderGlobal) {
            this.reorderTransitions = [];
            //store a copy for future use of cancel reorder
            this.groupConcepts = {};
            var containers = this.props.groups.map(function (g) {
                return document.getElementById("conceptListContainer-" + g.id);
            });
            this.dragulaService = dragula__WEBPACK_IMPORTED_MODULE_11__(containers, {
                revertOnSpill: true,
                moves: function (el, container, handle) {
                    console.log(handle);
                    return handle.classList.contains("conceptListHandle");
                }
            }).on('drop', function (el, container, handle) {
                console.log('element', el);
                console.log('container', container);
                console.log('handle', handle);
                // update groupstate and transitions
                var srcId = (handle.id).split('-')[1];
                var dstId = (container.id).split('-')[1];
                var elemId = el['id'];
                // prashanth
                var transition = {
                    'srcGroupId': srcId,
                    'dstGroupId': dstId,
                    'conceptId': elemId
                };
                this.reorderTransitions.push(transition);
                this.groupConcepts[srcId] = Array.from(handle.children).map(function (c) { return c['id']; });
                this.groupConcepts[dstId] = Array.from(container.children).map(function (c) { return c['id']; });
                console.log('current order=>', this.groupConcepts);
                console.log('current transitions=> ', this.reorderTransitions);
            }.bind(this));
            this.setState({
                orderGlobal: true
            });
        }
        else {
            this.dragulaService.destroy();
            var payload = {
                'transitions': this.reorderTransitions,
                'newGroupStates': this.groupConcepts
            };
            this.props.actions.reorderGlobalRequest(this.props.courseid, payload);
            this.setState({
                orderGlobal: false
            });
        }
    };
    CoursePlaylist.prototype.render = function () {
        var _this = this;
        var message = null;
        var addgroupform = null;
        var groups = null;
        var courses_list = [];
        var to_import_groups = [];
        if (this.state.showMessage) {
            message = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "alert alert-success alert-dismissable" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "close", "data-dismiss": "alert", "aria-hidden": "true" }, "\u00D7"),
                "You have successfully added the chapter at the bottom."));
        }
        if (this.state.showform) {
            addgroupform = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_GenericForm__WEBPACK_IMPORTED_MODULE_4__["default"], { heading: "Add Chapter", title: "", description: "", saveCallBack: this.addGroup, closeCallBack: this.closePanel });
        }
        if (!this.props.loaded) {
            groups = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "Loading...");
        }
        else {
            if (this.props.groups.length === 0) {
                groups = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "alert alert-danger alert-dismissable" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "close", "data-dismiss": "alert", "aria-hidden": "true" }, "\u00D7"),
                    "No content found in this course"));
            }
            else {
                /* doubt */
                if (this.props.groupid == "" || this.props.groupid == undefined) {
                    groups = this.props.groups.map(function (group, i) {
                        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Group__WEBPACK_IMPORTED_MODULE_5__["default"], { orderParent: _this.state.order, orderChild: _this.state.orderGlobal, key: group.id, deleteCallBack: _this.handleDeleteGroup, group: group, index: i, editCallBack: _this.handleEditGroup });
                    });
                }
                else {
                    // var check = this.props.groupid;
                    groups = this.props.groups.map(function (group, i) {
                        if (_this.props.groupid == group.id) {
                            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Group__WEBPACK_IMPORTED_MODULE_5__["default"], { orderParent: _this.state.order, orderChild: _this.state.orderGlobal, key: group.id, deleteCallBack: _this.handleDeleteGroup, group: group, index: i, editCallBack: _this.handleEditGroup });
                        }
                    });
                    groups = groups.filter(function (n) {
                        return n != undefined;
                    });
                    groups = groups[0];
                }
            }
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Container"], { className: "p-4 ml-3", style: { border: "1px solid rgb(190,190,190)" } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], { className: "d-flex align-items-center" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], { md: { span: 4 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", { style: { color: 'grey' } }, "Multimedia Book")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], { md: { span: 8 }, className: "text-right" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { className: "btn-sm mx-2 shadow", onClick: this.addgroupform },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fi__WEBPACK_IMPORTED_MODULE_7__["FiPlusCircle"], { size: 18 }),
                            ' ',
                            "Add Chapter")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { variant: "primary", style: { backgroundColor: 'white', color: 'black' }, className: "btn-sm" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_io__WEBPACK_IMPORTED_MODULE_8__["IoMdCloudUpload"], { size: 18 }),
                            ' ',
                            "Import Content")))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", { style: {
                    marginLeft: 5,
                    marginRight: 5,
                    height: '1px',
                    border: 'none',
                    backgroundColor: 'grey'
                } }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], { md: { span: 12 }, className: "d-flex justify-content-between" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { className: "btn-sm", onClick: this.handleOrderGlobal, style: { backgroundColor: 'white', color: 'black' } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_md__WEBPACK_IMPORTED_MODULE_9__["MdImportExport"], { size: 18 }),
                            ' ',
                            this.state.orderGlobal ? 'Save Order Global' : 'Reorder Global')),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Button"], { variant: "primary", className: "btn-sm", onClick: this.handleOrder, style: { backgroundColor: 'white', color: 'black' } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_md__WEBPACK_IMPORTED_MODULE_9__["MdImportExport"], { size: 18 }),
                            ' ',
                            this.state.order ? 'Save Order' : 'Reorder')))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], { md: { span: 12 }, className: "my-3" }, addgroupform)),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Col"], { md: { span: 12 } }, this.state.order || this.state.orderGlobal ?
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Accordion"], { activeKey: "-1", id: "groupListContainer" }, groups)
                    :
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_6__["Accordion"], { defaultActiveKey: "0", id: "groupListContainer" }, groups)))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "addgroupform", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "addGroup", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "handleEditGroup", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "closePanel", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "handleDeleteGroup", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "saveOrder", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "handleOrder", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursePlaylist.prototype, "handleOrderGlobal", null);
    return CoursePlaylist;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
var mapStateToProps = function (state, _a) {
    var courseid = _a.courseid, groupid = _a.groupid;
    var sGC = state.instructorCourseGC;
    if (sGC.instructorCourseGroupsMap.has(courseid)) {
        return {
            loaded: true,
            groups: sGC.instructorCourseGroupsMap.get(courseid),
            courseid: courseid,
            groupid: groupid
        };
    }
    else {
        return {
            loaded: false,
            groups: [],
            courseid: courseid,
            groupid: groupid
        };
    }
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_2__["bindActionCreators"])({
        loadCourseGroupsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["loadCourseGroupsRequest"],
        addCourseGroupRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["addCourseGroupRequest"],
        deleteCourseGroupRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["deleteCourseGroupRequest"],
        editCourseGroupRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["editCourseGroupRequest"],
        reorderCourseGroupsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["reorderCourseGroupsRequest"],
        reorderGlobalRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_10__["reorderGlobalRequest"]
    }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(mapStateToProps, mapDispatchToProps)(CoursePlaylist));


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Student/Actions/ActionTypes.ts":
/*!*******************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Student/Actions/ActionTypes.ts ***!
  \*******************************************************************/
/*! exports provided: studentCourseGroupsActionTypes, studentGroupConceptsActionTypes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "studentCourseGroupsActionTypes", function() { return studentCourseGroupsActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "studentGroupConceptsActionTypes", function() { return studentGroupConceptsActionTypes; });
var studentCourseGroupsActionTypes = {
    LOAD_STUDENT_COURSE_GROUPS_SUCCESS: 'LOAD_STUDENT_COURSE_GROUPS_SUCCESS',
    LOAD_STUDENT_COURSE_GROUPS_FAILURE: 'LOAD_STUDENT_COURSE_GROUPS_FAILURE',
};
var studentGroupConceptsActionTypes = {
    LOAD_STUDENT_GROUP_CONCEPTS_SUCCESS: 'LOAD_STUDENT_GROUP_CONCEPTS_SUCCESS',
    LOAD_STUDENT_GROUP_CONCEPTS_FAILURE: 'LOAD_STUDENT_GROUP_CONCEPTS_FAILURE',
};


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Student/Actions/Actions.ts":
/*!***************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Student/Actions/Actions.ts ***!
  \***************************************************************/
/*! exports provided: loadCourseGroupsRequest, loadCourseGroupsSuccess, loadGroupConceptsRequest, loadGroupConceptsSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCourseGroupsRequest", function() { return loadCourseGroupsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCourseGroupsSuccess", function() { return loadCourseGroupsSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadGroupConceptsRequest", function() { return loadGroupConceptsRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadGroupConceptsSuccess", function() { return loadGroupConceptsSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/CoursePlaylist/Student/Actions/ActionTypes.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");


function loadCourseGroupsRequest(courseid) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseid + "/groups/?format=json";
        return fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (response) {
            var groups = response === "" ? [] : response;
            dispatch(loadCourseGroupsSuccess(courseid, groups));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Group Loading Failed", 'error');
            throw (error);
        });
    };
}
function loadCourseGroupsSuccess(courseid, groups) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["studentCourseGroupsActionTypes"].LOAD_STUDENT_COURSE_GROUPS_SUCCESS,
        courseid: courseid,
        groups: groups
    };
}
function loadGroupConceptsRequest(groupid) {
    return function (dispatch) {
        var url = "/courseware/api/group/" + groupid + "/concepts/";
        console.log(url);
        return fetch(url)
            .then(function (res) { return res.json(); })
            .then(function (res) {
            var concepts = res === "" ? [] : res;
            dispatch(loadGroupConceptsSuccess(groupid, concepts));
        })
            .catch(function (err) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Load Concept Failed", "error");
            throw (err);
        });
    };
}
function loadGroupConceptsSuccess(groupid, concepts) {
    return {
        type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["studentGroupConceptsActionTypes"].LOAD_STUDENT_GROUP_CONCEPTS_SUCCESS,
        groupid: groupid,
        concepts: concepts
    };
}


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Student/Assets/toggleSwitch.sass":
/*!*********************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Student/Assets/toggleSwitch.sass ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursePlaylist/Student/Components/Group.tsx":
/*!*****************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Student/Components/Group.tsx ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Assets/toggleSwitch.sass */ "./src/Screens/CoursePlaylist/Student/Assets/toggleSwitch.sass");
/* harmony import */ var _Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Assets_toggleSwitch_sass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursePlaylist/Student/Actions/Actions.ts");
/* harmony import */ var react_icons_fc__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/fc */ "./node_modules/react-icons/fc/index.esm.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();




//Assets


// actions


var Group = /** @class */ (function (_super) {
    __extends(Group, _super);
    function Group() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Group.prototype.componentDidMount = function () {
        if (!this.props.loaded) {
            this.props.actions.loadGroupConceptsRequest(this.props.group.id);
        }
    };
    Group.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], { id: this.props.group.id },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Card"].Header, eventKey: "" + this.props.index },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { md: { span: 8 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", { className: "groupListHandle" }, this.props.group.title)),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { md: { span: 2 } }),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { md: { span: 1 } }),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { md: { span: 1 } })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { md: { span: 12 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", { style: { color: 'grey' } }, this.props.group.description)))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Accordion"].Collapse, { eventKey: "" + this.props.index },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Card"].Body, { style: { marginLeft: '5%', width: '90%' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Container"], { style: { minHeight: '20px' }, id: "conceptListContainer-" + this.props.group.id }, !this.props.loaded ? null :
                            this.props.concepts.map(function (concept, i) {
                                if (!concept.is_published) {
                                    return null;
                                }
                                var type_of_concept = 'section';
                                var bit = 0;
                                bit += (concept.quizzes.length != 0) ? 4 : 0;
                                bit += (concept.videos.length != 0) ? 2 : 0;
                                bit += (concept.pages.length != 0) ? 1 : 0;
                                console.log(bit);
                                type_of_concept = bit == 4 ? 'quiz' : type_of_concept;
                                type_of_concept = bit == 2 ? 'video' : type_of_concept;
                                type_of_concept = bit == 1 ? 'page' : type_of_concept;
                                console.log(concept);
                                var quizzes = concept.quizzes.map(function (quiz, j) {
                                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcQuestions"], { type: "button", size: 20, onClick: function () { return window.location.href = "/frontend/concept/" + concept.id; } });
                                });
                                var videos = concept.videos.map(function (quiz, j) {
                                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcCamcorderPro"], { type: "button", size: 20, onClick: function () { return window.location.href = "/frontend/concept/" + concept.id; } });
                                });
                                var pages = concept.pages.map(function (quiz, j) {
                                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcDocument"], { type: "button", size: 20, onClick: function () { return window.location.href = "/frontend/concept/" + concept.id; } });
                                });
                                return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], { key: concept.id, id: "" + concept.id, className: "border no-gutters", style: { padding: '1%' } },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { style: { marginLeft: '4%' }, xs: { span: 1 } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcFolder"], { className: type_of_concept == 'section' ? '' : "d-none", size: 20 }),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcDocument"], { className: type_of_concept == 'page' ? '' : "d-none", size: 20 }),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcCamcorderPro"], { className: type_of_concept == 'video' ? '' : "d-none", size: 20 }),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fc__WEBPACK_IMPORTED_MODULE_7__["FcQuestions"], { className: type_of_concept == 'quiz' ? '' : "d-none", size: 20 })),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { xs: { span: 7 } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], { to: "/frontend/concept/" + concept.id }, concept.title)),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], { xs: { span: 3 }, style: { overflowX: 'auto' } },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-inline-flex" },
                                            quizzes,
                                            videos,
                                            pages))));
                            })))))));
    };
    return Group;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
var mapStateToProps = function (state, _a) {
    var group = _a.group, index = _a.index;
    if (state.studentGroupConceptsMap.has(group.id)) {
        return {
            loaded: true,
            concepts: state.studentGroupConceptsMap.get(group.id),
            group: group,
            index: index
        };
    }
    else {
        return {
            loaded: false,
            concepts: [],
            index: index,
            group: group
        };
    }
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_2__["bindActionCreators"])({ loadGroupConceptsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_6__["loadGroupConceptsRequest"] }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(mapStateToProps, mapDispatchToProps)(Group));


/***/ }),

/***/ "./src/Screens/CoursePlaylist/Student/Containers/CoursePlaylist.tsx":
/*!**************************************************************************!*\
  !*** ./src/Screens/CoursePlaylist/Student/Containers/CoursePlaylist.tsx ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Components_Group__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Components/Group */ "./src/Screens/CoursePlaylist/Student/Components/Group.tsx");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursePlaylist/Student/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



//Components

//Assets 

// actions

;
var CoursePlaylist = /** @class */ (function (_super) {
    __extends(CoursePlaylist, _super);
    function CoursePlaylist() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CoursePlaylist.prototype.componentDidMount = function () {
        if (!this.props.loaded) {
            this.props.actions.loadCourseGroupsRequest(this.props.courseid);
        }
    };
    CoursePlaylist.prototype.render = function () {
        var _this = this;
        var groups = null;
        if (!this.props.loaded) {
            groups = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "Loading...");
        }
        else {
            if (this.props.groups.length === 0) {
                groups = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "alert alert-danger alert-dismissable" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "close", "data-dismiss": "alert", "aria-hidden": "true" }, "\u00D7"),
                    "No content found in this course"));
            }
            else {
                /* doubt */
                if (this.props.groupid == "" || this.props.groupid == undefined) {
                    groups = this.props.groups.map(function (group, i) {
                        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Group__WEBPACK_IMPORTED_MODULE_3__["default"], { key: group.id, group: group, index: i });
                    });
                }
                else {
                    // var check = this.props.groupid;
                    groups = this.props.groups.map(function (group, i) {
                        if (_this.props.groupid == group.id) {
                            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Group__WEBPACK_IMPORTED_MODULE_3__["default"], { key: group.id, group: group, index: i });
                        }
                    });
                    groups = groups.filter(function (n) {
                        return n != undefined;
                    });
                    groups = groups[0];
                }
            }
        }
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Container"], { style: { border: "1px solid rgb(190,190,190)", padding: '2%', marginLeft: '0%' } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], { className: "d-flex align-items-center" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], { md: { span: 4, offset: 0 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", { style: { color: 'grey' } }, "Multimedia Book"))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", { style: {
                    marginLeft: 5,
                    marginRight: 5,
                    height: '1px',
                    border: 'none',
                    backgroundColor: 'grey'
                } }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], { md: { span: 12 } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Accordion"], { defaultActiveKey: "0", id: "groupListContainer" }, groups)))));
    };
    return CoursePlaylist;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
var mapStateToProps = function (state, _a) {
    var courseid = _a.courseid, groupid = _a.groupid;
    if (state.studentCourseGroupsMap.has(courseid)) {
        return {
            loaded: true,
            groups: state.studentCourseGroupsMap.get(courseid),
            courseid: courseid,
            groupid: groupid
        };
    }
    else {
        return {
            loaded: false,
            groups: [],
            courseid: courseid,
            groupid: groupid
        };
    }
};
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_1__["bindActionCreators"])({ loadCourseGroupsRequest: _Actions_Actions__WEBPACK_IMPORTED_MODULE_5__["loadCourseGroupsRequest"] }, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(CoursePlaylist));


/***/ }),

/***/ "./src/Screens/CourseSettings/Instructor/CourseSettings.tsx":
/*!******************************************************************!*\
  !*** ./src/Screens/CourseSettings/Instructor/CourseSettings.tsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var CourseSettings = /** @class */ (function (_super) {
    __extends(CourseSettings, _super);
    function CourseSettings() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseSettings.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "CourseSettings PlaceHolder");
    };
    return CourseSettings;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseSettings);
;


/***/ }),

/***/ "./src/Screens/CourseStudent/Instructor/CourseStudent.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/CourseStudent/Instructor/CourseStudent.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var CourseStudent = /** @class */ (function (_super) {
    __extends(CourseStudent, _super);
    function CourseStudent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseStudent.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "CourseStudent PlaceHolder");
    };
    return CourseStudent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseStudent);
;


/***/ }),

/***/ "./src/Screens/CoursesList/Common/Assets/course.sass":
/*!***********************************************************!*\
  !*** ./src/Screens/CoursesList/Common/Assets/course.sass ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursesList/Common/Components/CourseDeleteModal.tsx":
/*!*************************************************************************!*\
  !*** ./src/Screens/CoursesList/Common/Components/CourseDeleteModal.tsx ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

;
var handleYes = function (props) {
    document.getElementsByTagName('body')[0].classList.remove('modal-open');
    var modalBackdrops = document.getElementsByClassName('modal-backdrop');
    for (var i = 0; i < modalBackdrops.length; i++) {
        modalBackdrops[i].parentNode.removeChild(modalBackdrops[i]);
    }
    props.callback();
};
var CourseDeleteModal = function (props) { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal fade', id: props.modal, role: 'dialog', "aria-labelledby": props.label, "aria-hidden": 'true' },
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-dialog' },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-content' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-header' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: 'button', className: 'close', "data-dismiss": 'modal', "aria-hidden": 'true' }, "\u00D7"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", { className: 'modal-title' }, props.modalTitle)),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-body' }, "" + props.warningMsg + props.title,
                "?"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-footer' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: 'button', className: 'btn btn-default', "data-dismiss": 'modal' }, "No"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: 'button', "data-dismiss": 'modal', onClick: function () { return handleYes(props); }, className: 'btn btn-danger' }, "Yes")))))); };
/* harmony default export */ __webpack_exports__["default"] = (CourseDeleteModal);


/***/ }),

/***/ "./src/Screens/CoursesList/Common/Containers/CoursesListPage.tsx":
/*!***********************************************************************!*\
  !*** ./src/Screens/CoursesList/Common/Containers/CoursesListPage.tsx ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var _Instructor_Containers_CoursesListInstructorPage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Instructor/Containers/CoursesListInstructorPage */ "./src/Screens/CoursesList/Instructor/Containers/CoursesListInstructorPage.tsx");
/* harmony import */ var _Student_Containers_CoursesListStudentPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Student/Containers/CoursesListStudentPage */ "./src/Screens/CoursesList/Student/Containers/CoursesListStudentPage.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


// Components


var CoursesListPage = /** @class */ (function (_super) {
    __extends(CoursesListPage, _super);
    function CoursesListPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CoursesListPage.prototype.render = function () {
        switch (Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode) {
            case 'I':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Instructor_Containers_CoursesListInstructorPage__WEBPACK_IMPORTED_MODULE_2__["default"], null);
            default:
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Student_Containers_CoursesListStudentPage__WEBPACK_IMPORTED_MODULE_3__["default"], null);
        }
    };
    return CoursesListPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CoursesListPage);


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Actions/ActionTypes.ts":
/*!*******************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Actions/ActionTypes.ts ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_INSTRUCTOR_COURSES_LIST_SUCCESS: 'LOAD_INSTRUCTOR_COURSES_LIST_SUCCESS',
    LOAD_INSTRUCTOR_COURSES_LIST_FAILURE: 'LOAD_INSTRUCTOR_COURSES_LIST_FAILURE',
    PUBLISH_COURSE_SUCCESS: 'PUBLISH_COURSE_SUCCESS',
    DELETE_COURSE_SUCCESS: 'DELETE_COURSE_SUCCESS',
    ADD_COURSE_SUCCESS: 'ADD_COURSE_SUCCESS',
    LOAD_IMPORT_COURSES_LIST_SUCCESS: 'LOAD_IMPORT_COURSES_LIST_SUCCESS',
    IMPORT_COURSE_SUCCESS: 'IMPORT_COURSE_SUCCESS',
    DOWNLOAD_COURSE_SUCCESS: 'DOWNLOAD_COURSE_SUCCESS',
    DELETE_ARCHIVE_SUCCESS: 'DELETE_ARCHIVE_SUCCESS',
    DISABLE_ARCHIVE_SUCCESS: 'DISABLE_ARCHIVE_SUCCESS',
    LOAD_INSTITUTE_LIST_SUCCESS: 'LOAD_INSTITUTE_LIST_SUCCESS',
    LOAD_DEPARTMENT_LIST_SUCCESS: 'LOAD_DEPARTMENT_LIST_SUCCESS',
    LOAD_ARCHIVE_STATUS_SUCCESS: 'LOAD_ARCHIVE_STATUS_SUCCESS'
});


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Actions/Actions.ts":
/*!***************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Actions/Actions.ts ***!
  \***************************************************************/
/*! exports provided: loadCoursesListRequest, loadArchiveStatus, loadImportCoursesListRequest, publishCourse, downloadCourse, deleteArchive, toggleArchive, deleteCourse, getAllInstitute, getAllDepartment, addCourse, importCourseRequest, importCourseSuccess, loadArchiveStatusSuccess, publishCourseSuccess, downloadCourseSuccess, deleteArchiveSuccess, toggleArchiveSuccess, deleteCourseSuccess, loadCoursesListSuccess, loadInstituteListSuccess, loadDepartmentListSuccess, loadCoursesListFailure, addCourseSuccess, loadImportCoursesListSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListRequest", function() { return loadCoursesListRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadArchiveStatus", function() { return loadArchiveStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadImportCoursesListRequest", function() { return loadImportCoursesListRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publishCourse", function() { return publishCourse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "downloadCourse", function() { return downloadCourse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteArchive", function() { return deleteArchive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggleArchive", function() { return toggleArchive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCourse", function() { return deleteCourse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllInstitute", function() { return getAllInstitute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllDepartment", function() { return getAllDepartment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCourse", function() { return addCourse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "importCourseRequest", function() { return importCourseRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "importCourseSuccess", function() { return importCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadArchiveStatusSuccess", function() { return loadArchiveStatusSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "publishCourseSuccess", function() { return publishCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "downloadCourseSuccess", function() { return downloadCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteArchiveSuccess", function() { return deleteArchiveSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggleArchiveSuccess", function() { return toggleArchiveSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCourseSuccess", function() { return deleteCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListSuccess", function() { return loadCoursesListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadInstituteListSuccess", function() { return loadInstituteListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDepartmentListSuccess", function() { return loadDepartmentListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListFailure", function() { return loadCoursesListFailure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCourseSuccess", function() { return addCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadImportCoursesListSuccess", function() { return loadImportCoursesListSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/CoursesList/Instructor/Actions/ActionTypes.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
/* harmony import */ var Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/GetCookie */ "./src/Utils/GetCookie.js");
/* harmony import */ var Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/CoursesListToMap */ "./src/Utils/CoursesListToMap.ts");
/* harmony import */ var Utils_InstituteListToMap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Utils/InstituteListToMap */ "./src/Utils/InstituteListToMap.ts");
/* harmony import */ var Utils_DepartmentListToMap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Utils/DepartmentListToMap */ "./src/Utils/DepartmentListToMap.ts");

// Utils





// TODO: Move this to Utils, better create a fetch wrapper in Utils which uses this
function handleErrorsAndExtractJson(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response.json();
}
function loadCoursesListRequest() {
    return function (dispatch) {
        var coursesListUrl = '/courseware/instructor_courses_json';
        return fetch(coursesListUrl)
            .then(function (response) { return response.json(); })
            .then(function (courses) {
            var coursesList = JSON.parse(courses['courses']);
            var coursesMap = Object(Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_3__["default"])(coursesList);
            dispatch(loadCoursesListSuccess(coursesMap));
        }).catch(function (error) {
            throw (error);
        });
    };
}
;
function loadArchiveStatus(courseid) {
    return function (dispatch) {
        var url = '/courseware/api/course/' + courseid + '/archive_status/?format=json';
        return fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (archive) {
            dispatch(loadArchiveStatusSuccess(courseid, archive));
        }).catch(function (error) {
            throw (error);
        });
    };
}
;
function loadImportCoursesListRequest() {
    return function (dispatch) {
        var get_all_courses_url = '/courseware/api/get_all_courses/?format=json';
        fetch(get_all_courses_url)
            .then(function (response) { return response.json(); })
            .then(function (data) {
            dispatch(loadImportCoursesListSuccess(data));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Import Course Failed', 'error');
            throw (error);
        });
    };
}
;
function publishCourse(courseId) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseId + "/publish/?format=json";
        fetch(url, {
            method: 'POST',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') }
        })
            .then(function (response) { return response.json(); })
            .then(function (response) {
            if (response.alert_msg) {
                alert(response.alert_msg);
            }
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])(response.msg, 'success');
            dispatch(publishCourseSuccess(courseId));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Request failed', 'error');
            throw (error);
        });
    };
}
;
function downloadCourse(courseId, chaps, assigns) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseId + "/submit_archive/?format=json";
        var fd = new FormData();
        fd.append('chapters', chaps);
        fd.append('p_assigns', assigns);
        fetch(url, {
            method: 'POST',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') },
            body: fd
        })
            .then(function (response) { return response.json(); })
            .then(function (response) {
            if (response.alert_msg) {
                alert(response.alert_msg);
            }
            // displayGlobalMessage(response.msg, 'success');
            dispatch(downloadCourseSuccess(courseId));
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Request failed...', 'error');
            throw (error);
        });
    };
}
;
function deleteArchive(courseId) {
    return function (dispatch) {
        if (confirm("Are you sure you want to delete the archive of this course?")) {
            var url = "/courseware/delete_archive/" + courseId + "/?format=json";
            fetch(url, {
                method: 'POST',
                headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') }
            })
                .then(function (response) { return response.json(); })
                .then(function (response) {
                if (response.success) {
                    Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])(response.msg, 'success');
                    dispatch(deleteArchiveSuccess(courseId));
                }
                else {
                    Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])(response.msg, 'error');
                }
            })
                .catch(function (error) {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Request failed', 'error');
                throw (error);
            });
        }
    };
}
function toggleArchive(courseId) {
    return function (dispatch) {
        var url = "/courseware/toggle_archive/" + courseId + "/?format=json";
        fetch(url, {
            method: 'POST',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') }
        })
            .then(function (response) { return response.json(); })
            .then(function (response) {
            if (response.success) {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])(response.msg, 'success');
                dispatch(toggleArchiveSuccess(courseId));
            }
            else {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])(response.msg, 'error');
            }
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Request failed', 'error');
            throw (error);
        });
    };
}
function deleteCourse(courseId) {
    return function (dispatch) {
        if (confirm("Are you sure you want to delete this course?")) {
            var url = "/courseware/api/course/" + courseId;
            fetch(url, {
                method: 'delete',
                headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') }
            })
                .then(function () {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('The course was successfully deleted', 'success');
                dispatch(deleteCourseSuccess(courseId));
            })
                .catch(function () {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('The course could not be deleted', 'error');
            });
        }
    };
}
;
function getAllInstitute() {
    return function (dispatch) {
        var getAllInstituteUrl = '/courseware/get_allinstitutes';
        return fetch(getAllInstituteUrl)
            .then(function (response) { return response.json(); })
            .then(function (institutes) {
            var instituteMap = Object(Utils_InstituteListToMap__WEBPACK_IMPORTED_MODULE_4__["default"])(institutes['institutes']);
            dispatch(loadInstituteListSuccess(instituteMap));
        })
            .catch(function (error) {
            throw (error);
        });
    };
}
function getAllDepartment() {
    return function (dispatch) {
        var getAllDepartmentUrl = '/courseware/get_alldepartments';
        return fetch(getAllDepartmentUrl)
            .then(function (response) { return response.json(); })
            .then(function (departments) {
            var departmentsMap = Object(Utils_DepartmentListToMap__WEBPACK_IMPORTED_MODULE_5__["default"])(departments['departments']);
            dispatch(loadDepartmentListSuccess(departmentsMap));
        })
            .catch(function (error) {
            throw (error);
        });
    };
}
function addCourse(data) {
    return function (dispatch) {
        var url = '/courseware/api/offering/?format=json';
        var fd = new FormData();
        fd.append('category', '1');
        fd.append('title', data.title);
        fd.append('enrollment_type', data.enrollment_type);
        if (data.image) {
            fd.append('image', data.image);
        }
        fd.append('institute', data.institute);
        fd.append('department', data.department);
        // fd.append('anonymity', data.anonymity);
        fd.append('notification_toAll', data.notification_toAll);
        fd.append('anonymity_reveal', data.anonymity_reveal);
        fd.append('courseCode', data.courseCode.toUpperCase());
        fd.append('course_type', data.course_type);
        fd.append('import_url', data.import_url);
        fd.append('import_file', data.import_file);
        if (data.course_type == 'I' && (data.import_url == null || data.import_url == '') && (data.import_file == null || data.import_file == '')) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Import File or URL Not Provided!!', 'error');
            return false;
        }
        fetch(url, {
            method: 'POST',
            headers: { 'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken') },
            body: fd
        })
            .then(function (response) {
            if (!response.ok) {
                var response2 = response.clone();
                response2.json().then(function (data) { return ({
                    data: data,
                    status: data.status
                }); })
                    .then(function (data) {
                    throw Error(data.data.non_field_errors);
                }).catch(function (error) {
                    console.log(error);
                    Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Add Course Failed: " + error, 'error');
                });
            }
            else {
                return response.json();
            }
        })
            .then(function (response) {
            if (response) {
                dispatch(addCourseSuccess(response));
                // window.location.href = `/courseware/course/${response.id}`;
                window.location.href = "frontend/courseware/courselist";
            }
        })
            .catch(function (error) {
            console.log(error);
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Add Course Failed: " + error, 'error');
        });
        return false;
    };
}
;
function importCourseRequest(srcCourseId, dstCourseTitle) {
    return function (dispatch) {
        var import_course_url = '/concept/copy_course/?format=json';
        var data = { 'src_course': srcCourseId, 'dst_course_title': dstCourseTitle };
        fetch(import_course_url, {
            method: 'POST',
            headers: {
                'X-CSRFToken': Object(Utils_GetCookie__WEBPACK_IMPORTED_MODULE_2__["default"])('csrftoken'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(function (response) { return response.json(); })
            .then(function (response) {
            if (response['success']) {
                dispatch(importCourseSuccess(response, dstCourseTitle));
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])('Course Imported Successfully', 'success');
            }
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_1__["default"])("Import Course Failed: " + error, 'error');
            throw (error);
        });
    };
}
;
function importCourseSuccess(response, title) {
    var course = {
        id: response.course_id,
        title: title,
        is_published: false
    };
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].IMPORT_COURSE_SUCCESS, course: course };
}
;
function loadArchiveStatusSuccess(courseid, archive) {
    var resp = {
        arch: archive,
        crseid: courseid
    };
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_ARCHIVE_STATUS_SUCCESS, resp: resp };
}
function publishCourseSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].PUBLISH_COURSE_SUCCESS, courseId: courseId };
}
;
function downloadCourseSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DOWNLOAD_COURSE_SUCCESS, courseId: courseId };
}
function deleteArchiveSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DELETE_ARCHIVE_SUCCESS, courseId: courseId };
}
;
function toggleArchiveSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DISABLE_ARCHIVE_SUCCESS, courseId: courseId };
}
;
function deleteCourseSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].DELETE_COURSE_SUCCESS, courseId: courseId };
}
;
function loadCoursesListSuccess(instructorCoursesMap) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTRUCTOR_COURSES_LIST_SUCCESS, instructorCoursesMap: instructorCoursesMap };
}
;
function loadInstituteListSuccess(instituteList) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTITUTE_LIST_SUCCESS, instituteList: instituteList };
}
;
function loadDepartmentListSuccess(departmentList) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_DEPARTMENT_LIST_SUCCESS, departmentList: departmentList };
}
;
function loadCoursesListFailure() {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTRUCTOR_COURSES_LIST_FAILURE };
}
;
function addCourseSuccess(course) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].ADD_COURSE_SUCCESS, course: course };
}
;
function loadImportCoursesListSuccess(data) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_IMPORT_COURSES_LIST_SUCCESS, data: data };
}
;


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Assets/CourseEnrolmentForm.sass":
/*!****************************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Assets/CourseEnrolmentForm.sass ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Assets/CoursesListInstructor.sass":
/*!******************************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Assets/CoursesListInstructor.sass ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Components/Course.tsx":
/*!******************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Components/Course.tsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_switch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-switch */ "./node_modules/react-switch/index.js");
/* harmony import */ var react_switch__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_switch__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Common/Assets/course.sass */ "./src/Screens/CoursesList/Common/Assets/course.sass");
/* harmony import */ var _Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2__);


// Assets

var Course = function (props) {
    var _a, _b, _c;
    var course = props.course;
    var formatter = new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    });
    var start_date = 'N/A';
    console.log("here", course.start_date);
    if (course.start_date != "Not Decided")
        start_date = formatter.format(Date.parse(course.start_date));
    console.log(start_date);
    // course.start_date = formatter.format(Date.parse(course.start_date))
    var display_content;
    var show_other_labs = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null);
    if (course.other_labs.length != 0) {
        show_other_labs = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "checkbox", className: "chapters", value: "other_labs" }),
            " Other Labs",
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null));
    }
    var form = course.chapters.map(function (chapter) {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { key: chapter.id },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "checkbox", className: "chapters", value: chapter.id, defaultChecked: true }),
            " ",
            chapter.title,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null)));
    });
    display_content = form;
    if (form.length == 0) {
        display_content = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", { className: "text-center" }, "No content to download"));
    }
    else {
        display_content = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-body" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", null, "Select Content to Download/Share/Import: "),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("form", { className: "form", method: "POST", id: 'archive-form' + course.id },
                    show_other_labs,
                    form)),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-footer" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "btn btn-primary download-archive-btn", onClick: function () { handleDownload(course.id, props.downloadCourse); } }, "Start Download"))));
    }
    if (course.archive.progress == 'Submitted') {
        react__WEBPACK_IMPORTED_MODULE_0__["useEffect"](function () {
            var interval = setInterval(function () {
                props.loadArchiveStatus(course.id);
            }, 10000);
            setTimeout(function () {
                clearInterval(interval);
            }, 10000);
            return function () { return clearInterval(interval); };
        });
    }
    var modal_content;
    if (course.archive.progress == 'Submitted') {
        modal_content = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'modal-body' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "load-download", src: "/static/img/spin.gif" }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                "\u00A0",
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", { className: "text-center" },
                    "Download will take time and is in progress in background.",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                    "Click on the download icon to check progress.")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-footer" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "btn btn-primary", "data-dismiss": "modal" }, "OK"))));
    }
    else if (course.archive.progress == 'not_present') {
        modal_content = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, display_content);
    }
    else if (course.archive.error != '') {
        modal_content = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-body" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", null, "Error Occured"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Message: "),
            course.archive.error,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            "Delete : ",
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: "#", onClick: function () { return props.deleteArchive(course.id); } }, "Link")));
    }
    else {
        modal_content = (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-body" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", null, "Course Export details"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", null,
                "Course: ",
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, course.title),
                " ready to download. "),
            "Link to download/share/import: ",
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: '/courseware/download_archive/' + course.archive.link },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, window.location.protocol + "//" + window.location.host + "/courseware/download_archive/" + course.archive.link)),
            "Active:",
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_switch__WEBPACK_IMPORTED_MODULE_1___default.a, { className: "active-switch", onChange: function () { return props.toggleArchive(course.id); }, checked: course.archive.active, height: 22 }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
            "Delete:",
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "del-ico" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "material-icons icon db-delete", onClick: function () { return props.deleteArchive(course.id); } }, " delete")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ol", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Click on link to download a zipped folder of the course content."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null,
                    "Copy URL to share with friends for them to download. Ensure Link is \"",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Active"),
                    "\"."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Avoid unecessary large download (you or friends). If goal is to copy course content to another course on Bodhitree, just copy/paste this link during course creation (Add course button) or in an existing course (Import Content button).")))));
    }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal fade archive-download-form", id: "course-download-form" + course.id, role: "dialog", "aria-hidden": "true" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-dialog", role: "form" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-content" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "closeButton-container" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "closeButton", "data-dismiss": "modal", "aria-label": "Close" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { "aria-hidden": "true" }, "\u00D7"))),
                    modal_content))),
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard col-md-3 col-sm-4" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard-container", id: "course.id" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info", onClick: function () { return handleClicks(props); } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "opacity" }),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info-label" },
                        " ",
                        course.enrollment_type == "O" ? "Open Course" : "Moderated Course",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { "data-toggle": "tooltip", title: course.enrollment_type == "O" ? "Course open to public." : "Course open only to approved students.", 
                            // style={{font-size:1rem}}
                            className: "material-icons icon info-icon" }, "help_outline")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-userInfo" }, " I"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseImageContainer" }, course.image ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: course.image })) : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: "/static/img/no_image.png" })))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseLabel", title: course.title, onClick: function () { return handleClicks(props); } },
                    course.courseCode,
                    ": ",
                    course.title),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                    course.is_published ?
                        (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "db-publish db-unpublish col-sm-6", onClick: function () { return props.publishCourse(course.id); } }, " UnPublish ")) :
                        (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "db-publish col-sm-6", onClick: function () { return props.publishCourse(course.id); } }, " Publish ")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "material-icons icon db-cloud_download col-sm-3", "data-toggle": 'modal', "data-target": '#course-download-form' + course.id },
                        " ",
                        "cloud_download",
                        " "),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { "data-toggle": "modal", className: "material-icons icon db-delete col-sm-3", onClick: function () { return props.deleteCourse(course.id); } }, " delete")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-flex flex-row db-footerLabel" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseCreated" },
                            "From ",
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { title: (_a = course.institute) === null || _a === void 0 ? void 0 : _a.name, className: "db-institute" }, ((_b = course.institute) === null || _b === void 0 ? void 0 : _b.name) != null ? (_c = course.institute) === null || _c === void 0 ? void 0 : _c.name : "Other"))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseUpdated" },
                            "Created ",
                            start_date != "" ? start_date : "01/01/2020")))))));
};
var handleClicks = function (props) {
    window.location.href = "/courseware/course/" + props.course.id + "/";
};
var handleDownload = function (courseId, downloadCourse) {
    var chapters = [];
    var p_assigns = [];
    var form = document.getElementById('archive-form' + courseId);
    var chaptersinp = form.getElementsByTagName('input');
    var assign_checked = false;
    for (var i = 0; i < chaptersinp.length; i++) {
        if (chaptersinp[i].checked) {
            chapters.push(chaptersinp[i].value);
            if (chaptersinp[i].value == "prog_assign") {
                assign_checked = true;
            }
        }
    }
    if (assign_checked == true) {
        var pa_form = document.getElementById('archive-form-pa' + courseId);
        var pa_inp = pa_form.getElementsByTagName('input');
        for (var i = 0; i < pa_inp.length; i++) {
            if (pa_inp[i].checked) {
                p_assigns.push(pa_inp[i].value);
            }
        }
    }
    downloadCourse(courseId, chapters, p_assigns);
    return true;
};
/* harmony default export */ __webpack_exports__["default"] = (Course);


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Components/CourseEnrolmentForm.tsx":
/*!*******************************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Components/CourseEnrolmentForm.tsx ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap4-form-validation */ "./node_modules/react-bootstrap4-form-validation/lib/index.js");
/* harmony import */ var react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var _Assets_CourseEnrolmentForm_sass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Assets/CourseEnrolmentForm.sass */ "./src/Screens/CoursesList/Instructor/Assets/CourseEnrolmentForm.sass");
/* harmony import */ var _Assets_CourseEnrolmentForm_sass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Assets_CourseEnrolmentForm_sass__WEBPACK_IMPORTED_MODULE_4__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




// Assets

;
;
// const CourseEnrolmentForm = (props:IProps) => {
//     $('.custom-file-input').on('change', function() {
//         var fileName = $(this).val().split('\\').pop();
//         $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
//     });
var CourseEnrolmentForm = /** @class */ (function (_super) {
    __extends(CourseEnrolmentForm, _super);
    function CourseEnrolmentForm(props) {
        var _this = _super.call(this, props) || this;
        _this.handleFileChange = function () {
            var fileName = jquery__WEBPACK_IMPORTED_MODULE_2__(_this).val().toString().split('\\').pop();
            jquery__WEBPACK_IMPORTED_MODULE_2__(_this).siblings('.custom-file-label').addClass('selected').html(fileName);
        };
        _this.handleCodeChange = function (e) {
            var _a;
            var value = e.target.value;
            if (value.length < 9) {
                jquery__WEBPACK_IMPORTED_MODULE_2__('#courseCodeMessge').text("");
                jquery__WEBPACK_IMPORTED_MODULE_2__('#courseCode').removeClass('bt-warning').removeClass('bt-error');
                _this.setState(__assign(__assign({}, _this.state), (_a = {}, _a[e.target.name] = value, _a)));
            }
            else {
                if (jquery__WEBPACK_IMPORTED_MODULE_2__('#courseCode').hasClass("is-invalid")) {
                    jquery__WEBPACK_IMPORTED_MODULE_2__("#courseCodeMessge").css("color", "orange");
                }
                else {
                    jquery__WEBPACK_IMPORTED_MODULE_2__('#courseCode').addClass('bt-warning').removeClass('bt-error');
                }
                jquery__WEBPACK_IMPORTED_MODULE_2__('#courseCodeMessge').html('Upto 8 characters(alphanumeric and -) are allowed for course code.');
                e.target.value = _this.state.courseCode;
            }
        };
        _this.handleChange = function (e) {
            var _a, _b;
            if (e.target.name == 'import_file') {
                _this.setState(__assign(__assign({}, _this.state), (_a = {}, _a[e.target.name] = e.target.files[0], _a)));
            }
            else {
                _this.setState(__assign(__assign({}, _this.state), (_b = {}, _b[e.target.name] = e.target.value, _b)));
            }
        };
        _this.handleChangeImage = function (e) {
            var fReader = new FileReader();
            fReader.readAsDataURL(e.target.files[0]);
            fReader.onloadend = function (event) {
                var img = document.getElementById("image-id");
                img.src = event.target.result;
            };
            jquery__WEBPACK_IMPORTED_MODULE_2__('#image-name').text(e.target.files[0].name);
            jquery__WEBPACK_IMPORTED_MODULE_2__('.deleteImage').addClass('addInline');
            _this.setState(__assign(__assign({}, _this.state), { 'image': e.target.files[0] }));
        };
        _this.selectOptions = function (list) {
            var options = [];
            list.forEach(function (item) {
                options.push({ value: item.id, label: item.name });
            });
            return options;
        };
        _this.handleDeleteImage = function (e) {
            jquery__WEBPACK_IMPORTED_MODULE_2__('#image-name').text('');
            jquery__WEBPACK_IMPORTED_MODULE_2__('.deleteImage').removeClass('addInline');
            jquery__WEBPACK_IMPORTED_MODULE_2__('#image-id').attr("src", "/static/img/upload_image1.png");
            _this.setState(__assign(__assign({}, _this.state), { 'image': '' }));
        };
        _this.handleChangeCheckbox = function (e, value) {
            var _a;
            _this.setState(__assign(__assign({}, _this.state), (_a = {}, _a[e.target.name] = value, _a)));
            console.log(_this.state);
        };
        _this.handleSelectChange = function (option, name) {
            var _a;
            var fieldName = (name == "institute") ? "selectedInstitute" : "selectedDepartment";
            _this.setState(__assign(__assign({}, _this.state), (_a = {}, _a[fieldName] = [option], _a[name] = option.value, _a)));
        };
        _this.handleSubmit = function () {
        };
        _this.state = {
            institute: '1',
            enrollment_type: 'O',
            department: '1',
            anonymity: 'partialAnonymous',
            notification_toAll: true,
            anonymity_reveal: true,
            courseCode: '',
            title: '',
            image: '',
            course_type: 'C',
            import_url: null,
            import_file: null,
            archive_import_status: null,
            error_msg: '',
            interval: null,
            selectedInstitute: [{ label: 'Others', value: '1' }],
            selectedDepartment: [{ label: 'Others', value: '1' }],
        };
        return _this;
    }
    CourseEnrolmentForm.prototype.loadStatus = function () {
        var _this = this;
        var url = '/courseware/import_status';
        return fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (response) {
            if (response["error"] == "AuthenticationError") {
                window.location.href = "/accounts/login";
            }
            else {
                _this.setState({ archive_import_status: response['archive_status'], error_msg: response['error'] });
                if (response['archive_status'] != 'Submitted') {
                    clearInterval(_this.state.interval);
                    _this.props.loadCoursesListRequest();
                }
            }
        });
    };
    CourseEnrolmentForm.prototype.componentDidMount = function () {
        var _this = this;
        this.loadStatus();
        var interval_ = setInterval(function () {
            _this.loadStatus();
        }, 10000);
        this.setState({ interval: interval_ });
    };
    CourseEnrolmentForm.prototype.render = function () {
        var _this = this;
        var instituteListHTML = [];
        var departmentListHTML = [];
        this.props.instituteList.forEach(function (institute) {
            instituteListHTML.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("option", { value: institute.id, key: institute.id }, institute.name));
        });
        this.props.departmentList.forEach(function (department) {
            departmentListHTML.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("option", { value: department.id, key: department.id }, department.name));
        });
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal fade", id: "course-enrolment-form", tabIndex: -1, role: "dialog", "aria-labelledby": "exampleModalCenterTitle", "aria-hidden": "true" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-dialog", role: "form" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-content" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "closeButton-container" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "button", className: "closeButton", "data-dismiss": "modal", "aria-label": "Close" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { "aria-hidden": "true" }, "\u00D7"))),
                    this.state.archive_import_status == 'not_present' ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["ValidationForm"], { className: 'form', encType: 'multipart/form-data', onSubmit: function (e) { e.preventDefault(); _this.props.addCourse(_this.state); } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-body" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-md-8' },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group row" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: 'col-form-label col-sm-12' }, "Title"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-sm-3 courseCode" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["TextInput"], { name: 'courseCode', id: 'courseCode', type: 'text', className: 'form-control courseCodeInput', required: true, pattern: "[a-zA-Z0-9-]+", placeholder: "Course Code", onChange: this.handleCodeChange }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: "courseCodeMessge", className: "invalid-feedback" })),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-sm-9" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["TextInput"], { name: 'title', id: 'title', type: 'text', className: 'form-control', required: true, maxLength: 100, placeholder: "Course Name", onChange: this.handleChange }))),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: "institute" }, "Institute"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_select__WEBPACK_IMPORTED_MODULE_3__["default"], { name: "institute", value: this.state.selectedInstitute, className: "myInput", options: this.selectOptions(this.props.instituteList), onChange: function (option) { return _this.handleSelectChange(option, "institute"); } })),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: 'col-form-label' }, "Department"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_select__WEBPACK_IMPORTED_MODULE_3__["default"], { name: "department", value: this.state.selectedDepartment, className: "myInput", options: this.selectOptions(this.props.departmentList), onChange: function (option) { return _this.handleSelectChange(option, "department"); } })),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", null, "Enrolment Type"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioGroup, { name: "enrollment_type", id: "enrollment_type", required: true, errorMessage: "Please select enrolment-type", valueSelected: this.state.enrollment_type, onChange: this.handleChange, defaultValue: "O" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioItem, { id: "O", label: "Open", value: "O" }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioItem, { id: "M", label: "Moderated", value: "M" }))),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", null, "Course Content"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioGroup, { name: "course_type", id: "course_type", required: true, errorMessage: "Please select course-type", valueSelected: this.state.course_type, onChange: this.handleChange, defaultValue: "C" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioItem, { id: "C", label: "Create on own", value: "C" }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Radio"].RadioItem, { id: "I", label: "Import", value: "I" }))),
                                    this.state.course_type == 'C' ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null)) :
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group row" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: 'col-form-label col-sm-12' }, "Import using File/URL"),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-sm-5 courseCode" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { name: 'import_file', id: 'import_file', type: 'file', accept: '.tar.gz', className: 'form-control', placeholder: ".tar.gz", ref: "imp_fl", onChange: this.handleChange })),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-sm-1" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null,
                                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "OR"))),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-sm-6" },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["TextInput"], { name: 'import_url', id: 'import_url', type: 'text', className: 'form-control', placeholder: "Paste URL here", onChange: this.handleChange }))),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: 'col-form-label' }, "Discussion Forum Settings"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Checkbox"], { name: "notification_toAll", label: "Reveal identity of 'anonymous' posts on Discussion Forum to instructor", id: "notification_toAll", value: this.state.notification_toAll, onChange: this.handleChangeCheckbox }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap4_form_validation__WEBPACK_IMPORTED_MODULE_1__["Checkbox"], { name: "anonymity_reveal", label: "Emails sent to all on every post", id: "anonymity_reveal", value: this.state.anonymity_reveal, onChange: this.handleChangeCheckbox })))),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-4" },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'form-group' },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: 'col-form-label' }, "Course Image"),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'custom-file image-upload' },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: 'file-input' },
                                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "upload_image_icon", id: "image-id", src: "/static/img/upload_image1.png" })),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { id: "file-input", name: "image", type: "file", accept: "image/*", onChange: this.handleChangeImage })),
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "image-name" },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { id: "image-name" }),
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "deleteImage", onClick: this.handleDeleteImage }, "\u00D7")))))),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "modal-footer" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { type: "submit", className: "btn btn-primary" }, "Add Course")))) : (this.state.archive_import_status == 'Submitted') ?
                        (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "load-import", src: "/static/img/spin.gif" }),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            "\u00A0",
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", { className: "text-center" },
                                "Import will take time and is in progress in background.",
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                                "Click on the Add Course button to check progress."),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h6", { className: "text-center" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: "/courseware/cancel_import" }, "Click here to Cancel the task"))))
                        : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: "/courseware/cancel_import" }, "Click here to cancel the task")),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            "\u00A0",
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "ERROR:")),
                            this.state.error_msg))))));
    };
    return CourseEnrolmentForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
/* harmony default export */ __webpack_exports__["default"] = (CourseEnrolmentForm);


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Components/CoursesListInstructor.tsx":
/*!*********************************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Components/CoursesListInstructor.tsx ***!
  \*********************************************************************************/
/*! exports provided: FormType, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormType", function() { return FormType; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _Assets_CoursesListInstructor_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assets/CoursesListInstructor.sass */ "./src/Screens/CoursesList/Instructor/Assets/CoursesListInstructor.sass");
/* harmony import */ var _Assets_CoursesListInstructor_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Assets_CoursesListInstructor_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");



// Assets

// Components

var FormType;
(function (FormType) {
    FormType[FormType["NONE"] = 0] = "NONE";
    FormType[FormType["ADD_COURSE"] = 1] = "ADD_COURSE";
    FormType[FormType["IMPORT_COURSE"] = 2] = "IMPORT_COURSE";
})(FormType || (FormType = {}));
;
;
var CoursesListInstructor = function (props) { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], { navbarClass: 'navbar-light' }),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "bd-content-header" },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "container" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-8 bd-content-title" }, " My Dashboard"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-md-4 offering-form-buttons' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: 'add-course-button ml-auto', type: 'button', "data-toggle": 'modal', "data-target": '#course-enrolment-form' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_1__["FontAwesomeIcon"], { className: 'plus-icon', icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__["faPlus"] }),
                        "Add Course")),
                props.form))),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'container' },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'instructor-courses-list' }, props.courses)))); };
/* harmony default export */ __webpack_exports__["default"] = (CoursesListInstructor);


/***/ }),

/***/ "./src/Screens/CoursesList/Instructor/Containers/CoursesListInstructorPage.tsx":
/*!*************************************************************************************!*\
  !*** ./src/Screens/CoursesList/Instructor/Containers/CoursesListInstructorPage.tsx ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Components_CoursesListInstructor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Components/CoursesListInstructor */ "./src/Screens/CoursesList/Instructor/Components/CoursesListInstructor.tsx");
/* harmony import */ var _Components_Course__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Components/Course */ "./src/Screens/CoursesList/Instructor/Components/Course.tsx");
/* harmony import */ var _Components_CourseEnrolmentForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Components/CourseEnrolmentForm */ "./src/Screens/CoursesList/Instructor/Components/CourseEnrolmentForm.tsx");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursesList/Instructor/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// Components



// Actions

;
var CoursesListInstructorPage = /** @class */ (function (_super) {
    __extends(CoursesListInstructorPage, _super);
    function CoursesListInstructorPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CoursesListInstructorPage.prototype.componentDidMount = function () {
        if (this.props.coursesMap.size === 0) {
            this.props.actions.loadCoursesListRequest();
        }
        this.props.actions.getAllInstitute();
        this.props.actions.getAllDepartment();
    };
    CoursesListInstructorPage.prototype.render = function () {
        var _this = this;
        var courses;
        if (this.props.coursesMap.size !== 0) {
            courses = [];
            this.props.coursesMap.forEach(function (course) {
                courses.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Course__WEBPACK_IMPORTED_MODULE_5__["default"], { course: course, publishCourse: _this.props.actions.publishCourse, deleteCourse: _this.props.actions.deleteCourse, downloadCourse: _this.props.actions.downloadCourse, loadArchiveStatus: _this.props.actions.loadArchiveStatus, deleteArchive: _this.props.actions.deleteArchive, toggleArchive: _this.props.actions.toggleArchive, key: course.id }));
            });
        }
        if (!courses) {
            courses = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'no-courses-offered-alert alert alert-danger alert-dismissable bt-alert' }, "No courses offered yet");
        }
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_CoursesListInstructor__WEBPACK_IMPORTED_MODULE_4__["default"], { toggleForm: this.toggleForm, form: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_CourseEnrolmentForm__WEBPACK_IMPORTED_MODULE_6__["default"], { addCourse: this.props.actions.addCourse, loadCoursesListRequest: this.props.actions.loadCoursesListRequest, instituteList: this.props.instituteList, departmentList: this.props.departmentList }), courses: courses });
    };
    CoursesListInstructorPage.prototype.toggleForm = function (formType) {
        this.setState({ showFormType: formType });
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], CoursesListInstructorPage.prototype, "toggleForm", null);
    return CoursesListInstructorPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
;
var mapStateToProps = function (state) { return ({
    coursesMap: state.instructorCoursesMap,
    instituteList: state.instituteList,
    departmentList: state.departmentList
}); };
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_2__["bindActionCreators"])(_Actions_Actions__WEBPACK_IMPORTED_MODULE_7__, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(mapStateToProps, mapDispatchToProps)(CoursesListInstructorPage));


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Actions/ActionTypes.ts":
/*!****************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Actions/ActionTypes.ts ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_STUDENT_COURSES_LIST_SUCCESS: 'LOAD_STUDENT_COURSES_LIST_SUCCESS',
    LOAD_STUDENT_COURSES_LIST_FAILURE: 'LOAD_STUDENT_COURSES_LIST_FAILURE',
    UNREGISTER_COURSE_SUCCESS: 'UNREGISTER_COURSE_SUCCESS'
});


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Actions/Actions.ts":
/*!************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Actions/Actions.ts ***!
  \************************************************************/
/*! exports provided: loadCoursesListRequest, unRegisterCourseRequest, unRegisterCourseSuccess, loadCoursesListSuccess, loadCoursesListFailure */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListRequest", function() { return loadCoursesListRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unRegisterCourseRequest", function() { return unRegisterCourseRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unRegisterCourseSuccess", function() { return unRegisterCourseSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListSuccess", function() { return loadCoursesListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListFailure", function() { return loadCoursesListFailure; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/CoursesList/Student/Actions/ActionTypes.ts");
/* harmony import */ var Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/CoursesListToMap */ "./src/Utils/CoursesListToMap.ts");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");



function loadCoursesListRequest() {
    return function (dispatch) {
        var coursesListUrl = '/courseware/student_courses_json';
        return fetch(coursesListUrl)
            .then(function (response) { return response.json(); })
            .then(function (courses) {
            var coursesList = JSON.parse(courses['courses']);
            var coursesMap = Object(Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_1__["default"])(coursesList);
            dispatch(loadCoursesListSuccess(coursesMap));
        }).catch(function (error) {
            throw (error);
        });
    };
}
;
function unRegisterCourseRequest(courseId) {
    return function (dispatch) {
        var url = "/courseware/api/course/" + courseId + "/deregister";
        fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (result) {
            if (result['message'] == "AuthentcationError") {
                window.location.href = "/accounts/login";
            }
            else if (result['message'] == "Error") {
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_2__["default"])('The course could not be unenrolled.', 'error');
            }
            else {
                dispatch(unRegisterCourseSuccess(courseId));
                Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_2__["default"])('The course was successfully unenrolled.', 'success');
            }
        })
            .catch(function (error) {
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_2__["default"])('The course could not be unenrolled.', 'error');
            throw (error);
        });
    };
}
;
function unRegisterCourseSuccess(courseId) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].UNREGISTER_COURSE_SUCCESS, courseId: courseId };
}
function loadCoursesListSuccess(studentCoursesMap) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_STUDENT_COURSES_LIST_SUCCESS, studentCoursesMap: studentCoursesMap };
}
;
function loadCoursesListFailure() {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_STUDENT_COURSES_LIST_FAILURE };
}
;


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Assets/CoursesListStudent.sass":
/*!************************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Assets/CoursesListStudent.sass ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/CoursesList/Student/Components/Course.tsx":
/*!***************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Components/Course.tsx ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Screens_CoursesList_Common_Components_CourseDeleteModal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens/CoursesList/Common/Components/CourseDeleteModal */ "./src/Screens/CoursesList/Common/Components/CourseDeleteModal.tsx");
/* harmony import */ var _Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Common/Assets/course.sass */ "./src/Screens/CoursesList/Common/Assets/course.sass");
/* harmony import */ var _Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Common_Assets_course_sass__WEBPACK_IMPORTED_MODULE_2__);

// Components

// Assets

;
var Course = function (props) {
    var _a, _b, _c;
    var course = props.course;
    var formatter = new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    });
    var start_date = '';
    try {
        start_date = formatter.format(Date.parse(course.start_date));
    }
    catch (e) {
        console.log(e);
    }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard col-md-3 col-sm-4" },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard-container  db-student", id: "" + course.id },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info", onClick: function () { return handleClicks(props); } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "opacity" }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info-label" },
                    course.enrollment_type == "O" ? "Open Course" : "Moderated Course",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { 
                        // data-toggle="tooltip" 
                        title: course.enrollment_type == "O" ? "Course open to public." : "Course open only to approved students.", 
                        // style={{font-size:1rem}}
                        className: "material-icons icon info-icon" }, "help_outline")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-userInfo" }, " S"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseImageContainer" }, course.image ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: course.image })) : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: "/static/img/no_image.png" })))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseLabel", title: course.title, onClick: function () { return handleClicks(props); } },
                course.courseCode,
                ": ",
                course.title),
            (course.enrollment_status == "P") ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-pendingStatus col-sm-10" }, "Enrollment Pending"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursesList_Common_Components_CourseDeleteModal__WEBPACK_IMPORTED_MODULE_1__["default"], { label: "label" + props.course.id, modal: "modal" + props.course.id, title: props.course.title, modalTitle: 'Confirm unregistration', warningMsg: 'Are you sure about deleting ', callback: props.unRegister }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: "material-icons icon db-delete col-sm-2 btn btn-sm", "data-toggle": 'modal', href: "#modal" + props.course.id }, "delete"))) : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursesList_Common_Components_CourseDeleteModal__WEBPACK_IMPORTED_MODULE_1__["default"], { label: "label" + props.course.id, modal: "modal" + props.course.id, title: props.course.title, modalTitle: 'Confirm unregistration', warningMsg: 'Sure about unregistering from ', callback: props.unRegister }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: "btn btn-sm db-publish", "data-toggle": 'modal', href: "#modal" + props.course.id }, "UnRegister"))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-flex flex-row db-footerLabel" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseCreated" },
                        "From ",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { title: (_a = course.institute) === null || _a === void 0 ? void 0 : _a.name, className: "db-institute" }, ((_b = course.institute) === null || _b === void 0 ? void 0 : _b.name) != null ? (_c = course.institute) === null || _c === void 0 ? void 0 : _c.name : "Other"))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseUpdated" },
                        "Created ",
                        start_date != "" ? start_date : "01/01/2020"))))));
};
var handleClicks = function (props) {
    window.location.href = "/courseware/course/" + props.course.id + "/";
};
/* harmony default export */ __webpack_exports__["default"] = (Course);


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Components/CoursesListStudent.tsx":
/*!***************************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Components/CoursesListStudent.tsx ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Assets_CoursesListStudent_sass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Assets/CoursesListStudent.sass */ "./src/Screens/CoursesList/Student/Assets/CoursesListStudent.sass");
/* harmony import */ var _Assets_CoursesListStudent_sass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Assets_CoursesListStudent_sass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");

// Assets

// Components

;
var CoursesListStudent = function (props) { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_2__["default"], { navbarClass: 'navbar-light' }),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'container' },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'row' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-md-8 mycourseTitle' }, "MY COURSES"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-md-8' }, "Click on 'Explore' option to register in courses.")),
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'row courseContainer' }, props.courses)))); };
/* harmony default export */ __webpack_exports__["default"] = (CoursesListStudent);


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Containers/CourseContainer.tsx":
/*!************************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Containers/CourseContainer.tsx ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Screens_CoursesList_Student_Components_Course__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens/CoursesList/Student/Components/Course */ "./src/Screens/CoursesList/Student/Components/Course.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// Components

;
var CourseContainer = /** @class */ (function (_super) {
    __extends(CourseContainer, _super);
    function CourseContainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CourseContainer.prototype.componentDidMount = function () {
        // this.setProgress();
    };
    CourseContainer.prototype.render = function () {
        var _this = this;
        var course = this.props.course;
        var baseUrl = '/courseware/course/';
        var courseUrl = "" + baseUrl + course.id + "/";
        var infoUrl = courseUrl + "information/";
        var start_msg = "Course End : " + course.end_date;
        if (course.coursetag === 1) {
            start_msg = "Course Start : " + course.start_date;
        }
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursesList_Student_Components_Course__WEBPACK_IMPORTED_MODULE_1__["default"], { course: course, start_msg: start_msg, unRegister: function () { return _this.props.unRegisterCourse(course.id); }, infoUrl: infoUrl, courseUrl: courseUrl });
    };
    CourseContainer.prototype.setProgress = function () {
        var course = this.props.course;
        var div = document.getElementById("pgbar" + course.id);
        div.style.width = course.progress + "%";
    };
    return CourseContainer;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (CourseContainer);
;


/***/ }),

/***/ "./src/Screens/CoursesList/Student/Containers/CoursesListStudentPage.tsx":
/*!*******************************************************************************!*\
  !*** ./src/Screens/CoursesList/Student/Containers/CoursesListStudentPage.tsx ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var Screens_CoursesList_Student_Containers_CourseContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Screens/CoursesList/Student/Containers/CourseContainer */ "./src/Screens/CoursesList/Student/Containers/CourseContainer.tsx");
/* harmony import */ var _Components_CoursesListStudent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Components/CoursesListStudent */ "./src/Screens/CoursesList/Student/Components/CoursesListStudent.tsx");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/CoursesList/Student/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



// Components


// Actions

;
var CoursesListStudentPage = /** @class */ (function (_super) {
    __extends(CoursesListStudentPage, _super);
    function CoursesListStudentPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CoursesListStudentPage.prototype.componentDidMount = function () {
        if (this.props.coursesMap.size === 0) {
            this.props.actions.loadCoursesListRequest();
        }
    };
    CoursesListStudentPage.prototype.render = function () {
        var _this = this;
        var courses;
        if (this.props.coursesMap.size !== 0) {
            courses = [];
            this.props.coursesMap.forEach(function (course) {
                courses.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_CoursesList_Student_Containers_CourseContainer__WEBPACK_IMPORTED_MODULE_3__["default"], { course: course, unRegisterCourse: function (courseId) { return _this.props.actions.unRegisterCourseRequest(courseId); }, key: course.id }));
            });
        }
        if (!courses) {
            courses = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'alert alert-danger alert-dismissable bt-alert' }, "No courses enrolled yet");
        }
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_CoursesListStudent__WEBPACK_IMPORTED_MODULE_4__["default"], { courses: courses });
    };
    return CoursesListStudentPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
;
var mapStateToProps = function (state) { return ({
    coursesMap: state.studentCoursesMap
}); };
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_1__["bindActionCreators"])(_Actions_Actions__WEBPACK_IMPORTED_MODULE_5__, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(CoursesListStudentPage));


/***/ }),

/***/ "./src/Screens/EmailArchive/Instructor/EmailArchive.tsx":
/*!**************************************************************!*\
  !*** ./src/Screens/EmailArchive/Instructor/EmailArchive.tsx ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var EmailArchive = /** @class */ (function (_super) {
    __extends(EmailArchive, _super);
    function EmailArchive() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EmailArchive.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "EmailArchive PlaceHolder");
    };
    return EmailArchive;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (EmailArchive);
;


/***/ }),

/***/ "./src/Screens/EmailStudentArchive/Student/EmailStudentArchive.tsx":
/*!*************************************************************************!*\
  !*** ./src/Screens/EmailStudentArchive/Student/EmailStudentArchive.tsx ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var EmailStudentArchive = /** @class */ (function (_super) {
    __extends(EmailStudentArchive, _super);
    function EmailStudentArchive() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EmailStudentArchive.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "EmailStudentArchive PlaceHolder");
    };
    return EmailStudentArchive;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (EmailStudentArchive);
;


/***/ }),

/***/ "./src/Screens/ErrorWrapper.tsx":
/*!**************************************!*\
  !*** ./src/Screens/ErrorWrapper.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// React Error Boundary component. Wrap it around any other react components
// to catch script errors in any child component.
var ErrorWrapper = /** @class */ (function (_super) {
    __extends(ErrorWrapper, _super);
    function ErrorWrapper(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { hasError: false };
        return _this;
    }
    ErrorWrapper.prototype.componentDidCatch = function (error, info) {
        this.setState({
            hasError: true,
        });
        if (info != null && info.componentStack != null) {
            error.stack = info.componentStack;
        }
        console.log("Oops we ran into an error - " + error);
    };
    ErrorWrapper.prototype.render = function () {
        if (this.state.hasError) {
            return null;
        }
        return this.props.children;
    };
    return ErrorWrapper;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ErrorWrapper);


/***/ }),

/***/ "./src/Screens/ExamsTab/Instructor/ExamsTab.tsx":
/*!******************************************************!*\
  !*** ./src/Screens/ExamsTab/Instructor/ExamsTab.tsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var ExamsTab = /** @class */ (function (_super) {
    __extends(ExamsTab, _super);
    function ExamsTab() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ExamsTab.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "ExamsTab PlaceHolder");
    };
    return ExamsTab;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ExamsTab);
;


/***/ }),

/***/ "./src/Screens/Forum/Instructor/Forum.tsx":
/*!************************************************!*\
  !*** ./src/Screens/Forum/Instructor/Forum.tsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Forum = /** @class */ (function (_super) {
    __extends(Forum, _super);
    function Forum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Forum.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Forum PlaceHolder");
    };
    return Forum;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Forum);
;


/***/ }),

/***/ "./src/Screens/Forum/Student/Forum.tsx":
/*!*********************************************!*\
  !*** ./src/Screens/Forum/Student/Forum.tsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Forum = /** @class */ (function (_super) {
    __extends(Forum, _super);
    function Forum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Forum.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Forum PlaceHolder");
    };
    return Forum;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Forum);
;


/***/ }),

/***/ "./src/Screens/LandingPage/Common/Assets/LandingPage.sass":
/*!****************************************************************!*\
  !*** ./src/Screens/LandingPage/Common/Assets/LandingPage.sass ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/LandingPage/Common/Components/LandingPage.tsx":
/*!*******************************************************************!*\
  !*** ./src/Screens/LandingPage/Common/Components/LandingPage.tsx ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Assets_LandingPage_sass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Assets/LandingPage.sass */ "./src/Screens/LandingPage/Common/Assets/LandingPage.sass");
/* harmony import */ var _Assets_LandingPage_sass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Assets_LandingPage_sass__WEBPACK_IMPORTED_MODULE_1__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// Assets

// Components
var LandingPage = /** @class */ (function (_super) {
    __extends(LandingPage, _super);
    function LandingPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LandingPage.prototype.render = function () {
        return window.location.href = "/accounts/landing";
    };
    return LandingPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (LandingPage);


/***/ }),

/***/ "./src/Screens/Layout.tsx":
/*!********************************!*\
  !*** ./src/Screens/Layout.tsx ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ErrorWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ErrorWrapper */ "./src/Screens/ErrorWrapper.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


var Layout = /** @class */ (function (_super) {
    __extends(Layout, _super);
    function Layout() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Layout.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ErrorWrapper__WEBPACK_IMPORTED_MODULE_1__["default"], null, this.props.children)));
    };
    return Layout;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Layout);


/***/ }),

/***/ "./src/Screens/Layout/Common/Assets/Navbar.sass":
/*!******************************************************!*\
  !*** ./src/Screens/Layout/Common/Assets/Navbar.sass ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Layout/Common/Components/LoginButton.tsx":
/*!**************************************************************!*\
  !*** ./src/Screens/Layout/Common/Components/LoginButton.tsx ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


// Assets
// Components
var LoginButton = /** @class */ (function (_super) {
    __extends(LoginButton, _super);
    function LoginButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoginButton.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: 'nav-item' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], { to: '/frontend/login', className: 'nav-link login-button' }, "Login"));
    };
    return LoginButton;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (LoginButton);
;


/***/ }),

/***/ "./src/Screens/Layout/Common/Components/Navbar.tsx":
/*!*********************************************************!*\
  !*** ./src/Screens/Layout/Common/Components/Navbar.tsx ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
/* harmony import */ var _Assets_Navbar_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assets/Navbar.sass */ "./src/Screens/Layout/Common/Assets/Navbar.sass");
/* harmony import */ var _Assets_Navbar_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Assets_Navbar_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _ProfileButton__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ProfileButton */ "./src/Screens/Layout/Common/Components/ProfileButton.tsx");
/* harmony import */ var _LoginButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./LoginButton */ "./src/Screens/Layout/Common/Components/LoginButton.tsx");
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/fi */ "./node_modules/react-icons/fi/index.esm.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



// Assets





// Components
var Navbar = /** @class */ (function (_super) {
    __extends(Navbar, _super);
    function Navbar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Navbar.prototype.getProfileButton = function () {
        switch (Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode) {
            case 'I':
            case 'C':
            case 'S':
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ProfileButton__WEBPACK_IMPORTED_MODULE_5__["default"], null);
            default:
                return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_LoginButton__WEBPACK_IMPORTED_MODULE_6__["default"], null);
        }
    };
    Navbar.prototype.setUserMode = function (mode) {
        Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])('/user/switch-mode/?format=json', {
            new_mode: mode
        }, 'application/x-www-form-urlencoded')
            .then(function () { window.location.reload(); })
            .catch(function (err) { return console.error(err); });
    };
    Navbar.prototype.render = function () {
        //     return <React.Fragment>
        //         <nav className={'navbar navbar-default navbar-expand-md ml-auto mr-auto ' + this.props.navbarClass}>
        //             <Link to='/frontend/courseware/courseslist' className='navbar-brand orange-text'>BodhiTree</Link>
        //             <button
        //                 className='navbar-toggler'
        //                 type='button'
        //                 data-toggle='collapse'
        //                 data-target='#navbarSupportedContent'
        //                 aria-controls='navbarSupportedContent'
        //                 aria-expanded='false'
        //                 aria-label='Toggle navigation'>
        //                 <span className='navbar-toggler-icon'></span>
        //             </button>
        //             <div className='collapse navbar-collapse' id='navbarSupportedContent'>
        //                 <ul className='navbar-nav ml-auto navlinks'>
        //                     <li className='nav-item'>
        //                         <Link to='/frontend/courseware/' className='nav-link'>Courses</Link>
        //                     </li>
        //                     <li className='nav-item'>
        //                         <a className='nav-link' href='#'>Contact Us</a>
        //                     </li>
        //                     {this.getProfileButton()}
        //                 </ul>
        //             </div>
        //         </nav>
        //     </React.Fragment>;
        // }
        var _this = this;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("nav", { className: "bd-navbar navbar navbar-default navbar-expand-md navbar-toggleable-md fixed-top", role: "navigation" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "container" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "bd-navbar-content" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "navbar-header" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "navbar-toggler navbar-toggler-right", type: "button", "data-toggle": "collapse", "data-target": "#bs-navbar-collapse-1", "aria-controls": "bs-navbar-collapse-1", "aria-expanded": "false", "aria-label": "Toggle navigation" },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "navbar-toggler-icon" })),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], { to: '/frontend', className: 'navbar-brand bd-navbar-brand' }, "BodhiTree"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], { to: "/frontend/courseware", id: "exploreButtonTrigger", className: "navbar-brand" }, "Explore\u00A0 \u00A0"),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], { className: "navbar-brand btn btn-primary exploreButton", id: "bodhiMyCourse", to: "/frontend/courseware/courseslist" }, " My Dashboard"))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "collapse navbar-collapse", id: "bs-navbar-collapse-1" },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { className: "nav navbar-nav navbar-right" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: 'nav-item' },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'nav-link', href: 'https://bit.ly/bodhi-manual' }, "User Manual")),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: 'nav-item dropdown' },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'nav-link dropdown-toggle', href: '#', id: "modeDropDown", role: 'button', "data-toggle": 'dropdown', "aria-haspopup": 'true', "aria-expanded": 'false' },
                                    "Role: ",
                                    Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode == "I" ? "Instructor" : "Student"),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'dropdown-menu', "aria-labelledby": 'modeDropDown' },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'dropdown-item', href: '#', onClick: function () { return _this.setUserMode('S'); } },
                                        "Student ",
                                        Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode == "S" && react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fi__WEBPACK_IMPORTED_MODULE_7__["FiCheck"], { size: 20 })),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'dropdown-divider' }),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'dropdown-item', href: '#', onClick: function () { return _this.setUserMode('I'); } },
                                        "Instructor ",
                                        Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().mode == "I" && react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_icons_fi__WEBPACK_IMPORTED_MODULE_7__["FiCheck"], { size: 20 })))),
                            this.getProfileButton()))))));
    };
    return Navbar;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Navbar);
;


/***/ }),

/***/ "./src/Screens/Layout/Common/Components/ProfileButton.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/Layout/Common/Components/ProfileButton.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/GetHtmlDataSet */ "./src/Utils/GetHtmlDataSet.js");
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



// Assets
// Components
var InstructorProfileButton = /** @class */ (function (_super) {
    __extends(InstructorProfileButton, _super);
    function InstructorProfileButton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    InstructorProfileButton.prototype.setUserMode = function (mode) {
        Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_2__["default"])('/user/switch-mode/?format=json', {
            new_mode: mode
        }, 'application/x-www-form-urlencoded')
            .then(function () { window.location.reload(); })
            .catch(function (err) { return console.error(err); });
    };
    InstructorProfileButton.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { className: 'nav-item dropdown' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'nav-link dropdown-toggle', href: '#', id: 'navbarDropdown', role: 'button', "data-toggle": 'dropdown', "aria-haspopup": 'true', "aria-expanded": 'false' }, Object(Utils_GetHtmlDataSet__WEBPACK_IMPORTED_MODULE_1__["default"])().fullName),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'dropdown-menu', "aria-labelledby": 'navbarDropdown' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'dropdown-item', href: '#' }, "Profile"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'dropdown-divider' }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { className: 'dropdown-item', href: '/accounts/logout/' }, "Sign Out")));
    };
    return InstructorProfileButton;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (InstructorProfileButton);
;


/***/ }),

/***/ "./src/Screens/Login/Common/Assets/LoginPage.sass":
/*!********************************************************!*\
  !*** ./src/Screens/Login/Common/Assets/LoginPage.sass ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Login/Common/Components/LoginPage.tsx":
/*!***********************************************************!*\
  !*** ./src/Screens/Login/Common/Components/LoginPage.tsx ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
/* harmony import */ var Utils_GetQueryStringParams__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/GetQueryStringParams */ "./src/Utils/GetQueryStringParams.ts");
/* harmony import */ var _Assets_LoginPage_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assets/LoginPage.sass */ "./src/Screens/Login/Common/Assets/LoginPage.sass");
/* harmony import */ var _Assets_LoginPage_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Assets_LoginPage_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



// Assets

// Components

var LoginPage = /** @class */ (function (_super) {
    __extends(LoginPage, _super);
    function LoginPage() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleLogin = function (e) {
            e.preventDefault();
            var data = Array.prototype.slice
                .call(e.target)
                .filter(function (el) { return el.name; })
                .reduce(function (form, el) {
                var _a;
                return (__assign(__assign({}, form), (_a = {}, _a[el.name] = el.value, _a)));
            }, {});
            data['app'] = true;
            Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_1__["default"])('/accounts/login/', data, 'application/x-www-form-urlencoded')
                .then(function (response) {
                if (!response.error) {
                    var queryStringParams = Object(Utils_GetQueryStringParams__WEBPACK_IMPORTED_MODULE_2__["default"])(_this.props.location.search);
                    window.location.href = 'next' in queryStringParams ? queryStringParams['next'] : '/frontend/courseware/courseslist/';
                }
            })
                .catch(function (err) { return console.error(err); });
        };
        _this.handleSignUp = function () {
            // TODO: Will use 'Link' once sign-up page is in new react
            window.location.href = '/accounts/signup/';
        };
        return _this;
    }
    LoginPage.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'login-page-component' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], { navbarClass: 'navbar-dark fixed-top' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'login-page-container' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'login-form-container' },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'flex-1' }),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'flex-1' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'login-form-logo' }, "BodhiTree"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("form", { className: 'login-form form', onSubmit: this.handleLogin },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { name: 'username', placeholder: 'email' }),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { name: 'password', type: 'password', placeholder: 'password' }),
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'login-button-row' },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: 'login-signup-button', type: 'submit' }, "Log In"),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: '#' }, "Forgot password?")))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'sign-up-row flex-1' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: 'sign-up-text' }, "Don't have an account?"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: 'login-signup-button', onClick: this.handleSignUp }, "Sign Up")))));
    };
    return LoginPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (LoginPage);


/***/ }),

/***/ "./src/Screens/Marks/Instructor/Marks.tsx":
/*!************************************************!*\
  !*** ./src/Screens/Marks/Instructor/Marks.tsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Marks = /** @class */ (function (_super) {
    __extends(Marks, _super);
    function Marks() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Marks.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Marks PlaceHolder");
    };
    return Marks;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Marks);
;


/***/ }),

/***/ "./src/Screens/Marks/Student/Marks.tsx":
/*!*********************************************!*\
  !*** ./src/Screens/Marks/Student/Marks.tsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Marks = /** @class */ (function (_super) {
    __extends(Marks, _super);
    function Marks() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Marks.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Marks PlaceHolder");
    };
    return Marks;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Marks);
;


/***/ }),

/***/ "./src/Screens/Mission/Common/Assets/MissionPage.sass":
/*!************************************************************!*\
  !*** ./src/Screens/Mission/Common/Assets/MissionPage.sass ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Mission/Common/Components/MissionPage.tsx":
/*!***************************************************************!*\
  !*** ./src/Screens/Mission/Common/Components/MissionPage.tsx ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
/* harmony import */ var _Assets_MissionPage_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assets/MissionPage.sass */ "./src/Screens/Mission/Common/Assets/MissionPage.sass");
/* harmony import */ var _Assets_MissionPage_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Assets_MissionPage_sass__WEBPACK_IMPORTED_MODULE_2__);


// Assets

var Mission = function () { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_1__["default"], { navbarClass: 'navbar-light' }),
    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'container' },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'row' },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'about-header' },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Our Mission: "),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, "Accessible quality technical education for all, through personalized, flexible, and hands-on complete learning.")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "There is tremendous scope today for improving the accessibility of quality technical education in many parts of the world, specifically in India. Our mission is to work toward this through a combination of online technical content, mixed with active learning: thinking and  problem solving while learning for effective understanding.  Our model encourages personalized self-learning, in addition to group learning, as well as instructor-guided flipped classrooms."),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Features:"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "The platform has been designed to mimic a classroom setting, yet providing enough flexibility so that students can study the material at their own pace and chosen time/group."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "The interactivity of classroom discussions has been captured via embedding questions/activities and feedback as part of the videos itself."),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "The duration of the videos has been kept short to retain student's focus."),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "All material has been organized into chapters which in turn are made of sections. A section hosts all material related to a concept in one place: videos, slides, reference material, practice problems, activities all on same page."))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null,
                "The end result of all this is what we term ",
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "\"PFC\" learning"),
                "."),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Personalized Learning:"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Unlike a classroom setting, it is as if the instructor is teaching just you."),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Unlike a classroom setting, everyone gets to answer questions during instruction without fear of embarrassment."))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Flexible Learning:"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Unlike fixed class timings, when it comes to learning, students get to chose the time, place and group"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Unlike class room setting, student can set their own pace: take as much time to view or answer a question"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Unlike class room setting, student can watch the video as many times until the concept is really clear."))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("strong", null, "Complete Learning:"),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Since each concept is complete, a student can move ahead only after complete mastery of concept. In a classroom setting, this is not possible since instructor may cover multiple concepts in one lecture and practice is often outside lecture hours. Here, one can intersperse video watching with practice."))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null))))); };
/* harmony default export */ __webpack_exports__["default"] = (Mission);


/***/ }),

/***/ "./src/Screens/NotFound/Common/Components/NotFound.tsx":
/*!*************************************************************!*\
  !*** ./src/Screens/NotFound/Common/Components/NotFound.tsx ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var NotFound = function () { return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "THESE ARE NOT THE DROIDS YOU ARE LOOKING FOR")); };
/* harmony default export */ __webpack_exports__["default"] = (NotFound);


/***/ }),

/***/ "./src/Screens/Page/Instructor/Page.tsx":
/*!**********************************************!*\
  !*** ./src/Screens/Page/Instructor/Page.tsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Page = /** @class */ (function (_super) {
    __extends(Page, _super);
    function Page() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Page.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Page PlaceHolder");
    };
    return Page;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Page);
;


/***/ }),

/***/ "./src/Screens/Page/Student/Page.tsx":
/*!*******************************************!*\
  !*** ./src/Screens/Page/Student/Page.tsx ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Page = /** @class */ (function (_super) {
    __extends(Page, _super);
    function Page() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Page.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Page PlaceHolder");
    };
    return Page;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Page);
;


/***/ }),

/***/ "./src/Screens/PageAdditionForm/Instructor/PageAdditionForm.tsx":
/*!**********************************************************************!*\
  !*** ./src/Screens/PageAdditionForm/Instructor/PageAdditionForm.tsx ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var PageAdditionForm = /** @class */ (function (_super) {
    __extends(PageAdditionForm, _super);
    function PageAdditionForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PageAdditionForm.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "PageAdditionForm PlaceHolder");
    };
    return PageAdditionForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (PageAdditionForm);
;


/***/ }),

/***/ "./src/Screens/Progress/Instructor/Progress.tsx":
/*!******************************************************!*\
  !*** ./src/Screens/Progress/Instructor/Progress.tsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Progress = /** @class */ (function (_super) {
    __extends(Progress, _super);
    function Progress() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Progress.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Progress PlaceHolder");
    };
    return Progress;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Progress);
;


/***/ }),

/***/ "./src/Screens/Progress/Student/Progress.tsx":
/*!***************************************************!*\
  !*** ./src/Screens/Progress/Student/Progress.tsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Progress = /** @class */ (function (_super) {
    __extends(Progress, _super);
    function Progress() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Progress.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Progress PlaceHolder");
    };
    return Progress;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Progress);
;


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Actions/ActionTypes.ts":
/*!*****************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Actions/ActionTypes.ts ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    LOAD_PUBLIC_COURSES_LIST_FAILURE: 'LOAD_PUBLIC_COURSES_LIST_FAILURE',
    LOAD_PUBLIC_COURSES_LIST_SUCCESS: 'LOAD_PUBLIC_COURSES_LIST_SUCCESS',
    LOAD_INSTITUTE_LIST_SUCCESS: 'LOAD_INSTITUTE_LIST_SUCCESS',
    LOAD_DEPARTMENT_LIST_SUCCESS: 'LOAD_DEPARTMENT_LIST_SUCCESS',
    LOAD_PAGINATION_INFO_SUCCESS: 'LOAD_PAGINATION_INFO_SUCCESS',
});


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Actions/Actions.ts":
/*!*************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Actions/Actions.ts ***!
  \*************************************************************/
/*! exports provided: loadCoursesListRequest, getAllInstitute, getAllDepartment, loadCoursesListSuccess, loadCoursesListFailure, loadInstituteListSuccess, loadDepartmentListSuccess, loadPaginationInfoSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListRequest", function() { return loadCoursesListRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllInstitute", function() { return getAllInstitute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllDepartment", function() { return getAllDepartment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListSuccess", function() { return loadCoursesListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadCoursesListFailure", function() { return loadCoursesListFailure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadInstituteListSuccess", function() { return loadInstituteListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDepartmentListSuccess", function() { return loadDepartmentListSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadPaginationInfoSuccess", function() { return loadPaginationInfoSuccess; });
/* harmony import */ var _ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ActionTypes */ "./src/Screens/PublicCourses/Common/Actions/ActionTypes.ts");
/* harmony import */ var Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Utils/CoursesListToMap */ "./src/Utils/CoursesListToMap.ts");
/* harmony import */ var Utils_InstituteListToMap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/InstituteListToMap */ "./src/Utils/InstituteListToMap.ts");
/* harmony import */ var Utils_DepartmentListToMap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/DepartmentListToMap */ "./src/Utils/DepartmentListToMap.ts");

// Utils



function loadCoursesListRequest(parameters, state, limit, offset) {
    if (parameters === void 0) { parameters = null; }
    if (state === void 0) { state = null; }
    if (limit === void 0) { limit = 16; }
    if (offset === void 0) { offset = 0; }
    return function (dispatch) {
        var coursesListUrl = '/courseware/api/all_courses/' + (parameters ? parameters : '');
        var coursesListUrlCostom = '/courseware/api/all_courses/?course_info__is_published=True&limit=' + limit;
        if (state) {
            /*
            courseInstitute: null,
            courseDepartment: null,
            searchparam: null
            */
            coursesListUrlCostom += '&institute__name=' + (state.courseInstitute ? state.courseInstitute : '') +
                '&department__name=' + (state.courseDepartment ? state.courseDepartment : '') +
                '&search=' + (state.searchParam ? state.searchParam : '');
        }
        coursesListUrlCostom += '&offset=' + offset;
        return fetch(coursesListUrlCostom)
            .then(function (response) { return response.json(); })
            .then(function (data) {
            var paginationInfo = {
                totalCourses: data.count,
                nextLink: data.next,
                prevLink: data.previous
            };
            var coursesMap = Object(Utils_CoursesListToMap__WEBPACK_IMPORTED_MODULE_1__["default"])(data.results);
            dispatch(loadCoursesListSuccess(coursesMap));
            dispatch(loadPaginationInfoSuccess(paginationInfo));
        }).catch(function (error) {
            throw (error);
        });
    };
}
;
function getAllInstitute() {
    return function (dispatch) {
        var getAllInstituteUrl = '/courseware/get_allinstitutes';
        return fetch(getAllInstituteUrl)
            .then(function (response) { return response.json(); })
            .then(function (institutes) {
            var instituteMap = Object(Utils_InstituteListToMap__WEBPACK_IMPORTED_MODULE_2__["default"])(institutes['institutes']);
            dispatch(loadInstituteListSuccess(instituteMap));
        })
            .catch(function (error) {
            throw (error);
        });
    };
}
function getAllDepartment() {
    return function (dispatch) {
        var getAllDepartmentUrl = '/courseware/get_alldepartments';
        return fetch(getAllDepartmentUrl)
            .then(function (response) { return response.json(); })
            .then(function (departments) {
            var departmentsMap = Object(Utils_DepartmentListToMap__WEBPACK_IMPORTED_MODULE_3__["default"])(departments['departments']);
            dispatch(loadDepartmentListSuccess(departmentsMap));
        })
            .catch(function (error) {
            throw (error);
        });
    };
}
function loadCoursesListSuccess(publicCoursesMap) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_PUBLIC_COURSES_LIST_SUCCESS, publicCoursesMap: publicCoursesMap };
}
;
function loadCoursesListFailure() {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_PUBLIC_COURSES_LIST_FAILURE };
}
;
function loadInstituteListSuccess(instituteList) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_INSTITUTE_LIST_SUCCESS, instituteList: instituteList };
}
;
function loadDepartmentListSuccess(departmentList) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_DEPARTMENT_LIST_SUCCESS, departmentList: departmentList };
}
;
function loadPaginationInfoSuccess(paginationInfo) {
    return { type: _ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"].LOAD_PAGINATION_INFO_SUCCESS, paginationInfo: paginationInfo };
}


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Assets/FilterForm.sass":
/*!*****************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Assets/FilterForm.sass ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Assets/PublicCourses.sass":
/*!********************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Assets/PublicCourses.sass ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Components/Course.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Components/Course.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

;
var Course = function (props) {
    var _a, _b, _c;
    var course = props.course;
    var formatter = new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    });
    var start_date = '';
    try {
        start_date = formatter.format(Date.parse(course.course_info.start_time));
    }
    catch (e) {
        console.log(e);
    }
    // let start_date = Date.parse(course.course_info.start_time)
    // console.log(start_date)
    /*
<div className='course-box' id={`course${course.id}`} style={{ backgroundImage: 'url(' + course.image + ')' }}>
<div className='course-title' onClick={() => handleClicks(props)}>{course.title}</div>
<div className='view-course' onClick={() => handleClicks(props)}>View Course</div>
</div>
*/
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard col-md-3 col-sm-4" },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "dashboard-container db-student", id: "" + course.id },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info", onClick: function () { return handleClicks(props); } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "opacity" }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-info-label" },
                    course.enrollment_type == "O" ? "Open Course" : "Moderated Course",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { 
                        // data-toggle="tooltip" 
                        title: course.enrollment_type == "O" ? "Course open to public." : "Course open only to approved students.", 
                        // style={{font-size:1rem}}
                        className: "material-icons icon info-icon" }, "help_outline")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseImageContainer" }, course.image ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: course.image })) : (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { className: "courseImage", src: "/static/img/no_image.png" })))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "db-courseLabel", title: course.title, onClick: function () { return handleClicks(props); } },
                course.courseCode,
                ": ",
                course.title),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "d-flex flex-row db-footerLabel" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseCreated" },
                        "From ",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { title: (_a = course.institute) === null || _a === void 0 ? void 0 : _a.name, className: "db-institute" }, ((_b = course.institute) === null || _b === void 0 ? void 0 : _b.name) != null ? (_c = course.institute) === null || _c === void 0 ? void 0 : _c.name : "Other"))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { width: '50%' } },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "db-courseUpdated" },
                        "Created ",
                        start_date != "" ? start_date : "01/01/2020"))))));
};
var handleClicks = function (props) {
    window.location.href = "/courseware/course/" + props.course.id + "/";
};
/* harmony default export */ __webpack_exports__["default"] = (Course);


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Components/Pagination.tsx":
/*!********************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Components/Pagination.tsx ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function PageLink(_a) {
    var number = _a.number, pLink = _a.pLink, currentPage = _a.currentPage, handlePageChange = _a.handlePageChange;
    if (pLink == null) {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: number, className: "page-item disabled" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: "page-link" }, number)));
    }
    else if (number == currentPage) {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: number, className: "page-item active", onClick: function () { return handlePageChange(number); } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: "page-link" }, number)));
    }
    else {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", { key: number, className: "page-item", onClick: function () { return handlePageChange(number); } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { className: "page-link" }, number)));
    }
}
var Pagination = function (_a) {
    var pageLimit = _a.pageLimit, currentPage = _a.currentPage, setCurrentPage = _a.setCurrentPage, reLoadCourses = _a.reLoadCourses, props = _a.props;
    var pageNumbers = [];
    var paginationInfo = props.paginationInfo;
    var handlePageChange = function (pageNumber) {
        if (pageNumber == 'Next') {
            pageNumber = currentPage + 1;
        }
        if (pageNumber == 'Previous') {
            pageNumber = currentPage - 1;
        }
        setCurrentPage(pageNumber);
        reLoadCourses(undefined, pageNumber);
    };
    for (var i = 1; i <= Math.ceil(paginationInfo.totalCourses / pageLimit); i++) {
        pageNumbers.push(i);
    }
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("nav", { className: "mx-auto mt-3 col-md-12" },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", { className: "pagination" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](PageLink, { number: 'Previous', pLink: props.paginationInfo.prevLink, currentPage: currentPage, handlePageChange: handlePageChange }),
            pageNumbers.map(function (number) {
                if ((number >= currentPage - 6 && number <= currentPage + 4) || (currentPage <= 5 && number <= 10)) {
                    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](PageLink, { number: number, pLink: number, currentPage: currentPage, handlePageChange: handlePageChange });
                    // <li key={number} className="page-item">
                    // <a href="!#" className="page-link">{number}</a>
                    // </li>
                }
            }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](PageLink, { number: 'Next', pLink: props.paginationInfo.nextLink, currentPage: currentPage, handlePageChange: handlePageChange }))));
};
/* harmony default export */ __webpack_exports__["default"] = (Pagination);


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Containers/FilterForm.tsx":
/*!********************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Containers/FilterForm.tsx ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.browser.esm.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



var FilterForm = /** @class */ (function (_super) {
    __extends(FilterForm, _super);
    function FilterForm(props) {
        var _this = _super.call(this, props) || this;
        _this.handleChange = function (e, props) {
            var targetName = e.target.getAttribute('name');
            var targetValue = e.target.value;
            if (targetValue.length > 0) {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm .' + targetName + ' + .clear').addClass('addClear');
                _this.setState({ searchParam: targetValue });
            }
            else {
                jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm .' + targetName + ' + .clear').removeClass('addClear');
                _this.setState({ searchParam: null });
            }
            props.filterByChangeState(targetName, targetValue);
        };
        _this.handleClear = function (e, props) {
            var targetName = e.target.getAttribute('name');
            jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm .' + targetName).val('');
            jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm .' + targetName + ' + .clear').removeClass('addClear');
            // changeState(targetName, '', props)
            _this.setState({ searchParam: null });
            props.filterByChangeState(targetName, '');
            props.reLoadCourses(undefined, _this.state);
            // let filterparams='';
            // filterparams += '?institute__name='+ (this.state.courseDepartment!=null ? this.state.courseDepartment.value : '') 
            //                 + '&department__name='+ (this.state.courseDepartment!=null ? this.state.courseDepartment.value : '');
            // this.props.actions.loadCoursesListRequest(filterparams, this.state);
        };
        _this.state = {
            courseInstitute: null,
            courseDepartment: null,
            searchParam: null
        };
        return _this;
    }
    FilterForm.prototype.componentDidMount = function () {
        jquery__WEBPACK_IMPORTED_MODULE_2__("#filterForm input").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    };
    FilterForm.prototype.handleSelectChange = function (e, action, name, props) {
        var _a, _b;
        if (action == 'clear') {
            this.setState(__assign(__assign({}, this.state), (_a = {}, _a[name] = [], _a)));
            props.filterByChangeState(name, '');
        }
        else {
            var targetValue = e.value;
            this.setState(__assign(__assign({}, this.state), (_b = {}, _b[name] = [{ label: targetValue, value: targetValue }], _b)));
            props.filterByChangeState(name, targetValue);
        }
    };
    FilterForm.prototype.handleClearAll = function (e, props) {
        e.preventDefault();
        this.setState({
            courseInstitute: null,
            courseDepartment: null,
            searchParam: null
        });
        jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm input').val('');
        jquery__WEBPACK_IMPORTED_MODULE_2__('.filterForm .form-group .clear').removeClass('addClear');
        props.clearAllState();
        // props.reLoadCourses(undefined, this.state)
        // this.props.actions.loadCoursesListRequest();
    };
    FilterForm.prototype.render = function () {
        var _this = this;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: "filterForm", className: "filterForm" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "filterForm-title" }, "Filter Courses"),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("form", { className: "filterForm-form", onSubmit: function (e) { return handleSubmit(e); } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "text", className: "form-control searchParam", placeholder: "Course Title/ CourseCode", name: "searchParam", onChange: function (e) { return _this.handleChange(e, _this.props); } }),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { className: "clear", onClick: function (e) { return _this.handleClear(e, _this.props); } }, "\u00D7")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_select__WEBPACK_IMPORTED_MODULE_1__["default"], { isClearable: "true", name: "courseInstitute", value: this.state.courseInstitute, placeholder: "Course Institute", options: selectOptions(this.props.instituteList), onChange: function (e, _a) {
                            var action = _a.action;
                            _this.handleSelectChange(e, action, "courseInstitute", _this.props);
                        } })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "form-group" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_select__WEBPACK_IMPORTED_MODULE_1__["default"], { isClearable: "true", name: "courseDepartment", value: this.state.courseDepartment, placeholder: "Course Department", className: "myInput", options: selectOptions(this.props.departmentList), onChange: function (e, _a) {
                            var action = _a.action;
                            _this.handleSelectChange(e, action, "courseDepartment", _this.props);
                        } })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "clearAllButton" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", { className: "bodhi-button", onClick: function (e) { return _this.handleClearAll(e, _this.props); } }, " Clear All")))));
    };
    return FilterForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
// const handleChange = (e, props) => {
//     let targetName = e.target.getAttribute('name');
//     let targetValue = e.target.value;
//     if(targetValue.length > 0){
//         $('.filterForm .'+targetName+' + .clear').addClass('addClear')
//     }else{
//         $('.filterForm .'+targetName+' + .clear').removeClass('addClear')
//     }
//     props.filterByChangeState(targetName, targetValue);   
// }
var changeState = function (targetName, targetValue, props) {
    props.filterByChangeState(targetName, targetValue);
};
// const handleClear = (e, props) => {
//     let targetName = e.target.getAttribute('name'); 
//     $('.filterForm .'+targetName).val('')
//     $('.filterForm .'+targetName+' + .clear').removeClass('addClear')
//     changeState(targetName, '', props)
// } 
var handleSubmit = function (e) {
    e.preventDefault();
};
var handleClicks = function (props) {
    window.location.href = "/courseware/course/" + props.course.id + "/";
};
var selectOptions = function (list) {
    var options = [];
    list.forEach(function (item) {
        options.push({ value: item.name, label: item.name });
    });
    return options;
};
/* harmony default export */ __webpack_exports__["default"] = (FilterForm);


/***/ }),

/***/ "./src/Screens/PublicCourses/Common/Containers/PublicCoursesPage.tsx":
/*!***************************************************************************!*\
  !*** ./src/Screens/PublicCourses/Common/Containers/PublicCoursesPage.tsx ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _Assets_PublicCourses_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assets/PublicCourses.sass */ "./src/Screens/PublicCourses/Common/Assets/PublicCourses.sass");
/* harmony import */ var _Assets_PublicCourses_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Assets_PublicCourses_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Assets_FilterForm_sass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Assets/FilterForm.sass */ "./src/Screens/PublicCourses/Common/Assets/FilterForm.sass");
/* harmony import */ var _Assets_FilterForm_sass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_Assets_FilterForm_sass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _Components_Course__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Components/Course */ "./src/Screens/PublicCourses/Common/Components/Course.tsx");
/* harmony import */ var Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Screens/Layout/Common/Components/Navbar */ "./src/Screens/Layout/Common/Components/Navbar.tsx");
/* harmony import */ var _FilterForm__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./FilterForm */ "./src/Screens/PublicCourses/Common/Containers/FilterForm.tsx");
/* harmony import */ var _Components_Pagination__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Components/Pagination */ "./src/Screens/PublicCourses/Common/Components/Pagination.tsx");
/* harmony import */ var _Actions_Actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Actions/Actions */ "./src/Screens/PublicCourses/Common/Actions/Actions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



// Assets


// Components




// Actions

;
var PublicCoursesPage = /** @class */ (function (_super) {
    __extends(PublicCoursesPage, _super);
    function PublicCoursesPage(props) {
        var _this = _super.call(this, props) || this;
        _this.setCurrentPage = function (pageNumber) {
            _this.setState({ currentPage: pageNumber });
        };
        _this.reLoadCourses = function (state, pageNumber, pageLimit) {
            if (state === void 0) { state = null; }
            if (pageNumber === void 0) { pageNumber = ''; }
            if (pageLimit === void 0) { pageLimit = null; }
            if (!state) {
                state = _this.state;
            }
            if (!pageLimit) {
                pageLimit = _this.state.pageLimit;
            }
            if (pageNumber == '') {
                pageNumber = '1';
                _this.setCurrentPage(1);
            }
            var offset = pageLimit * (parseInt(pageNumber) - 1);
            // if(pageNumber=='-1' && this.state.currentPage >0){
            //     pageNumber= ''+ (this.state.currentPage -1);
            // }
            // if(pageNumber=='+1' && this.state.currentPage < Math.ceil(this.state.totalCount / this.state.pageLimit) ){
            //     pageNumber= ''+ (this.state.currentPage +1);
            // }
            _this.props.actions.loadCoursesListRequest(undefined, state, pageLimit, offset);
        };
        _this.state = {
            coursesMap: _this.props.coursesMap,
            searchParam: '',
            courseDepartment: '',
            courseInstitute: '',
            currentPage: 1,
            pageLimit: 16,
            paginationInfo: _this.props.paginationInfo,
        };
        return _this;
    }
    PublicCoursesPage.prototype.componentDidMount = function () {
        if (this.props.coursesMap.size === 0) {
            this.props.actions.loadCoursesListRequest();
        }
        this.setState(__assign(__assign({}, this.state), { coursesMap: this.props.coursesMap }));
        this.props.actions.getAllInstitute();
        this.props.actions.getAllDepartment();
    };
    PublicCoursesPage.prototype.filterByChangeState = function (name, value) {
        var _a;
        this.setState(__assign(__assign({}, this.state), (_a = {}, _a[name] = value, _a)), function () {
            this.reLoadCourses(this.state);
        });
    };
    PublicCoursesPage.prototype.clearAllState = function () {
        this.setState(__assign(__assign({}, this.state), { searchParam: '', courseDepartment: '', courseInstitute: '' }), function () {
            this.reLoadCourses(this.state);
        });
    };
    PublicCoursesPage.prototype.render = function () {
        var courses;
        var courseCount = 0;
        var pagination = null;
        var indexStart = 0;
        var indexEnd = 0;
        if (this.props.coursesMap.size !== 0) {
            courses = [];
            this.props.coursesMap.forEach(function (course) {
                // if(course.title.toLowerCase().indexOf(this.state.courseName.toLowerCase()) >= 0
                // && course.institute && course.institute.name.toLowerCase().indexOf(this.state.courseInstitute.toLowerCase()) >= 0
                // && course.department && course.department.name.toLowerCase().indexOf(this.state.courseDepartment.toLowerCase()) >= 0
                // ) {
                if (course.course_info.is_published) {
                    courses.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Course__WEBPACK_IMPORTED_MODULE_5__["default"], { course: course, key: course.id }));
                }
                // courseCount++;
                // }
            });
        }
        if (courses && this.props.paginationInfo != null) {
            pagination = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Components_Pagination__WEBPACK_IMPORTED_MODULE_8__["default"], { currentPage: this.state.currentPage, setCurrentPage: this.setCurrentPage, pageLimit: this.state.pageLimit, reLoadCourses: this.reLoadCourses, props: this.props });
            courseCount = this.props.paginationInfo.totalCourses;
            indexStart = (this.state.currentPage - 1) * this.state.pageLimit + 1;
            var end = (indexStart + this.state.pageLimit - 1);
            indexEnd = (end < courseCount) ? end : courseCount;
        }
        if (!courses) {
            if (this.state.searchParam != '' ||
                this.state.courseDepartment != '' ||
                this.state.courseInstitute != '') {
                courses = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'alert alert-danger alert-dismissable bt-alert' }, "No results match your search");
            }
            else {
                courses = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'alert alert-danger alert-dismissable bt-alert' }, "No courses offered yet");
            }
        }
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Screens_Layout_Common_Components_Navbar__WEBPACK_IMPORTED_MODULE_6__["default"], { navbarClass: 'navbar-light' }),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "row public-courses-container" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-3 filterForm-container " },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_FilterForm__WEBPACK_IMPORTED_MODULE_7__["default"], { clearAllState: this.clearAllState.bind(this), filterByChangeState: this.filterByChangeState.bind(this), filterState: this.state, courseCount: courseCount, instituteList: this.props.instituteList, departmentList: this.props.departmentList, reLoadCourses: this.reLoadCourses, 
                        // 
                        actions: this.props.actions })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: 'col-md-8 row public-courses-list' },
                    (courseCount > 0) ? (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { id: "courseCountLabel", className: "col-md-12 infoLabel" },
                        "Showing",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { id: "courseCount" },
                            " ",
                            indexStart,
                            " - ",
                            indexEnd),
                        " out of ",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { id: "courseCount" },
                            " ",
                            courseCount),
                        " results")) : "",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "col-md-12 courseCard-container" }, courses),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "bt-pafination col-md-12" }, pagination))));
    };
    return PublicCoursesPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
;
;
var mapStateToProps = function (state) { return ({
    coursesMap: state.publicCoursesMap,
    instituteList: state.instituteList,
    departmentList: state.departmentList,
    paginationInfo: state.paginationInfo,
}); };
var mapDispatchToProps = function (dispatch) { return ({
    actions: Object(redux__WEBPACK_IMPORTED_MODULE_1__["bindActionCreators"])(_Actions_Actions__WEBPACK_IMPORTED_MODULE_9__, dispatch)
}); };
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(PublicCoursesPage));


/***/ }),

/***/ "./src/Screens/PublicProgress/Student/PublicProgress.tsx":
/*!***************************************************************!*\
  !*** ./src/Screens/PublicProgress/Student/PublicProgress.tsx ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var PublicProgress = /** @class */ (function (_super) {
    __extends(PublicProgress, _super);
    function PublicProgress() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PublicProgress.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "PublicProgress PlaceHolder");
    };
    return PublicProgress;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (PublicProgress);
;


/***/ }),

/***/ "./src/Screens/Rga/Instructor/Rga.tsx":
/*!********************************************!*\
  !*** ./src/Screens/Rga/Instructor/Rga.tsx ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Rga = /** @class */ (function (_super) {
    __extends(Rga, _super);
    function Rga() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rga.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Rga PlaceHolder");
    };
    return Rga;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Rga);
;


/***/ }),

/***/ "./src/Screens/Rga/Student/Rga.tsx":
/*!*****************************************!*\
  !*** ./src/Screens/Rga/Student/Rga.tsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Rga = /** @class */ (function (_super) {
    __extends(Rga, _super);
    function Rga() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rga.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Rga PlaceHolder");
    };
    return Rga;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Rga);
;


/***/ }),

/***/ "./src/Screens/Schedule/Instructor/Schedule.tsx":
/*!******************************************************!*\
  !*** ./src/Screens/Schedule/Instructor/Schedule.tsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Schedule = /** @class */ (function (_super) {
    __extends(Schedule, _super);
    function Schedule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Schedule.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Schedule PlaceHolder");
    };
    return Schedule;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Schedule);
;


/***/ }),

/***/ "./src/Screens/Schedule/Student/Schedule.tsx":
/*!***************************************************!*\
  !*** ./src/Screens/Schedule/Student/Schedule.tsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var Schedule = /** @class */ (function (_super) {
    __extends(Schedule, _super);
    function Schedule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Schedule.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "Schedule PlaceHolder");
    };
    return Schedule;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Schedule);
;


/***/ }),

/***/ "./src/Screens/StudentActivityOptionsList/Instructor/StudentActivityOptionsList.tsx":
/*!******************************************************************************************!*\
  !*** ./src/Screens/StudentActivityOptionsList/Instructor/StudentActivityOptionsList.tsx ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var StudentActivityOptionsList = /** @class */ (function (_super) {
    __extends(StudentActivityOptionsList, _super);
    function StudentActivityOptionsList() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StudentActivityOptionsList.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "StudentActivityOptionsList PlaceHolder");
    };
    return StudentActivityOptionsList;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (StudentActivityOptionsList);
;


/***/ }),

/***/ "./src/Screens/TimedQuiz/Student/TimedQuiz.tsx":
/*!*****************************************************!*\
  !*** ./src/Screens/TimedQuiz/Student/TimedQuiz.tsx ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var TimedQuiz = /** @class */ (function (_super) {
    __extends(TimedQuiz, _super);
    function TimedQuiz() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TimedQuiz.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "TimedQuiz PlaceHolder");
    };
    return TimedQuiz;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (TimedQuiz);
;


/***/ }),

/***/ "./src/Screens/TimedQuizAdmin/Instructor/TimedQuizAdmin.tsx":
/*!******************************************************************!*\
  !*** ./src/Screens/TimedQuizAdmin/Instructor/TimedQuizAdmin.tsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

;
var TimedQuizAdmin = /** @class */ (function (_super) {
    __extends(TimedQuizAdmin, _super);
    function TimedQuizAdmin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TimedQuizAdmin.prototype.render = function () {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "TimedQuizAdmin PlaceHolder");
    };
    return TimedQuizAdmin;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (TimedQuizAdmin);
;


/***/ }),

/***/ "./src/Screens/Video/Instructor/Assets/video.sass":
/*!********************************************************!*\
  !*** ./src/Screens/Video/Instructor/Assets/video.sass ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/ConceptVideoBox.tsx":
/*!*********************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/ConceptVideoBox.tsx ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var _VideoContent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./VideoContent */ "./src/Screens/Video/Instructor/Components/VideoContent.tsx");
/* harmony import */ var _VideoTOC__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./VideoTOC */ "./src/Screens/Video/Instructor/Components/VideoTOC.tsx");
/* harmony import */ var _VideoSlide__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./VideoSlide */ "./src/Screens/Video/Instructor/Components/VideoSlide.tsx");
/* harmony import */ var _Video__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Video */ "./src/Screens/Video/Instructor/Components/Video.tsx");
/* harmony import */ var _VideoVote__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./VideoVote */ "./src/Screens/Video/Instructor/Components/VideoVote.tsx");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var Utils_PostData__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Utils/PostData */ "./src/Utils/PostData.js");
/* harmony import */ var _Assets_video_sass__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Assets/video.sass */ "./src/Screens/Video/Instructor/Assets/video.sass");
/* harmony import */ var _Assets_video_sass__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_Assets_video_sass__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _MarkerForm__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./MarkerForm */ "./src/Screens/Video/Instructor/Components/MarkerForm.tsx");
/* harmony import */ var _SectionMarker__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./SectionMarker */ "./src/Screens/Video/Instructor/Components/SectionMarker.tsx");
/* harmony import */ var _QuizMarker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./QuizMarker */ "./src/Screens/Video/Instructor/Components/QuizMarker.tsx");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// utils





var ConceptVideoBox = /** @class */ (function (_super) {
    __extends(ConceptVideoBox, _super);
    function ConceptVideoBox(props) {
        var _this = _super.call(this, props) || this;
        var data = _this.props.data;
        var cur_marker = 0;
        if (data.content.markers.length == 0) {
            cur_marker = -1;
        }
        _this.videoRef = react__WEBPACK_IMPORTED_MODULE_0__["createRef"]();
        _this.state = {
            title: data.content.title,
            content: data.content.content,
            currentMarker: cur_marker,
            markers: data.content.markers,
            startTime: 0,
            videoLength: data.content.duration,
            markerArea: null,
            showMarkerEdit: false,
            showMarkerForm: false
        };
        return _this;
    }
    ConceptVideoBox.prototype.updateDesc = function (newTitle, newContent) {
        this.setState({
            title: newTitle,
            content: newContent
        });
        // this.props.refresh();
    };
    ConceptVideoBox.prototype.changeCurrent = function (index) {
        var start_time = this.state.markers[index]['time'];
        console.log('changeCurrent=>', index, start_time);
        this.setState({
            currentMarker: index,
            startTime: start_time,
        });
        this.showMarkerEdit();
    };
    ConceptVideoBox.prototype.setMarkerArea = function (index) {
    };
    ConceptVideoBox.prototype.updateSeek = function (time) {
        this.setState({
            startTime: time
        });
    };
    ConceptVideoBox.prototype.showMarkerForm = function (flag) {
        if (flag === void 0) { flag = true; }
        if (flag) {
            this.setState({
                showMarkerEdit: false,
                showMarkerForm: true
            });
            // pause the video using ref
            this.videoRef.current.customPause();
        }
        else {
            this.setState({
                showMarkerEdit: false,
                showMarkerForm: false
            });
            // play the video using ref
            this.videoRef.current.customPlay();
        }
    };
    ConceptVideoBox.prototype.showMarkerEdit = function (flag) {
        if (flag === void 0) { flag = true; }
        if (flag) {
            this.setState({
                showMarkerEdit: true,
                showMarkerForm: false
            });
            // pause the video using ref
            this.videoRef.current.customPause();
        }
        else {
            this.setState({
                showMarkerEdit: false,
                showMarkerForm: false
            });
            // play the video using ref
            this.videoRef.current.customPlay();
        }
    };
    ConceptVideoBox.prototype.saveMarker = function (title, type) {
        var _this = this;
        // get current time of video from ref
        var ctime = this.videoRef.current.getCurrentTime();
        if (type === 'S') {
            // save section marker
            var payload = {
                title: title,
                type: type,
                video: this.props.data.content.id,
                time: ctime
            };
            var url = "/video/api/marker/?format=json";
            Object(Utils_PostData__WEBPACK_IMPORTED_MODULE_8__["default"])(url, payload)
                .then(function (res) {
                console.log(res);
                var len = _this.state.markers.length;
                _this.setState({
                    markers: res['markers']
                }, function () { _this.changeCurrent(len); });
                // this.props.refresh();
            })
                .catch(function (err) { return console.log(err); });
        }
        else {
        }
    };
    ConceptVideoBox.prototype.render = function () {
        var _this = this;
        var data = this.props.data;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], { style: { overflow: 'auto' } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { md: { span: 4 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"], { defaultActiveKey: "0" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Header, eventKey: "0" }, "Description"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Collapse, { eventKey: "0" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Body, null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_VideoContent__WEBPACK_IMPORTED_MODULE_2__["default"], { content: this.state.content, videoId: data.content.id, title: this.state.title, saveCallback: this.updateDesc, is_editable: data.content.is_editable })))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Header, eventKey: "1" }, "Table Of Content"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Collapse, { eventKey: "1" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Body, null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_VideoTOC__WEBPACK_IMPORTED_MODULE_3__["default"], { markers: this.state.markers, clickCallback: this.changeCurrent })))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Toggle, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Header, eventKey: "2" }, "Slides"),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Accordion"].Collapse, { eventKey: "2" },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Card"].Body, null,
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_VideoSlide__WEBPACK_IMPORTED_MODULE_4__["default"], { id: data.content.id, other_file: data.content.other_file })))))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { md: { span: 8 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "video-panel" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { className: "video-panel-heading", "md-span": 12 },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", null, this.state.title))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { "md-span": 12 },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Video__WEBPACK_IMPORTED_MODULE_5__["default"], { ref: this.videoRef, videoFile: data.content.video_file, videoStartTime: this.state.startTime }))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { "md-span": 12 },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_VideoVote__WEBPACK_IMPORTED_MODULE_6__["default"], { upvotes: data.content.upvotes, downvotes: data.content.downvotes, addMarker: this.showMarkerForm, updateSeek: this.updateSeek }))),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], { "md-span": 12 }, this.state.showMarkerForm ?
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_MarkerForm__WEBPACK_IMPORTED_MODULE_10__["default"], { saveCallback: this.saveMarker, cancelCallback: function () { return _this.showMarkerForm(false); } })
                            :
                                (this.state.showMarkerEdit ?
                                    (this.state.markers[this.state.currentMarker].type === 'Q' ?
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_QuizMarker__WEBPACK_IMPORTED_MODULE_12__["default"], { marker: this.state.markers[this.state.currentMarker], cancelCallback: function () { return _this.showMarkerEdit(false); } })
                                        :
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_SectionMarker__WEBPACK_IMPORTED_MODULE_11__["default"], { marker: this.state.markers[this.state.currentMarker], cancelCallback: function () { return _this.showMarkerEdit(false); } }))
                                    :
                                        null)))))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "updateDesc", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "changeCurrent", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "setMarkerArea", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "updateSeek", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "showMarkerForm", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "showMarkerEdit", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_1__["default"]
    ], ConceptVideoBox.prototype, "saveMarker", null);
    return ConceptVideoBox;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (ConceptVideoBox);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/MarkerForm.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/MarkerForm.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MarkerForm = /** @class */ (function (_super) {
    __extends(MarkerForm, _super);
    function MarkerForm(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            type: 'S',
            isModule: false,
            title: undefined
        };
        return _this;
    }
    MarkerForm.prototype.handleTypeSelect = function (eventKey, event) {
        this.setState({
            type: eventKey
        });
    };
    MarkerForm.prototype.handleQuizSelect = function (eventKey, event) {
        this.setState({
            isModule: eventKey === '1'
        });
    };
    MarkerForm.prototype.onSave = function () {
        this.props.saveCallback(this.state.title, this.state.type);
    };
    MarkerForm.prototype.render = function () {
        var _this = this;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 1 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: 'danger', onClick: this.props.cancelCallback }, "Cancel")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 3 }, style: { marginLeft: '2%' } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["DropdownButton"], { variant: "light", title: this.state.type === 'Q' ? 'Quiz Marker' : 'Section Marker', id: "dropdown-button-drop-right-2", onSelect: this.handleTypeSelect },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, { eventKey: "S" }, "Section Marker"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, { eventKey: "Q" }, "Quiz Marker"))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 3 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: 'text', style: { width: '99%' }, placeholder: 'Marker Title', onChange: function (e) { return _this.setState({ title: e.target.value }); } })),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 2 }, style: { marginLeft: '-4%' } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["DropdownButton"], { variant: "light", title: this.state.isModule ? 'Add Module' : 'Add Question', id: "dropdown-button-drop-right-3", onSelect: this.handleQuizSelect },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, { eventKey: '0' }, "Add Question"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, { eventKey: '1' }, "Add Module"))),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 3 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { style: { float: 'right' }, variant: 'success', onClick: this.onSave }, "Add"))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], MarkerForm.prototype, "handleTypeSelect", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], MarkerForm.prototype, "handleQuizSelect", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], MarkerForm.prototype, "onSave", null);
    return MarkerForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (MarkerForm);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/QuizMarker.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/QuizMarker.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Form */ "./node_modules/react-bootstrap/esm/Form.js");
/* harmony import */ var react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Col */ "./node_modules/react-bootstrap/esm/Col.js");
/* harmony import */ var react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Row */ "./node_modules/react-bootstrap/esm/Row.js");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Container */ "./node_modules/react-bootstrap/esm/Container.js");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Button */ "./node_modules/react-bootstrap/esm/Button.js");
/* harmony import */ var _material_ui_icons_AddCircleOutline__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/AddCircleOutline */ "./node_modules/@material-ui/icons/AddCircleOutline.js");
/* harmony import */ var _material_ui_icons_AddCircleOutline__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AddCircleOutline__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var Utils_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Utils/common */ "./src/Utils/common.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};








var MarkerEdit = /** @class */ (function (_super) {
    __extends(MarkerEdit, _super);
    function MarkerEdit(props) {
        var _this = _super.call(this, props) || this;
        _this.handleCancel = function () {
            _this.props.cancelCallback();
        };
        _this.handleSave = function (event) {
            var form = event.currentTarget;
            console.log(form);
            event.preventDefault();
            event.stopPropagation();
        };
        _this.handleTypeChange = function (type) {
            _this.setState({ type: type });
        };
        _this.handleAddOption = function () {
            _this.setState(function (state) {
                var opt = __spreadArrays(state.options);
                opt.push('');
                return __assign(__assign({}, state), { options: opt });
            });
        };
        _this.state = {
            type: 'mcq',
            options: ['', '']
        };
        return _this;
    }
    MarkerEdit.prototype.componentDidMount = function () {
    };
    MarkerEdit.prototype.render = function () {
        var _this = this;
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_4__["default"], { style: { background: 'white' } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { textAlign: "center" } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", null, "Edit Quiz")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"], { onSubmit: this.handleSave },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '4', controlId: 'time' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'time', disabled: true, value: "" + Object(Utils_common__WEBPACK_IMPORTED_MODULE_7__["toTime"])(this.props.marker.time) })),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '8', controlId: 'title' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'text', defaultValue: this.props.marker.quiz_title, placeholder: 'Quiz Title' }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__["default"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], null, "Question Type")),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__["default"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 3 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "radio", id: "mcq", defaultChecked: true, name: "qtype", onClick: function (e) { return _this.handleTypeChange('mcq'); } }),
                        ' ',
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: "mcq" }, "Multiple Choice")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 3 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "radio", id: "scq", name: "qtype", onClick: function (e) { return _this.handleTypeChange('scq'); } }),
                        ' ',
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: "scq" }, "Single Choice")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 3 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "radio", id: "fxd", name: "qtype", onClick: function (e) { return _this.handleTypeChange('fxd'); } }),
                        ' ',
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: 'fxd' }, "Fixed")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 3 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { type: "radio", id: 'qg', name: "qtype", onClick: function (e) { return _this.handleTypeChange('qg'); } }),
                        ' ',
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", { htmlFor: "qg" }, "Question Group"))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '12', controlId: 'description' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'text', defaultValue: '', placeholder: 'Question Description' }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
                this.state.type === 'mcq' || this.state.type === 'scq' ?
                    (
                    // mcq or scq
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                        this.state.options.map(function (opt, i) {
                            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, { key: i },
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '12', controlId: "option-" + i },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'text', defaultValue: opt, placeholder: "Option " + i }),
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { id: "ans-" + i, style: {
                                            position: 'absolute',
                                            right: '50px',
                                            top: '6px'
                                        }, type: _this.state.type === 'mcq' ? 'checkbox' : 'radio', name: _this.state.type, onClick: function (e) { return console.log(e.target); } }))));
                        }),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { variant: 'light', onClick: this.handleAddOption },
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_AddCircleOutline__WEBPACK_IMPORTED_MODULE_6___default.a, { fontSize: "small" }),
                            "Add Option")))
                    :
                        this.state.type === 'fxd' ?
                            (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                                this.state.options.map(function (opt, i) {
                                    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, { key: i },
                                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '12', controlId: "answer-" + i },
                                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'text', defaultValue: opt, placeholder: "Answer " + i }))));
                                }),
                                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { variant: 'light', onClick: this.handleAddOption },
                                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_material_ui_icons_AddCircleOutline__WEBPACK_IMPORTED_MODULE_6___default.a, { fontSize: "small" }),
                                    "Add Answer")))
                            :
                                this.state.type === 'qg' ?
                                    (null)
                                    :
                                        (null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '12', controlId: 'hint' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { type: 'text', defaultValue: '', placeholder: 'Hint' }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '3', controlId: 'marks' },
                        "Marks",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { type: 'number', defaultValue: '0' })),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: { span: 3, offset: 3 }, controlId: 'attempts' },
                        "Attempts",
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { type: 'number', defaultValue: '1' }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__["default"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 2 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { style: { border: '1px solid grey', width: '100%' }, variant: 'light', onClick: this.handleCancel }, "Delete")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: { span: 4, offset: 4 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { style: { border: '1px solid grey', width: '100%' }, variant: 'light' }, "Advanced Settings")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 2 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { style: { width: '100%' }, type: 'submit' }, "Save"))))));
    };
    return MarkerEdit;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (MarkerEdit);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/SectionMarker.tsx":
/*!*******************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/SectionMarker.tsx ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Form */ "./node_modules/react-bootstrap/esm/Form.js");
/* harmony import */ var react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap/Col */ "./node_modules/react-bootstrap/esm/Col.js");
/* harmony import */ var react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/Row */ "./node_modules/react-bootstrap/esm/Row.js");
/* harmony import */ var react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap/Container */ "./node_modules/react-bootstrap/esm/Container.js");
/* harmony import */ var react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/Button */ "./node_modules/react-bootstrap/esm/Button.js");
/* harmony import */ var Utils_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Utils/common */ "./src/Utils/common.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();







var MarkerEdit = /** @class */ (function (_super) {
    __extends(MarkerEdit, _super);
    function MarkerEdit(props) {
        var _this = _super.call(this, props) || this;
        _this.handleCancel = function () {
            _this.props.cancelCallback();
        };
        _this.handleSave = function (event) {
            var form = event.currentTarget;
            console.log(form);
            event.preventDefault();
            event.stopPropagation();
        };
        _this.state = {};
        return _this;
    }
    MarkerEdit.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Container__WEBPACK_IMPORTED_MODULE_4__["default"], { style: { background: 'white' } },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { style: { textAlign: "center" } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", null, "Edit Section")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"], { onSubmit: this.handleSave },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Row, null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '4', controlId: 'time' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'time', disabled: true, value: "" + Object(Utils_common__WEBPACK_IMPORTED_MODULE_6__["toTime"])(this.props.marker.time) })),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Group, { as: react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], md: '8', controlId: 'title' },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Form__WEBPACK_IMPORTED_MODULE_1__["default"].Control, { required: true, type: 'text', defaultValue: this.props.marker.title, placeholder: 'Section Title' }))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Row__WEBPACK_IMPORTED_MODULE_3__["default"], null,
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: 2 },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { style: { border: '1px solid grey', width: '100%' }, variant: 'light', onClick: this.handleCancel }, "Delete")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Col__WEBPACK_IMPORTED_MODULE_2__["default"], { md: { span: 2, offset: 8 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap_Button__WEBPACK_IMPORTED_MODULE_5__["default"], { style: { width: '100%' }, type: 'submit' }, "Save"))))));
    };
    return MarkerEdit;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (MarkerEdit);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/Video.tsx":
/*!***********************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/Video.tsx ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_video_js_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-video-js-player */ "./node_modules/react-video-js-player/build/index.js");
/* harmony import */ var react_video_js_player__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_video_js_player__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Assets_video_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assets/video.sass */ "./src/Screens/Video/Instructor/Assets/video.sass");
/* harmony import */ var _Assets_video_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Assets_video_sass__WEBPACK_IMPORTED_MODULE_2__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Video = /** @class */ (function (_super) {
    __extends(Video, _super);
    function Video(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    Video.prototype.onPlayerReady = function (player) {
        console.log("Player is ready: ", player);
        this.player = player;
        this.player.currentTime(this.props.videoStartTime);
        this.player.addRemoteTextTrack({
            label: "English",
            src: "/media/static/video/a.srt",
            srclang: "en",
            default: true
        }, false);
    };
    Video.prototype.onVideoPlay = function (duration) {
        console.log("Video played at: ", duration);
    };
    Video.prototype.onVideoPause = function (duration) {
        console.log("Video paused at: ", duration);
    };
    Video.prototype.onVideoTimeUpdate = function (duration) {
        console.log("Time updated: ", duration);
    };
    Video.prototype.onVideoSeeking = function (duration) {
        console.log("Video seeking: ", duration);
    };
    Video.prototype.onVideoSeeked = function (from, to) {
        console.log("Video seeked from " + from + " to " + to);
    };
    Video.prototype.onVideoEnd = function () {
        console.log("Video ended");
    };
    Video.prototype.componentDidUpdate = function (oldProps) {
        if (oldProps.videoStartTime !== this.props.videoStartTime) {
            this.player.currentTime(this.props.videoStartTime);
            this.player.pause();
        }
    };
    Video.prototype.customPlay = function () {
        this.player.play();
    };
    Video.prototype.customPause = function () {
        this.player.pause();
    };
    Video.prototype.getCurrentTime = function () {
        return this.player.currentTime();
    };
    Video.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_video_js_player__WEBPACK_IMPORTED_MODULE_1___default.a, { controls: true, src: this.props.videoFile, height: "480", bigPlayButtonCentered: false, className: "video-react-component", onReady: this.onPlayerReady.bind(this), onPlay: this.onVideoPlay.bind(this), onPause: this.onVideoPause.bind(this), onTimeUpdate: this.onVideoTimeUpdate.bind(this), onSeeking: this.onVideoSeeking.bind(this), onSeeked: this.onVideoSeeked.bind(this), onEnd: this.onVideoEnd.bind(this) })));
    };
    return Video;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Video);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/VideoContent.tsx":
/*!******************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/VideoContent.tsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/PatchData */ "./src/Utils/PatchData.js");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var VideoContent = /** @class */ (function (_super) {
    __extends(VideoContent, _super);
    function VideoContent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            showForm: false
        };
        return _this;
    }
    VideoContent.prototype.onSave = function (event) {
        var _this = this;
        event.preventDefault();
        event.stopPropagation();
        var url = "/video/api/video/" + this.props.videoId + "/?format=json";
        var form = event.currentTarget;
        var data = {
            title: form["title"].value,
            content: form["description"].value
        };
        Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, data)
            .then(function (response) {
            _this.props.saveCallback(data.title, data.content);
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__["default"])("Description Saved Successfully", "success");
            _this.setState({ showForm: false });
        })
            .catch(function (err) {
            console.log(err);
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__["default"])("Description Save Failed", "error");
        });
    };
    VideoContent.prototype.render = function () {
        var _this = this;
        return (this.state.showForm ?
            (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"], { onSubmit: this.onSave },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], controlId: "title" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, null, "Title"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, { required: true, type: "text", defaultValue: this.props.title })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], controlId: "description" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, null, "Description"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, { as: "textarea", rows: 4, defaultValue: this.props.content })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"] },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "success", type: "submit" }, "Save")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "danger", type: "button", onClick: function (event) { event.preventDefault(); _this.setState({ showForm: false }); } }, "Cancel")))))
            :
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "video-desc" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h4", null, this.props.title),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, this.props.content),
                    this.props.is_editable ?
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { onClick: function () { _this.setState({ showForm: true }); } }, "Edit")
                        : null));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], VideoContent.prototype, "onSave", null);
    return VideoContent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (VideoContent);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/VideoSlide.tsx":
/*!****************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/VideoSlide.tsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
/* harmony import */ var Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/PatchData */ "./src/Utils/PatchData.js");
/* harmony import */ var Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Utils/DisplayGlobalMessage */ "./src/Utils/DisplayGlobalMessage.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var VideoSlide = /** @class */ (function (_super) {
    __extends(VideoSlide, _super);
    function VideoSlide(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            showForm: false
        };
        return _this;
    }
    VideoSlide.prototype.onSave = function (event) {
        var _this = this;
        var url = "/video/api/video/" + this.props.id + "/?format=json";
        event.preventDefault();
        var form = event.currentTarget;
        console.log('form=>', form);
        var formData = new FormData();
        formData.append('other_file', form['slide']['files'][0]);
        console.log('formData=>', formData);
        Object(Utils_PatchData__WEBPACK_IMPORTED_MODULE_3__["default"])(url, formData, "FormData")
            .then(function (response) {
            console.log('slide upload response=>', response);
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__["default"])("Slide Updated", "success");
            _this.setState({ showForm: false });
        })
            .catch(function (error) {
            console.log('slide upload error=>', error);
            Object(Utils_DisplayGlobalMessage__WEBPACK_IMPORTED_MODULE_4__["default"])("Slide Upload Failed", "error");
        });
    };
    VideoSlide.prototype.onRemove = function () {
        console.log('remove not implemented');
    };
    VideoSlide.prototype.render = function () {
        var _this = this;
        return (this.state.showForm ?
            (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"], { onSubmit: this.onSave },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], controlId: "slide" },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, null, "Slide"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, { type: "file" })),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, { as: react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"] },
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "success", type: "submit" }, "Save")),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { sm: { span: 4, offset: 2 } },
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "danger", type: "button", onClick: function (event) { event.preventDefault(); _this.setState({ showForm: false }); } }, "Cancel")))))
            :
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "video-slide" },
                    this.props.other_file !== '/media/' ?
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", { href: this.props.other_file, target: "_blank" }, "View") :
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, "No Slides Uploaded"),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "primary", onClick: function () { _this.setState({ showForm: true }); } }, "Change")),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null,
                            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: "danger", onClick: this.onRemove }, "Remove")))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], VideoSlide.prototype, "onSave", null);
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], VideoSlide.prototype, "onRemove", null);
    return VideoSlide;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (VideoSlide);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/VideoTOC.tsx":
/*!**************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/VideoTOC.tsx ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var Utils_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/common */ "./src/Utils/common.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var VideoTOC = /** @class */ (function (_super) {
    __extends(VideoTOC, _super);
    function VideoTOC(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    VideoTOC.prototype.handleClick = function (index) {
        this.props.clickCallback(index);
        console.log('handleClick');
        return false;
    };
    VideoTOC.prototype.render = function () {
        var _this = this;
        var markers = this.props.markers.map(function (marker, index) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, { key: marker.id, action: true, onClick: _this.handleClick.bind(_this, index) },
                marker.type === 'S' ? marker.title : "Quiz : " + marker.quiz_title,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { float: 'right' } }, Object(Utils_common__WEBPACK_IMPORTED_MODULE_2__["toTime"])(marker.time))));
        });
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], null, markers));
    };
    return VideoTOC;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (VideoTOC);


/***/ }),

/***/ "./src/Screens/Video/Instructor/Components/VideoVote.tsx":
/*!***************************************************************!*\
  !*** ./src/Screens/Video/Instructor/Components/VideoVote.tsx ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var autobind_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! autobind-decorator */ "./node_modules/autobind-decorator/lib/esm/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VideoVote = /** @class */ (function (_super) {
    __extends(VideoVote, _super);
    function VideoVote(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    VideoVote.prototype.handleOnChange = function (event) {
        event.preventDefault();
        event.stopPropagation();
        var time = event.target.value.trim();
        time = time.split(':');
        if (time.length === 2) {
            time = parseInt(time[0]) * 60 + parseInt(time[1]);
            this.props.updateSeek(time);
        }
    };
    VideoVote.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { md: 4 },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", { name: "time", type: "time", defaultValue: "00:00", onChange: this.handleOnChange, className: "form-control form-marker-time" })),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { md: { span: 2 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], { variant: 'light', onClick: this.props.addMarker }, "Add Quiz/Marker")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { md: { span: 2, offset: 2 } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { color: "green" } },
                    "Upvotes:",
                    this.props.upvotes)),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], { "md-span": 2 },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { color: "red" } },
                    "Downvotes:",
                    this.props.downvotes))));
    };
    __decorate([
        autobind_decorator__WEBPACK_IMPORTED_MODULE_2__["default"]
    ], VideoVote.prototype, "handleOnChange", null);
    return VideoVote;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (VideoVote);


/***/ }),

/***/ "./src/Screens/Video/Student/Assets/videoStud.sass":
/*!*********************************************************!*\
  !*** ./src/Screens/Video/Student/Assets/videoStud.sass ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Screens/Video/Student/Components/Video.tsx":
/*!********************************************************!*\
  !*** ./src/Screens/Video/Student/Components/Video.tsx ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_video_js_player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-video-js-player */ "./node_modules/react-video-js-player/build/index.js");
/* harmony import */ var react_video_js_player__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_video_js_player__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assets/videoStud.sass */ "./src/Screens/Video/Student/Assets/videoStud.sass");
/* harmony import */ var _Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Video = /** @class */ (function (_super) {
    __extends(Video, _super);
    function Video(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    Video.prototype.onPlayerReady = function (player) {
        console.log("Player is ready: ", player);
        this.player = player;
        this.player.currentTime(this.props.videoStartTime);
        this.player.addRemoteTextTrack({
            label: "English",
            src: "/media/static/video/a.srt",
            srclang: "en",
            default: true
        }, false);
    };
    Video.prototype.onVideoPlay = function (duration) {
        console.log("Video played at: ", duration);
    };
    Video.prototype.onVideoPause = function (duration) {
        console.log("Video paused at: ", duration);
    };
    Video.prototype.onVideoTimeUpdate = function (duration) {
        console.log("Time updated: ", duration);
    };
    Video.prototype.onVideoSeeking = function (duration) {
        console.log("Video seeking: ", duration);
    };
    Video.prototype.onVideoSeeked = function (from, to) {
        console.log("Video seeked from " + from + " to " + to);
    };
    Video.prototype.onVideoEnd = function () {
        console.log("Video ended");
    };
    Video.prototype.componentWillUpdate = function (newProps) {
        console.log(newProps);
        this.player.currentTime(newProps.videoStartTime);
        this.player.pause();
    };
    Video.prototype.render = function () {
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_video_js_player__WEBPACK_IMPORTED_MODULE_1___default.a, { controls: true, src: this.props.videoFile, height: "480", bigPlayButtonCentered: false, className: "video-react-component", onReady: this.onPlayerReady.bind(this), onPlay: this.onVideoPlay.bind(this), onPause: this.onVideoPause.bind(this), onTimeUpdate: this.onVideoTimeUpdate.bind(this), onSeeking: this.onVideoSeeking.bind(this), onSeeked: this.onVideoSeeked.bind(this), onEnd: this.onVideoEnd.bind(this) })));
    };
    return Video;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (Video);


/***/ }),

/***/ "./src/Screens/Video/Student/Components/VideoTOC.tsx":
/*!***********************************************************!*\
  !*** ./src/Screens/Video/Student/Components/VideoTOC.tsx ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assets/videoStud.sass */ "./src/Screens/Video/Student/Assets/videoStud.sass");
/* harmony import */ var _Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Assets_videoStud_sass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var Utils_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Utils/common */ "./src/Utils/common.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();




var VideoTOC = /** @class */ (function (_super) {
    __extends(VideoTOC, _super);
    function VideoTOC(props) {
        return _super.call(this, props) || this;
    }
    VideoTOC.prototype.handleClick = function (index) {
        this.props.clickCallback(index);
        console.log('handleClick');
        return false;
    };
    VideoTOC.prototype.render = function () {
        var _this = this;
        var markers = this.props.markers.map(function (marker, index) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"].Item, { key: marker.id, action: true, onClick: _this.handleClick.bind(_this, index) },
                marker.title,
                marker.type === 'S' ? marker.title : "Quiz : " + marker.quiz_title,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { float: 'right' } }, Object(Utils_common__WEBPACK_IMPORTED_MODULE_3__["toTime"])(marker.time))));
        });
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], null, markers));
    };
    return VideoTOC;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (VideoTOC);


/***/ }),

/***/ "./src/Screens/index.ts":
/*!******************************!*\
  !*** ./src/Screens/index.ts ***!
  \******************************/
/*! exports provided: instructorCoursesListActionTypes, studentCoursesListActionTypes, studentGroupConceptsActionTypes, studentCourseGroupsActionTypes, instructorCourseGCActionTypes, instructorConceptDataActionTypes, studentConceptDataActionTypes, publicCoursesListActionTypes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CoursesList_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CoursesList/Instructor/Actions/ActionTypes */ "./src/Screens/CoursesList/Instructor/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "instructorCoursesListActionTypes", function() { return _CoursesList_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _CoursesList_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CoursesList/Student/Actions/ActionTypes */ "./src/Screens/CoursesList/Student/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "studentCoursesListActionTypes", function() { return _CoursesList_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _CoursePlaylist_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CoursePlaylist/Student/Actions/ActionTypes */ "./src/Screens/CoursePlaylist/Student/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "studentGroupConceptsActionTypes", function() { return _CoursePlaylist_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_2__["studentGroupConceptsActionTypes"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "studentCourseGroupsActionTypes", function() { return _CoursePlaylist_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_2__["studentCourseGroupsActionTypes"]; });

/* harmony import */ var _CoursePlaylist_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CoursePlaylist/Instructor/Actions/ActionTypes */ "./src/Screens/CoursePlaylist/Instructor/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "instructorCourseGCActionTypes", function() { return _CoursePlaylist_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _Concept_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Concept/Instructor/Actions/ActionTypes */ "./src/Screens/Concept/Instructor/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "instructorConceptDataActionTypes", function() { return _Concept_Instructor_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _Concept_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Concept/Student/Actions/ActionTypes */ "./src/Screens/Concept/Student/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "studentConceptDataActionTypes", function() { return _Concept_Student_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_5__["default"]; });

/* harmony import */ var _PublicCourses_Common_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./PublicCourses/Common/Actions/ActionTypes */ "./src/Screens/PublicCourses/Common/Actions/ActionTypes.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "publicCoursesListActionTypes", function() { return _PublicCourses_Common_Actions_ActionTypes__WEBPACK_IMPORTED_MODULE_6__["default"]; });










/***/ }),

/***/ "./src/Utils/CoursesListToMap.ts":
/*!***************************************!*\
  !*** ./src/Utils/CoursesListToMap.ts ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return coursesListToMap; });
function coursesListToMap(coursesList) {
    var coursesMap = new Map();
    coursesList.forEach(function (course) {
        coursesMap.set(course.id, course);
    });
    return coursesMap;
}
;


/***/ }),

/***/ "./src/Utils/DeleteData.js":
/*!*********************************!*\
  !*** ./src/Utils/DeleteData.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return deleteData; });
/* harmony import */ var _GetCookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GetCookie */ "./src/Utils/GetCookie.js");


function getDeleteBody(data, contentType) {
    switch (contentType) {
        case "FormData":
            return data;
        default:
            return JSON.stringify(data);
    }
}

async function deleteData(url = '', data = {},contentType="application/json") {
    
    const customHeaders = {
        'X-CSRFToken': Object(_GetCookie__WEBPACK_IMPORTED_MODULE_0__["default"])('csrftoken')
    }
    if(contentType!=="FormData"){
        customHeaders['Content-Type']  = contentType;
    }
    const response = await fetch(url, {
        method: 'DELETE',
        mode: 'cors',
        cache: 'no-cache',
        headers: customHeaders,
        referrer: 'no-referrer',
        body: getDeleteBody(data, contentType)
    });

    return response;
}

/***/ }),

/***/ "./src/Utils/DepartmentListToMap.ts":
/*!******************************************!*\
  !*** ./src/Utils/DepartmentListToMap.ts ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return departmentListToMap; });
function departmentListToMap(departmentList) {
    var departmentMap = new Map();
    departmentList.forEach(function (department) {
        departmentMap.set(department.id, department);
    });
    return departmentMap;
}
;


/***/ }),

/***/ "./src/Utils/DisplayGlobalMessage.ts":
/*!*******************************************!*\
  !*** ./src/Utils/DisplayGlobalMessage.ts ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return displayGlobalMessage; });
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-toastify */ "./node_modules/react-toastify/esm/react-toastify.js");

function getToastType(type) {
    switch (type) {
        case 'info':
            return react_toastify__WEBPACK_IMPORTED_MODULE_0__["ToastType"].INFO;
        case 'warning':
            return react_toastify__WEBPACK_IMPORTED_MODULE_0__["ToastType"].WARNING;
        case 'error':
            return react_toastify__WEBPACK_IMPORTED_MODULE_0__["ToastType"].ERROR;
        case 'success':
            return react_toastify__WEBPACK_IMPORTED_MODULE_0__["ToastType"].SUCCESS;
        default:
            return react_toastify__WEBPACK_IMPORTED_MODULE_0__["ToastType"].DEFAULT;
    }
}
function displayGlobalMessage(message, type) {
    // Type can be : ['info', 'success', 'error', 'warning']
    Object(react_toastify__WEBPACK_IMPORTED_MODULE_0__["toast"])(message, {
        position: react_toastify__WEBPACK_IMPORTED_MODULE_0__["toast"].POSITION.TOP_CENTER,
        type: getToastType(type),
        autoClose: 1700
    });
}


/***/ }),

/***/ "./src/Utils/GetCookie.js":
/*!********************************!*\
  !*** ./src/Utils/GetCookie.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getCookie; });
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/Utils/GetHtmlDataSet.js":
/*!*************************************!*\
  !*** ./src/Utils/GetHtmlDataSet.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getHtmlDataSet; });
var reactAppDataSet = null;

function getHtmlDataSet() {
    if (!reactAppDataSet) {
        reactAppDataSet = document.getElementById('react-app').dataset;
    }

    return reactAppDataSet;
}


/***/ }),

/***/ "./src/Utils/GetQueryStringParams.ts":
/*!*******************************************!*\
  !*** ./src/Utils/GetQueryStringParams.ts ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getQueryStringParams; });
function getQueryStringParams(query) {
    return query ?
        (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce(function (params, param) {
            var _a = param.split('='), key = _a[0], value = _a[1];
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
        }, {})
        :
            {};
}
;


/***/ }),

/***/ "./src/Utils/InstituteListToMap.ts":
/*!*****************************************!*\
  !*** ./src/Utils/InstituteListToMap.ts ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return instituteListToMap; });
function instituteListToMap(institutesList) {
    var intituteMap = new Map();
    institutesList.forEach(function (institute) {
        intituteMap.set(institute.id, institute);
    });
    return intituteMap;
}
;


/***/ }),

/***/ "./src/Utils/PatchData.js":
/*!********************************!*\
  !*** ./src/Utils/PatchData.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return patchData; });
/* harmony import */ var _GetCookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GetCookie */ "./src/Utils/GetCookie.js");


function getPatchBody(data, contentType) {
    switch (contentType) {
        case "FormData":
            return data;
        default:
            return JSON.stringify(data);
    }
}

async function patchData(url = '', data = {},contentType="application/json") {
    
    const customHeaders = {
        'X-CSRFToken': Object(_GetCookie__WEBPACK_IMPORTED_MODULE_0__["default"])('csrftoken')
    }
    if(contentType!=="FormData"){
        customHeaders['Content-Type']  = contentType;
    }
    const response = await fetch(url, {
        method: 'PATCH',
        mode: 'cors',
        cache: 'no-cache',
        headers: customHeaders,
        referrer: 'no-referrer',
        body: getPatchBody(data, contentType)
    });

    return await response.json();
}

/***/ }),

/***/ "./src/Utils/PostData.js":
/*!*******************************!*\
  !*** ./src/Utils/PostData.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return postData; });
/* harmony import */ var _GetCookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GetCookie */ "./src/Utils/GetCookie.js");


function getPostBody(data, contentType) {
    switch (contentType) {
        case 'application/x-www-form-urlencoded':
            var formBody = new URLSearchParams();
            for (var key in data) {
                formBody.append(key, data[key]);
            }
            return formBody;
        case "FormData":
            return data;
        default:
            return JSON.stringify(data);
    }
}

async function postData(url = '', data = {}, contentType='application/json') {
    const customHeaders = {
        'X-CSRFToken': Object(_GetCookie__WEBPACK_IMPORTED_MODULE_0__["default"])('csrftoken')
    }
    if(contentType!=="FormData"){
        customHeaders['Content-Type']  = contentType;
    }
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: customHeaders,
        referrer: 'no-referrer',
        body: getPostBody(data, contentType)
    });

    return await response.json();
}

/***/ }),

/***/ "./src/Utils/bootstrap.sass":
/*!**********************************!*\
  !*** ./src/Utils/bootstrap.sass ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./src/Utils/common.ts":
/*!*****************************!*\
  !*** ./src/Utils/common.ts ***!
  \*****************************/
/*! exports provided: toTime */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toTime", function() { return toTime; });
function toTime(t) {
    var sec = t % 60;
    var min = (t - sec) / 60;
    sec = Math.floor(sec);
    if (sec < 10) {
        sec = "0" + sec;
    }
    if (min < 10) {
        min = "0" + min;
    }
    return min + ":" + sec;
}


/***/ }),

/***/ "./src/index.tsx":
/*!***********************!*\
  !*** ./src/index.tsx ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var Utils_bootstrap_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! Utils/bootstrap.sass */ "./src/Utils/bootstrap.sass");
/* harmony import */ var Utils_bootstrap_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(Utils_bootstrap_sass__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! history */ "./node_modules/history/esm/history.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-router-redux */ "./node_modules/react-router-redux/es/index.js");
/* harmony import */ var _store_configure__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./store/configure */ "./src/store/configure.ts");
/* harmony import */ var _store_InitialState__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./store/InitialState */ "./src/store/InitialState.ts");
/* harmony import */ var _Routes__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Routes */ "./src/Routes.tsx");








// tslint:disable-next-line:import-name



var history = Object(history__WEBPACK_IMPORTED_MODULE_3__["createBrowserHistory"])();
var store = Object(_store_configure__WEBPACK_IMPORTED_MODULE_8__["default"])(_store_InitialState__WEBPACK_IMPORTED_MODULE_9__["default"]);
function renderApp() {
    react_dom__WEBPACK_IMPORTED_MODULE_5__["render"](react__WEBPACK_IMPORTED_MODULE_4__["createElement"](react_redux__WEBPACK_IMPORTED_MODULE_6__["Provider"], { store: store },
        react__WEBPACK_IMPORTED_MODULE_4__["createElement"](react_router_redux__WEBPACK_IMPORTED_MODULE_7__["ConnectedRouter"], { history: history, children: _Routes__WEBPACK_IMPORTED_MODULE_10__["routes"] })), document.getElementById('react-app'));
}
renderApp();


/***/ }),

/***/ "./src/store/InitialState.ts":
/*!***********************************!*\
  !*** ./src/store/InitialState.ts ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
;
/* harmony default export */ __webpack_exports__["default"] = ({
    instructorCoursesMap: new Map(),
    instructorImportCoursesList: [],
    studentCoursesMap: new Map(),
    publicCoursesMap: new Map(),
    instituteList: new Map(),
    departmentList: new Map(),
    paginationInfo: null,
    studentCourseGroupsMap: new Map(),
    studentGroupConceptsMap: new Map(),
    instructorCourseGC: {
        instructorCourseGroupsMap: new Map(),
        instructorGroupConceptsMap: new Map()
    },
    instructorConceptDataMap: new Map(),
    studentConceptDataMap: new Map()
});


/***/ }),

/***/ "./src/store/configure.ts":
/*!********************************!*\
  !*** ./src/store/configure.ts ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return configureStore; });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-devtools-extension */ "./node_modules/redux-devtools-extension/index.js");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index */ "./src/store/index.ts");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux-thunk */ "./node_modules/redux-thunk/lib/index.js");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_3__);




function configureStore(initialState) {
    return Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(_index__WEBPACK_IMPORTED_MODULE_2__["default"], initialState, Object(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_1__["composeWithDevTools"])(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(redux_thunk__WEBPACK_IMPORTED_MODULE_3___default.a)));
}
;


/***/ }),

/***/ "./src/store/index.ts":
/*!****************************!*\
  !*** ./src/store/index.ts ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");
/* harmony import */ var Reducers_Instructor_instructorCoursesListReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Reducers/Instructor/instructorCoursesListReducer */ "./src/Reducers/Instructor/instructorCoursesListReducer.ts");
/* harmony import */ var Reducers_Instructor_instructorCourseGCReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Reducers/Instructor/instructorCourseGCReducer */ "./src/Reducers/Instructor/instructorCourseGCReducer.ts");
/* harmony import */ var Reducers_Instructor_instructorConceptDataReducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! Reducers/Instructor/instructorConceptDataReducer */ "./src/Reducers/Instructor/instructorConceptDataReducer.ts");
/* harmony import */ var Reducers_Student_studentCoursesListReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! Reducers/Student/studentCoursesListReducer */ "./src/Reducers/Student/studentCoursesListReducer.ts");
/* harmony import */ var Reducers_Common_publicCoursesReducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! Reducers/Common/publicCoursesReducer */ "./src/Reducers/Common/publicCoursesReducer.ts");
/* harmony import */ var Reducers_Student_studentCourseGroupsReducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! Reducers/Student/studentCourseGroupsReducer */ "./src/Reducers/Student/studentCourseGroupsReducer.ts");
/* harmony import */ var Reducers_Student_studentGroupConceptsReducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! Reducers/Student/studentGroupConceptsReducer */ "./src/Reducers/Student/studentGroupConceptsReducer.ts");
/* harmony import */ var Reducers_Student_studentConceptDataReducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! Reducers/Student/studentConceptDataReducer */ "./src/Reducers/Student/studentConceptDataReducer.ts");

// instructor reducers



// student reducers


// import paginationInfoReducer from 'Reducers/Common/publicCoursesReducer'



var rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
    instructorCoursesMap: Reducers_Instructor_instructorCoursesListReducer__WEBPACK_IMPORTED_MODULE_1__["instructorCoursesMapReducer"],
    instructorImportCoursesList: Reducers_Instructor_instructorCoursesListReducer__WEBPACK_IMPORTED_MODULE_1__["instructorImportCoursesListReducer"],
    publicCoursesMap: Reducers_Common_publicCoursesReducer__WEBPACK_IMPORTED_MODULE_5__["publicCoursesReducer"],
    instituteList: Reducers_Instructor_instructorCoursesListReducer__WEBPACK_IMPORTED_MODULE_1__["instructorInstituteListReducer"],
    departmentList: Reducers_Instructor_instructorCoursesListReducer__WEBPACK_IMPORTED_MODULE_1__["instructorDepartmentListReducer"],
    paginationInfo: Reducers_Common_publicCoursesReducer__WEBPACK_IMPORTED_MODULE_5__["paginationInfoReducer"],
    instructorCourseGC: Reducers_Instructor_instructorCourseGCReducer__WEBPACK_IMPORTED_MODULE_2__["default"],
    instructorConceptDataMap: Reducers_Instructor_instructorConceptDataReducer__WEBPACK_IMPORTED_MODULE_3__["default"],
    studentCoursesMap: Reducers_Student_studentCoursesListReducer__WEBPACK_IMPORTED_MODULE_4__["default"],
    studentCourseGroupsMap: Reducers_Student_studentCourseGroupsReducer__WEBPACK_IMPORTED_MODULE_6__["default"],
    studentGroupConceptsMap: Reducers_Student_studentGroupConceptsReducer__WEBPACK_IMPORTED_MODULE_7__["default"],
    studentConceptDataMap: Reducers_Student_studentConceptDataReducer__WEBPACK_IMPORTED_MODULE_8__["default"],
});
/* harmony default export */ __webpack_exports__["default"] = (rootReducer);


/***/ })

/******/ });
//# sourceMappingURL=main.js.map